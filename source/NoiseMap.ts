const Utils = require('./Utils');
import SimplexNoise = require('simplex-noise');

export class NoiseMap {

    static generate(scale: number, octaves: number, isAddingFalloff: boolean = true): number[][] {
        const noiseMap    = [];
        const simplex     = new SimplexNoise(Math.random);
        const persistence = 0.6;
        const lacunarity  = 1.9;
        let maxHeight     = 0;
        let minHeight     = 9999;
        for (let y = 0; y < Utils.worldHeight; y++) {
            const noiseRow = [];
            for (let x = 0; x < Utils.worldWidth; x++) {
                let amplitude   = 1;
                let frequency   = 1;
                let noiseHeight = 0;

                for (let i = 0; i < octaves; i++) {
                    let sampleX = x / scale * frequency;
                    let sampleY = y / scale * frequency;

                    let simplexValue = simplex.noise2D(sampleX, sampleY) * 2;
                    noiseHeight += simplexValue * amplitude;

                    amplitude *= persistence;
                    frequency *= lacunarity;
                }
                if (noiseHeight > maxHeight) {
                    maxHeight = noiseHeight;
                }
                if (noiseHeight < minHeight) {
                    minHeight = noiseHeight;
                }
                noiseRow.push(noiseHeight);
            }
            noiseMap.push(noiseRow);
        }
        let falloffMap;
        if (isAddingFalloff) {
            falloffMap = Utils.generateFalloffMap();
        }
        for (let y = 0; y < Utils.worldHeight; y++) {
            for (let x = 0; x < Utils.worldWidth; x++) {
                noiseMap[y][x] = (noiseMap[y][x] - minHeight) / (maxHeight - minHeight);
                if (isAddingFalloff) {
                    noiseMap[y][x] -= falloffMap[y][x];
                    noiseMap[y][x] = Math.max(noiseMap[y][x], 0);
                }
            }
        }
        return noiseMap;
    }

}
