import {Coord} from './HunkerInterfaces';

const Utils = require('./Utils');
import {UtilsServer} from './UtilsServer';
import {WorldServer} from './WorldServer';

export class Timer {

    static allTimeoutsArr: { [timerType: string]: { [blockCoordStr: string]: NodeJS.Timeout } } = {};
    static readonly allTimerTypesArr                                                            = ['timeToLive'];

    static init() {
        UtilsServer.redisClient.hgetall('timeToLiveTimer', function (error, timeToLiveTimersArr) {
            if (error) {
                throw error;
            } else if (timeToLiveTimersArr != null) {
                for (let blockCoordStr in timeToLiveTimersArr) {
                    Timer.addTimer('timeToLive', blockCoordStr, parseInt(timeToLiveTimersArr[blockCoordStr]), true);
                }
            }
        });
    }

    static addTimer(timerType: 'timeToLive', blockCoordStr: string, activateTimerTime: number, isInit: boolean = false) {
        if (Timer.allTimerTypesArr.indexOf(timerType) == -1) {
            throw new Error("Timer type of \"" + timerType + "\" is invalid");
        }
        if (activateTimerTime <= Utils.time()) {
            if (timerType == 'timeToLive') {
                Timer.timeToLiveTimerEnded(blockCoordStr);
            }
        } else {
            if (!(timerType in Timer.allTimeoutsArr)) {
                Timer.allTimeoutsArr[timerType] = {};
            }
            Timer.allTimeoutsArr[timerType][blockCoordStr] = setTimeout(function () {
                if (timerType == 'timeToLive') {
                    Timer.timeToLiveTimerEnded(blockCoordStr);
                }
            }, activateTimerTime - Utils.time());
            if (!isInit) {
                UtilsServer.redisClient.hset('timeToLiveTimer', blockCoordStr, activateTimerTime.toString());
            }
        }
    }

    static removeTimersForBlock(blockCoordStr: string): void {
        for (let i in Timer.allTimerTypesArr) {
            const timerType = Timer.allTimerTypesArr[i];
            if (timerType in Timer.allTimeoutsArr && blockCoordStr in Timer.allTimeoutsArr[timerType]) {
                clearTimeout(Timer.allTimeoutsArr[timerType][blockCoordStr]);
                delete Timer.allTimeoutsArr[timerType][blockCoordStr];
            }
            UtilsServer.redisClient.hdel(timerType + 'Timer', blockCoordStr);
        }
    }

    // noinspection JSUnusedGlobalSymbols
    static timeToLiveTimerEnded(blockCoordStr: string): void {
        let blockLayerNum         = blockCoordStr.split(':')[0];
        let splitBlockCoordStrArr = blockCoordStr.split(':')[1].split(',');
        let blockCoordArr: Coord  = {
            'layerNum': parseInt(blockLayerNum),
            'x':        parseInt(splitBlockCoordStrArr[0]),
            'y':        parseInt(splitBlockCoordStrArr[1]),
        };
        WorldServer.destroyBlock(blockCoordArr, blockCoordStr);
        // Emit to all players.
        UtilsServer.io.emit('blockUpdate', {
            'blockCoordArr': blockCoordArr,
            'blockDataArr':  null,
        });
    }

}
