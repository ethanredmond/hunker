const Items = require('./public/generated/Items');
const Utils = require('./Utils');
import {HotbarSlot} from './HunkerInterfaces';

export class HotbarServer {

    static addItemsAtIndex(hotbarItemsArr: HotbarSlot[], hotbarSlotArr: HotbarSlot, newItemIndex: number): HotbarSlot[] {
        hotbarItemsArr = Utils.dereferenceObj(hotbarItemsArr);
        if (hotbarItemsArr[newItemIndex]['itemId'] == hotbarSlotArr['itemId'] && hotbarSlotArr['itemId'] in Items.itemsArr && Items.itemsArr[hotbarSlotArr['itemId']]['isStackable']) { // Add to existing stack.
            hotbarItemsArr[newItemIndex]['itemCount'] += hotbarSlotArr['itemCount'];
        } else {
            hotbarItemsArr[newItemIndex]['itemId']    = hotbarSlotArr['itemId']; // Start a new stack.
            hotbarItemsArr[newItemIndex]['itemCount'] = hotbarSlotArr['itemCount'];
            if (hotbarSlotArr['itemDurability']) {
                hotbarItemsArr[newItemIndex]['itemDurability'] = hotbarSlotArr['itemDurability'];
            } else if (hotbarSlotArr['itemId'] in Items.itemsArr && Items.itemsArr[hotbarSlotArr['itemId']]['itemDurability']) {
                hotbarItemsArr[newItemIndex]['itemDurability'] = Items.itemsArr[hotbarSlotArr['itemId']]['itemDurability'];
            }
        }
        return hotbarItemsArr;
    }

    static addItemsFromArr(hotbarItemsArr: HotbarSlot[], itemsToAddArr: HotbarSlot[]): HotbarSlot[] {
        for (let i in itemsToAddArr) {
            const itemArr      = itemsToAddArr[i];
            const newItemIndex = Utils.getNewItemIndex(hotbarItemsArr, itemArr['itemId']);
            if (newItemIndex != null) {
                hotbarItemsArr = HotbarServer.addItemsAtIndex(hotbarItemsArr, itemArr, newItemIndex);
            } else {
                break;
            }
        }
        return hotbarItemsArr;
    }

    static removeItemsAtIndex(hotbarItemsArr: HotbarSlot[], itemIndex: number, numItemsToRemove: number): HotbarSlot[] {
        hotbarItemsArr[itemIndex]['itemCount'] -= numItemsToRemove;
        if (hotbarItemsArr[itemIndex]['itemCount'] <= 0) {
            hotbarItemsArr[itemIndex]['itemId']    = null;
            hotbarItemsArr[itemIndex]['itemCount'] = null;
            delete hotbarItemsArr[itemIndex]['itemDurability'];
        }
        return hotbarItemsArr;
    }

    static removeItemsFromArr(hotbarItemsArr: HotbarSlot[], inputItemsToRemoveArr: { [itemId: string]: number }) {
        let itemsToRemoveArr = Utils.dereferenceObj(inputItemsToRemoveArr);
        for (let slotIndex in hotbarItemsArr) {
            if (hotbarItemsArr[slotIndex]['itemId'] in itemsToRemoveArr) {
                const itemsToDeductNum = Math.min(hotbarItemsArr[slotIndex]['itemCount'], itemsToRemoveArr[hotbarItemsArr[slotIndex]['itemId']]);
                hotbarItemsArr         = HotbarServer.removeItemsAtIndex(hotbarItemsArr, parseInt(slotIndex), itemsToDeductNum);
                if (itemsToRemoveArr[hotbarItemsArr[slotIndex]['itemId']] <= 0) {
                    delete itemsToRemoveArr[hotbarItemsArr[slotIndex]['itemId']];
                }
                if (Object.keys(itemsToRemoveArr).length <= 0) {
                    break;
                }
            }
        }
        return hotbarItemsArr;
    }

}
