const Utils = require('./Utils');
const Items = require('./public/generated/Items');
import {RedisClient} from 'redis';
import crypto = require('crypto');
import {PlayerServer} from './PlayerServer';
import {Coord, ItemId, ToolType, WorldLayer} from './HunkerInterfaces';
import SocketIO = require('socket.io');

export class UtilsServer {

    static playerSurviveNightPoints                     = 200;
    static playerDefaultRangeDistance                   = 1;
    static playerDefaultRangeRadius                     = 35;
    static tierMultArr: { [tier: string]: number }      = {
        'wood':    2,
        'stone':   3,
        'silver':  4,
        'gold':    5,
        'diamond': 6,
    };
    static tierBlockMultArr: { [tier: string]: number } = {
        'wood':    2,
        'stone':   4,
        'silver':  6,
        'gold':    8,
        'diamond': 12,
    };
    static redisClient: RedisClient                     = null;
    static io: SocketIO.Server                          = null;

    static isNearBlock(playerCoordArr: Coord, blockCoordsArr: { [blockCoordStr: string]: Coord }) {
        for (let coordStr in blockCoordsArr) {
            const coordArr = blockCoordsArr[coordStr];
            if (
                playerCoordArr['x'] / Utils.tileSize <= coordArr['x'] + 2
                &&
                playerCoordArr['x'] / Utils.tileSize >= coordArr['x'] - 1
                &&
                playerCoordArr['y'] / Utils.tileSize <= coordArr['y'] + 2
                &&
                playerCoordArr['y'] / Utils.tileSize >= coordArr['y'] - 1
            ) {
                return true;
            }
        }
        return false;
    }

    static isAnyPlayerAtBlock(coordArr: Coord): boolean {
        for (let playerId in PlayerServer.allPlayerObjs) {
            if (Math.floor(PlayerServer.allPlayerObjs[playerId]['x'] / Utils.tileSize) == coordArr['x'] && Math.floor(PlayerServer.allPlayerObjs[playerId]['y'] / Utils.tileSize) == coordArr['y']) {
                return true;
            }
        }
        return false;
    }

    // Mostly from: https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/
    // Read about good hashing and salting practices here: https://crackstation.net/hashing-security.htm#properhashing
    static generateSalt(lengthNum: number): string {
        return crypto.randomBytes(Math.ceil(lengthNum / 2)).toString('hex').slice(0, lengthNum);
    }

    static getPasswordHash(passwordCode: string, salt: string): string {
        const hash = crypto.createHmac('sha512', salt);
        hash.update(passwordCode);
        return hash.digest('hex');
    }

    static checkBlockNearWater(floorLayerArr: WorldLayer, x: number, y: number, waterRadiusNum: number) {
        return UtilsServer.checkRadiusForBlock(x, y, waterRadiusNum, function (xLoop: number, yLoop: number) {
            const worldCell = floorLayerArr[yLoop][xLoop];
            return (worldCell && Items.itemsArr['water']['tileIndexesArr'].indexOf(worldCell[0]) != -1);
        });
    }

    static checkRadiusForBlock(centerX: number, centerY: number, radiusNum: number, checkBlockFunction: (xLoop: number, yLoop: number) => boolean) {
        for (let y = Math.max(centerY - radiusNum, 0); y <= Math.min(centerY + radiusNum, Utils.worldHeight - 1); y++) {
            for (let x = Math.max(centerX - radiusNum, 0); x <= Math.min(centerX + radiusNum, Utils.worldWidth - 1); x++) {
                if (checkBlockFunction(x, y)) {
                    return true;
                }
            }
        }
        return false;
    }

    static isCoord3x3Empty(worldArr: WorldLayer, x: number, y: number) {
        return !UtilsServer.checkRadiusForBlock(x, y, 1, function (xLoop: number, yLoop: number) {
            if (x != xLoop || y != yLoop) { // Not center block.
                if (UtilsServer.isCoordCollidingWithWorld(worldArr, xLoop, yLoop)[0]) {
                    return true;
                }
            }
            return false;
        });
    }

    static isCoordCollidingWithWorld(worldArr: WorldLayer, xRounded: number, yRounded: number): [boolean, ItemId | null] {
        if (
            yRounded in worldArr
            &&
            xRounded in worldArr[yRounded]
            &&
            worldArr[yRounded][xRounded] != null
        ) {
            const worldArrBlock = worldArr[yRounded][xRounded];
            let itemId          = null;
            if (worldArrBlock[0] in Utils.tileIndexToItemIdArr) {
                itemId = Utils.tileIndexToItemIdArr[worldArrBlock[0]];
            }
            const isDisablingCollision = (
                itemId
                &&
                itemId in Items.itemsArr
                &&
                Items.itemsArr[itemId]['isDisablingCollision']
            );
            if (!isDisablingCollision) {
                return [true, itemId];
            } else {
                return [false, itemId];
            }
        }
        return [false, null];
    }

    public static isCorrectTool(selectedItemId: ItemId | null, correctToolType: ToolType | null): boolean {
        return (
            selectedItemId.indexOf(`${correctToolType}_`) == 0
            ||
            (
                correctToolType == 'scythe'
                &&
                selectedItemId.indexOf('hoe_') == 0
            )
        );
    }

}
