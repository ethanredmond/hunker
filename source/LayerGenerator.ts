const Items   = require('./public/generated/Items');
const Utils   = require('./Utils');
const Dungeon = require('./Dungeon');
import {BiomeName, BoundsArr, Coord, DirectionLetter, ItemId, StructureDetailsArr, WorldLayer, WorldRow} from './HunkerInterfaces';
import {WorldServer} from './WorldServer';
import {NoiseMap} from './NoiseMap';
import {CaveGenerator} from './CaveGenerator';
import {UtilsServer} from './UtilsServer';
import {LootTable} from './LootTable';

type TerrainId = keyof typeof LayerGenerator.terrainDataArr & string;

export class LayerGenerator {

    static layerBiomeCoordsArr: BiomeName[][]                                                                                                                              = [];
    static noiseMapsArr: { [layerNum: string]: { 'temperatureNoiseMap': number[][], 'humidityNoiseMap': number[][], 'altNoiseMap': number[][], }, }                        = {};
    static terrainDataArr: { [terrainId: string]: { floorIdsArr: string[] | null, isOverwritingTerrain: boolean, minBlocks: number, maxBlocks: number, passNum: number } } = {
        // @formatter:off
        'tree':                 {'floorIdsArr': Items.itemsArr['tree']          ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'tree_birch':           {'floorIdsArr': Items.itemsArr['tree_birch']    ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'tree_acacia':          {'floorIdsArr': ['sand', 'grass'],                                           'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'stone':                {'floorIdsArr': ['snow', 'grass', 'sand', 'ice', 'cave', 'dungeon'],         'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks': 10, 'passNum': 1},
        'tree_apple':           {'floorIdsArr': Items.itemsArr['tree_apple']    ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 3, 'maxBlocks':  4, 'passNum': 1},
        'cactus':               {'floorIdsArr': Items.itemsArr['cactus']        ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'present_red':          {'floorIdsArr': ['snow'],                                                    'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'present_blue':         {'floorIdsArr': ['snow'],                                                    'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'present_green':        {'floorIdsArr': ['snow'],                                                    'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'tuft':                 {'floorIdsArr': Items.itemsArr['tuft']          ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'tuft_long':            {'floorIdsArr': Items.itemsArr['tuft_long']     ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'tuft_dead':            {'floorIdsArr': Items.itemsArr['tuft_dead']     ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'flower_red':           {'floorIdsArr': ['grass'],                                                   'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'flower_blue':          {'floorIdsArr': ['snow'],                                                    'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'flower_purple':        {'floorIdsArr': ['grass'],                                                   'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'flower_white':         {'floorIdsArr': ['grass'],                                                   'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'flower_yellow':        {'floorIdsArr': ['grass'],                                                   'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'wheat_stage_4':        {'floorIdsArr': ['grass'],                                                   'isOverwritingTerrain': false, 'minBlocks': 2, 'maxBlocks':  3, 'passNum': 1},
        'carrot_stage_4':       {'floorIdsArr': ['grass'],                                                   'isOverwritingTerrain': false, 'minBlocks': 2, 'maxBlocks':  3, 'passNum': 1},
        'sugar_cane_stage_4':   {'floorIdsArr': ['sand'],                                                    'isOverwritingTerrain': false, 'minBlocks': 2, 'maxBlocks':  3, 'passNum': 1},
        'ore_coal':             {'floorIdsArr': ['snow', 'grass', 'sand', 'ice', 'cave', 'dungeon'],         'isOverwritingTerrain': true,  'minBlocks': 4, 'maxBlocks':  9, 'passNum': 2},
        'ore_silver':           {'floorIdsArr': ['snow', 'grass', 'sand', 'ice', 'cave', 'dungeon'],         'isOverwritingTerrain': true,  'minBlocks': 4, 'maxBlocks':  6, 'passNum': 2},
        'ore_gold':             {'floorIdsArr': ['snow', 'grass', 'sand', 'ice', 'cave', 'dungeon'],         'isOverwritingTerrain': true,  'minBlocks': 4, 'maxBlocks':  6, 'passNum': 2},
        'ore_diamond':          {'floorIdsArr': ['snow', 'grass', 'sand', 'ice', 'cave', 'dungeon'],         'isOverwritingTerrain': true,  'minBlocks': 4, 'maxBlocks':  6, 'passNum': 2},
        'mushroom_tan':         {'floorIdsArr': Items.itemsArr['mushroom_tan']  ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'mushroom_red':         {'floorIdsArr': Items.itemsArr['mushroom_red']  ['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'mushroom_brown':       {'floorIdsArr': Items.itemsArr['mushroom_brown']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'mushroom_blue':        {'floorIdsArr': ['cave'],                                                    'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks':  1, 'passNum': 1},
        'water':                {'floorIdsArr': null,                                                        'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks':  5, 'passNum': 1},
        'grass_flowers':        {'floorIdsArr': null,                                                        'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks':  5, 'passNum': 1},
        'lava':                 {'floorIdsArr': null,                                                        'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks':  5, 'passNum': 1},
        'snow':                 {'floorIdsArr': null,                                                        'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks':  5, 'passNum': 1},
        // @formatter:on
    };
    static prefabsArr: { numPerLayer: { [layerNum: string]: number }, isLocked: boolean, structureDetailsArr: StructureDetailsArr }[]                                      = [
        {
            'numPerLayer':         {'-1': 5, '-2': 5, '-3': 5},
            'isLocked':            false,
            'structureDetailsArr': {
                'substitutionsArr':      {' ': null, 'l': 'log', 'p': 'planks', 'd': 'door', 'h': 'hearth', 'b': 'workbench', 'f': 'fence_wood', 'w': 'wheat_stage_4', 't': 'chest', 'c': 'lootcrate_wood', 's': 'lootcrate_stone'},
                'structureArr':          [
                    'l,p,p,d,p,p,p,p,l'.split(','),
                    'p,h,b, ,f,w,w, ,p'.split(','),
                    'p,t, , , ,w,w,w,d'.split(','),
                    'p,c, ,s,f,c,w,w,p'.split(','),
                    'l,p,d,p,p,p,p,p,l'.split(','),
                ],
                'floorSubstitutionsArr': {'C': 'cave', 'G': 'carpet_blue', 'D': 'dirt', 'W': 'water'},
                'structureFloorArr':     [
                    'C,C,C,G,C,C,C,C,C'.split(','),
                    'C,C,C,G,G,D,D,W,C'.split(','),
                    'C,C,G,G,G,D,D,D,G'.split(','),
                    'C,G,G,G,G,D,D,D,C'.split(','),
                    'C,C,G,C,C,C,C,C,C'.split(','),
                ],
            },
        },
        {
            'numPerLayer':         {'-1': 1, '-2': 1, '-3': 1},
            'isLocked':            true,
            'structureDetailsArr': {
                'substitutionsArr':      {' ': null, 's': 'brick_stone', 'B': 'button', 't': 'tree', 'w': 'wheat_stage_4', 'd': 'door', 'h': 'hearth', 'b': 'bookshelf', 'f': 'fence_stone', 'F': 'fence_wood', 'T': 'chest', 'c': 'lootcrate_wood', 'n': 'tnt', 'C': 'wire'},
                'structureArr':          [
                    's,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s'.split(','),
                    's,C,C,s, , , , , ,t, , ,s, , , ,s,C,C,C,T,s,C,n,s,s'.split(','),
                    's,C,T,s, , ,t, , , , ,B,s,B, , ,s,b,d,b,b,b,b,b,b,s'.split(','),
                    's, , ,d, , ,F,F,F, , , ,d, , , ,s,b, , , , ,c, ,b,s'.split(','),
                    's, ,F,s, ,F,w,w,w, , , ,s, , , ,s,b, , , , , , ,b,s'.split(','),
                    's,B,C,s, ,F,c, ,w,F,t, ,s, , , ,s,f,f,f,f,f, ,f,f,s'.split(','),
                    's,d,s,s, ,F,w,w,w,F, , ,s, , ,B,s,B, , , , , , , ,s'.split(','),
                    's,B, ,s, , ,F,F,F, , , ,s, , , ,d, , , , , , , , ,s'.split(','),
                    's, ,n,s,c, , , , , , , ,s, , , ,s,B,c, , , , , , ,s'.split(','),
                    's, , ,s, , , , , ,t, , ,s, , , ,s, , , , , , , , ,s'.split(','),
                    's,T,C,s, , ,t, , , , ,T,s,d,d,d,s, , , , , , , , ,s'.split(','),
                    's,s,s,s,s,s,s,s,s,s,s,s,s,C,C,C,s,s,s,s,s,s,s,s,s,s'.split(','),
                    ' , , , , , , , , , , , , , ,B, , , , , , , , , , , '.split(','),
                ],
                'floorSubstitutionsArr': {'C': 'cave', 'G': 'carpet_red', 'g': 'grass', 'd': 'dirt', 'w': 'water'},
                'structureFloorArr':     [
                    'C,C,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,d,d,d,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,d,w,d,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,d,d,d,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,G,G,G,g,g,g,g,g,g,g,g,G,G,G,G,G,G,G,G,G,G,G,G,G,G'.split(','),
                    'C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C'.split(','),
                ],
            },
        },
        // {
        //     'numPerLayer':         {'0': 5},
        //     'structureDetailsArr': {
        //         'substitutionsArr':      {' ': null, 'l': 'log', 'p': 'planks', 'd': 'door', 'h': 'hearth', 'b': 'workbench', 'f': 'fence_wood', 'w': 'wheat_stage_4', 't': 'chest', 'c': 'lootcrate_wood', 's': 'lootcrate_stone'},
        //         'structureArr':          [
        //             'l,p,p,d,l,f,f,f,f'.split(','),
        //             'p,h,b, ,p,w,w, ,f'.split(','),
        //             'p,t, , , ,w,w,w,f'.split(','),
        //             'p,c, ,s,p,c,w,w,f'.split(','),
        //             'l,p,d,p,l,f,f,f,f'.split(','),
        //         ],
        //         'floorSubstitutionsArr': {'C': 'cave', 'G': 'carpet_blue', 'D': 'dirt', 'W': 'water'},
        //         'structureFloorArr':     [
        //             'C,C,C,G,C,C,C,C,C'.split(','),
        //             'C,C,C,G,G,D,D,W,C'.split(','),
        //             'C,C,G,G,G,D,D,D,G'.split(','),
        //             'C,G,G,G,G,D,D,D,C'.split(','),
        //             'C,C,G,C,C,C,C,C,C'.split(','),
        //         ],
        //     },
        // },
    ];

    private readonly layerNum: number;
    public readonly seed: string;
    public worldLayerArr: WorldLayer                                                 = [];
    public floorLayerArr: WorldLayer                                                 = [];
    public heightNoiseMap: number[][]                                                = [];
    public allStructureCoordsArr: BoundsArr[]                                        = [];
    public allDungeonObjsArr: { dungeonObj: Dungeon, dungeonBoundsArr: BoundsArr }[] = [];

    constructor(layerNum: number, seed: string) {
        this.layerNum                            = layerNum;
        [this.worldLayerArr, this.floorLayerArr] = LayerGenerator.initLayer(layerNum);
        this.seed                                = seed || Utils.randId(8);
        // @ts-ignore
        Math.seedrandom(this.seed + layerNum);
        if (layerNum === 0) {
            this.generateOverworldLayer();
        } else {
            this.generateCaveLayer();
        }
    }

    static getBiomeNameFromCoords(coordArr: Coord): BiomeName {
        if (!this.noiseMapsArr[coordArr['layerNum']]) {
            const altNoiseMap: null[][] = [];
            for (let y = 0; y < Utils.worldHeight; y++) {
                let altNoiseRow: null[] = [];
                for (let x = 0; x < Utils.worldWidth; x++) {
                    altNoiseRow.push(null);
                }
                altNoiseMap.push(altNoiseRow);
            }
            this.noiseMapsArr[coordArr['layerNum']] = {
                'temperatureNoiseMap': NoiseMap.generate(100, 4, false),
                'humidityNoiseMap':    NoiseMap.generate(100, 4, false),
                'altNoiseMap':         altNoiseMap, // NoiseMap.generate(100, 5, false),
            };
        }
        const temperatureNoiseMap = this.noiseMapsArr[coordArr['layerNum']]['temperatureNoiseMap'];
        const humidityNoiseMap    = this.noiseMapsArr[coordArr['layerNum']]['humidityNoiseMap'];
        const altNoiseMap         = this.noiseMapsArr[coordArr['layerNum']]['altNoiseMap'];
        const noiseTemperature    = temperatureNoiseMap[coordArr['y']][coordArr['x']];
        const noiseHumidity       = humidityNoiseMap[coordArr['y']][coordArr['x']];
        const noiseAlt            = altNoiseMap[coordArr['y']][coordArr['x']];
        for (let biomeNameLoop in Utils.biomeDetailsArr[coordArr['layerNum']]) {
            if (
                noiseTemperature >= Utils.biomeDetailsArr[coordArr['layerNum']][biomeNameLoop]['temperatureMin']
                &&
                noiseTemperature < Utils.biomeDetailsArr[coordArr['layerNum']][biomeNameLoop]['temperatureMax']
                &&
                noiseHumidity >= Utils.biomeDetailsArr[coordArr['layerNum']][biomeNameLoop]['humidityMin']
                &&
                noiseHumidity < Utils.biomeDetailsArr[coordArr['layerNum']][biomeNameLoop]['humidityMax']
                &&
                noiseAlt >= Utils.biomeDetailsArr[coordArr['layerNum']][biomeNameLoop]['altMin']
                &&
                noiseAlt < Utils.biomeDetailsArr[coordArr['layerNum']][biomeNameLoop]['altMax']
            ) {
                return biomeNameLoop;
            }
        }
        return null;
    }

    generateOverworldLayer() {
        this.heightNoiseMap                = NoiseMap.generate(60, 4);
        LayerGenerator.layerBiomeCoordsArr = [];
        for (let y = 0; y < Utils.worldHeight; y++) {
            LayerGenerator.layerBiomeCoordsArr[y] = [];
            for (let x = 0; x < Utils.worldWidth; x++) {
                const noiseHeight                        = this.heightNoiseMap[y][x];
                let floorItemId;
                LayerGenerator.layerBiomeCoordsArr[y][x] = LayerGenerator.getBiomeNameFromCoords({'layerNum': this.layerNum, 'x': x, 'y': y});
                if (noiseHeight <= 0.4) {
                    if (noiseHeight > 0.3) {
                        floorItemId = 'sand';
                    } else {
                        floorItemId = 'water';
                    }
                } else if (LayerGenerator.layerBiomeCoordsArr[y][x] && LayerGenerator.layerBiomeCoordsArr[y][x] in Utils.biomeDetailsArr[this.layerNum]) {
                    floorItemId = Utils.biomeDetailsArr[this.layerNum][LayerGenerator.layerBiomeCoordsArr[y][x]]['floorItemId'];
                } else {
                    console.log('biomeName', LayerGenerator.layerBiomeCoordsArr[y][x]);
                }
                this.floorLayerArr[y][x] = [Items.itemsArr[floorItemId]['tileIndexesArr'][0], Items.itemsArr[floorItemId]['structureDurability']];
            }
        }

        this.addOverworldRivers();

        this.addBlocksToSpawnArr();
    }

    addOverworldRivers(): void {
        let numRivers = 0;

        // Sort noise map.
        let allHeightsArr: [number, Coord][] = [];
        for (let y = 0; y < Utils.worldHeight; y++) {
            for (let x = 0; x < Utils.worldWidth; x++) {
                if (this.heightNoiseMap[y][x] >= 0.45) {
                    allHeightsArr.push([this.heightNoiseMap[y][x], {'x': x, 'y': y}]);
                }
            }
        }
        allHeightsArr.sort(function (a, b) {
            return b[0] - a[0];
        });

        River.allSourceCoordsArr = [];
        River.allCoordStrsArr    = [];
        River.allCoordsArr       = [];

        const maxRivers = 15;
        for (let i = 0; i < allHeightsArr.length && numRivers < maxRivers; i++) {
            const coordArr = allHeightsArr[i][1];
            if (River.allCoordStrsArr.indexOf(coordArr['x'] + ',' + coordArr['y']) == -1) {
                // Not too close to another river source.
                let isTooCloseToRiverSource = false;
                for (let sourceCoordArr of River.allSourceCoordsArr) {
                    if (Utils.distance(sourceCoordArr, coordArr) <= 30) {
                        isTooCloseToRiverSource = true;
                    }
                }
                if (isTooCloseToRiverSource) {
                    continue;
                }

                numRivers++;
                River.allSourceCoordsArr.push(coordArr);

                // Find next lowest neighbour.
                let lowestNeighbourHeight          = 1;
                let lowestNeighbourDirectionLetter = null;
                for (let directionLetter of Utils.directionsArr) {
                    const newCoord       = Utils.getNewCoordFromDirection(coordArr, directionLetter, 1);
                    const newCoordHeight = this.heightNoiseMap[newCoord['y']][newCoord['x']];
                    if (newCoordHeight < lowestNeighbourHeight) {
                        lowestNeighbourHeight          = newCoordHeight;
                        lowestNeighbourDirectionLetter = directionLetter;
                    }
                }

                const riverThicknessNum = Utils.randElFromArr(River.thicknessNumsArr);
                // River going one way.
                const river1Obj         = new River(coordArr, this, Utils.invertDirectionLetter(lowestNeighbourDirectionLetter), riverThicknessNum);
                river1Obj.applyRiver();
                // River going opposite way.
                const river2Obj = new River(coordArr, this, lowestNeighbourDirectionLetter, riverThicknessNum);
                river2Obj.applyRiver();
            }
        }

        River.thickenAll(this);
        River.addSand(this);
    }

    generateCaveLayer() {
        this.setCaveBiomes();
        const caveGenerator = new CaveGenerator(
            0.39,
            3,
            3,
            3,
            [[Items.itemsArr['stone']['tileIndexesArr'][0], Items.itemsArr['stone']['structureDurability']]],
            [null],
            false,
            this.layerNum,
            this.worldLayerArr,
            this.floorLayerArr,
            LayerGenerator.layerBiomeCoordsArr,
        );
        caveGenerator.generateLayerArr(this.worldLayerArr);
        this.worldLayerArr = Utils.dereferenceObj(caveGenerator.layerArr);
        // @formatter:off
            for (let i = 0; i < Utils.worldBlocksNum * 0.0100; i++) { this.addTerrainAtRandomBlock('ore_coal'); }
        if (this.layerNum == -1) {
            for (let i = 0; i < Utils.worldBlocksNum * 0.0080; i++) { this.addTerrainAtRandomBlock('ore_silver'); }
        } else if (this.layerNum == -2) {
            for (let i = 0; i < Utils.worldBlocksNum * 0.0040; i++) { this.addTerrainAtRandomBlock('ore_silver'); }
            for (let i = 0; i < Utils.worldBlocksNum * 0.0040; i++) { this.addTerrainAtRandomBlock('ore_gold'); }
        } else if (this.layerNum == -3) {
            for (let i = 0; i < Utils.worldBlocksNum * 0.0030; i++) { this.addTerrainAtRandomBlock('ore_silver'); }
            for (let i = 0; i < Utils.worldBlocksNum * 0.0030; i++) { this.addTerrainAtRandomBlock('ore_gold'); }
            for (let i = 0; i < Utils.worldBlocksNum * 0.0020; i++) { this.addTerrainAtRandomBlock('ore_diamond'); }
        }
            for (let i = 0; i < Utils.worldBlocksNum * 0.0025; i++) { this.addTerrainAtRandomBlock('mushroom_tan'); }
            for (let i = 0; i < Utils.worldBlocksNum * 0.0025; i++) { this.addTerrainAtRandomBlock('mushroom_red'); }
            for (let i = 0; i < Utils.worldBlocksNum * 0.0050; i++) { this.addTerrainAtRandomBlock('mushroom_brown'); }
        let numMushrooms = 10;
        if (this.layerNum == -2) {
            numMushrooms = 25;
        } else if (this.layerNum == -3) {
            numMushrooms = 50;
        }
        for (let i = 0; i < Utils.worldBlocksNum * numMushrooms / 10000; i++) { this.addTerrainAtRandomBlock('mushroom_blue'); }
        // @formatter:on
        this.addStructuresToLayer();
        this.addPoolsToLayer();
        this.addBiomeFloorsToCaveLayer();
    }

    private isInvalidTerrainCoord(terrainId: TerrainId, terrainX: number, terrainY: number) {
        return (
            (
                LayerGenerator.terrainDataArr[terrainId]['isOverwritingTerrain']
                &&
                this.worldLayerArr[terrainY][terrainX] == null
            )
            ||
            (
                !LayerGenerator.terrainDataArr[terrainId]['isOverwritingTerrain']
                &&
                this.worldLayerArr[terrainY][terrainX] != null
            )
            ||
            (
                LayerGenerator.terrainDataArr[terrainId]['floorIdsArr']
                &&
                LayerGenerator.terrainDataArr[terrainId]['floorIdsArr'].indexOf(Utils.tileIndexToItemIdArr[this.floorLayerArr[terrainY][terrainX][0]]) == -1
            )
            ||
            (
                Items.itemsArr[terrainId]['cropRequiredDistanceToWater']
                &&
                !UtilsServer.checkBlockNearWater(this.floorLayerArr, terrainX, terrainY, Items.itemsArr[terrainId]['cropRequiredDistanceToWater'])
            )
            ||
            (
                terrainId.indexOf('mushroom_') === 0 // Only for mushrooms,
                &&
                this.layerNum < 0
                &&
                Utils.tileIndexToItemIdArr[this.floorLayerArr[terrainY][terrainX][0]] != 'dirt_coarse' // Don't require stone below if is on coarse dirt.
                &&
                (
                    terrainY + 1 >= Utils.worldHeight // Mushroom is at edge of world, or,
                    ||
                    this.worldLayerArr[terrainY + 1][terrainX] == null // Block below mushroom is empty, or,
                    ||
                    (
                        this.worldLayerArr[terrainY + 1][terrainX][0] != Items.itemsArr['stone']['tileIndexesArr'][0] // Block below mushroom isn't stone.
                        &&
                        this.worldLayerArr[terrainY + 1][terrainX][0] != Items.itemsArr['brick_stone']['tileIndexesArr'][0] // Block below mushroom isn't stone brick.
                        &&
                        this.worldLayerArr[terrainY + 1][terrainX][0] != Items.itemsArr['brick_stone_small']['tileIndexesArr'][0] // Block below mushroom isn't stone brick.
                        &&
                        this.worldLayerArr[terrainY + 1][terrainX][0] != Items.itemsArr['obsidian']['tileIndexesArr'][0] // Block below mushroom isn't stone brick.
                    )
                )
            )
            ||
            (
                (
                    terrainId == 'tree'
                    ||
                    terrainId == 'tree_birch'
                    ||
                    terrainId == 'tree_acacia'
                    ||
                    terrainId == 'cactus'
                )
                &&
                !UtilsServer.isCoord3x3Empty(this.worldLayerArr, terrainX, terrainY)
            )
        );
    }

    addTerrainAtRandomBlock(terrainId: TerrainId) {
        let terrainX      = null;
        let terrainY      = null;
        let numIterations = 0;
        while (
            numIterations < 50
            &&
            (
                terrainX == null
                ||
                terrainY == null
                ||
                this.isInvalidTerrainCoord(terrainId, terrainX, terrainY)
            )
            ) {
            terrainX = Utils.randBetween(0, Utils.worldWidth - 1);
            terrainY = Utils.randBetween(0, Utils.worldHeight - 1);
            numIterations++;
        }
        if (numIterations == 50) {
            return;
        }
        this.addTerrainAtBlock(terrainId, terrainX, terrainY);
    }

    addTerrainAtBlock(terrainId: TerrainId, terrainX: number, terrainY: number) {
        this.addTerrainBlock(terrainId, terrainX, terrainY);

        if (LayerGenerator.terrainDataArr[terrainId]['maxBlocks'] > 1) {
            let numIterations        = 0;
            const numBlocksInTerrain = Utils.randBetween(LayerGenerator.terrainDataArr[terrainId]['minBlocks'], LayerGenerator.terrainDataArr[terrainId]['maxBlocks']);
            for (let i = 0; i < numBlocksInTerrain - 1; i++) {
                let newTerrainX = -1;
                let newTerrainY = -1;
                numIterations   = 0;
                while (
                    numIterations < 50
                    &&
                    (
                        newTerrainX < 0
                        ||
                        newTerrainX >= Utils.worldWidth
                        ||
                        newTerrainY < 0
                        ||
                        newTerrainY >= Utils.worldHeight
                        ||
                        this.isInvalidTerrainCoord(terrainId, newTerrainX, newTerrainY)
                    )
                    ) {
                    let xDiff = 0;
                    let yDiff = 0;
                    if (Utils.randBool()) {
                        xDiff = (Utils.randBool()) ? 1 : -1;
                        yDiff = 0;
                    } else {
                        xDiff = 0;
                        yDiff = (Utils.randBool()) ? 1 : -1;
                    }
                    newTerrainX = terrainX + xDiff;
                    newTerrainY = terrainY + yDiff;
                    numIterations++;
                }
                if (numIterations >= 50) {
                    break;
                }
                if (!(newTerrainY in this.worldLayerArr) || !(newTerrainX in this.worldLayerArr[newTerrainY])) {
                    console.log('newTerrainX', newTerrainX, 'newTerrainY', newTerrainY, 'numIterations', numIterations);
                }
                this.addTerrainBlock(terrainId, newTerrainX, newTerrainY);
                terrainY = newTerrainY;
                terrainX = newTerrainX;
            }
        }
    }

    addTerrainBlock(terrainId: keyof typeof LayerGenerator.terrainDataArr, terrainX: number, terrainY: number) {
        let floorBlockId = null;
        if (
            terrainId in Items.itemsArr
            &&
            Items.itemsArr[terrainId]['cropRequiredFloorIdsArr']
            &&
            Items.itemsArr[terrainId]['cropRequiredFloorIdsArr'].indexOf(Utils.tileIndexToItemIdArr[this.floorLayerArr[terrainY][terrainX][0]]) == -1
            &&
            Items.itemsArr[terrainId]['cropRequiredFloorIdsArr'].indexOf('dirt') != -1
        ) {
            floorBlockId = 'dirt';
        }
        if (Items.itemsArr[terrainId]['isFloor']) {
            this.floorLayerArr[terrainY][terrainX] = [Utils.randElFromArr(Items.itemsArr[terrainId]['tileIndexesArr']), Items.itemsArr[terrainId]['structureDurability']];
        } else {
            this.worldLayerArr[terrainY][terrainX] = [Utils.randElFromArr(Items.itemsArr[terrainId]['tileIndexesArr']), Items.itemsArr[terrainId]['structureDurability']];
            if (floorBlockId) {
                this.floorLayerArr[terrainY][terrainX] = [Utils.randElFromArr(Items.itemsArr[floorBlockId]['tileIndexesArr']), Items.itemsArr[floorBlockId]['structureDurability']];
            }
        }
    }

    addStructuresToLayer() {
        // this.addDungeonsToLayer();
        // this.addPrefabsToLayer();
    }

    addPrefabsToLayer() {
        for (let i in LayerGenerator.prefabsArr) {
            if (this.layerNum in LayerGenerator.prefabsArr[i]['numPerLayer']) {
                for (let j = 0; j < LayerGenerator.prefabsArr[i]['numPerLayer'][this.layerNum]; j++) {
                    this.addStructure(LayerGenerator.prefabsArr[i]['structureDetailsArr'], LayerGenerator.prefabsArr[i]['isLocked']);
                }
            }
        }
    }

    addDungeonsToLayer() {
        if (this.layerNum <= -1) {
            let dungeonsNum    = 5;
            let dungeonSizeNum = 49;
            let wallItemId     = 'brick_stone';
            let mainWallItemId = 'brick_red';
            if (this.layerNum == -2) {
                dungeonsNum    = 3;
                dungeonSizeNum = 79;
                wallItemId     = 'brick_stone_small';
                mainWallItemId = 'brick_red_small';
            } else if (this.layerNum == -3) {
                dungeonsNum    = 1;
                dungeonSizeNum = 99;
                wallItemId     = 'brick_stone_small';
                mainWallItemId = 'brick_red_small';
            }
            for (let i = 0; i < dungeonsNum; i++) {
                const dungeonObj       = new Dungeon(dungeonSizeNum, dungeonSizeNum, this.layerNum);
                const dungeonBoundsArr = this.addStructure({
                    'substitutionsArr':      {
                        ' ':  null,
                        'w':  wallItemId,
                        'b':  mainWallItemId,
                        'lw': 'lootcrate_wood',
                        'ls': 'lootcrate_stone',
                        'lb': 'lootcrate_blue',
                        'lr': 'lootcrate_red',
                        'lg': 'lootcrate_green',
                        'd':  'door',
                        'r':  'torch_red',
                        'c':  'chest',
                        'h':  'hearth',
                        'e':  'workbench',
                        's':  'bookshelf',
                    },
                    'structureArr':          dungeonObj.structureArr,
                    'floorSubstitutionsArr': {'D': 'dungeon'},
                    'structureFloorArr':     dungeonObj.structureFloorArr,
                }, false);
                if (dungeonBoundsArr) {
                    this.allDungeonObjsArr.push({'dungeonObj': dungeonObj, 'dungeonBoundsArr': dungeonBoundsArr});
                }
            }
        }
    }

    addStructure(structureDetailsArr: StructureDetailsArr, isLocked: boolean): BoundsArr {
        for (let numIterations = 0; numIterations < 100; numIterations++) {
            const corner1X    = Utils.randBetween(0, Utils.worldWidth - structureDetailsArr['structureArr'][0].length);
            const corner1Y    = Utils.randBetween(0, Utils.worldHeight - structureDetailsArr['structureArr'].length);
            const corner2X    = corner1X + structureDetailsArr['structureArr'][0].length - 1;
            const corner2Y    = corner1Y + structureDetailsArr['structureArr'].length - 1;
            let isOverlapping = false;
            for (let i in this.allStructureCoordsArr) {
                if (
                    !(
                        this.allStructureCoordsArr[i]['x1'] > corner2X
                        ||
                        this.allStructureCoordsArr[i]['x2'] < corner1X
                        ||
                        this.allStructureCoordsArr[i]['y1'] > corner2Y
                        ||
                        this.allStructureCoordsArr[i]['y2'] < corner1Y
                    )
                ) {
                    isOverlapping = true;
                    break;
                }
            }
            if (isOverlapping) {
                continue;
            }
            this.allStructureCoordsArr.push({'x1': corner1X, 'y1': corner1Y, 'x2': corner2X, 'y2': corner2Y});
            for (let y = 0; y < structureDetailsArr['structureArr'].length; y++) {
                for (let x = 0; x < structureDetailsArr['structureArr'][y].length; x++) {
                    if (structureDetailsArr['structureArr'][y][x] !== '0') { // Don't change if 0.
                        let worldItemId = structureDetailsArr['substitutionsArr'][structureDetailsArr['structureArr'][y][x]];
                        if (!worldItemId) {
                            this.worldLayerArr[corner1Y + y][corner1X + x] = null;
                        } else {
                            this.worldLayerArr[corner1Y + y][corner1X + x] = [Items.itemsArr[worldItemId]['tileIndexesArr'][0], (isLocked ? -1 : Items.itemsArr[worldItemId]['structureDurability'])];
                            if (!Utils.isWorldPage && UtilsServer.redisClient && worldItemId.indexOf('lootcrate_') == -1 && worldItemId.indexOf('present_') == -1) {
                                const coordStr = this.layerNum + ':' + (corner1X + x) + ',' + (corner1Y + y);
                                if (worldItemId in LootTable.tablesArr) {
                                    LootTable.setContainerArrForBlock(worldItemId, coordStr);
                                } else if (Items.itemsArr[worldItemId]['isContainer']) {
                                    WorldServer.allContainersArr[coordStr] = [];
                                    for (let i = 0; i < (Items.itemsArr[worldItemId]['maxItemsInContainerNum'] || 1); i++) {
                                        WorldServer.allContainersArr[coordStr].push({'itemId': null, 'itemCount': null});
                                    }
                                    WorldServer.updateContainerRedis(coordStr, null);
                                }
                            }
                        }
                    }
                    // Add floor.
                    if (
                        structureDetailsArr['structureFloorArr']
                        &&
                        structureDetailsArr['structureFloorArr'][y][x] !== '0'
                    ) { // Don't change if 0.
                        let floorItemId = structureDetailsArr['floorSubstitutionsArr'][structureDetailsArr['structureFloorArr'][y][x]];
                        if (!(floorItemId in Items.itemsArr)) {
                            console.log(floorItemId);
                            console.log(structureDetailsArr['structureFloorArr'][y][x]);
                        }
                        this.floorLayerArr[corner1Y + y][corner1X + x] = [Items.itemsArr[floorItemId]['tileIndexesArr'][0], Items.itemsArr[floorItemId]['structureDurability']];
                    }
                }
            }
            return {
                'x1': corner1X,
                'y1': corner1Y,
                'x2': corner2X,
                'y2': corner2Y,
            };
        }
    }

    addPoolsToLayer() {
        let lavaChanceToStartAlive  = null;
        let waterChanceToStartAlive = null;
        if (this.layerNum == 0) {
            waterChanceToStartAlive = 0.75;
        } else if (this.layerNum == -1) {
            lavaChanceToStartAlive  = 0.72;
            waterChanceToStartAlive = 0.72;
        } else if (this.layerNum == -2) {
            lavaChanceToStartAlive  = 0.70;
            waterChanceToStartAlive = 0.75;
        } else if (this.layerNum == -3) {
            lavaChanceToStartAlive = 0.65;
        }
        if (lavaChanceToStartAlive) {
            const lavaGenerator = new CaveGenerator(
                lavaChanceToStartAlive,
                5,
                5,
                4,
                [[Utils.randElFromArr(Items.itemsArr['cave']['tileIndexesArr']), Items.itemsArr['cave']['structureDurability']]],
                [[Utils.randElFromArr(Items.itemsArr['lava']['tileIndexesArr']), Items.itemsArr['lava']['structureDurability']]],
                true,
                this.layerNum,
                this.worldLayerArr,
                this.floorLayerArr,
            );
            lavaGenerator.generateLayerArr(this.floorLayerArr);
            this.floorLayerArr = Utils.dereferenceObj(lavaGenerator.layerArr);
        }
        if (waterChanceToStartAlive) {
            const waterGenerator = new CaveGenerator(
                waterChanceToStartAlive,
                5,
                5,
                4,
                [[Utils.randElFromArr(Items.itemsArr['cave']['tileIndexesArr']), Items.itemsArr['cave']['structureDurability']]],
                [[Utils.randElFromArr(Items.itemsArr['water']['tileIndexesArr']), Items.itemsArr['water']['structureDurability']]],
                true,
                this.layerNum,
                this.worldLayerArr,
                this.floorLayerArr,
            );
            waterGenerator.generateLayerArr(this.floorLayerArr);
            this.floorLayerArr = Utils.dereferenceObj(waterGenerator.layerArr);
        }

    }

    setCaveBiomes() {
        LayerGenerator.layerBiomeCoordsArr = [];
        for (let y = 0; y < this.floorLayerArr.length; y++) {
            LayerGenerator.layerBiomeCoordsArr[y] = [];
            for (let x = 0; x < this.floorLayerArr[y].length; x++) {
                LayerGenerator.layerBiomeCoordsArr[y][x] = LayerGenerator.getBiomeNameFromCoords({'layerNum': this.layerNum, 'x': x, 'y': y});
            }
        }
    }

    addBiomeFloorsToCaveLayer() {
        for (let y = 0; y < this.floorLayerArr.length; y++) {
            for (let x = 0; x < this.floorLayerArr[y].length; x++) {
                if (LayerGenerator.layerBiomeCoordsArr[y][x] && LayerGenerator.layerBiomeCoordsArr[y][x] in Utils.biomeDetailsArr[this.layerNum]) {
                    const floorItemId        = Utils.biomeDetailsArr[this.layerNum][LayerGenerator.layerBiomeCoordsArr[y][x]]['floorItemId'];
                    this.floorLayerArr[y][x] = [Items.itemsArr[floorItemId]['tileIndexesArr'][0], Items.itemsArr[floorItemId]['structureDurability']];
                }
            }
        }
        this.addBlocksToSpawnArr();
    }

    static initLayer(layerNum: number): [WorldLayer, WorldLayer] {
        const worldLayerArr: WorldLayer = [];
        const floorLayerArr: WorldLayer = [];
        for (let y = 0; y < Utils.worldHeight; y++) {
            let worldRow: WorldRow = [];
            let floorRow: WorldRow = [];
            for (let x = 0; x < Utils.worldWidth; x++) {
                if (layerNum == 0) {
                    worldRow.push(null);
                    floorRow.push([Utils.randElFromArr(Items.itemsArr['water']['tileIndexesArr']), Items.itemsArr['water']['structureDurability']]);
                } else if (layerNum <= -1) {
                    worldRow.push(null);
                    floorRow.push([Utils.randElFromArr(Items.itemsArr['cave']['tileIndexesArr']), Items.itemsArr['cave']['structureDurability']]);
                }
            }
            worldLayerArr.push(worldRow);
            floorLayerArr.push(floorRow);
        }
        return [worldLayerArr, floorLayerArr];
    }

    private addBlocksToSpawnArr() {
        for (let passNum = 1; passNum <= 2; passNum++) {
            for (let y = 0; y < Utils.worldHeight; y++) {
                for (let x = 0; x < Utils.worldWidth; x++) {
                    const biomeName = LayerGenerator.layerBiomeCoordsArr[y][x];
                    if (biomeName && biomeName in Utils.biomeDetailsArr[this.layerNum] && Utils.biomeDetailsArr[this.layerNum][biomeName]['blocksToSpawnArr']) {
                        for (let itemId in Utils.biomeDetailsArr[this.layerNum][biomeName]['blocksToSpawnArr']) {
                            if (
                                LayerGenerator.terrainDataArr[itemId]['passNum'] == passNum
                                &&
                                Utils.biomeDetailsArr[this.layerNum][biomeName]['blocksToSpawnArr'][itemId] >= Math.random()
                                &&
                                !this.isInvalidTerrainCoord(itemId, x, y)
                            ) {
                                if (itemId == 'ore_silver' || itemId == 'ore_gold') {
                                    this.addTerrainBlock(itemId, x, y);
                                } else {
                                    this.addTerrainAtBlock(itemId, x, y);
                                }

                                // Add presents to christmas trees.
                                if (itemId == 'tree' && biomeName == 'christmas' && Utils.randBetween(1, 5) == 1) {
                                    const emptySquaresArr: Coord[] = [];
                                    // @formatter:off
                                    if (!this.worldLayerArr[y][x + 1]) { emptySquaresArr.push({'x': x + 1, 'y': y}); }
                                    if (!this.worldLayerArr[y][x - 1]) { emptySquaresArr.push({'x': x - 1, 'y': y}); }
                                    if (!this.worldLayerArr[y + 1][x]) { emptySquaresArr.push({'x': x, 'y': y + 1}); }
                                    if (!this.worldLayerArr[y - 1][x]) { emptySquaresArr.push({'x': x, 'y': y - 1}); }
                                    // @formatter:on
                                    if (emptySquaresArr.length) {
                                        const emptySquareCoord = Utils.randElFromArr(emptySquaresArr);
                                        this.addTerrainAtBlock(Utils.randElFromArr(['present_red', 'present_blue', 'present_green']), emptySquareCoord['x'], emptySquareCoord['y']);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

class River {

    static isDebugging: boolean           = false;
    static allSourceCoordsArr: Coord[]    = [];
    static allCoordStrsArr: string[]      = [];
    static allCoordsArr: Coord[]          = [];
    static debuggingBlockIdsArr: ItemId[] = ['mushroom_tan', 'mushroom_brown', 'mushroom_red', 'ore_gold', 'ore_silver', 'brick_red', 'lava'];
    static allRiverObjs: River[]          = [];
    static thicknessNumsArr: number[]     = [1, 2, 2, 2, 3, 3];

    private lengthNum: number              = 0;
    private readonly sourceCoordArr: Coord;
    private readonly allCoordsArr: Coord[] = [];
    private readonly layerGenerator: LayerGenerator;
    private readonly debuggingBlockId: ItemId;
    private readonly forbiddenDirectionLetter: DirectionLetter;
    private readonly thicknessNum: number;

    constructor(
        sourceCoordArr: Coord,
        layerGenerator: LayerGenerator,
        forbiddenDirectionLetter: DirectionLetter,
        thicknessNum: number,
    ) {
        this.sourceCoordArr           = sourceCoordArr;
        this.layerGenerator           = layerGenerator;
        this.forbiddenDirectionLetter = forbiddenDirectionLetter;
        this.debuggingBlockId         = Utils.randElFromArr(River.debuggingBlockIdsArr);
        this.thicknessNum             = thicknessNum;
        River.allRiverObjs.push(this);
    }

    applyRiver(): void {
        let riverUpdateCoord = this.sourceCoordArr;
        for (; this.lengthNum < 1000; this.lengthNum++) {
            River.allCoordStrsArr.push(riverUpdateCoord['x'] + ',' + riverUpdateCoord['y']);
            River.allCoordsArr.push(riverUpdateCoord);
            this.allCoordsArr.push(riverUpdateCoord);
            this.layerGenerator.floorLayerArr[riverUpdateCoord['y']][riverUpdateCoord['x']] = [Utils.randElFromArr(Items.itemsArr['water']['tileIndexesArr']), Items.itemsArr['water']['structureDurability']];
            if (River.isDebugging) {
                if (this.lengthNum == 0) { // Is source.
                    this.layerGenerator.worldLayerArr[riverUpdateCoord['y']][riverUpdateCoord['x']] = [Utils.randElFromArr(Items.itemsArr['ore_diamond']['tileIndexesArr']), Items.itemsArr['ore_diamond']['structureDurability']];
                } else {
                    this.layerGenerator.worldLayerArr[riverUpdateCoord['y']][riverUpdateCoord['x']] = [Utils.randElFromArr(Items.itemsArr[this.debuggingBlockId]['tileIndexesArr']), Items.itemsArr[this.debuggingBlockId]['structureDurability']];
                }
            } else {
                this.layerGenerator.worldLayerArr[riverUpdateCoord['y']][riverUpdateCoord['x']] = null;
            }

            // Stop if river reached ocean.
            if (this.layerGenerator.heightNoiseMap[riverUpdateCoord['y']][riverUpdateCoord['x']] <= 0.3) {
                break;
            }

            // Find next lowest neighbour.
            let lowestNeighbourHeight = 1;
            let directionLettersArr   = Utils.shuffleArray(Utils.dereferenceObj(Utils.directionsArr));
            let lowestNeighbourCoord;
            for (let i in directionLettersArr) {
                let directionLetter = directionLettersArr[i];
                if (directionLetter != this.forbiddenDirectionLetter) {
                    const newCoord       = Utils.getNewCoordFromDirection(riverUpdateCoord, directionLetter, 1);
                    const newCoordHeight = this.layerGenerator.heightNoiseMap[newCoord['y']][newCoord['x']];
                    if (newCoordHeight < lowestNeighbourHeight) {
                        lowestNeighbourHeight = newCoordHeight;
                        lowestNeighbourCoord  = newCoord;
                    }
                } else {
                    directionLettersArr.splice(parseInt(i), 1); // Remove forbidden direction.
                }
            }
            if (lowestNeighbourCoord) {
                riverUpdateCoord = lowestNeighbourCoord;
            } else {
                riverUpdateCoord = Utils.getNewCoordFromDirection(riverUpdateCoord, Utils.randElFromArr(directionLettersArr), 1);
            }
        }
    }

    static thickenAll(layerGenerator: LayerGenerator) {
        // Thicken rivers.
        const newRiverCoordsArr: Coord[] = [];
        for (const riverObj of River.allRiverObjs) {
            for (const riverCoord of riverObj.allCoordsArr) {
                if (riverObj.thicknessNum > 1) {
                    UtilsServer.checkRadiusForBlock(riverCoord['x'], riverCoord['y'], 1, function (xLoop: number, yLoop: number): boolean {
                        if (
                            ( // Not center, and,
                                xLoop != riverCoord['x']
                                ||
                                yLoop != riverCoord['y']
                            )
                            &&
                            (
                                xLoop - riverCoord['x'] > 0 // Isn't on the left of center, or, // This part is to make 2-wide rivers not have water on the left side.
                                ||
                                riverObj.thicknessNum > 2 // Is 3 thick, and,
                            )
                            &&
                            River.allCoordStrsArr.indexOf(xLoop + ',' + yLoop) == -1 // Not in a river already.
                        ) {
                            layerGenerator.floorLayerArr[yLoop][xLoop] = [Items.itemsArr['water']['tileIndexesArr'][0], Items.itemsArr['water']['structureDurability']];
                            layerGenerator.worldLayerArr[yLoop][xLoop] = null;
                            River.allCoordStrsArr.push(xLoop + ',' + yLoop);
                            newRiverCoordsArr.push({'x': xLoop, 'y': yLoop});
                        }
                        return false;
                    });
                }
            }
        }
        River.allCoordsArr = River.allCoordsArr.concat(newRiverCoordsArr);
    }

    static addSand(layerGenerator: LayerGenerator) {
        // Add sand around rivers.
        for (const i in River.allCoordsArr) {
            const riverCoord = River.allCoordsArr[i];
            UtilsServer.checkRadiusForBlock(riverCoord['x'], riverCoord['y'], 1, function (xLoop: number, yLoop: number): boolean {
                if (
                    (
                        xLoop != riverCoord['x']
                        ||
                        yLoop != riverCoord['y']
                    )
                    &&
                    River.allCoordStrsArr.indexOf(xLoop + ',' + yLoop) == -1
                    &&
                    layerGenerator.floorLayerArr[yLoop][xLoop][0] != Items.itemsArr['water']['tileIndexesArr'][0]
                    &&
                    layerGenerator.floorLayerArr[yLoop][xLoop][0] != Items.itemsArr['sand']['tileIndexesArr'][0]
                ) {
                    layerGenerator.floorLayerArr[yLoop][xLoop] = [Items.itemsArr['sand']['tileIndexesArr'][0], Items.itemsArr['sand']['structureDurability']];
                    layerGenerator.worldLayerArr[yLoop][xLoop] = null;
                }
                return false;
            });
        }
    }

}
