const Utils = require('./Utils');
const Items = require('./public/generated/Items');
import {UtilsServer} from './UtilsServer';
import {WorldServer} from './WorldServer';
import {HotbarServer} from './HotbarServer';
import {EntityServer} from './EntityServer';
import {Timer} from './Timer';
import {LootTable} from './LootTable';
import {BlockUpdateData, Coord, DifficultyStr, EntityDataArr, GameModeStr, HotbarSlot, ItemEnchantmentsArr, ItemId, JoinDetailsArr, MovementData, PlayerDeathDetailsArr, PrivatePlayerData, PublicPlayerData, ServerResponse, ToolType, WorldCell} from './HunkerInterfaces';
import {DayNightCycleServer} from './DayNightCycleServer';
import redis = require('redis');
import SocketIO = require('socket.io');

export class PlayerServer {

    static allPlayerObjs: { [id: string]: PlayerServer }      = {};
    static deathSuffixTextArr: { [deathKey: string]: string } = {
        'hunger':       "starved to death",
        'warmth':       "froze to death",
        'wolf':         "got rekt by a wolf",
        'polar_bear':   "got rekt by a polar bear",
        'zombie':       "got rekt by a zombie",
        'axehead':      "got rekt by Axehead",
        'player':       "got slayed",
        'disconnected': "popped out of existence",
        'command':      "committed sudoku",
        'lava':         "tried to swim in lava",
        'explosion':    "got asploded",
        'arrow':        "got sniped",
    };

    public readonly userName: string;
    private readonly name: string;
    private readonly image: string;
    public readonly id: string;
    private angle: number;
    public isPaused: boolean;
    private isCrafting: boolean;
    private hasSprintedInLastTick: boolean;
    private craftingItemId: ItemId | null;
    private currentSaveCount: number;
    private nextSaveCount: number;

    public currentLayerNum: number;
    public x: number;
    public y: number;
    public maxHealth: number;
    public maxWarmth: number;
    public maxHunger: number;
    public health: number;
    public warmth: number;
    public hunger: number;
    public playerScore: number;
    private joinedTime: number;
    public selectedItemIndex: number;
    public selectedItemId: ItemId | null;
    public nightsSurvivedNum: number;
    public isGhost: boolean;
    private lastMovementIsGhost: boolean;
    public lastAttackMillis: number;
    public ridingEntityId: string;
    public gameModeStr: GameModeStr;
    public difficultyStr: DifficultyStr;
    private craftingTimeout: NodeJS.Timeout;
    private clickRailTimeout: NodeJS.Timeout;

    public hotbarItemsArr: HotbarSlot[];
    private accessoryItemsArr: HotbarSlot[];

    constructor(socket: SocketIO.Socket, playerJoinDetailsArr: JoinDetailsArr, userName: string, responseFn: (response: ServerResponse) => void) {
        // @formatter:off
        if (!(playerJoinDetailsArr['image'] in Utils.allPlayerImagesArr))       { responseFn({'status': 'error', 'errorMessage': "Invalid character"}); return; }
        if (Utils.isDev && Utils.isRestrictingJoining && userName != 'ethan')   { responseFn({'status': 'error', 'errorMessage': Utils.randElFromArr(["One minute please...", "Fixing bugs...", "Breaking stuff..."])}); return; }
        // @formatter:on
        // Set player name.
        let name = playerJoinDetailsArr['name'].replace(/[^ -~]/gi, '').substring(0, 15); // 15 characters long.
        // Get usernames, names and images of all players.
        let allUserNames = [];
        let allNames     = [];
        let allImages    = [];
        if (PlayerServer.allPlayerObjs) {
            for (const playerId in PlayerServer.allPlayerObjs) {
                if (playerId != socket.id) { // Not the current player.
                    allUserNames.push(PlayerServer.allPlayerObjs[playerId].userName);
                    allNames.push(PlayerServer.allPlayerObjs[playerId].name);
                    allImages.push(PlayerServer.allPlayerObjs[playerId].image);
                }
            }
        }
        if (userName && allUserNames.indexOf(userName) != -1) {
            responseFn({'status': 'error', 'errorMessage': "Already joined"});
            return;
        }
        // throw new Error(userName);
        if (!name) { // If didn't set a name.
            name = 'UNNAMED_1';
            // If UNNAMED_1 is taken, increment to UNNAMED_2 and check if taken, etc.
            while (allNames.indexOf(name) != -1) {
                name = name.replace(/\d+$/, function (i) {
                    return (parseInt(i) + 1).toString();
                });
            }
            // @formatter:off
        } else if (allNames.indexOf(name) != -1)                          { responseFn({'status': 'error', 'errorMessage': "Name taken"       }); return; }
        if (!(playerJoinDetailsArr['image'] in Utils.allPlayerImagesArr)) { responseFn({'status': 'error', 'errorMessage': "Invalid character"}); return; }
        if (allImages.indexOf(playerJoinDetailsArr['image']) != -1)       { responseFn({'status': 'error', 'errorMessage': "Character taken"  }); return; }
        // @formatter:on
        this.userName = userName;
        this.name     = name;
        this.image    = playerJoinDetailsArr['image'];
        if (playerJoinDetailsArr['gameModeStr'] && playerJoinDetailsArr['gameModeStr'] in Utils.playerGameModesArr) {
            this.gameModeStr = playerJoinDetailsArr['gameModeStr'];
        } else {
            this.gameModeStr = 'survival';
        }
        if (playerJoinDetailsArr['difficultyStr'] && playerJoinDetailsArr['difficultyStr'] in Utils.playerDifficultiesArr) {
            this.difficultyStr = playerJoinDetailsArr['difficultyStr'];
        } else {
            this.difficultyStr = 'normal';
        }
        this.id                    = socket.id;
        this.angle                 = 0;
        this.isPaused              = false;
        this.isCrafting            = false;
        this.hasSprintedInLastTick = false;
        this.craftingItemId        = null;
        this.currentSaveCount      = 0;
        this.nextSaveCount         = 0;

        this.setPlayerDetails(
            function (playerObj: PlayerServer) {
                playerObj.currentLayerNum = WorldServer.spawnLayerNum;
                playerObj.x               = (WorldServer.spawnX * Utils.tileSize) + (Utils.tileSize / 2);
                playerObj.y               = (WorldServer.spawnY * Utils.tileSize) + (Utils.tileSize / 2);
                if (WorldServer.worldArr[WorldServer.spawnLayerNum][WorldServer.spawnY][WorldServer.spawnX]) { // Square at spawn.
                    const newCoordArr = WorldServer.findEmptySquareInRadius(WorldServer.spawnX, WorldServer.spawnY, WorldServer.spawnLayerNum);
                    if (newCoordArr) {
                        playerObj.x = newCoordArr['x'];
                        playerObj.y = newCoordArr['y'];
                    }
                }
                playerObj.maxHealth           = Utils.playerDefaultMaxHealth;
                playerObj.maxWarmth           = Utils.playerDefaultMaxWarmth;
                playerObj.maxHunger           = Utils.playerDefaultMaxHunger;
                playerObj.health              = playerObj.maxHealth;
                playerObj.warmth              = playerObj.maxWarmth;
                playerObj.hunger              = playerObj.maxHunger * 1.5;
                playerObj.playerScore         = 0;
                playerObj.joinedTime          = Utils.time();
                playerObj.selectedItemIndex   = 0;
                playerObj.selectedItemId      = null;
                playerObj.nightsSurvivedNum   = 0;
                playerObj.isGhost             = false;
                playerObj.lastMovementIsGhost = false;
                if (playerObj.userName) {
                    UtilsServer.redisClient.hmset('player:' + playerObj.userName, {
                        'currentLayerNum':   playerObj.currentLayerNum,
                        'x':                 playerObj.x,
                        'y':                 playerObj.y,
                        'maxHealth':         playerObj.maxHealth,
                        'maxWarmth':         playerObj.maxWarmth,
                        'maxHunger':         playerObj.maxHunger,
                        'health':            playerObj.health,
                        'warmth':            playerObj.warmth,
                        'hunger':            playerObj.hunger,
                        'playerScore':       playerObj.playerScore,
                        'joinedTime':        playerObj.joinedTime,
                        'selectedItemIndex': playerObj.selectedItemIndex,
                        'nightsSurvivedNum': playerObj.nightsSurvivedNum,
                        'isGhost':           (playerObj.isGhost ? '1' : '0'),
                    });
                }
            },
            function (playerObj: PlayerServer): void {
                playerObj.hotbarItemsArr = [];
                for (let i = 0; i < 9 * 2; i++) {
                    playerObj.hotbarItemsArr.push({'itemId': null, 'itemCount': null});
                }
                playerObj.hotbarItemsArr[0]['itemId']    = Utils.randElFromArr(['apple', 'wheat', 'bread', 'meat_raw', 'meat_cooked', 'carrot']);
                playerObj.hotbarItemsArr[0]['itemCount'] = Utils.randBetween(1, 5);
                playerObj.hotbarItemsArr[1]['itemId']    = Utils.randElFromArr(['planks', 'stone', 'log', 'twine', 'coal']);
                playerObj.hotbarItemsArr[1]['itemCount'] = Utils.randBetween(1, 5);
                playerObj.hotbarItemsArr[2]['itemId']    = Utils.randElFromArr(['axe_wood', 'pick_wood', 'hoe_wood']);
                playerObj.hotbarItemsArr[2]['itemCount'] = 1;
                // Start out with a fire at night.
                if (DayNightCycleServer.stage == 'night') {
                    playerObj.hotbarItemsArr[3]['itemId']    = 'fire';
                    playerObj.hotbarItemsArr[3]['itemCount'] = 1;
                }
                playerObj.selectedItemId    = playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemId'];
                playerObj.accessoryItemsArr = [];
                for (let i = 0; i < 9; i++) {
                    playerObj.accessoryItemsArr.push({'itemId': null, 'itemCount': null});
                }
                if (playerObj.userName) {
                    UtilsServer.redisClient.hmset('playerHotbar:' + playerObj.userName, WorldServer.formatContentsForRedis(playerObj.hotbarItemsArr));
                    UtilsServer.redisClient.hmset('playerAccessories:' + playerObj.userName, WorldServer.formatContentsForRedis(playerObj.accessoryItemsArr));
                }
            },
            function (playerObj: PlayerServer, responseFn: (response: ServerResponse) => void): void {
                PlayerServer.allPlayerObjs[playerObj.id] = playerObj;

                // Send the current game state to the new player.
                // Tell the new player its details.
                const allPublicPlayerDataArr: { [playerId: string]: PublicPlayerData } = {};
                for (let playerId in PlayerServer.allPlayerObjs) {
                    allPublicPlayerDataArr[playerId] = PlayerServer.allPlayerObjs[playerId].getPublicPlayerDataArr();
                }
                responseFn({
                    'status':                 'success',
                    'publicPlayerDataArr':    allPublicPlayerDataArr,
                    'privatePlayerDataArr':   playerObj.getPrivatePlayerDataArr(),
                    'currentLayerNum':        playerObj.currentLayerNum,
                    'floorArr':               WorldServer.floorArr,
                    'worldArr':               WorldServer.worldArr,
                    'biomeCoordsPerLayerArr': WorldServer.biomeCoordsPerLayerArr,
                    'dayNightStage':          DayNightCycleServer.stage,
                    'gameStartMillis':        DayNightCycleServer.gameStartMillis,
                });

                console.log("Player " + ('"' + playerObj.name + '"').padEnd(17) + " joined.");

                // Update all other players of the new player.
                socket.broadcast.emit('playerJoined', {
                    'playerInfo': playerObj.getPublicPlayerDataArr(),
                });
            },
            responseFn,
        );
    }

    setPlayerDetails(
        setDefaultPlayerProperties: (playerObj: PlayerServer) => void,
        setDefaultPlayerHotbar: (playerObj: PlayerServer) => void,
        callbackFn: Function,
        responseFn: (response: ServerResponse) => void,
    ): void {
        const playerObj = this;
        if (playerObj.userName) {
            UtilsServer.redisClient.hgetall('player:' + playerObj.userName, function (
                error: redis.RedisError,
                playerArr: {
                    currentLayerNum: string,
                    x: string,
                    y: string,
                    maxHealth: string,
                    maxWarmth: string,
                    maxHunger: string,
                    health: string,
                    warmth: string,
                    hunger: string,
                    playerScore: string,
                    joinedTime: string,
                    selectedItemIndex: string,
                    nightsSurvivedNum: string,
                    isGhost: string,
                },
            ) {
                if (error) {
                    throw error;
                } else if (playerArr == null) { // If player isn't saved in Redis.
                    setDefaultPlayerProperties(playerObj);
                    setDefaultPlayerHotbar(playerObj);
                    callbackFn(playerObj, responseFn);
                } else {
                    // Get player from Redis.
                    playerObj.currentLayerNum = parseInt(playerArr['currentLayerNum'] || '0');
                    playerObj.x               = parseFloat(playerArr['x']);
                    playerObj.y               = parseFloat(playerArr['y']);
                    playerObj.maxHealth       = parseFloat(playerArr['maxHealth']) || Utils.playerDefaultMaxHealth;
                    playerObj.maxWarmth       = parseFloat(playerArr['maxWarmth']) || Utils.playerDefaultMaxWarmth;
                    playerObj.maxHunger       = parseFloat(playerArr['maxHunger']) || Utils.playerDefaultMaxHunger;
                    playerObj.health          = parseFloat(playerArr['health']);
                    playerObj.warmth          = parseFloat(playerArr['warmth']);
                    playerObj.hunger          = parseFloat(playerArr['hunger']);
                    console.log(playerObj.maxHunger);
                    console.log(playerObj.hunger);
                    playerObj.playerScore         = parseInt(playerArr['playerScore']);
                    playerObj.joinedTime          = parseInt(playerArr['joinedTime']);
                    playerObj.selectedItemIndex   = parseInt(playerArr['selectedItemIndex']);
                    playerObj.nightsSurvivedNum   = parseInt(playerArr['nightsSurvivedNum'] || '0');
                    playerObj.isGhost             = (playerArr['isGhost'] == '1');
                    playerObj.lastMovementIsGhost = playerObj.isGhost;
                    UtilsServer.redisClient.hgetall('playerHotbar:' + playerObj.userName, function (error, hotbarArr) {
                        if (error) {
                            throw error;
                        } else if (hotbarArr == null) { // If hotbar isn't saved in Redis.
                            setDefaultPlayerHotbar(playerObj);
                            callbackFn(playerObj, responseFn);
                        } else {
                            playerObj.hotbarItemsArr = PlayerServer.processRedisItemsArr(hotbarArr);
                            playerObj.selectedItemId = playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemId'];

                            UtilsServer.redisClient.hgetall('playerAccessories:' + playerObj.userName, function (error, accessoriesArr) {
                                if (error) {
                                    throw error;
                                } else if (accessoriesArr == null) { // If hotbar isn't saved in Redis.
                                    callbackFn(playerObj, responseFn);
                                } else {
                                    playerObj.accessoryItemsArr = PlayerServer.processRedisItemsArr(accessoriesArr);
                                    callbackFn(playerObj, responseFn);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            setDefaultPlayerProperties(playerObj);
            setDefaultPlayerHotbar(playerObj);
            callbackFn(playerObj, responseFn);
        }
    }

    static processRedisItemsArr(itemsArr: { [slotIndex: number]: string }): HotbarSlot[] {
        const processedItemsArr = [];
        for (let slotIndex in itemsArr) {
            const slotDetailsArr = itemsArr[slotIndex].split(':');
            const itemId         = slotDetailsArr[0];
            if (itemId && slotDetailsArr[1]) {
                const itemDetailsArr = slotDetailsArr[1].split(',');
                const itemCount      = parseInt(itemDetailsArr[0]);
                if (itemCount) {
                    const itemArr: HotbarSlot = {'itemId': itemId, 'itemCount': itemCount};
                    if (itemDetailsArr[1]) {
                        itemArr['itemDurability'] = parseInt(itemDetailsArr[1]);
                    }
                    if (itemDetailsArr[2]) {
                        const redisItemEnchantmentsArr = itemDetailsArr[2].split('|');
                        if (redisItemEnchantmentsArr && redisItemEnchantmentsArr.length) {
                            itemArr['itemEnchantmentsArr'] = {};
                            for (const redisItemEnchantmentStr of redisItemEnchantmentsArr) {
                                const enchantmentType = redisItemEnchantmentStr.split('.')[0];
                                const tierNum         = redisItemEnchantmentStr.split('.')[1];
                                if (enchantmentType in Utils.allEnchantmentsArr) {
                                    itemArr['itemEnchantmentsArr'][enchantmentType] = parseInt(tierNum);
                                }
                            }
                        }
                    }
                    processedItemsArr.push(itemArr);
                }
            } else {
                processedItemsArr.push({'itemId': null, 'itemCount': null});
            }
        }
        return processedItemsArr;
    }

    updateHotbarRedis(prevHotbarArr: HotbarSlot[]): void {
        if (this.userName) {
            for (let slotIndex in this.hotbarItemsArr) {
                let slotDetails     = this.hotbarItemsArr[slotIndex];
                let prevSlotDetails = prevHotbarArr[slotIndex];
                if (
                    !prevSlotDetails
                    ||
                    prevSlotDetails['itemId'] != slotDetails['itemId']
                    ||
                    prevSlotDetails['itemCount'] != slotDetails['itemCount']
                    ||
                    prevSlotDetails['itemDurability'] != slotDetails['itemDurability']
                    ||
                    (
                        (
                            'itemEnchantmentsArr' in prevSlotDetails
                            ||
                            'itemEnchantmentsArr' in slotDetails
                        )
                        &&
                        PlayerServer.itemEnchantmentsArrToStr(prevSlotDetails['itemEnchantmentsArr'] ?? {}) != PlayerServer.itemEnchantmentsArrToStr(slotDetails['itemEnchantmentsArr'] ?? {})
                    )
                ) {
                    if (slotDetails['itemId'] && slotDetails['itemCount']) {
                        UtilsServer.redisClient.hset(
                            'playerHotbar:' + this.userName,
                            slotIndex,
                            slotDetails['itemId'] + ':' + slotDetails['itemCount'] +
                            (slotDetails['itemDurability'] ? ',' + slotDetails['itemDurability'] : '') +
                            ('itemEnchantmentsArr' in slotDetails && slotDetails['itemEnchantmentsArr'] ? ',' + PlayerServer.itemEnchantmentsArrToStr(slotDetails['itemEnchantmentsArr']) : ''),
                        );
                    } else {
                        UtilsServer.redisClient.hset('playerHotbar:' + this.userName, slotIndex, 'null');
                    }
                }
            }
        }
    }

    updateAccessoriesRedis(prevAccessoriesArr: HotbarSlot[]): void {
        if (this.userName) {
            for (let slotIndex in this.accessoryItemsArr) {
                let slotDetails     = this.accessoryItemsArr[slotIndex];
                let prevSlotDetails = prevAccessoriesArr[slotIndex];
                if (
                    !prevSlotDetails
                    ||
                    prevSlotDetails['itemId'] != slotDetails['itemId']
                    ||
                    prevSlotDetails['itemCount'] != slotDetails['itemCount']
                    ||
                    prevSlotDetails['itemDurability'] != slotDetails['itemDurability']
                    ||
                    (
                        (
                            'itemEnchantmentsArr' in prevSlotDetails
                            ||
                            'itemEnchantmentsArr' in slotDetails
                        )
                        &&
                        PlayerServer.itemEnchantmentsArrToStr(prevSlotDetails['itemEnchantmentsArr'] ?? {}) != PlayerServer.itemEnchantmentsArrToStr(slotDetails['itemEnchantmentsArr'] ?? {})
                    )
                ) {
                    if (slotDetails['itemId'] && slotDetails['itemCount']) {
                        UtilsServer.redisClient.hset(
                            'playerAccessories:' + this.userName,
                            slotIndex,
                            slotDetails['itemId'] + ':' + slotDetails['itemCount'] +
                            (slotDetails['itemDurability'] ? ',' + slotDetails['itemDurability'] : '') +
                            ('itemEnchantmentsArr' in slotDetails && slotDetails['itemEnchantmentsArr'] ? ',' + PlayerServer.itemEnchantmentsArrToStr(slotDetails['itemEnchantmentsArr']) : ''),
                        );
                    } else {
                        UtilsServer.redisClient.hset('playerAccessories:' + this.userName, slotIndex, 'null');
                    }
                }
            }
        }
    }

    static itemEnchantmentsArrToStr(itemEnchantmentsArr: ItemEnchantmentsArr): string {
        let outStr = '';
        for (const enchantmentType in itemEnchantmentsArr) {
            if (outStr) {
                outStr += '|';
            }
            outStr += `${enchantmentType}.${itemEnchantmentsArr[enchantmentType]}`;
        }
        return outStr;
    }

    getPrivatePlayerDataArr(): PrivatePlayerData {
        return {
            'maxHealth':         this.maxHealth,
            'maxWarmth':         this.maxWarmth,
            'maxHunger':         this.maxHunger,
            'health':            this.health,
            'warmth':            this.warmth,
            'hunger':            this.hunger,
            'joinedTime':        this.joinedTime,
            'selectedItemIndex': this.selectedItemIndex,
            'hotbarItemsArr':    this.hotbarItemsArr,
            'accessoryItemsArr': this.accessoryItemsArr,
        };
    }

    getPublicPlayerDataArr(): PublicPlayerData {
        return {
            'id':              this.id,
            'name':            this.name,
            'image':           this.image,
            'currentLayerNum': this.currentLayerNum,
            'x':               this.x,
            'y':               this.y,
            'angle':           this.angle,
            'playerScore':     this.playerScore,
            'isCrafting':      this.isCrafting,
            'isGhost':         this.isGhost,
            'craftingItemId':  this.craftingItemId,
            'isPaused':        this.isPaused,
            'selectedItemId':  this.hotbarItemsArr[this.selectedItemIndex]['itemId'],
        };
    }

    startSwing(socket: SocketIO.Socket, hitBlockCoordArr: Coord, responseFn: (response: ServerResponse) => void) {
        const _this         = this;
        const originalAngle = this.angle;
        setTimeout(function () {
            _this.hitBlock(socket, hitBlockCoordArr, responseFn, originalAngle);
        }, Utils.playerSwingTimeMillis / 4); // Should be divided by two but divided by four accounts for lag.
    }

    hitBlock(socket: SocketIO.Socket, hitBlockCoordArr: Coord, responseFn: (response: ServerResponse) => void, originalAngle: number) {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on

        // Set damage value.
        let entityDamageValue = 1;
        if (this.selectedItemId && this.selectedItemId in Items.itemsArr && Items.itemsArr[this.selectedItemId]['entityDamageValue'] != null) {
            entityDamageValue = Items.itemsArr[this.selectedItemId]['entityDamageValue'];
        }
        hitBlockCoordArr['layerNum'] = this.currentLayerNum;
        const hitBlockCoordStr       = hitBlockCoordArr['layerNum'] + ':' + hitBlockCoordArr['x'] + ',' + hitBlockCoordArr['y'];
        const prevBlockDataJson      = JSON.stringify(WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']]);
        const prevFloorDataJson      = JSON.stringify(WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']]);
        const prevHotbarItemsArrJson = JSON.stringify(this.hotbarItemsArr);
        const prevSelectedItemId     = this.selectedItemId;
        const prevHotbarArr          = Utils.dereferenceObj(this.hotbarItemsArr);
        const prevPlayerScore        = Utils.dereferenceObj(this.playerScore);
        let prevContainerArr         = null;
        if (hitBlockCoordStr in WorldServer.allContainersArr) {
            prevContainerArr = WorldServer.allContainersArr[hitBlockCoordStr];
        }
        let responseData: ServerResponse      = {
            'status': 'success',
        };
        responseData['allSoundEffectKeysArr'] = [];
        if (this.selectedItemId && this.selectedItemId in Items.itemsArr && Items.itemsArr[this.selectedItemId]['attackCooldownMillis']) {
            responseData['attackCooldownMillis'] = Items.itemsArr[this.selectedItemId]['attackCooldownMillis'];
            let currentMillis                    = Utils.time();
            let attackCooldownResetPcnt          = (currentMillis - (this.lastAttackMillis || 0)) / Items.itemsArr[this.selectedItemId]['attackCooldownMillis'];
            attackCooldownResetPcnt              = Math.max(attackCooldownResetPcnt, 0);
            attackCooldownResetPcnt              = Math.min(attackCooldownResetPcnt, 1);
            entityDamageValue *= 0.2 + ((attackCooldownResetPcnt ** 2) * 0.8);
            entityDamageValue                    = Math.round(entityDamageValue);
            this.lastAttackMillis                = currentMillis;
        }
        const attackSquareArr = this.attack(entityDamageValue, originalAngle);
        if (attackSquareArr && attackSquareArr['allSoundEffectKeysArr'] && attackSquareArr['allSoundEffectKeysArr'].length) {
            responseData['allSoundEffectKeysArr'] = responseData['allSoundEffectKeysArr'].concat(attackSquareArr['allSoundEffectKeysArr']);
        }
        if (WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']] != null) { // Something at square.
            const destroyBlockArr = this.destroyBlock(hitBlockCoordStr, hitBlockCoordArr);
            if (destroyBlockArr && destroyBlockArr['allSoundEffectKeysArr'] && destroyBlockArr['allSoundEffectKeysArr'].length) {
                responseData['allSoundEffectKeysArr'] = responseData['allSoundEffectKeysArr'].concat(destroyBlockArr['allSoundEffectKeysArr']);
            }
        } else {
            const hitFloorArr = this.hitFloor(hitBlockCoordArr);
            if (hitFloorArr && hitFloorArr['allSoundEffectKeysArr'] && hitFloorArr['allSoundEffectKeysArr'].length) {
                responseData['allSoundEffectKeysArr'] = responseData['allSoundEffectKeysArr'].concat(hitFloorArr['allSoundEffectKeysArr']);
            }
        }
        if (responseData['allSoundEffectKeysArr'].length == 0) {
            delete responseData['allSoundEffectKeysArr'];
        }
        if (prevBlockDataJson != JSON.stringify(WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']])) {
            responseData['blockUpdateDataArr'] = {
                'blockCoordArr': hitBlockCoordArr,
                'blockDataArr':  WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']],
            };
            socket.broadcast.emit('blockUpdate', responseData['blockUpdateDataArr']); // Emit to all other players.
            WorldServer.updateSquareRedis(hitBlockCoordArr);
        }
        if (prevFloorDataJson != JSON.stringify(WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']])) {
            responseData['floorUpdateDataArr'] = {
                'floorCoordArr': hitBlockCoordArr,
                'floorDataArr':  WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']],
            };
            socket.broadcast.emit('floorUpdate', responseData['floorUpdateDataArr']); // Emit to all other players.
        }
        if (prevHotbarItemsArrJson != JSON.stringify(this.hotbarItemsArr)) {
            responseData['hotbarItemsArr'] = this.hotbarItemsArr;
        }
        if (prevContainerArr || hitBlockCoordStr in WorldServer.allContainersArr) {
            WorldServer.updateContainerRedis(hitBlockCoordStr, prevContainerArr);
        }
        if (prevPlayerScore != this.playerScore) {
            responseData['playerScore'] = this.playerScore;
            if (this.userName) {
                UtilsServer.redisClient.hincrby('player:' + this.userName, 'playerScore', this.playerScore - prevPlayerScore);
                UtilsServer.redisClient.hincrby('user:' + this.userName, 'userScore', this.playerScore - prevPlayerScore);
            }
        }
        responseFn(responseData);
        if (prevSelectedItemId != this.selectedItemId) {
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
        }
        this.updateHotbarRedis(prevHotbarArr);
    }

    attack(entityDamageValue: number, originalAngle: number) {
        // Get attack range stats.
        let attackRangeDistance = UtilsServer.playerDefaultRangeDistance;
        if (this.selectedItemId && Items.itemsArr[this.selectedItemId]['attackRangeDistance']) {
            attackRangeDistance = Items.itemsArr[this.selectedItemId]['attackRangeDistance'];
        }
        attackRangeDistance *= Utils.tileSize;
        let attackRangeRadius = UtilsServer.playerDefaultRangeRadius;
        if (this.selectedItemId && Items.itemsArr[this.selectedItemId]['attackRangeRadius']) {
            attackRangeRadius = Items.itemsArr[this.selectedItemId]['attackRangeRadius'];
        }

        // Attack things.
        const allSoundEffectKeysArr = [];
        let hasDamagedEntity        = false;
        for (let entityId in EntityServer.allEntityObjs) {
            const entityObj = EntityServer.allEntityObjs[entityId];
            if (this.currentLayerNum == entityObj.layerNum) {
                if (Utils.distance(this, entityObj) <= attackRangeDistance + (Utils.allEntityTypesArr[entityObj.type]['hitboxMult'] - 1) * Utils.tileSize) { // Distance within range.
                    const rotation = Utils.rotation(entityObj, this);
                    const angle    = Utils.toDegrees(rotation);
                    if (originalAngle <= angle + attackRangeRadius && originalAngle >= angle - attackRangeRadius) { // Angle within range.
                        let knockbackMult = 0;
                        if (this.selectedItemId && this.selectedItemId in Items.itemsArr && Items.itemsArr[this.selectedItemId]['attackKnockbackMult']) {
                            knockbackMult = Items.itemsArr[this.selectedItemId]['attackKnockbackMult'];
                        }
                        if (
                            this.hotbarItemsArr[this.selectedItemIndex]['itemEnchantmentsArr']
                            &&
                            'knockback' in this.hotbarItemsArr[this.selectedItemIndex]['itemEnchantmentsArr']
                        ) {
                            knockbackMult *= this.hotbarItemsArr[this.selectedItemIndex]['itemEnchantmentsArr']['knockback'] * 3 + 2;
                        }
                        this.playerDamageEntity(entityObj, -entityDamageValue, rotation, knockbackMult);
                        hasDamagedEntity = true;
                        if (this.selectedItemId && this.selectedItemId.indexOf('axe_') == 0) { // Only hit one entity with axe.
                            break;
                        }
                    }
                }
            }
        }
        if (hasDamagedEntity && this.decreaseSelectedItemDurability('sword')) {
            allSoundEffectKeysArr.push('break');
        }
        return {'allSoundEffectKeysArr': allSoundEffectKeysArr};
    }

    playerDamageEntity(entityObj: EntityServer, entityHealthUpdate: number, rotation: number, knockbackMult: number): void {
        if (this.hasRingEffect('ring_blood')) { // Heal if have ring of blood.
            this.updateHealth(Math.round(Math.abs(entityHealthUpdate) / 3), true, '');
        }
        if (rotation) {
            entityObj.addKnockback(rotation, knockbackMult);
        }
        const isEntityDead = entityObj.updateHealth(entityHealthUpdate);
        if (isEntityDead) {
            if (!entityObj.isBaby) { // No points or drops if is baby.
                if (Utils.allEntityTypesArr[entityObj.type]['pointsGivenWhenKilled'] != null) {
                    this.playerScore += Utils.allEntityTypesArr[entityObj.type]['pointsGivenWhenKilled'];
                }
                const lootKey = Utils.allEntityTypesArr[entityObj.type]['lootKey'] ?? entityObj.type;
                if (lootKey in LootTable.tablesArr) {
                    const lootItemsArr  = LootTable.getLootItemsArr(lootKey);
                    this.hotbarItemsArr = HotbarServer.addItemsFromArr(this.hotbarItemsArr, lootItemsArr);
                    this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                } else if (Utils.allEntityTypesArr[entityObj.type]['yieldItemsArr'] != null) {

                    const yieldItemsArr = Utils.dereferenceObj(Utils.allEntityTypesArr[entityObj.type]['yieldItemsArr']);
                    if (!Utils.allEntityTypesArr[entityObj.type]['isProjectile'] && entityObj.sizeMult != 1) {
                        for (let i in yieldItemsArr) {
                            yieldItemsArr[i]['itemCount'] *= entityObj.sizeMult;
                            yieldItemsArr[i]['itemCount'] *= entityObj.sizeMult;
                            yieldItemsArr[i]['itemCount'] = Math.round(yieldItemsArr[i]['itemCount']);
                            yieldItemsArr[i]['itemCount'] = Math.max(yieldItemsArr[i]['itemCount'], 1);
                        }
                    }

                    if (entityObj.ridingPlayerId && entityObj.ridingPlayerId != this.id) {
                        PlayerServer.allPlayerObjs[entityObj.ridingPlayerId].addItemsAndEmit(yieldItemsArr);
                    } else {
                        this.hotbarItemsArr = HotbarServer.addItemsFromArr(this.hotbarItemsArr, yieldItemsArr);
                        this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                    }
                }
            }
        }
    }

    addItemsAndEmit(addItemsArr: HotbarSlot[]): void {
        const prevHotbarArr      = Utils.dereferenceObj(this.hotbarItemsArr);
        const prevSelectedItemId = this.selectedItemId;
        this.hotbarItemsArr      = HotbarServer.addItemsFromArr(this.hotbarItemsArr, addItemsArr);
        this.selectedItemId      = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
        if (prevSelectedItemId != this.selectedItemId) {
            UtilsServer.io.emit('selectItem', this.id, this.selectedItemId); // Emit to all players.
        }
        const responseData = {
            'status':         'success',
            'hotbarItemsArr': this.hotbarItemsArr,
        };
        UtilsServer.io.to(this.id).emit('handleMiscEmit', responseData);
        this.updateHotbarRedis(prevHotbarArr);
    }

    decreaseSelectedItemDurability(correctToolType: ToolType | null = null): boolean {
        if (this.hotbarItemsArr[this.selectedItemIndex]) {
            // Add durability if should have it, but doesn't.
            if (
                !('itemDurability' in this.hotbarItemsArr[this.selectedItemIndex])
                &&
                this.selectedItemId
                &&
                Items.itemsArr[this.selectedItemId]['itemDurability']
            ) {
                this.hotbarItemsArr[this.selectedItemIndex]['itemDurability'] = Items.itemsArr[this.selectedItemId]['itemDurability'];
            }
            if (
                'itemDurability' in this.hotbarItemsArr[this.selectedItemIndex]
                &&
                !Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']
            ) {
                // Decrease durability.
                if (this.selectedItemId && (!correctToolType || UtilsServer.isCorrectTool(this.selectedItemId, correctToolType))) { // Is correct tool.
                    this.hotbarItemsArr[this.selectedItemIndex]['itemDurability']--;
                } else {
                    this.hotbarItemsArr[this.selectedItemIndex]['itemDurability'] -= 2;
                }
                if (this.hotbarItemsArr[this.selectedItemIndex]['itemDurability'] <= 0) { // Item broke.
                    this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
                    this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                    return true;
                }
            }
        }
        return false;
    }

    decreaseAccessoryItemDurability(itemIndex: number, decreaseAmount: number): boolean {
        if (this.accessoryItemsArr[itemIndex]) {
            const itemId = this.accessoryItemsArr[itemIndex]['itemId'];
            // Add durability if should have it, but doesn't.
            if (
                !('itemDurability' in this.accessoryItemsArr[itemIndex])
                &&
                itemId
                &&
                Items.itemsArr[itemId]['itemDurability']
            ) {
                this.accessoryItemsArr[itemIndex]['itemDurability'] = Items.itemsArr[itemId]['itemDurability'];
            }
            if (
                'itemDurability' in this.accessoryItemsArr[itemIndex]
                &&
                !Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']
            ) {
                // Decrease durability.
                this.accessoryItemsArr[itemIndex]['itemDurability'] -= decreaseAmount;
                if (this.accessoryItemsArr[itemIndex]['itemDurability'] <= 0) { // Item broke.
                    this.accessoryItemsArr = HotbarServer.removeItemsAtIndex(this.accessoryItemsArr, itemIndex, 1);
                    return true;
                }
            }
        }
        return false;
    }

    hasCorrectTieredTool(blockItemId: ItemId): boolean {
        return (
            Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks'] // Has infinite blocks, or,
            ||
            !Items.itemsArr[blockItemId]['itemTier'] // Item doesn't have a tier set, or,
            ||
            Items.itemsArr[blockItemId]['itemTier'] == 'wood' // Item is tier wood, or,
            ||
            (
                this.selectedItemId // Has an item selected, and,
                &&
                this.selectedItemId in Items.itemsArr // Is a valid item, and,
                &&
                Items.itemsArr[this.selectedItemId]['isTool'] // Item is a tool, and,
                &&
                UtilsServer.isCorrectTool(this.selectedItemId, Items.itemsArr[blockItemId]['whichTool']) // Is the correct tool for this block, and,
                &&
                UtilsServer.tierMultArr[Items.itemsArr[this.selectedItemId]['itemTier']] // Item has a tier set, and,
                &&
                UtilsServer.tierMultArr[Items.itemsArr[this.selectedItemId]['itemTier']] + 1 >= UtilsServer.tierMultArr[Items.itemsArr[blockItemId]['itemTier']] // Is the same tier or higher as the block.
            )
        );
    }

    destroyBlock(hitBlockCoordStr: string, hitBlockCoordArr: Coord) {
        if (WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']]) { // Something at square.
            const blockItemId = Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][0]];
            if (blockItemId == 'tree_apple') {
                const newItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, Items.itemsArr[blockItemId]['dropItemId']);
                if (newItemIndex != null) {
                    this.hotbarItemsArr                                                                         = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': Items.itemsArr[blockItemId]['dropItemId'], 'itemCount': 1}, newItemIndex);
                    this.selectedItemId                                                                         = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                    WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][0] = Utils.randElFromArr(Items.itemsArr['tree_apple_0']['tileIndexesArr']);
                    WorldServer.cropsToGrowCoordsArr[hitBlockCoordStr]                                          = hitBlockCoordArr;
                    return;
                }
            } else if (blockItemId == 'grindstone' && !this.selectedItemId) {
                WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][3]++;
                if (WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][3] >= 20) {
                    return {'allSoundEffectKeysArr': ['grindstone', 'pop']};
                }
                return {'allSoundEffectKeysArr': ['grindstone']};
            }
            if (WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][1] == -1) {
                // Item is unbreakable.
                return;
            }
            const blockDamageValue = this.getGetBlockDamageValue(blockItemId);
            WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][1] -= blockDamageValue;
            if (WorldServer.worldArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][1] <= 0) {
                // Block destroyed.
                let itemsToAddArr          = [];
                const dropItemId           = Items.itemsArr[blockItemId]['dropItemId'];
                const hasCorrectTieredTool = this.hasCorrectTieredTool(blockItemId);
                if (dropItemId != null && Items.itemsArr[dropItemId]['isObtainable'] && hasCorrectTieredTool) {
                    itemsToAddArr.push({'itemId': dropItemId, 'itemCount': 1});
                }
                // Add items from crop.
                if (Items.itemsArr[blockItemId]['cropSeedId']) {
                    if (Items.itemsArr[blockItemId]['cropNextStageId']) { // Crop isn't fully grown.
                        itemsToAddArr.push({'itemId': Items.itemsArr[blockItemId]['cropSeedId'], 'itemCount': 1});
                    } else { // Crop is fully grown.
                        itemsToAddArr.push({'itemId': Items.itemsArr[blockItemId]['cropSeedId'], 'itemCount': Utils.randBetween(1, 2)});
                        if (Items.itemsArr[blockItemId]['cropYieldItemId']) {
                            itemsToAddArr.push({'itemId': Items.itemsArr[blockItemId]['cropYieldItemId'], 'itemCount': 1});
                        }
                    }
                    if (Items.itemsArr[blockItemId]['pointsGivenOnDestroy']) {
                        this.playerScore += Items.itemsArr[blockItemId]['pointsGivenOnDestroy'];
                    }
                }
                // Add items from container.
                const blockCoordStr = hitBlockCoordArr['layerNum'] + ':' + hitBlockCoordArr['x'] + ',' + hitBlockCoordArr['y'];
                if (Items.itemsArr[blockItemId]['isContainer'] && blockCoordStr in WorldServer.allContainersArr) {
                    for (let slotIndex in WorldServer.allContainersArr[blockCoordStr]) {
                        let slotDetails = WorldServer.allContainersArr[blockCoordStr][slotIndex];
                        if (slotDetails['itemId'] && slotDetails['itemCount']) {
                            itemsToAddArr.push(slotDetails);
                        }
                    }
                }
                let allSoundEffectKeysArr = [];
                if (Items.itemsArr[blockItemId]['breakSoundEffectKey']) {
                    allSoundEffectKeysArr.push(Items.itemsArr[blockItemId]['breakSoundEffectKey']);
                }
                if (this.decreaseSelectedItemDurability(Items.itemsArr[blockItemId]['whichTool'])) {
                    allSoundEffectKeysArr.push('break');
                }
                if (Object.keys(itemsToAddArr).length) {
                    this.hotbarItemsArr = HotbarServer.addItemsFromArr(this.hotbarItemsArr, itemsToAddArr);
                    this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                }
                WorldServer.destroyBlock(hitBlockCoordArr, hitBlockCoordStr);
                return {'allSoundEffectKeysArr': allSoundEffectKeysArr};
            }
        }
        return null;
    }

    hitFloor(hitBlockCoordArr: Coord) {
        if (
            this.selectedItemId
            &&
            (
                this.selectedItemId.indexOf('hoe_') == 0
                ||
                this.selectedItemId.indexOf('shovel_') == 0
            )
            &&
            this.selectedItemId in Items.itemsArr
        ) {
            if (WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][1] == -1) {
                // Block is unbreakable.
                return;
            }
            const floorItemIndex      = WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][0];
            const floorItemId         = Utils.tileIndexToItemIdArr[floorItemIndex];
            const blockDamageValue    = this.getGetBlockDamageValue(floorItemId, (this.selectedItemId.indexOf('hoe_') == 0));
            WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][1] -= Math.max(blockDamageValue - 1, 0);
            let allSoundEffectKeysArr = [];
            if (WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']][1] <= 0) {
                let newFloorItemId;
                if (this.selectedItemId.indexOf('hoe_') == 0 && Items.itemsArr[floorItemId]['canTill']) {
                    if (floorItemId == 'dirt') {
                        // Un-till square.
                        newFloorItemId = WorldServer.getMostCommonNeighbourFloorItemId(this.currentLayerNum, hitBlockCoordArr['x'], hitBlockCoordArr['y']);
                    } else {
                        newFloorItemId = 'dirt';
                    }
                } else {
                    if (this.currentLayerNum == 0) {
                        if (floorItemId == 'ice') {
                            newFloorItemId = 'snow';
                        } else if (floorItemId == 'snow') {
                            newFloorItemId = 'grass';
                        } else if (floorItemId == 'grass' || floorItemId == 'dirt' || floorItemId == 'swamp' || floorItemId == 'bog') {
                            newFloorItemId = 'sand';
                        } else if (floorItemId == 'sand' && this.selectedItemId.indexOf('shovel_') == 0) {
                            newFloorItemId = 'water';
                        } else {
                            newFloorItemId = WorldServer.getMostCommonNeighbourFloorItemId(this.currentLayerNum, hitBlockCoordArr['x'], hitBlockCoordArr['y']);
                        }
                    } else {
                        newFloorItemId = 'cave';
                    }
                    if (floorItemId == 'bridge') {
                        if (floorItemIndex == Items.itemsArr['bridge']['altTileIndexesArr'][0]) {
                            newFloorItemId = 'water';
                        } else if (floorItemIndex == Items.itemsArr['bridge']['altTileIndexesArr'][1]) {
                            newFloorItemId = 'lava';
                        }
                    }
                    if (Items.itemsArr[floorItemId]['dropItemId']) {
                        this.hotbarItemsArr = HotbarServer.addItemsFromArr(this.hotbarItemsArr, [{'itemId': Items.itemsArr[floorItemId]['dropItemId'], 'itemCount': 1}]);
                        this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                    }
                }
                if (Items.itemsArr[floorItemId]['breakSoundEffectKey']) {
                    allSoundEffectKeysArr.push(Items.itemsArr[floorItemId]['breakSoundEffectKey']);
                }
                if (this.decreaseSelectedItemDurability()) {
                    allSoundEffectKeysArr.push('break');
                }
                WorldServer.floorArr[this.currentLayerNum][hitBlockCoordArr['y']][hitBlockCoordArr['x']] = [Utils.randElFromArr(Items.itemsArr[newFloorItemId]['tileIndexesArr']), Items.itemsArr[newFloorItemId]['structureDurability']];
            }
            WorldServer.updateFloorSquareRedis(hitBlockCoordArr);
            return {'allSoundEffectKeysArr': allSoundEffectKeysArr};
        }
    }

    getGetBlockDamageValue(blockItemId: ItemId, isHoe: boolean = false) {
        if (Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            return 1000;
        }
        // console.log('-----');
        let blockDamageValue = 3;
        blockDamageValue /= Items.itemsArr[blockItemId]['blockHardness'];
        if (this.selectedItemId && (isHoe || UtilsServer.isCorrectTool(this.selectedItemId, Items.itemsArr[blockItemId]['whichTool']))) { // Is correct tool.
            // console.log('correct tool');
            blockDamageValue *= 1.5;
            if (
                this.selectedItemId
                &&
                this.selectedItemId in Items.itemsArr
                &&
                Items.itemsArr[this.selectedItemId]['isTool']
                &&
                Items.itemsArr[this.selectedItemId]['itemTier'] in UtilsServer.tierBlockMultArr
                &&
                UtilsServer.tierBlockMultArr[Items.itemsArr[this.selectedItemId]['itemTier']] // Item has a tier set, and,
            ) {
                // console.log('tier', UtilsServer.tierBlockMultArr[Items.itemsArr[this.selectedItemId]['itemTier']]);
                blockDamageValue *= UtilsServer.tierBlockMultArr[Items.itemsArr[this.selectedItemId]['itemTier']];
            }
            if (isHoe) {
                blockDamageValue *= 0.5;
            }
        } else {
            // console.log('incorrect tool');
            blockDamageValue /= 1.5;
            if (this.selectedItemId && !isHoe && this.selectedItemId.indexOf('sword') == 0) { // Swords do zero block damage if wrong tool.
                blockDamageValue = 1;
            }
        }
        blockDamageValue = Math.round(blockDamageValue);
        blockDamageValue = Math.max(1, blockDamageValue);
        // console.log(blockDamageValue);
        return blockDamageValue;
    }

    feedEntities(socket: SocketIO.Socket, responseFn: (response: ServerResponse) => void): void {
        if (this.selectedItemId && !Items.itemsArr[this.selectedItemId]['canFeedEntities']) {
            return;
        }

        // Get attack range stats.
        const attackRangeDistance = UtilsServer.playerDefaultRangeDistance * Utils.tileSize;
        const attackRangeRadius   = UtilsServer.playerDefaultRangeRadius;
        const nowTime             = Utils.time();

        // Feed entities.
        for (let entityId in EntityServer.allEntityObjs) {
            const entityObj = EntityServer.allEntityObjs[entityId];
            if (this.currentLayerNum == entityObj.layerNum) {
                if (Utils.distance(this, entityObj) <= attackRangeDistance + (Utils.allEntityTypesArr[entityObj.type]['hitboxMult'] - 1) * Utils.tileSize) { // Distance within range.
                    const angle = Utils.toDegrees(Utils.rotation(entityObj, this));
                    if (this.angle <= angle + attackRangeRadius && this.angle >= angle - attackRangeRadius) { // Angle within range.
                        const entityObj = EntityServer.allEntityObjs[entityId];
                        if (!entityObj.isBaby) {
                            if (this.selectedItemId == 'shears' && entityObj.type == 'sheep' && entityObj.hasWool) {
                                // Check has space.
                                const newItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, 'wool');
                                if (newItemIndex == null) {
                                    return;
                                }
                                // Shear sheep.
                                entityObj.hasWool         = false;
                                entityObj.regrowWoolTime  = nowTime + Utils.cycleLengthMillis; // Grow back in a day.
                                const prevSelectedItemId  = this.selectedItemId;
                                const prevHotbarArr       = Utils.dereferenceObj(this.hotbarItemsArr);
                                let woolCount             = Utils.randBetween(1, 2);
                                woolCount *= entityObj.sizeMult;
                                woolCount *= entityObj.sizeMult;
                                woolCount                 = Math.round(woolCount);
                                woolCount                 = Math.max(woolCount, 1);
                                this.hotbarItemsArr       = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': 'wool', 'itemCount': woolCount}, newItemIndex);
                                let allSoundEffectKeysArr = ['shear'];
                                if (this.decreaseSelectedItemDurability()) {
                                    allSoundEffectKeysArr.push('break');
                                }
                                this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                                if (prevSelectedItemId != this.selectedItemId) {
                                    socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
                                }
                                this.updateHotbarRedis(prevHotbarArr);
                                responseFn({
                                    'status':                'success',
                                    'hotbarItemsArr':        this.hotbarItemsArr,
                                    'allSoundEffectKeysArr': allSoundEffectKeysArr,
                                });
                                return;
                            }
                            if (this.selectedItemId == 'bucket' && entityObj.type == 'cow') {
                                // Check has space.
                                let newItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, 'bucket_milk');
                                if (newItemIndex == null) {
                                    return;
                                }
                                const prevSelectedItemId = this.selectedItemId;
                                const prevHotbarArr      = Utils.dereferenceObj(this.hotbarItemsArr);
                                if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
                                    this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
                                }
                                if (
                                    this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == null
                                    ||
                                    this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == 'bucket_milk'
                                ) {
                                    newItemIndex = this.selectedItemIndex;
                                }
                                this.hotbarItemsArr       = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': 'bucket_milk', 'itemCount': 1}, newItemIndex);
                                let allSoundEffectKeysArr = ['milk'];
                                this.selectedItemId       = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                                if (prevSelectedItemId != this.selectedItemId) {
                                    socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
                                }
                                this.updateHotbarRedis(prevHotbarArr);
                                responseFn({
                                    'status':                'success',
                                    'hotbarItemsArr':        this.hotbarItemsArr,
                                    'allSoundEffectKeysArr': allSoundEffectKeysArr,
                                });
                                return;
                            }
                            if (!entityObj.isFed && entityObj.nextFeedTime <= nowTime && Utils.allEntityTypesArr[entityObj.type]['feedingItemId'] == this.selectedItemId) { // Can only be fed after a cycle.
                                entityObj.isFed        = true;
                                entityObj.nextFeedTime = nowTime + Utils.cycleLengthMillis;
                                if (!(entityObj.type in EntityServer.allFedEntityIdsByTypeArr)) {
                                    EntityServer.allFedEntityIdsByTypeArr[entityObj.type] = [];
                                }
                                EntityServer.allFedEntityIdsByTypeArr[entityObj.type].push(entityId);
                                if (EntityServer.allFedEntityIdsByTypeArr[entityObj.type].length >= 2) { // More than two entities of the same type.
                                    // Un-feed them.
                                    let otherEntitySizeMult = 1;
                                    for (let i in EntityServer.allFedEntityIdsByTypeArr[entityObj.type]) {
                                        const entityId_loop  = EntityServer.allFedEntityIdsByTypeArr[entityObj.type][i];
                                        const entityObj_loop = EntityServer.allEntityObjs[entityId_loop];
                                        if (entityObj_loop) {
                                            entityObj_loop.isFed = false;
                                            if (entityId_loop != entityObj.id) {
                                                otherEntitySizeMult = entityObj_loop.sizeMult;
                                            }
                                        }
                                    }
                                    EntityServer.allFedEntityIdsByTypeArr = {};

                                    // Make a baby entity.
                                    const sizeMultMutation                   = (1 - Utils.randBetween(Utils.allEntityTypesArr[entityObj.type]['minSizeMult'] * 100, Utils.allEntityTypesArr[entityObj.type]['maxSizeMult'] * 100) / 100) / 10;
                                    const parentAverageSizeMult              = (entityObj.sizeMult + otherEntitySizeMult) / 2;
                                    const newEntityDetailsArr: EntityDataArr = {
                                        'type':     entityObj.type,
                                        'layerNum': entityObj.layerNum,
                                        'x':        entityObj.x,
                                        'y':        entityObj.y,
                                        'angle':    entityObj.angle,
                                        'sizeMult': parentAverageSizeMult + sizeMultMutation,
                                        'isBaby':   true,
                                    };
                                    if (Utils.allEntityTypesArr[entityObj.type]['cyclesUntilAdultNum']) {
                                        newEntityDetailsArr['growUpTime'] = nowTime + (Utils.cycleLengthMillis * Utils.allEntityTypesArr[entityObj.type]['cyclesUntilAdultNum']);
                                    }
                                    new EntityServer(newEntityDetailsArr);

                                    this.playerScore += Utils.allEntityTypesArr[entityObj.type]['pointsGivenWhenKilled'];
                                    if (this.userName) {
                                        UtilsServer.redisClient.hincrby('player:' + this.userName, 'playerScore', Utils.allEntityTypesArr[entityObj.type]['pointsGivenWhenKilled']);
                                        UtilsServer.redisClient.hincrby('user:' + this.userName, 'userScore', Utils.allEntityTypesArr[entityObj.type]['pointsGivenWhenKilled']);
                                    }
                                }
                                const prevSelectedItemId = this.selectedItemId;
                                const prevHotbarArr      = Utils.dereferenceObj(this.hotbarItemsArr);
                                if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
                                    this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
                                    this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
                                    if (prevSelectedItemId != this.selectedItemId) {
                                        socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
                                    }
                                    this.updateHotbarRedis(prevHotbarArr);
                                }
                                responseFn({
                                    'status':                   'success',
                                    'hotbarItemsArr':           this.hotbarItemsArr,
                                    'playerScore':              this.playerScore,
                                    'allParticleEffectKeysArr': ['hearts'],
                                });
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    rideEntities(socket: SocketIO.Socket, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isGhost)   { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)  { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on
        // Get attack range stats.
        const attackRangeDistance = UtilsServer.playerDefaultRangeDistance * Utils.tileSize * 1.5;
        const attackRangeRadius   = UtilsServer.playerDefaultRangeRadius;

        for (let entityId in EntityServer.allEntityObjs) {
            const entityObj = EntityServer.allEntityObjs[entityId];
            if (this.currentLayerNum == entityObj.layerNum) {
                if (Utils.distance(this, entityObj) <= attackRangeDistance + (Utils.allEntityTypesArr[entityObj.type]['hitboxMult'] - 1) * Utils.tileSize) { // Distance within range.
                    const angle = Utils.toDegrees(Utils.rotation(entityObj, this));
                    if (this.angle <= angle + attackRangeRadius && this.angle >= angle - attackRangeRadius) { // Angle within range.
                        const entityObj = EntityServer.allEntityObjs[entityId];
                        if (!entityObj.isBaby && Utils.allEntityTypesArr[entityObj.type]['isRideable'] && !entityObj.isRiding) {
                            this.ridingEntityId      = entityObj.id;
                            entityObj.x              = this.x;
                            entityObj.y              = this.y;
                            entityObj.isRiding       = true;
                            entityObj.ridingPlayerId = this.id;
                            responseFn({
                                'status':         'success',
                                'ridingEntityId': this.ridingEntityId,
                            });
                            return;
                        }
                    }
                }
            }
        }
    }

    dismountEntity(socket: SocketIO.Socket, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isGhost)           { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"      }); return; }
        if (this.isPaused)          { responseFn({'status': 'error', 'errorMessage': "Currently paused"     }); return; }
        if (!this.ridingEntityId)   { responseFn({'status': 'error', 'errorMessage': "Not riding anything"  }); return; }
        // @formatter:on
        if (this.ridingEntityId in EntityServer.allEntityObjs) {
            EntityServer.allEntityObjs[this.ridingEntityId].isRiding       = false;
            EntityServer.allEntityObjs[this.ridingEntityId].ridingPlayerId = null;
        }
        this.ridingEntityId = null;
        responseFn({
            'status':         'success',
            'ridingEntityId': null,
        });
    }

    toggleBlock(socket: SocketIO.Socket, blockCoordArr: Coord, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on

        if (
            WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']] != null
            &&
            WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']][0] in Utils.tileIndexToItemIdArr // Is a valid block, and,
            &&
            Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']][0]] in Items.itemsArr
            &&
            Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']][0]]]['toggleItemId'] // Block is toggleable.
        ) {
            blockCoordArr['layerNum'] = this.currentLayerNum;
            WorldServer.toggleBlock(blockCoordArr);
            if (Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']][0]] == 'button_down') {
                setTimeout(function () {
                    WorldServer.toggleBlock(blockCoordArr);
                }, 1000);
            }
            responseFn({'status': 'success'});
        } else {
            responseFn({'status': 'error', 'errorMessage': "Not toggleable"});
        }
    }

    triggerExplosive(explosiveBlockCoordArr: Coord, responseFn: (response: ServerResponse) => void) {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on

        if (
            WorldServer.worldArr[this.currentLayerNum][explosiveBlockCoordArr['y']][explosiveBlockCoordArr['x']] != null
            &&
            WorldServer.worldArr[this.currentLayerNum][explosiveBlockCoordArr['y']][explosiveBlockCoordArr['x']][0] in Utils.tileIndexToItemIdArr // Is a valid block, and,
            &&
            Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][explosiveBlockCoordArr['y']][explosiveBlockCoordArr['x']][0]]]['explosionPower'] // Block is explosive.
        ) {
            const _this = this;
            setTimeout(function () {
                if (WorldServer.worldArr[_this.currentLayerNum][explosiveBlockCoordArr['y']][explosiveBlockCoordArr['x']]) {
                    WorldServer.createExplosion({
                        'layerNum': _this.currentLayerNum,
                        'x':        explosiveBlockCoordArr['x'],
                        'y':        explosiveBlockCoordArr['y'],
                    }, Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[_this.currentLayerNum][explosiveBlockCoordArr['y']][explosiveBlockCoordArr['x']][0]]]['explosionPower']);
                }
            }, 2000);
            responseFn({'status': 'success', 'allSoundEffectKeysArr': ['fuse']});
        } else {
            responseFn({'status': 'error', 'errorMessage': "Not tnt"});
        }
    }

    clickRail(responseFn: (response: ServerResponse) => void) {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on

        const xRounded = Math.floor(this.x / Utils.tileSize);
        const yRounded = Math.floor(this.y / Utils.tileSize);
        if (
            WorldServer.worldArr[this.currentLayerNum][yRounded][xRounded] != null
            &&
            WorldServer.worldArr[this.currentLayerNum][yRounded][xRounded][0] in Utils.tileIndexToItemIdArr // Is a valid block, and,
            &&
            Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][yRounded][xRounded][0]].indexOf('rail') == 0 // Block is a rail.
        ) {
            if ( // Block below is a rail.
                this.currentLayerNum - 1 in WorldServer.worldArr
                &&
                WorldServer.worldArr[this.currentLayerNum - 1][yRounded][xRounded] != null
                &&
                WorldServer.worldArr[this.currentLayerNum - 1][yRounded][xRounded][0] in Utils.tileIndexToItemIdArr // Is a valid block, and,
                &&
                Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum - 1][yRounded][xRounded][0]].indexOf('rail') == 0 // Block is a rail.
            ) {
                if (this.isGhost) {
                    responseFn({'status': 'error', 'errorMessage': "Ghosts can't descend"});
                } else {
                    responseFn({'status': 'success'});
                    const _this           = this;
                    this.clickRailTimeout = setTimeout(function () {
                        delete _this.clickRailTimeout;
                        _this.setCurrentLayerNum(_this.currentLayerNum - 1, responseFn);
                    }, 500);
                }
            } else if ( // Block above is a rail.
                this.currentLayerNum + 1 in WorldServer.worldArr
                &&
                WorldServer.worldArr[this.currentLayerNum + 1][yRounded][xRounded] != null
                &&
                WorldServer.worldArr[this.currentLayerNum + 1][yRounded][xRounded][0] in Utils.tileIndexToItemIdArr // Is a valid block, and,
                &&
                Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum + 1][yRounded][xRounded][0]].indexOf('rail') == 0 // Block is a rail.
            ) {
                responseFn({'status': 'success'});
                const _this           = this;
                this.clickRailTimeout = setTimeout(function () {
                    delete _this.clickRailTimeout;
                    _this.setCurrentLayerNum(_this.currentLayerNum + 1, responseFn);
                }, 500);
            } else {
                responseFn({'status': 'error', 'errorMessage': "No rail on other side"});
            }
        } else {
            responseFn({'status': 'error', 'errorMessage': "Not on a rail"});
        }
    }

    cancelClickRail(responseFn: (response: ServerResponse) => void) {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on

        if (this.clickRailTimeout) {
            responseFn({'status': 'success'});
            clearTimeout(this.clickRailTimeout);
            delete this.clickRailTimeout;
        } else {
            responseFn({'status': 'error', 'errorMessage': "Can't cancel rail"});
        }
    }

    fillBucket(socket: SocketIO.Socket, coordArr: Coord, responseFn: (response: ServerResponse) => void) {
        // @formatter:off
        if (this.isCrafting)                 { responseFn({'status': 'error', 'errorMessage': "Currently crafting" }); return; }
        if (this.isPaused)                   { responseFn({'status': 'error', 'errorMessage': "Currently paused"   }); return; }
        if (this.selectedItemId != 'bucket') { responseFn({'status': 'error', 'errorMessage': "Bucket not selected"}); return; }
        // @formatter:on

        // Check no block at destination.
        if (WorldServer.worldArr[this.currentLayerNum][coordArr['y']][coordArr['x']] != null) {
            responseFn({'status': 'error', 'errorMessage': "Block at destination"});
            return;
        }

        // Check floor is valid.
        if (
            WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']] == null
            ||
            !(WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']][0] in Utils.tileIndexToItemIdArr)
        ) {
            responseFn({'status': 'error', 'errorMessage': "Invalid floor"});
            return;
        }

        // Check floor is water or lava.
        const floorItemId = Utils.tileIndexToItemIdArr[WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']][0]];
        if (floorItemId == 'water' && floorItemId == 'lava') {
            responseFn({'status': 'error', 'errorMessage': "Not water or lava"});
            return;
        }

        // Remove bucket from inventory.
        const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
        if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
        }
        const newItemId = `bucket_${floorItemId}`;
        let newItemIndex;
        if (
            this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == null
            ||
            this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == newItemId
        ) {
            newItemIndex = this.selectedItemIndex;
        } else {
            // noinspection TypeScriptValidateJSTypes
            newItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, newItemId);
        }

        // Check has space for new bucket.
        if (newItemIndex == null) {
            // Hotbar full, so add bucket back to inventory.
            this.hotbarItemsArr = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': this.selectedItemId, 'itemCount': 1}, this.selectedItemIndex);
            responseFn({'status': 'error', 'errorMessage': "Hotbar full"});
            return;
        }

        const responseData: ServerResponse = {
            'status':                'success',
            'allSoundEffectKeysArr': [`bucket_fill_${floorItemId}`],
        };

        // Add new bucket to hotbar.
        if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            this.hotbarItemsArr = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': newItemId, 'itemCount': 1}, newItemIndex);
            this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
            this.updateHotbarRedis(prevHotbarArr);
            responseData['hotbarItemsArr'] = this.hotbarItemsArr;
        }

        // Remove water or lava from floor.
        let newFloorItemId;
        if (this.currentLayerNum == 0) {
            newFloorItemId = WorldServer.getMostCommonNeighbourFloorItemId(this.currentLayerNum, coordArr['x'], coordArr['y']);
        } else {
            newFloorItemId = 'cave';
        }
        WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']] = [Utils.randElFromArr(Items.itemsArr[newFloorItemId]['tileIndexesArr']), Items.itemsArr[newFloorItemId]['structureDurability']];
        coordArr['layerNum']                                                     = this.currentLayerNum;
        WorldServer.updateFloorSquareRedis(coordArr);
        const floorUpdateDataArr = {
            'floorCoordArr': coordArr,
            'floorDataArr':  WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']],
        };
        socket.broadcast.emit('floorUpdate', floorUpdateDataArr); // Emit to all other players.
        responseData['floorUpdateDataArr'] = floorUpdateDataArr;

        responseFn(responseData);
    }

    placeEntity(socket: SocketIO.Socket, coordArr: Coord, responseFn: (response: ServerResponse) => void) {
        // @formatter:off
        if (this.isCrafting)                                           { responseFn({'status': 'error', 'errorMessage': "Currently crafting" }); return; }
        if (this.isPaused)                                             { responseFn({'status': 'error', 'errorMessage': "Currently paused"   }); return; }
        if (!this.selectedItemId)                                      { responseFn({'status': 'error', 'errorMessage': "No item selected"   }); return; }
        if (!Items.itemsArr[this.selectedItemId]['entityToPlaceType']) { responseFn({'status': 'error', 'errorMessage': "Item has no entity" }); return; }
        // @formatter:on

        // Check no block at destination.
        if (WorldServer.worldArr[this.currentLayerNum][coordArr['y']][coordArr['x']] != null) {
            responseFn({'status': 'error', 'errorMessage': "Block at destination"});
            return;
        }

        // Check floor is valid.
        if (
            WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']] == null
            ||
            !(WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']][0] in Utils.tileIndexToItemIdArr)
        ) {
            responseFn({'status': 'error', 'errorMessage': "Invalid floor"});
            return;
        }

        // Check floor is water or lava.
        const floorItemId = Utils.tileIndexToItemIdArr[WorldServer.floorArr[this.currentLayerNum][coordArr['y']][coordArr['x']][0]];
        if (floorItemId != 'water') {
            responseFn({'status': 'error', 'errorMessage': "Floor not water"});
            return;
        }

        // Create new entity.
        new EntityServer({
            'type':     Items.itemsArr[this.selectedItemId]['entityToPlaceType'],
            'layerNum': this.currentLayerNum,
            'x':        coordArr['x'] * Utils.tileSize + Utils.tileSize / 2,
            'y':        coordArr['y'] * Utils.tileSize + Utils.tileSize / 2,
            'isBaby':   false,
        });

        const responseData: ServerResponse = {
            'status': 'success',
        };

        // Remove boat from inventory.
        const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
        if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
            this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
            this.updateHotbarRedis(prevHotbarArr);
            responseData['hotbarItemsArr'] = this.hotbarItemsArr;
        }
        responseFn(responseData);
    }

    placeBlock(socket: SocketIO.Socket, placeBlockCoordArr: Coord, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                                                                                                         { responseFn({'status': 'error', 'errorMessage': "Currently crafting"           }); return; }
        if (this.isGhost)                                                                                                                                            { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"              }); return; }
        if (this.isPaused)                                                                                                                                           { responseFn({'status': 'error', 'errorMessage': "Currently paused"             }); return; }
        if (!this.selectedItemId)                                                                                                                                    { responseFn({'status': 'error', 'errorMessage': "No item selected"             }); return; }
        let itemToPlaceId = Items.itemsArr[this.selectedItemId]['itemToPlaceId'];
        if (!itemToPlaceId)                                                                                                                                          { responseFn({'status': 'error', 'errorMessage': "Item not placeable"           }); return; }
        if (!Items.itemsArr[itemToPlaceId]['tileIndexesArr'])                                                                                                        { responseFn({'status': 'error', 'errorMessage': "Item has no tile"             }); return; }
        if (WorldServer.worldArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']] != null)                                                    { responseFn({'status': 'error', 'errorMessage': "Block at destination"         }); return; }
        let returnItemId;
        let returnItemIndex;
        if (this.selectedItemId && this.selectedItemId in Items.itemsArr && Items.itemsArr[this.selectedItemId]['returnItemId']) {
            returnItemId    = Items.itemsArr[this.selectedItemId]['returnItemId'];
            returnItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, returnItemId);
            if (returnItemIndex == null)                                                                                                                             { responseFn({'status': 'error', 'errorMessage': "Hotbar full"                  }); return; }
        }

        const floorTileIndex = WorldServer.floorArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']][0];
        const floorTileId = Utils.tileIndexToItemIdArr[floorTileIndex];
        if (Items.itemsArr[itemToPlaceId]['cropRequiredFloorIdsArr'] && Items.itemsArr[itemToPlaceId]['cropRequiredFloorIdsArr'].indexOf(floorTileId) == -1)         { responseFn({'status': 'error', 'errorMessage': "Wrong floor type"             }); return; }
        // @formatter:on
        placeBlockCoordArr['layerNum'] = this.currentLayerNum;
        let floorUpdateDataArr;
        let blockUpdateDataArr;
        if (Items.itemsArr[itemToPlaceId]['isFloor']) {
            // console.log('-----------');
            // console.log(itemToPlaceId);
            // console.log(Items.itemsArr[itemToPlaceId]['tileIndexesArr']);
            // console.log(WorldServer.floorArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']]);
            if (Items.itemsArr[itemToPlaceId]['tileIndexesArr'].indexOf(floorTileIndex) != -1) {
                responseFn({'status': 'error', 'errorMessage': "Block at destination"});
                return;
            }
            let tileIndex = Utils.randElFromArr(Items.itemsArr[itemToPlaceId]['tileIndexesArr']);
            if (itemToPlaceId == 'bridge') {
                if (floorTileId != 'water' && floorTileId != 'lava') {
                    responseFn({'status': 'error', 'errorMessage': "Item not placeable"});
                    return;
                }

                if (floorTileId == 'water') {
                    tileIndex = Items.itemsArr[itemToPlaceId]['altTileIndexesArr'][0];
                } else if (floorTileId == 'lava') {
                    tileIndex = Items.itemsArr[itemToPlaceId]['altTileIndexesArr'][1];
                }
            }
            WorldServer.floorArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']] = [tileIndex, Items.itemsArr[itemToPlaceId]['structureDurability']];
            WorldServer.updateFloorSquareRedis(placeBlockCoordArr);
            floorUpdateDataArr = {
                'floorCoordArr': placeBlockCoordArr,
                'floorDataArr':  WorldServer.floorArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']],
            };
        } else {
            if (
                !Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']
                &&
                !Items.itemsArr[itemToPlaceId]['isDisablingCollision']
                &&
                UtilsServer.isAnyPlayerAtBlock(placeBlockCoordArr)
            ) {
                responseFn({'status': 'error', 'errorMessage': "Can't place on players"});
                return;
            }
            if (Items.itemsArr[itemToPlaceId]['cropRequiredDistanceToWater'] && !UtilsServer.checkBlockNearWater(WorldServer.floorArr[this.currentLayerNum], placeBlockCoordArr['x'], placeBlockCoordArr['y'], Items.itemsArr[itemToPlaceId]['cropRequiredDistanceToWater'])) {
                responseFn({'status': 'error', 'errorMessage': "Place closer to water"});
                return;
            }
            if (itemToPlaceId == 'bed' && this.currentLayerNum != 0) {
                responseFn({'status': 'error', 'errorMessage': "Can only place on overworld"});
                return;
            }
            // const floorItemId = Utils.tileIndexToItemIdArr[WorldServer.floorArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']][0]];
            // if (itemToPlaceId == 'torch' && floorItemId == 'dungeon') {
            //     responseFn({'status': 'error', 'errorMessage': "Wrong torch"});
            //     return;
            // }
            const worldCell: WorldCell = [Utils.randElFromArr(Items.itemsArr[itemToPlaceId]['tileIndexesArr']), Items.itemsArr[itemToPlaceId]['structureDurability']];
            if (itemToPlaceId == 'bed') {
                if (this.userName) {
                    worldCell[2] = this.userName;
                } else {
                    worldCell[2] = this.id;
                }
            }
            WorldServer.worldArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']] = worldCell;
            WorldServer.updateSquareRedis(placeBlockCoordArr);
            blockUpdateDataArr = {
                'blockCoordArr': placeBlockCoordArr,
                'blockId':       itemToPlaceId,
                'blockDataArr':  WorldServer.worldArr[this.currentLayerNum][placeBlockCoordArr['y']][placeBlockCoordArr['x']],
            };
            let blockCoordStr  = placeBlockCoordArr['layerNum'] + ':' + placeBlockCoordArr['x'] + ',' + placeBlockCoordArr['y'];
            if (Items.itemsArr[itemToPlaceId]['heaterWarmth'] != null) {
                WorldServer.allHeaterCoords[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
            }
            if (Items.itemsArr[itemToPlaceId]['isContainer']) {
                WorldServer.allContainersArr[blockCoordStr] = [];
                for (let i = 0; i < (Items.itemsArr[itemToPlaceId]['maxItemsInContainerNum'] || 1); i++) {
                    WorldServer.allContainersArr[blockCoordStr].push({'itemId': null, 'itemCount': null});
                }
                WorldServer.updateContainerRedis(blockCoordStr, null);
            }
            if (itemToPlaceId == 'workbench') {
                WorldServer.allWorkbenchCoords[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
            } else if (Items.itemsArr[itemToPlaceId]['cropSeedId']) {
                WorldServer.cropsToGrowCoordsArr[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
            } else if (itemToPlaceId == 'hearth' || itemToPlaceId == 'hearth_lit') {
                WorldServer.allHearthCoords[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
            } else if (itemToPlaceId == 'grindstone') {
                WorldServer.allGrindstoneCoords[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
            } else if (itemToPlaceId == 'fire') {
                Timer.addTimer('timeToLive', blockCoordStr, Utils.time() + (Items.itemsArr[itemToPlaceId]['timeToLiveSecs'] * 1000));
            } else if (itemToPlaceId.indexOf('mushroom_') == 0) {
                WorldServer.mushroomsToSpreadCoordsArr[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
            }
        }
        const prevSelectedItemId           = this.selectedItemId;
        const responseData: ServerResponse = {
            'status': 'success',
        };
        if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
            this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
            if (returnItemId) {
                if (
                    this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == null
                    ||
                    this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == returnItemId
                ) {
                    returnItemIndex = this.selectedItemIndex;
                } else {
                    returnItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, returnItemId);
                }
                this.hotbarItemsArr = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': returnItemId, 'itemCount': 1}, returnItemIndex);
            }
            this.updateHotbarRedis(prevHotbarArr);
            this.selectedItemId            = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
            responseData['hotbarItemsArr'] = this.hotbarItemsArr;
        }
        if (Items.itemsArr[itemToPlaceId]['hitSoundEffectKey']) {
            responseData['allSoundEffectKeysArr'] = [Items.itemsArr[itemToPlaceId]['hitSoundEffectKey']];
        }
        if (Items.itemsArr[itemToPlaceId]['hexCode']) {
            responseData['allParticleEffectKeysArr'] = [Items.itemsArr[itemToPlaceId]['hexCode']];
        }
        if (blockUpdateDataArr) {
            responseData['blockUpdateDataArr'] = blockUpdateDataArr;
            socket.broadcast.emit('blockUpdate', blockUpdateDataArr); // Emit to all other players.
        } else if (floorUpdateDataArr) {
            responseData['floorUpdateDataArr'] = floorUpdateDataArr;
            socket.broadcast.emit('floorUpdate', floorUpdateDataArr); // Emit to all other players.
        }
        responseFn(responseData);
        if (prevSelectedItemId != this.selectedItemId) {
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
        }
    }

    useItem(socket: SocketIO.Socket, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                  { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)                                                     { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)                                                    { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        if (!this.selectedItemId || !(this.selectedItemId in Items.itemsArr)) { responseFn({'status': 'error', 'errorMessage': "No item selected"  }); return; }
        if (!Items.itemsArr[this.selectedItemId]['isUsable'])                 { responseFn({'status': 'error', 'errorMessage': "Item not usable"   }); return; }
        let returnItemId;
        let returnItemIndex;
        if (this.selectedItemId && this.selectedItemId in Items.itemsArr && Items.itemsArr[this.selectedItemId]['returnItemId']) {
            returnItemId    = Items.itemsArr[this.selectedItemId]['returnItemId'];
            returnItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, returnItemId);
            if (returnItemIndex == null)                                                                                                                                                                                                                                  { responseFn({'status': 'error', 'errorMessage': "Hotbar full"                  }); return; }
        }
        // @formatter:on

        let errorMessage;
        let didUseItem = false;
        if (Items.itemsArr[this.selectedItemId]['hungerValue']) {
            if (Utils.playerGameModesArr[this.gameModeStr]['hasHunger'] && this.hunger < this.maxHunger) {
                this.hunger += parseInt(Items.itemsArr[this.selectedItemId]['hungerValue']); // Gain hunger from consumed item.
                didUseItem = true;
            } else {
                errorMessage = "Not hungry";
            }
        }
        if (Items.itemsArr[this.selectedItemId]['warmthValue']) {
            if (Utils.playerGameModesArr[this.gameModeStr]['hasWarmth'] && this.warmth < this.maxWarmth) {
                this.warmth += parseInt(Items.itemsArr[this.selectedItemId]['warmthValue']); // Gain warmth from consumed item.
                didUseItem = true;
            } else {
                errorMessage = "Not cold";
            }
        }
        if (Items.itemsArr[this.selectedItemId]['healthValue']) {
            if (Utils.playerGameModesArr[this.gameModeStr]['hasHealth'] && this.health < this.maxHealth) {
                this.health += parseInt(Items.itemsArr[this.selectedItemId]['healthValue']); // Gain health from consumed item.
                didUseItem = true;
            } else {
                errorMessage = "Not injured";
            }
        }
        const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
        if (Items.itemsArr[this.selectedItemId]['additionalHotbarSlotsNum']) { // Gain slots from consumed item.
            const additionalHotbarSlotsNum = parseInt(Items.itemsArr[this.selectedItemId]['additionalHotbarSlotsNum']);
            const actualHotbarSlotsNum     = this.hotbarItemsArr.length;
            const targetHotbarSlotsNum     = 9 + 9 + additionalHotbarSlotsNum;
            if (targetHotbarSlotsNum > actualHotbarSlotsNum) { // Item adds more slots than had previously.
                if (
                    this.selectedItemId != 'backpack' // Item isn't a backpack, or,
                    ||
                    (this.selectedItemId == 'backpack' && actualHotbarSlotsNum == (9 + 9 + Items.itemsArr['bag']['additionalHotbarSlotsNum'])) // Item is a backpack, and person has the same number of slots as with a bag.
                ) {
                    const hotbarSlotsToAddNum = targetHotbarSlotsNum - actualHotbarSlotsNum;
                    for (let i = 0; i < hotbarSlotsToAddNum; i++) {
                        this.hotbarItemsArr.push({'itemId': null, 'itemCount': null});
                    }
                    didUseItem = true;
                } else {
                    errorMessage = "Use a bag before backpack";
                }
            } else {
                errorMessage = "Already have enough slots";
            }
        }
        if (!didUseItem && errorMessage) {
            responseFn({'status': 'error', 'errorMessage': errorMessage});
            return;
        }
        const prevSelectedItemId = this.selectedItemId;
        if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, this.selectedItemIndex, 1);
        }
        if (returnItemId) {
            if (
                this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == null
                ||
                this.hotbarItemsArr[this.selectedItemIndex]['itemId'] == returnItemId
            ) {
                returnItemIndex = this.selectedItemIndex;
            } else {
                returnItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, returnItemId);
            }
            this.hotbarItemsArr = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, {'itemId': returnItemId, 'itemCount': 1}, returnItemIndex);
        }
        this.updateHotbarRedis(prevHotbarArr);
        this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
        responseFn({
            'status':          'success',
            'hotbarItemsArr':  this.hotbarItemsArr,
            'statBarsDataArr': {
                'hunger': this.hunger,
                'warmth': this.warmth,
                'health': this.health,
            },
        });
        if (prevSelectedItemId != this.selectedItemId) {
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
        }
    }

    fireProjectile(socket: SocketIO.Socket, projectileData: { projectileCharge: number }, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                  { responseFn({'status': 'error', 'errorMessage': "Currently crafting"         }); return; }
        if (this.isGhost)                                                     { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"            }); return; }
        if (this.isPaused)                                                    { responseFn({'status': 'error', 'errorMessage': "Currently paused"           }); return; }
        if (!this.selectedItemId || !(this.selectedItemId in Items.itemsArr)) { responseFn({'status': 'error', 'errorMessage': "No item selected"           }); return; }
        if (!Items.itemsArr[this.selectedItemId]['isFiringProjectiles'])      { responseFn({'status': 'error', 'errorMessage': "Item can't fire projectiles"}); return; }
        const projectileAmmoIndex = Utils.hasAmmoForProjectile(this);
        if (projectileAmmoIndex === null)                                     { responseFn({'status': 'error', 'errorMessage': "No ammo found"              }); return; }
        // @formatter:on
        EntityServer.createProjectile({'x': this.x, 'y': this.y, 'layerNum': this.currentLayerNum}, this.angle, this.selectedItemId, projectileData['projectileCharge'], this.id);
        const prevSelectedItemId = this.selectedItemId;
        let allSoundEffectKeysArr;
        if (!Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks']) {
            const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
            this.hotbarItemsArr = HotbarServer.removeItemsAtIndex(this.hotbarItemsArr, projectileAmmoIndex, 1);
            if (this.decreaseSelectedItemDurability()) {
                allSoundEffectKeysArr = ['break'];
            }
            this.updateHotbarRedis(prevHotbarArr);
            this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
        }
        responseFn({
            'status':                'success',
            'hotbarItemsArr':        this.hotbarItemsArr,
            'allSoundEffectKeysArr': allSoundEffectKeysArr,
        });
        if (prevSelectedItemId != this.selectedItemId) {
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
        }
    }

    openContainer(containerCoordArr: Coord, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                  { responseFn({'status': 'error', 'errorMessage': "Currently crafting"         }); return; }
        if (this.isGhost)                                                     { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"            }); return; }
        if (this.isPaused)                                                    { responseFn({'status': 'error', 'errorMessage': "Currently paused"           }); return; }
        // @formatter:on
        const worldCell = WorldServer.worldArr[containerCoordArr['layerNum']][containerCoordArr['y']][containerCoordArr['x']];
        if (worldCell) {
            const worldItemId       = Utils.tileIndexToItemIdArr[worldCell[0]];
            const containerCoordStr = containerCoordArr['layerNum'] + ':' + containerCoordArr['x'] + ',' + containerCoordArr['y'];
            if (!(containerCoordStr in WorldServer.allContainersArr) && (worldItemId.indexOf('lootcrate_') == 0 || worldItemId.indexOf('present_') == 0)) {
                LootTable.setContainerArrForBlock(worldItemId, containerCoordStr);
            }
            if (containerCoordStr in WorldServer.allContainersArr) {
                responseFn({
                    'status':       'success',
                    'containerArr': WorldServer.allContainersArr[containerCoordStr],
                });
            } else {
                responseFn({
                    'status':       'error',
                    'errorMessage': "Container not found",
                });
            }
        }
    }

    enchantItem(tableCoordArr: Coord, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                  { responseFn({'status': 'error', 'errorMessage': "Currently crafting"         }); return; }
        if (this.isGhost)                                                     { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"            }); return; }
        if (this.isPaused)                                                    { responseFn({'status': 'error', 'errorMessage': "Currently paused"           }); return; }
        tableCoordArr['layerNum'] = this.currentLayerNum;
        const worldCell = WorldServer.worldArr[tableCoordArr['layerNum']][tableCoordArr['y']][tableCoordArr['x']];
        if (!worldCell)                                                       { responseFn({'status': 'error', 'errorMessage': "Nothing at block"           }); return; }
        const worldItemId = Utils.tileIndexToItemIdArr[worldCell[0]];
        if (worldItemId != 'enchanting_table')                                { responseFn({'status': 'error', 'errorMessage': "Not an enchanting table"    }); return; }
        // @formatter:on
        const tableCoordStr = tableCoordArr['layerNum'] + ':' + tableCoordArr['x'] + ',' + tableCoordArr['y'];
        if (tableCoordStr in WorldServer.allContainersArr) {
            const prevContainerArr = Utils.dereferenceObj(WorldServer.allContainersArr[tableCoordStr]);
            if (WorldServer.allContainersArr[tableCoordStr][0]['itemId'] == null) {
                responseFn({'status': 'error', 'errorMessage': "Nothing to enchant"});
                return;
            }
            if (WorldServer.allContainersArr[tableCoordStr][1]['itemId'] != 'silver') {
                responseFn({'status': 'error', 'errorMessage': "Need silver"});
                return;
            }

            const allEnchantmentTypes = [];
            for (const enchantmentType in Utils.allEnchantmentsArr) {
                if (enchantmentType == 'unbreaking' && Items.itemsArr[WorldServer.allContainersArr[tableCoordStr][0]['itemId']]['itemDurability']) {
                    allEnchantmentTypes.push(enchantmentType);
                    continue;
                }
                for (const itemType of Utils.allEnchantmentsArr[enchantmentType]['itemTypesArr']) {
                    if (WorldServer.allContainersArr[tableCoordStr][0]['itemId'].indexOf(itemType) == 0) {
                        allEnchantmentTypes.push(enchantmentType);
                        break;
                    }
                }
            }

            const enchantmentType = Utils.randElFromArr(allEnchantmentTypes);
            if (!('itemEnchantmentsArr' in WorldServer.allContainersArr[tableCoordStr][0])) {
                WorldServer.allContainersArr[tableCoordStr][0]['itemEnchantmentsArr'] = {};
            }
            WorldServer.allContainersArr[tableCoordStr][0]['itemEnchantmentsArr'][enchantmentType] = Utils.randBetween(Utils.allEnchantmentsArr[enchantmentType]['tierMin'], Utils.allEnchantmentsArr[enchantmentType]['tierMax']);
            WorldServer.allContainersArr[tableCoordStr][1]['itemId']                               = null;
            WorldServer.allContainersArr[tableCoordStr][1]['itemCount']                            = null;

            WorldServer.updateContainerRedis(tableCoordStr, prevContainerArr);
            responseFn({
                'status':       'success',
                'containerArr': WorldServer.allContainersArr[tableCoordStr],
            });
        } else {
            responseFn({
                'status':       'error',
                'errorMessage': "Container not found",
            });
        }
    }

    selectItem(socket: SocketIO.Socket, itemToSelectIndex: number, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Currently paused"  }); return; }
        // @formatter:on

        this.selectedItemIndex = Math.max(Math.min(itemToSelectIndex, 8), 0);
        if (this.userName) {
            UtilsServer.redisClient.hset('player:' + this.userName, 'selectedItemIndex', this.selectedItemIndex.toString());
        }
        if (this.selectedItemIndex in this.hotbarItemsArr) {
            this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
            responseFn({
                'status':            'success',
                'selectedItemIndex': this.selectedItemIndex,
            });
        }
    }

    swapItem(socket: SocketIO.Socket, swapItemData: import('./HunkerInterfaces').SwapItemData, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                                                                                                                    { responseFn({'status': 'error', 'errorMessage': "Currently crafting"   }); return; }
        if (this.isGhost)                                                                                                                                                       { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"      }); return; }
        if (this.isPaused)                                                                                                                                                      { responseFn({'status': 'error', 'errorMessage': "Currently paused"     }); return; }
        let blockId;
        let swapItemContainerCoordStr;
        if ('swapItemContainerCoordArr' in swapItemData) {
            swapItemData['swapItemContainerCoordArr']['layerNum'] = this.currentLayerNum;
            const worldCell = WorldServer.worldArr[this.currentLayerNum][swapItemData['swapItemContainerCoordArr']['y']][swapItemData['swapItemContainerCoordArr']['x']];
            if (worldCell == null)                                                                                                                                              { responseFn({'status': 'error', 'errorMessage': "No block at destination"}); return; }
            swapItemContainerCoordStr = this.currentLayerNum + ':' + swapItemData['swapItemContainerCoordArr']['x'] + ',' + swapItemData['swapItemContainerCoordArr']['y'];
            if (!(swapItemContainerCoordStr in WorldServer.allContainersArr))                                                                                                   { responseFn({'status': 'error', 'errorMessage': "Container not found"  }); return; }
            blockId = Utils.tileIndexToItemIdArr[worldCell[0]];
            if (!Items.itemsArr[blockId]['canRemoveItems'])                                                                                                                     { responseFn({'status': 'error', 'errorMessage': "Can't take items from a " + Utils.lcfirst(Items.itemsArr[blockId]['label'])}); return; }
        }
        // @formatter:on

        let prevContainerArr;
        if (swapItemContainerCoordStr) {
            prevContainerArr = Utils.dereferenceObj(WorldServer.allContainersArr[swapItemContainerCoordStr]);
        }
        const prevHotbarArr         = Utils.dereferenceObj(this.hotbarItemsArr);
        const prevAccessoryItemsArr = Utils.dereferenceObj(this.accessoryItemsArr);
        let swapFromArr;
        let swapToArr;
        if (swapItemData['swapFromItemType'] == 'hotbar' && swapItemData['swapToItemType'] == 'hotbar') { // Hotbar -> hotbar.
            swapFromArr = this.hotbarItemsArr;
            swapToArr   = this.hotbarItemsArr;
        } else if (swapItemData['swapFromItemType'] == 'hotbar' && swapItemData['swapToItemType'] == 'container') { // Hotbar -> container.
            swapFromArr = this.hotbarItemsArr;
            swapToArr   = WorldServer.allContainersArr[swapItemContainerCoordStr];
        } else if (swapItemData['swapFromItemType'] == 'hotbar' && swapItemData['swapToItemType'] == 'accessories') { // Hotbar -> accessories.
            swapFromArr = this.hotbarItemsArr;
            swapToArr   = this.accessoryItemsArr;
        } else if (swapItemData['swapFromItemType'] == 'container' && swapItemData['swapToItemType'] == 'container') { // Container -> container.
            swapFromArr = WorldServer.allContainersArr[swapItemContainerCoordStr];
            swapToArr   = WorldServer.allContainersArr[swapItemContainerCoordStr];
        } else if (swapItemData['swapFromItemType'] == 'container' && swapItemData['swapToItemType'] == 'hotbar') { // Container -> hotbar.
            swapFromArr = WorldServer.allContainersArr[swapItemContainerCoordStr];
            swapToArr   = this.hotbarItemsArr;
        } else if (swapItemData['swapFromItemType'] == 'container' && swapItemData['swapToItemType'] == 'accessories') { // Container -> accessories.
            swapFromArr = WorldServer.allContainersArr[swapItemContainerCoordStr];
            swapToArr   = this.accessoryItemsArr;
        } else if (swapItemData['swapFromItemType'] == 'accessories' && swapItemData['swapToItemType'] == 'accessories') { // Accessories -> accessories.
            swapFromArr = this.accessoryItemsArr;
            swapToArr   = this.accessoryItemsArr;
        } else if (swapItemData['swapFromItemType'] == 'accessories' && swapItemData['swapToItemType'] == 'hotbar') { // Accessories -> hotbar.
            swapFromArr = this.accessoryItemsArr;
            swapToArr   = this.hotbarItemsArr;
        } else if (swapItemData['swapFromItemType'] == 'accessories' && swapItemData['swapToItemType'] == 'container') { // Accessories -> container.
            swapFromArr = this.accessoryItemsArr;
            swapToArr   = WorldServer.allContainersArr[swapItemContainerCoordStr];
        }

        if (swapFromArr && swapToArr) {
            const swapFromItem = swapFromArr[swapItemData['swapFromItemIndex']];
            const swapToItem   = swapToArr[swapItemData['swapToItemIndex']];

            if (
                (swapItemData['swapToItemType'] == 'accessories' && swapFromItem['itemId'] && (!Items.itemsArr[swapFromItem['itemId']]['accessoriesSlotIndexesArr'] || Items.itemsArr[swapFromItem['itemId']]['accessoriesSlotIndexesArr'].indexOf(swapItemData['swapToItemIndex']) == -1))
                ||
                (swapItemData['swapFromItemType'] == 'accessories' && swapToItem['itemId'] && (!Items.itemsArr[swapToItem['itemId']]['accessoriesSlotIndexesArr'] || Items.itemsArr[swapToItem['itemId']]['accessoriesSlotIndexesArr'].indexOf(swapItemData['swapFromItemIndex']) == -1))
            ) {
                responseFn({
                    'status':            'error',
                    'errorMessage':      "Wrong slot",
                    'hotbarItemsArr':    this.hotbarItemsArr,
                    'accessoryItemsArr': this.accessoryItemsArr,
                });
                return;
            }
            if (
                (
                    blockId == 'hearth'
                    ||
                    blockId == 'hearth_lit'
                )
                &&
                (
                    (
                        swapItemData['swapToItemType'] == 'container'
                        &&
                        swapFromItem['itemId']
                        &&
                        (
                            (swapItemData['swapToItemIndex'] == 0 && !Items.itemsArr[swapFromItem['itemId']]['burnTimeSecs'])
                            ||
                            (swapItemData['swapToItemIndex'] == 1 && !Items.itemsArr[swapFromItem['itemId']]['smeltingOutputItemId'])
                        )
                    )
                    ||
                    (
                        swapItemData['swapFromItemType'] == 'container'
                        &&
                        swapToItem['itemId']
                        &&
                        (
                            (swapItemData['swapFromItemIndex'] == 0 && !Items.itemsArr[swapToItem['itemId']]['burnTimeSecs'])
                            ||
                            (swapItemData['swapFromItemIndex'] == 1 && !Items.itemsArr[swapToItem['itemId']]['smeltingOutputItemId'])
                        )
                    )
                )
            ) {
                responseFn({
                    'status':         'error',
                    'errorMessage':   "Wrong slot",
                    'hotbarItemsArr': this.hotbarItemsArr,
                    'containerArr':   WorldServer.allContainersArr[swapItemContainerCoordStr],
                });
                return;
            }

            if (
                blockId == 'windmill'
                &&
                (
                    (
                        swapItemData['swapToItemType'] == 'container'
                        &&
                        swapFromItem['itemId']
                        &&
                        (
                            (swapItemData['swapToItemIndex'] == 0 && swapFromItem['itemId'] != 'wheat')
                            ||
                            (swapItemData['swapToItemIndex'] == 1 && swapFromItem['itemId'] != 'flour')
                        )
                    )
                )
            ) {
                responseFn({
                    'status':         'error',
                    'errorMessage':   "Wrong slot",
                    'hotbarItemsArr': this.hotbarItemsArr,
                    'containerArr':   WorldServer.allContainersArr[swapItemContainerCoordStr],
                });
                return;
            }

            if (swapFromItem['itemId'] == swapToItem['itemId'] && Items.itemsArr[swapFromItem['itemId']] && Items.itemsArr[swapFromItem['itemId']]['isStackable']) {
                // Combine items.
                swapToArr[swapItemData['swapToItemIndex']]['itemCount'] += swapFromItem['itemCount'];
                swapFromArr[swapItemData['swapFromItemIndex']] = {'itemId': null, 'itemCount': null};
            } else {
                swapToArr[swapItemData['swapToItemIndex']]     = swapFromItem;
                swapFromArr[swapItemData['swapFromItemIndex']] = swapToItem;
            }
        } else {
            responseFn({
                'status':            'error',
                'errorMessage':      "Invalid swap",
                'hotbarItemsArr':    this.hotbarItemsArr,
                'accessoryItemsArr': this.accessoryItemsArr,
            });
            return;
        }

        const responseData: ServerResponse = {
            'status': 'success',
        };
        if (prevContainerArr && prevContainerArr != WorldServer.allContainersArr[swapItemContainerCoordStr]) {
            if (Items.itemsArr[blockId]['isRemovingContainerIfEmpty']) {
                const blockUpdateDataArr = this.removeContainerIfEmpty(blockId, swapItemContainerCoordStr, swapItemData['swapItemContainerCoordArr']);
                if (blockUpdateDataArr) {
                    responseData['blockUpdateDataArr'] = blockUpdateDataArr;
                    socket.broadcast.emit('blockUpdate', responseData['blockUpdateDataArr']); // Emit to all other players.
                }
            }
            if (!responseData['blockUpdateDataArr']) {
                WorldServer.updateContainerRedis(swapItemContainerCoordStr, prevContainerArr);
            }
            responseData['containerArr'] = WorldServer.allContainersArr[swapItemContainerCoordStr];
        }
        if (prevHotbarArr != this.hotbarItemsArr) {
            this.updateHotbarRedis(prevHotbarArr);
            const prevSelectedItemId = this.selectedItemId;
            if (this.selectedItemIndex in this.hotbarItemsArr) {
                this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
            } else {
                this.selectedItemId = null;
            }
            if (prevSelectedItemId != this.selectedItemId) {
                socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
            }
            responseData['hotbarItemsArr'] = this.hotbarItemsArr;
        }
        if (prevAccessoryItemsArr != this.accessoryItemsArr) {
            this.updateAccessoriesRedis(prevAccessoryItemsArr);
            responseData['accessoryItemsArr'] = this.accessoryItemsArr;
        }
        responseFn(responseData);
    }

    dropItem(socket: SocketIO.Socket, dropItemData: import('./HunkerInterfaces').DropItemData, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                                                                         { responseFn({'status': 'error', 'errorMessage': "Currently crafting"           }); return; }
        if (this.isGhost)                                                                                                            { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"              }); return; }
        if (this.isPaused)                                                                                                           { responseFn({'status': 'error', 'errorMessage': "Currently paused"             }); return; }
        if (dropItemData['itemRowType'] == 'accessories') {
            if (!(dropItemData['itemToDropIndex'] in this.accessoryItemsArr))                                                        { responseFn({'status': 'error', 'errorMessage': "Nothing to drop"              }); return; }
        } else {
            if (!(dropItemData['itemToDropIndex'] in this.hotbarItemsArr))                                                           { responseFn({'status': 'error', 'errorMessage': "Nothing to drop"              }); return; }
        }
        let itemToDropArr = Utils.dereferenceObj(this.hotbarItemsArr[dropItemData['itemToDropIndex']]);
        let itemToDropId = this.hotbarItemsArr[dropItemData['itemToDropIndex']]['itemId'];
        let itemCount = this.hotbarItemsArr[dropItemData['itemToDropIndex']]['itemCount'];
        if (dropItemData['itemRowType'] == 'accessories') {
            itemToDropArr = Utils.dereferenceObj(this.accessoryItemsArr[dropItemData['itemToDropIndex']]);
            itemToDropId = this.accessoryItemsArr[dropItemData['itemToDropIndex']]['itemId'];
            itemCount = this.accessoryItemsArr[dropItemData['itemToDropIndex']]['itemCount'];
        }
        if (itemToDropId == null || itemCount == null)                                                                               { responseFn({'status': 'error', 'errorMessage': "Nothing to drop"              }); return; }
        // @formatter:on
        dropItemData['layerNum'] = this.currentLayerNum;
        const dropItemCoordStr   = dropItemData['layerNum'] + ':' + dropItemData['x'] + ',' + dropItemData['y'];
        let newItemIndex         = null;
        let blockId              = null;
        if (WorldServer.worldArr[this.currentLayerNum][dropItemData['y']][dropItemData['x']] != null) {
            blockId = Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][dropItemData['y']][dropItemData['x']][0]];
            // @formatter:off
            if (!Items.itemsArr[blockId]['isContainer'])                                                                             { responseFn({'status': 'error', 'errorMessage': "Block isn't a container"      }); return; }
            if (!(dropItemCoordStr in WorldServer.allContainersArr))                                                                       { responseFn({'status': 'error', 'errorMessage': "Block isn't a container"      }); return; }
            // @formatter:on
            if (blockId == 'hearth') {
                if (itemToDropId == 'coal') {
                    newItemIndex = 0;
                } else if (
                    Items.itemsArr[itemToDropId]['smeltingOutputItemId'] // Item can be used as smelting input, and,
                    &&
                    (
                        itemToDropId == WorldServer.allContainersArr[dropItemCoordStr][1]['itemId'] // Same item in input slot.
                        ||
                        (
                            !WorldServer.allContainersArr[dropItemCoordStr][1]['itemId'] // Nothing in input slot, and,
                            &&
                            (
                                !WorldServer.allContainersArr[dropItemCoordStr][2]['itemId'] // Nothing in output slot, or,
                                ||
                                WorldServer.allContainersArr[dropItemCoordStr][2]['itemId'] == Items.itemsArr[itemToDropId]['smeltingOutputItemId'] // Dropped item's smelting output is the same as the item in the output slot.
                            )
                        )
                    )
                ) {
                    newItemIndex = 1;
                } else {
                    // @formatter:off
                    responseFn({'status': 'error', 'errorMessage': "Wrong item type"              }); return;
                    // @formatter:on
                }
            } else if (blockId == 'windmill') {
                if (itemToDropId == 'wheat') {
                    newItemIndex = 0;
                } else if (itemToDropId == 'flour') {
                    newItemIndex = 1;
                } else {
                    // @formatter:off
                    responseFn({'status': 'error', 'errorMessage': "Wrong item type"              }); return;
                    // @formatter:on
                }
            } else {
                newItemIndex = Utils.getNewItemIndex(WorldServer.allContainersArr[dropItemCoordStr], itemToDropId);
                if (newItemIndex == null && !Items.itemsArr[blockId]['maxItemsInContainerNum']) {
                    WorldServer.allContainersArr[dropItemCoordStr].push({'itemId': null, 'itemCount': null});
                    newItemIndex = Utils.getNewItemIndex(WorldServer.allContainersArr[dropItemCoordStr], itemToDropId);
                }
            }
            // @formatter:off
            if (newItemIndex == null)                                                                                                { responseFn({'status': 'error', 'errorMessage': "Container full"               }); return; }
            // @formatter:on
        } else {
            newItemIndex = 0;
            // @formatter:off
            if (UtilsServer.isAnyPlayerAtBlock(dropItemData))                                                                        { responseFn({'status': 'error', 'errorMessage': "Can't place on top of players"}); return; }
            // @formatter:on
        }

        let numItemsToDrop = 1;
        if (dropItemData['isDroppingWholeStack']) {
            numItemsToDrop = itemCount;
        } else {
            itemToDropArr['itemCount'] = 1;
        }
        let blockUpdateDataArr: BlockUpdateData = {
            'blockCoordArr': {
                'layerNum': dropItemData['layerNum'],
                'x':        dropItemData['x'],
                'y':        dropItemData['y'],
            },
        };
        let prevContainerArr                    = null;
        if (dropItemCoordStr in WorldServer.allContainersArr) { // Drop into existing container.
            prevContainerArr              = Utils.dereferenceObj(WorldServer.allContainersArr[dropItemCoordStr]);
            blockUpdateDataArr['blockId'] = blockId;
        } else {
            blockUpdateDataArr['blockId']                                                    = 'crate';
            WorldServer.worldArr[this.currentLayerNum][dropItemData['y']][dropItemData['x']] = [Utils.randElFromArr(Items.itemsArr[blockUpdateDataArr['blockId']]['tileIndexesArr']), Items.itemsArr[blockUpdateDataArr['blockId']]['structureDurability']];
            WorldServer.updateSquareRedis(dropItemData);
            blockUpdateDataArr['blockDataArr']             = WorldServer.worldArr[this.currentLayerNum][dropItemData['y']][dropItemData['x']];
            WorldServer.allContainersArr[dropItemCoordStr] = [];
            for (let i = 0; i < (Items.itemsArr[blockUpdateDataArr['blockId']]['maxItemsInContainerNum'] || 1); i++) {
                WorldServer.allContainersArr[dropItemCoordStr].push({'itemId': null, 'itemCount': null});
            }
            Timer.addTimer('timeToLive', dropItemCoordStr, Utils.time() + (Items.itemsArr['crate']['timeToLiveSecs'] * 1000));
        }
        WorldServer.allContainersArr[dropItemCoordStr] = HotbarServer.addItemsAtIndex(WorldServer.allContainersArr[dropItemCoordStr], itemToDropArr, newItemIndex);
        WorldServer.updateContainerRedis(dropItemCoordStr, prevContainerArr);
        if (dropItemData['itemRowType'] == 'accessories') {
            const prevAccessoryItemsArr = Utils.dereferenceObj(this.accessoryItemsArr);
            this.accessoryItemsArr[dropItemData['itemToDropIndex']]['itemCount'] -= numItemsToDrop;
            if (this.accessoryItemsArr[dropItemData['itemToDropIndex']]['itemCount'] <= 0) {
                this.accessoryItemsArr[dropItemData['itemToDropIndex']]['itemId']    = null;
                this.accessoryItemsArr[dropItemData['itemToDropIndex']]['itemCount'] = null;
            }
            this.updateAccessoriesRedis(prevAccessoryItemsArr);
        } else {
            const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
            this.hotbarItemsArr[dropItemData['itemToDropIndex']]['itemCount'] -= numItemsToDrop;
            if (this.hotbarItemsArr[dropItemData['itemToDropIndex']]['itemCount'] <= 0) {
                this.hotbarItemsArr[dropItemData['itemToDropIndex']]['itemId']    = null;
                this.hotbarItemsArr[dropItemData['itemToDropIndex']]['itemCount'] = null;
            }
            this.updateHotbarRedis(prevHotbarArr);
            const prevSelectedItemId = this.selectedItemId;
            this.selectedItemId      = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
            if (prevSelectedItemId != this.selectedItemId) {
                socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
            }
        }
        responseFn({
            'status':             'success',
            'hotbarItemsArr':     this.hotbarItemsArr,
            'accessoryItemsArr':  this.accessoryItemsArr,
            'blockUpdateDataArr': blockUpdateDataArr,
        });
        socket.broadcast.emit('blockUpdate', blockUpdateDataArr); // Emit to all other players.
    }

    takeItem(socket: SocketIO.Socket, takeItemData: import('./HunkerInterfaces').TakeItemData, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                                                { responseFn({'status': 'error', 'errorMessage': "Currently crafting"     }); return; }
        if (this.isGhost)                                                                                   { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"        }); return; }
        if (this.isPaused)                                                                                  { responseFn({'status': 'error', 'errorMessage': "Currently paused"       }); return; }
        takeItemData['layerNum'] = this.currentLayerNum;
        const takeItemCoordStr = takeItemData['layerNum'] + ':' + takeItemData['x'] + ',' + takeItemData['y'];
        if (WorldServer.worldArr[this.currentLayerNum][takeItemData['y']][takeItemData['x']] == null)       { responseFn({'status': 'error', 'errorMessage': "No block at destination"}); return; }
        const blockId = Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][takeItemData['y']][takeItemData['x']][0]];
        if (!Items.itemsArr[blockId]['isContainer'])                                                        { responseFn({'status': 'error', 'errorMessage': Items.itemsArr[blockId]['label'] + " isn't a container"}); return; }
        if (!Items.itemsArr[blockId]['canRemoveItems'])                                                     { responseFn({'status': 'error', 'errorMessage': "Can't take items from a " + Utils.lcfirst(Items.itemsArr[blockId]['label'])}); return; }
        if (!(takeItemCoordStr in WorldServer.allContainersArr))                                            { responseFn({'status': 'error', 'errorMessage': Items.itemsArr[blockId]['label'] + " isn't a container"}); return; }
        // @formatter:on
        let takeItemIndex = null;
        let takeItemArr   = null;
        for (let i = WorldServer.allContainersArr[takeItemCoordStr].length - 1; i >= 0; i--) { // Count backwards to get the last item in the container.
            const containerItemArr = WorldServer.allContainersArr[takeItemCoordStr][i];
            if (containerItemArr['itemId'] && containerItemArr['itemCount']) {
                takeItemIndex = i;
                takeItemArr   = containerItemArr;
                if (!takeItemData['isTakingWholeStack']) {
                    takeItemArr['itemCount'] = 1;
                }
                break;
            }
        }
        takeItemArr = Utils.dereferenceObj(takeItemArr);
        // @formatter:off
        if (takeItemArr == null)                                                                            { responseFn({'status': 'error', 'errorMessage': Items.itemsArr[blockId]['label'] + " is empty"}); return; }
        const newItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, takeItemArr['itemId']);
        if (newItemIndex == null)                                                                           { responseFn({'status': 'error', 'errorMessage': "Hotbar full"            }); return; }
        // @formatter:on

        // Add items to hotbar.
        const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
        this.hotbarItemsArr = HotbarServer.addItemsAtIndex(this.hotbarItemsArr, takeItemArr, newItemIndex);
        this.updateHotbarRedis(prevHotbarArr);
        const prevSelectedItemId = this.selectedItemId;
        this.selectedItemId      = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];

        // Remove items from container.
        const prevContainerArr                         = Utils.dereferenceObj(WorldServer.allContainersArr[takeItemCoordStr]);
        WorldServer.allContainersArr[takeItemCoordStr] = HotbarServer.removeItemsAtIndex(WorldServer.allContainersArr[takeItemCoordStr], takeItemIndex, takeItemArr['itemCount']);
        WorldServer.updateContainerRedis(takeItemCoordStr, prevContainerArr);

        let blockUpdateDataArr;
        if (Items.itemsArr[blockId]['isRemovingContainerIfEmpty']) {
            blockUpdateDataArr = this.removeContainerIfEmpty(blockId, takeItemCoordStr, takeItemData);
        }
        const responseData: ServerResponse = {
            'status':         'success',
            'hotbarItemsArr': this.hotbarItemsArr,
        };
        if (blockUpdateDataArr) {
            socket.broadcast.emit('blockUpdate', blockUpdateDataArr); // Emit to all other players.
            responseData['blockUpdateDataArr'] = blockUpdateDataArr;
        }
        responseFn(responseData);
        if (prevSelectedItemId != this.selectedItemId) {
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
        }
    }

    removeContainerIfEmpty(blockId: string, blockCoordStr: string, blockCoordArr: Coord): { blockCoordArr: Coord, blockId: string, blockDataArr: WorldCell } {
        let isContainerEmpty = true;
        for (let slotIndex in WorldServer.allContainersArr[blockCoordStr]) {
            if (WorldServer.allContainersArr[blockCoordStr][slotIndex]['itemId'] && WorldServer.allContainersArr[blockCoordStr][slotIndex]['itemCount']) {
                isContainerEmpty = false;
                break;
            }
        }
        if (isContainerEmpty) {
            WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']] = null;
            WorldServer.updateSquareRedis(blockCoordArr);
            const prevContainerArr = Utils.dereferenceObj(WorldServer.allContainersArr[blockCoordStr]);
            delete WorldServer.allContainersArr[blockCoordStr];
            WorldServer.updateContainerRedis(blockCoordStr, prevContainerArr);
            return {
                'blockCoordArr': blockCoordArr,
                'blockId':       blockId,
                'blockDataArr':  WorldServer.worldArr[this.currentLayerNum][blockCoordArr['y']][blockCoordArr['x']],
            };
        }
        return null;
    }

    startCraftingItem(socket: SocketIO.Socket, itemId: ItemId, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting)                                                                                                   { responseFn({'status': 'error', 'errorMessage': "Already crafting"          }); return; }
        if (this.isGhost)                                                                                                      { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"           }); return; }
        if (this.isPaused)                                                                                                     { responseFn({'status': 'error', 'errorMessage': "Currently paused"          }); return; }
        if (!(itemId in Items.itemsArr))                                                                                       { responseFn({'status': 'error', 'errorMessage': "Invalid item"              }); return; }
        if (!Items.itemsArr[itemId]['isObtainable'])                                                                           { responseFn({'status': 'error', 'errorMessage': "Item not obtainable"       }); return; }
        const hasInfiniteBlocks = Utils.playerGameModesArr[this.gameModeStr]['hasInfiniteBlocks'];
        if (!hasInfiniteBlocks && !Utils.hasRequiredItemsForCrafting(this.hotbarItemsArr, Items.itemsArr[itemId]['requiredItemsForCraftingArr']))    { responseFn({'status': 'error', 'errorMessage': "Missing items for crafting"}); return; }
        const newItemIndex = Utils.getNewItemIndex(this.hotbarItemsArr, itemId);
        if (newItemIndex == null)                                                                                              { responseFn({'status': 'error', 'errorMessage': "Hotbar full"               }); return; }
        // @formatter:on
        if (!hasInfiniteBlocks) {
            let requiredBlockForCraftingCoordsArr: { [blockCoordStr: string]: Coord };
            if (Items.itemsArr[itemId]['requiredBlockForCraftingItemId'] == 'workbench') {
                requiredBlockForCraftingCoordsArr = WorldServer.allWorkbenchCoords;
            } else if (Items.itemsArr[itemId]['requiredBlockForCraftingItemId'] == 'fire') {
                requiredBlockForCraftingCoordsArr = WorldServer.allHeaterCoords;
            }
            if (requiredBlockForCraftingCoordsArr && !UtilsServer.isNearBlock({'x': this.x, 'y': this.y}, requiredBlockForCraftingCoordsArr)) {
                responseFn({'status': 'error', 'errorMessage': "Need to be near a " + Items.itemsArr[itemId]['requiredBlockForCraftingItemId']});
                return;
            }
        }

        const prevSelectedItemId = this.selectedItemId;
        if (!hasInfiniteBlocks) {
            const prevHotbarArr = Utils.dereferenceObj(this.hotbarItemsArr);
            this.hotbarItemsArr = HotbarServer.removeItemsFromArr(this.hotbarItemsArr, Items.itemsArr[itemId]['requiredItemsForCraftingArr']);
            this.updateHotbarRedis(prevHotbarArr);
            this.selectedItemId = this.hotbarItemsArr[this.selectedItemIndex]['itemId'];
        }
        this.isCrafting              = true;
        this.craftingItemId          = itemId;
        const itemCraftingTimeMillis = Utils.itemCraftingTimeMillis(itemId);
        const _this                  = this;
        this.craftingTimeout         = setTimeout(function () {
            _this.isCrafting      = false;
            _this.craftingItemId  = null;
            _this.craftingTimeout = null;
            const prevHotbarArr   = Utils.dereferenceObj(_this.hotbarItemsArr);
            let craftedItemsNum   = Items.itemsArr[itemId]['craftedItemsNum'];
            if (Utils.playerGameModesArr[_this.gameModeStr]['hasInfiniteBlocks']) {
                craftedItemsNum = 1;
            }
            _this.hotbarItemsArr = HotbarServer.addItemsAtIndex(_this.hotbarItemsArr, {'itemId': itemId, 'itemCount': craftedItemsNum}, newItemIndex);
            _this.updateHotbarRedis(prevHotbarArr);
            _this.selectedItemId = _this.hotbarItemsArr[_this.selectedItemIndex]['itemId'];
            if (Items.itemsArr[itemId]['pointsGivenOnCraft']) {
                _this.playerScore += Items.itemsArr[itemId]['pointsGivenOnCraft'];
                if (_this.userName) {
                    UtilsServer.redisClient.hincrby('player:' + _this.userName, 'playerScore', Items.itemsArr[itemId]['pointsGivenOnCraft']);
                    UtilsServer.redisClient.hincrby('user:' + _this.userName, 'userScore', Items.itemsArr[itemId]['pointsGivenOnCraft']);
                }
            }
            UtilsServer.io.to(_this.id).emit('finishCraftingItem', { // Emit to specific player.
                'status':         'success',
                'hotbarItemsArr': _this.hotbarItemsArr,
                'playerScore':    _this.playerScore,
            });
            socket.broadcast.emit('isCrafting', _this.id, {'isCrafting': _this.isCrafting, 'craftingItemId': _this.craftingItemId}); // Emit to all other players.
        }, itemCraftingTimeMillis);
        responseFn({
            'status':                 'success',
            'itemCraftingTimeMillis': itemCraftingTimeMillis,
            'hotbarItemsArr':         this.hotbarItemsArr,
        });
        if (prevSelectedItemId != this.selectedItemId) {
            socket.broadcast.emit('selectItem', this.id, this.selectedItemId); // Emit to all other players.
        }
        socket.broadcast.emit('isCrafting', this.id, {'isCrafting': this.isCrafting, 'craftingItemId': this.craftingItemId}); // Emit to all other players.
    }

    playGame(socket: SocketIO.Socket, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (!this.isPaused)  { responseFn({'status': 'error', 'errorMessage': "Not paused"        }); return; }
        // @formatter:on
        this.isPaused = false;
        socket.broadcast.emit('isPaused', this.id, this.isPaused); // Emit to all other players.
        responseFn({
            'status':   'success',
            'isPaused': this.isPaused,
        });
    }

    pauseGame(socket: SocketIO.Socket, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        if (this.isPaused)   { responseFn({'status': 'error', 'errorMessage': "Already paused"    }); return; }
        // @formatter:on
        this.isPaused = true;
        socket.broadcast.emit('isPaused', this.id, this.isPaused); // Emit to all other players.
        responseFn({
            'status':   'success',
            'isPaused': this.isPaused,
        });
    }

    changeSetting(settingName: 'gameMode' | 'difficulty', settingKey: GameModeStr | DifficultyStr, responseFn: (response: ServerResponse) => void): void {
        // @formatter:off
        if (this.isCrafting) { responseFn({'status': 'error', 'errorMessage': "Currently crafting"}); return; }
        if (this.isGhost)    { responseFn({'status': 'error', 'errorMessage': "You're a ghost!"   }); return; }
        // @formatter:on
        if (settingName != 'gameMode' && settingName != 'difficulty') {
            responseFn({'status': 'error', 'errorMessage': "Invalid setting"});
            return;
        }
        if (settingName == 'gameMode' && !(settingKey in Utils.playerGameModesArr)) {
            responseFn({'status': 'error', 'errorMessage': "Invalid setting"});
            return;
        } else if (settingName == 'difficulty' && !(settingKey in Utils.playerDifficultiesArr)) {
            responseFn({'status': 'error', 'errorMessage': "Invalid setting"});
            return;
        }
        if (settingName == 'gameMode') {
            this.gameModeStr = settingKey;
        } else {
            this.difficultyStr = settingKey;
        }
        responseFn({'status': 'success'});
    }

    hasRingEffect(itemId: ItemId): boolean {
        if (this.selectedItemId == itemId) {
            return true;
        }
        for (let i in Items.itemsArr[itemId]['accessoriesSlotIndexesArr']) {
            const slotIndex = Items.itemsArr[itemId]['accessoriesSlotIndexesArr'][i];
            if (this.accessoryItemsArr[slotIndex]['itemId'] == itemId) {
                return true;
            }
        }
        return false;
    }

    movement(socket: SocketIO.Socket, movementData: MovementData): void {
        movementData['x']     = Math.round(movementData['x'] * 10) / 10;
        movementData['y']     = Math.round(movementData['y'] * 10) / 10;
        movementData['angle'] = Math.round(movementData['angle'] * 10) / 10;
        if (this.ridingEntityId) {
            if (this.ridingEntityId in EntityServer.allEntityObjs) {
                EntityServer.allEntityObjs[this.ridingEntityId].x     = this.x;
                EntityServer.allEntityObjs[this.ridingEntityId].y     = this.y;
                EntityServer.allEntityObjs[this.ridingEntityId].angle = movementData['ridingEntityAngle'];
            } else {
                delete this.ridingEntityId;
            }
        }
        movementData['ridingEntityId'] = this.ridingEntityId;
        if (movementData['isSprinting']) {
            this.hasSprintedInLastTick = true;
        }
        this.currentSaveCount++;
        if (this.currentSaveCount >= this.nextSaveCount) {
            if (this.isGhost && (this.x != movementData['x'] || this.y != movementData['y'])) {
                const newXRounded = Math.floor(movementData['x'] / Utils.tileSize);
                const newYRounded = Math.floor(movementData['y'] / Utils.tileSize);
                if (WorldServer.worldArr[this.currentLayerNum][newYRounded][newXRounded]) {
                    const worldIndex  = WorldServer.worldArr[this.currentLayerNum][newYRounded][newXRounded][0];
                    const worldItemId = Utils.tileIndexToItemIdArr[worldIndex];
                    if (
                        worldItemId == 'spawn'
                        ||
                        (
                            worldItemId == 'bed'
                            &&
                            (
                                (
                                    this.userName
                                    &&
                                    WorldServer.worldArr[this.currentLayerNum][newYRounded][newXRounded][2] == this.userName
                                )
                                ||
                                (
                                    !this.userName
                                    &&
                                    WorldServer.worldArr[this.currentLayerNum][newYRounded][newXRounded][2] == this.id
                                )
                            )
                        )
                    ) {
                        this.revive();
                    }
                }
            }
            if (this.lastMovementIsGhost != this.isGhost) {
                movementData['isGhost']  = this.isGhost;
                this.lastMovementIsGhost = this.isGhost;
            }
            if (this.hasRingEffect('ring_flowers')) {
                if (this.x != movementData['x'] || this.y != movementData['y']) {
                    const newXRounded = Math.floor(movementData['x'] / Utils.tileSize);
                    const newYRounded = Math.floor(movementData['y'] / Utils.tileSize);
                    if (!WorldServer.worldArr[this.currentLayerNum][newYRounded][newXRounded]) {
                        const floorIndex  = WorldServer.floorArr[this.currentLayerNum][newYRounded][newXRounded][0];
                        const floorItemId = Utils.tileIndexToItemIdArr[floorIndex];
                        if (floorItemId == 'grass') {
                            const oldFloorCell                                                   = WorldServer.floorArr[this.currentLayerNum][newYRounded][newXRounded];
                            WorldServer.floorArr[this.currentLayerNum][newYRounded][newXRounded] = [Utils.randElFromArr(Items.itemsArr['grass_flowers']['tileIndexesArr']), Items.itemsArr['ring_flowers']['structureDurability']];
                            UtilsServer.io.emit('floorUpdate', {
                                'floorCoordArr': {
                                    'layerNum': this.currentLayerNum,
                                    'x':        newXRounded,
                                    'y':        newYRounded,
                                },
                                'floorDataArr':  WorldServer.floorArr[this.currentLayerNum][newYRounded][newXRounded],
                            });
                            const _this = this;
                            setTimeout(function () {
                                WorldServer.floorArr[_this.currentLayerNum][newYRounded][newXRounded] = oldFloorCell;
                                UtilsServer.io.emit('floorUpdate', {
                                    'floorCoordArr': {
                                        'layerNum': _this.currentLayerNum,
                                        'x':        newXRounded,
                                        'y':        newYRounded,
                                    },
                                    'floorDataArr':  WorldServer.floorArr[_this.currentLayerNum][newYRounded][newXRounded],
                                });
                            }, 2000);
                        }
                    }
                }
            }
            if (this.userName) {
                if (this.x != movementData['x']) UtilsServer.redisClient.hset('player:' + this.userName, 'x', this.x.toString());
                if (this.y != movementData['y']) UtilsServer.redisClient.hset('player:' + this.userName, 'y', this.y.toString());
                this.nextSaveCount += 10;
            }
        }
        this.x     = movementData['x'];
        this.y     = movementData['y'];
        this.angle = movementData['angle'];
        socket.broadcast.emit('playerMovement', this.id, movementData); // Emit to all other players about the player that moved.
    }

    setCurrentLayerNum(newLayerNum: number, responseFn: (response: ServerResponse) => void): void {
        if (typeof newLayerNum != 'number' || isNaN(newLayerNum)) {
            responseFn({
                'status':       'error',
                'errorMessage': "\"" + newLayerNum + "\" isn't a number, try <code>/layer -1</code>",
            });
            return;
        }
        if (newLayerNum > Utils.startLayerNum || newLayerNum < Utils.endLayerNum) {
            responseFn({
                'status':       'error',
                'errorMessage': "Invalid layer, try something between " + Utils.startLayerNum + " and " + Utils.endLayerNum,
            });
            return;
        }
        this.currentLayerNum = newLayerNum;
        if (this.userName) {
            UtilsServer.redisClient.hset('player:' + this.userName, 'currentLayerNum', this.currentLayerNum.toString());
        }
        responseFn({'status': 'success'});
        UtilsServer.io.emit('playerCurrentLayerNum', this.id, this.currentLayerNum); // Emit to all players.
    }

    updateHealth(healthUpdate: number, isEmittingHealthUpdate: boolean, deathKey: string): boolean {
        if (
            !Utils.playerGameModesArr[this.gameModeStr]['hasHealth']
            ||
            this.isGhost
        ) {
            return false;
        }
        // Don't take damage on rails.
        const xRounded = Math.floor(this.x / Utils.tileSize);
        const yRounded = Math.floor(this.y / Utils.tileSize);
        if (WorldServer.worldArr[this.currentLayerNum][yRounded][xRounded]) {
            const worldItemId = Utils.tileIndexToItemIdArr[WorldServer.worldArr[this.currentLayerNum][yRounded][xRounded][0]];
            if (worldItemId == 'rail') {
                return false;
            }
        }

        if (healthUpdate < 0) {
            healthUpdate *= Utils.playerDifficultiesArr[this.difficultyStr]['difficultyMult'];
        }

        const responseData: ServerResponse = {
            'status': 'success',
        };
        if (
            healthUpdate < 0 // Is a health decrease, and,
            &&
            deathKey != 'hunger' // Isn't caused by hunger, and,
            &&
            deathKey != 'warmth' // Isn't caused by freezing, and,
            &&
            deathKey != 'lava' // Isn't caused by lava.
        ) {
            const prevAccessoryItemsArr   = Utils.dereferenceObj(this.accessoryItemsArr);
            let playerDamageReductionPcnt = 0;
            for (let i = 0; i < this.accessoryItemsArr.length; i++) {
                if (
                    this.accessoryItemsArr[i]['itemId'] != null
                    &&
                    this.accessoryItemsArr[i]['itemId'] in Items.itemsArr
                    &&
                    Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['damageReductionPcnt']
                ) {
                    playerDamageReductionPcnt += Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['damageReductionPcnt'];
                }
                if (this.decreaseAccessoryItemDurability(i, 1)) { // Item broke.
                    responseData['allSoundEffectKeysArr'] = ['break'];
                }
            }
            healthUpdate *= 1 - playerDamageReductionPcnt;
            healthUpdate = Math.round(healthUpdate * 10) / 10; // Round to 1dp.
            if (prevAccessoryItemsArr != this.accessoryItemsArr) {
                this.updateAccessoriesRedis(prevAccessoryItemsArr);
                responseData['accessoryItemsArr'] = this.accessoryItemsArr;
            }
        }

        this.health += healthUpdate;
        this.health = Math.max(this.health, 0);
        this.health = Math.min(this.health, this.maxHealth);
        if (this.health <= 0) {
            this.kill(deathKey);
            return true;
        } else {
            if (this.userName) {
                UtilsServer.redisClient.hset('player:' + this.userName, 'health', this.health.toString());
            }
            if (isEmittingHealthUpdate) {
                responseData['statBarsDataArr'] = {'health': this.health};
                UtilsServer.io.to(this.id).emit('updateStatBars', responseData); // Emit to specific player.
            }
            return false;
        }
    }

    kill(deathKey: string): void {
        let deathSuffixText = "died";
        if (deathKey in PlayerServer.deathSuffixTextArr) {
            deathSuffixText = PlayerServer.deathSuffixTextArr[deathKey];
        }

        let blockUpdateDataArr: BlockUpdateData = null;
        // Add death crate if:
        //     - Didn't have an account, and,
        //     - Disconnected.
        if (!this.userName && deathKey == 'disconnected') {
            // Add death crate.
            const playerXRounded   = Math.floor(this.x / Utils.tileSize);
            const playerYRounded   = Math.floor(this.y / Utils.tileSize);
            const containerDetails = [];
            for (const slotIndex in this.hotbarItemsArr) {
                if (
                    this.hotbarItemsArr[slotIndex]
                    &&
                    'itemId' in this.hotbarItemsArr[slotIndex]
                    &&
                    'itemCount' in this.hotbarItemsArr[slotIndex]
                    &&
                    this.hotbarItemsArr[slotIndex]['itemId']
                    &&
                    this.hotbarItemsArr[slotIndex]['itemCount']
                ) {
                    containerDetails.push(this.hotbarItemsArr[slotIndex]);
                }
            }
            for (const slotIndex in this.accessoryItemsArr) {
                if (
                    this.accessoryItemsArr[slotIndex]
                    &&
                    'itemId' in this.accessoryItemsArr[slotIndex]
                    &&
                    'itemCount' in this.accessoryItemsArr[slotIndex]
                    &&
                    this.accessoryItemsArr[slotIndex]['itemId']
                    &&
                    this.accessoryItemsArr[slotIndex]['itemCount']
                ) {
                    containerDetails.push(this.accessoryItemsArr[slotIndex]);
                }
            }
            // Add bag and backpack to death crate if had them when they died.
            if (this.hotbarItemsArr.length >= 9 + 9 + Items.itemsArr['bag']['additionalHotbarSlotsNum']) {
                containerDetails.push({'itemId': 'bag', 'itemCount': 1});
                if (this.hotbarItemsArr.length >= 9 + 9 + Items.itemsArr['backpack']['additionalHotbarSlotsNum']) {
                    containerDetails.push({'itemId': 'backpack', 'itemCount': 1});
                }
            }
            if (
                containerDetails.length
                &&
                (
                    !WorldServer.worldArr[this.currentLayerNum][playerYRounded][playerXRounded]
                    ||
                    Items.itemsArr['rail']['tileIndexesArr'].indexOf(WorldServer.worldArr[this.currentLayerNum][playerYRounded][playerXRounded][0]) == -1
                )
            ) { // Don't add a death crate if player had no items.
                WorldServer.worldArr[this.currentLayerNum][playerYRounded][playerXRounded] = [Utils.randElFromArr(Items.itemsArr['death_crate']['tileIndexesArr']), Items.itemsArr['death_crate']['structureDurability']];
                const coordArr                                                             = {
                    'layerNum': this.currentLayerNum,
                    'x':        playerXRounded,
                    'y':        playerYRounded,
                };
                WorldServer.updateSquareRedis(coordArr);
                blockUpdateDataArr                           = {
                    'blockCoordArr': coordArr,
                    'blockId':       'death_crate',
                    'blockDataArr':  WorldServer.worldArr[this.currentLayerNum][playerYRounded][playerXRounded],
                };
                const playerCoordStr                         = this.currentLayerNum + ':' + playerXRounded + ',' + playerYRounded;
                WorldServer.allContainersArr[playerCoordStr] = Utils.dereferenceObj(containerDetails);
                WorldServer.updateContainerRedis(playerCoordStr, null);
                UtilsServer.io.emit('blockUpdate', blockUpdateDataArr); // Emit to all players.
                const _this = this;
                setTimeout(function () {
                    if (WorldServer.worldArr[_this.currentLayerNum][playerYRounded][playerXRounded] != null && playerCoordStr in WorldServer.allContainersArr) {
                        // Destroy death crate.
                        WorldServer.worldArr[_this.currentLayerNum][playerYRounded][playerXRounded] = null;
                        WorldServer.updateSquareRedis(coordArr);
                        blockUpdateDataArr['blockDataArr'] = null;
                        const prevContainerArr             = Utils.dereferenceObj(WorldServer.allContainersArr[playerCoordStr]);
                        delete WorldServer.allContainersArr[playerCoordStr];
                        WorldServer.updateContainerRedis(playerCoordStr, prevContainerArr);
                        UtilsServer.io.emit('blockUpdate', blockUpdateDataArr); // Emit to all players.
                    }
                }, Items.itemsArr[blockUpdateDataArr['blockId']]['timeToLiveSecs'] * 1000);
            }
        }

        // Dismount entity.
        if (this.ridingEntityId) {
            if (this.ridingEntityId in EntityServer.allEntityObjs) {
                EntityServer.allEntityObjs[this.ridingEntityId].isRiding       = false;
                EntityServer.allEntityObjs[this.ridingEntityId].ridingPlayerId = null;
            }
            this.ridingEntityId = null;
            UtilsServer.io.to(this.id).emit('handleMiscEmit', {
                'status':         'success',
                'ridingEntityId': this.ridingEntityId,
            }); // Emit to specific player.
        }

        // Emit to all players to disconnect this player.
        const responseData = {
            'playerId':        this.id,
            'deathSuffixText': deathSuffixText,
            'deathKey':        deathKey,
        };
        if (deathKey != 'disconnected') {
            const prevHotbarArr              = Utils.dereferenceObj(this.hotbarItemsArr);
            const deathDurabilityDecreaseNum = 50;
            for (let slotIndex = 0; slotIndex < 9; slotIndex++) {
                if (
                    this.hotbarItemsArr[slotIndex]
                    &&
                    'itemId' in this.hotbarItemsArr[slotIndex]
                    &&
                    'itemCount' in this.hotbarItemsArr[slotIndex]
                    &&
                    'itemDurability' in this.hotbarItemsArr[slotIndex]
                    &&
                    this.hotbarItemsArr[slotIndex]['itemId']
                    &&
                    this.hotbarItemsArr[slotIndex]['itemCount']
                    &&
                    this.hotbarItemsArr[slotIndex]['itemDurability']
                ) {
                    this.hotbarItemsArr[slotIndex]['itemDurability'] = Math.max(this.hotbarItemsArr[slotIndex]['itemDurability'] - deathDurabilityDecreaseNum, 1);
                }
            }
            for (const slotIndex in this.accessoryItemsArr) {
                if (
                    this.accessoryItemsArr[slotIndex]
                    &&
                    'itemId' in this.accessoryItemsArr[slotIndex]
                    &&
                    'itemCount' in this.accessoryItemsArr[slotIndex]
                    &&
                    'itemDurability' in this.accessoryItemsArr[slotIndex]
                    &&
                    this.accessoryItemsArr[slotIndex]['itemId']
                    &&
                    this.accessoryItemsArr[slotIndex]['itemCount']
                    &&
                    this.accessoryItemsArr[slotIndex]['itemDurability']
                ) {
                    this.accessoryItemsArr[slotIndex]['itemDurability'] = Math.max(this.accessoryItemsArr[slotIndex]['itemDurability'] - deathDurabilityDecreaseNum, 1);
                }
            }
            this.updateHotbarRedis(prevHotbarArr);
        }
        UtilsServer.io.emit('playerDied', responseData); // Emit to all players.
        if (this.id in UtilsServer.io.sockets.connected) {
            this.nightsSurvivedNum = 0;
            if (this.userName) {
                UtilsServer.redisClient.hset('player:' + this.userName, 'playerScore', this.playerScore.toString());
            }
            const responseData: PlayerDeathDetailsArr = {
                'playerId':          this.id,
                'deathSuffixText':   deathSuffixText,
                'deathKey':          deathKey,
                'playerScore':       this.playerScore,
                'hotbarItemsArr':    this.hotbarItemsArr,
                'accessoryItemsArr': this.accessoryItemsArr,
                'nightsSurvivedNum': this.nightsSurvivedNum,
                'daysSurvivedNum':   Math.round((Utils.time() - this.joinedTime) / Utils.cycleLengthMillis * 10) / 10,
            };
            UtilsServer.io.to(this.id).emit('youDied', responseData); // Emit to specific player.
        }
        clearTimeout(this.craftingTimeout);
        if (deathKey == 'disconnected') {
            delete PlayerServer.allPlayerObjs[this.id];
        } else {
            this.isGhost     = true;
            this.health      = 0;
            this.warmth      = 0;
            this.hunger      = 0;
            this.playerScore = 0;
            if (this.userName) {
                UtilsServer.redisClient.hset('player:' + this.userName, 'isGhost', (this.isGhost ? '1' : '0'));
                UtilsServer.redisClient.hset('player:' + this.userName, 'health', this.health.toString());
                UtilsServer.redisClient.hset('player:' + this.userName, 'warmth', this.warmth.toString());
                UtilsServer.redisClient.hset('player:' + this.userName, 'hunger', this.hunger.toString());
                UtilsServer.redisClient.hset('player:' + this.userName, 'playerScore', this.playerScore.toString());
            }
        }
    }

    revive(): void {
        this.isGhost = false;
        this.health  = this.maxHealth;
        this.warmth  = this.maxWarmth;
        this.hunger  = this.maxHunger;
        if (this.userName) {
            UtilsServer.redisClient.hset('player:' + this.userName, 'isGhost', (this.isGhost ? '1' : '0'));
            UtilsServer.redisClient.hset('player:' + this.userName, 'health', this.health.toString());
            UtilsServer.redisClient.hset('player:' + this.userName, 'warmth', this.warmth.toString());
            UtilsServer.redisClient.hset('player:' + this.userName, 'hunger', this.hunger.toString());
        }
        UtilsServer.io.to(this.id).emit('updateStatBars', {
            'status':          'success',
            'isGhost':         this.isGhost,
            'statBarsDataArr': {
                'health': this.health,
                'warmth': this.warmth,
                'hunger': this.hunger,
            },
        }); // Emit to specific player.
    }

    updateStatBars(is5thTick: boolean): void {
        if (!this.isPaused && !this.isGhost && Utils.playerGameModesArr[this.gameModeStr]['hasHealth']) {
            let playerPrevHealth = this.health;
            let playerPrevWarmth = this.warmth;
            let playerPrevHunger = this.hunger;

            // Handle suffocation and lava damage.
            const xRounded                      = Math.floor(this.x / Utils.tileSize);
            const yRounded                      = Math.floor(this.y / Utils.tileSize);
            let isCollidingWithWorld;
            let worldItemId;
            [isCollidingWithWorld, worldItemId] = UtilsServer.isCoordCollidingWithWorld(WorldServer.worldArr[this.currentLayerNum], xRounded, yRounded);
            if (isCollidingWithWorld && worldItemId != 'spawn' && worldItemId != 'bed') {
                const newCoordArr = WorldServer.findEmptySquareInRadius(xRounded, yRounded, this.currentLayerNum);
                if (newCoordArr) {
                    UtilsServer.io.emit('playerMovement', this.id, newCoordArr); // Emit to all players.
                }
            }
            // Don't update stat bars on rails.
            if (worldItemId == 'rail') {
                return;
            }
            const floorItemId = Utils.tileIndexToItemIdArr[WorldServer.floorArr[this.currentLayerNum][yRounded][xRounded][0]];
            if (floorItemId == 'lava' && this.updateHealth(-5, false, 'lava')) {
                return; // Died.
            }
            if (is5thTick) {

                if (Utils.playerGameModesArr[this.gameModeStr]['hasHunger']) {
                    // If no hunger left, take starvation damage.
                    if (this.hunger <= 0) {
                        if (this.updateHealth(-2, false, 'hunger')) {
                            return; // Died.
                        }
                    } else {
                        // If still has hunger left, decrease it.
                        let hungerDecreaseNum = 0.5;
                        if (this.hasSprintedInLastTick) {
                            hungerDecreaseNum = 1;
                        }
                        this.hunger -= hungerDecreaseNum * Utils.playerDifficultiesArr[this.difficultyStr]['difficultyMult'];
                    }
                }
                if (Utils.playerGameModesArr[this.gameModeStr]['hasWarmth']) {
                    // Warm player up if by a heater.
                    let warmthFromHeater = null;
                    for (let coordStr in WorldServer.allHeaterCoords) {
                        const coordArr = WorldServer.allHeaterCoords[coordStr];
                        if (coordArr['layerNum'] == this.currentLayerNum) {
                            if (!WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) {
                                delete WorldServer.allHeaterCoords[coordStr];
                                continue;
                            }
                            const blockIndex = WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][0];
                            if (!(blockIndex in Utils.tileIndexToItemIdArr)) {
                                delete WorldServer.allHeaterCoords[coordStr];
                                continue;
                            }
                            const blockId = Utils.tileIndexToItemIdArr[blockIndex];
                            if (!(blockId in Items.itemsArr) || Items.itemsArr[blockId]['heaterWarmth'] == null) {
                                delete WorldServer.allHeaterCoords[coordStr];
                                continue;
                            }
                            const heaterRadius = Items.itemsArr[blockId]['heaterRadius'];
                            const heaterWarmth = Items.itemsArr[blockId]['heaterWarmth'];
                            if (heaterRadius && (heaterWarmth != null || heaterWarmth > warmthFromHeater)) {
                                const heaterDistance = Utils.distance(
                                    {'x': this.x, 'y': this.y},
                                    {'x': (coordArr['x'] * Utils.tileSize) + (Utils.tileSize / 2), 'y': (coordArr['y'] * Utils.tileSize) + (Utils.tileSize / 2)},
                                );
                                if (heaterDistance <= heaterRadius) {
                                    warmthFromHeater = heaterWarmth;
                                }
                            }
                        }
                    }
                    if (floorItemId.indexOf('carpet') == 0 && warmthFromHeater < 0.5) {
                        warmthFromHeater = 0.5;
                    }
                    if (worldItemId == 'bed' && warmthFromHeater < 1) {
                        warmthFromHeater = 1;
                    }
                    if (warmthFromHeater != null) {
                        if (this.warmth < this.maxWarmth && warmthFromHeater) {
                            this.warmth += warmthFromHeater;
                        }
                    } else {
                        let heatLoss = 2; // Night heat loss.
                        if (DayNightCycleServer.stage == 'day' && this.currentLayerNum == 0) {
                            heatLoss = 0; // Day heat loss.
                        }
                        heatLoss *= Utils.playerDifficultiesArr[this.difficultyStr]['difficultyMult'];
                        // Reduce heat loss if has any items that reduce heat loss.
                        let playerHeatLossReductionPcnt = 0;
                        for (let i = 0; i < this.accessoryItemsArr.length; i++) {
                            if (
                                this.accessoryItemsArr[i]['itemId'] != null
                                &&
                                this.accessoryItemsArr[i]['itemId'] in Items.itemsArr
                                &&
                                Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['heatLossReductionPcnt']
                            ) {
                                playerHeatLossReductionPcnt += Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['heatLossReductionPcnt'];
                            }
                        }
                        if (playerHeatLossReductionPcnt) {
                            heatLoss *= (1 - playerHeatLossReductionPcnt);
                        }
                        this.warmth -= Math.round(heatLoss * 10) / 10; // Round to 1dp.
                    }
                    if (warmthFromHeater != 0) {
                        // If no warmth left, take cold damage.
                        if (this.warmth <= 0 && this.updateHealth(-2, false, 'warmth')) {
                            return; // Died.
                        }
                    }
                }

                if (this.health < this.maxHealth) {
                    if (this.hasRingEffect('ring_regen')) {
                        this.updateHealth(+3, false, '');

                        // Regenerate health if has more than half warmth and hunger.
                    } else if (
                        (
                            (
                                !Utils.playerGameModesArr[this.gameModeStr]['hasWarmth']
                                ||
                                this.warmth >= this.maxWarmth / 2
                            )
                            &&
                            (
                                !Utils.playerGameModesArr[this.gameModeStr]['hasHunger']
                                ||
                                this.hunger >= this.maxHunger / 2
                            )
                        )
                        ||
                        (
                            !Utils.playerGameModesArr[this.gameModeStr]['hasWarmth']
                            &&
                            !Utils.playerGameModesArr[this.gameModeStr]['hasHunger']
                        )
                    ) {
                        if (this.updateHealth(+2, false, '')) {
                            return; // Died.
                        }
                        if (Utils.playerGameModesArr[this.gameModeStr]['hasWarmth']) {
                            this.warmth -= Utils.playerDifficultiesArr[this.difficultyStr]['difficultyMult'];
                        }
                        if (Utils.playerGameModesArr[this.gameModeStr]['hasHunger']) {
                            this.hunger -= Utils.playerDifficultiesArr[this.difficultyStr]['difficultyMult'];
                        }
                    }
                }
            }

            this.warmth = Math.round(this.warmth * 10) / 10;
            this.hunger = Math.round(this.hunger * 10) / 10;
            this.warmth = Math.round(this.warmth * 10) / 10;
            // Don't let warmth and hunger go below zero.
            this.warmth = Math.max(this.warmth, 0);
            this.hunger = Math.max(this.hunger, 0);

            if (
                playerPrevHealth != this.health
                ||
                playerPrevWarmth != this.warmth
                ||
                playerPrevHunger != this.hunger
            ) {
                const statBarsDataArr: { health?: number, warmth?: number, hunger?: number } = {};
                if (playerPrevHealth != this.health) {
                    statBarsDataArr['health'] = this.health;
                    if (this.userName) {
                        UtilsServer.redisClient.hset('player:' + this.userName, 'health', this.health.toString());
                    }
                }
                if (playerPrevWarmth != this.warmth) {
                    statBarsDataArr['warmth'] = this.warmth;
                    if (this.userName) {
                        UtilsServer.redisClient.hset('player:' + this.userName, 'warmth', this.warmth.toString());
                    }
                }
                if (playerPrevHunger != this.hunger) {
                    statBarsDataArr['hunger'] = this.hunger;
                    if (this.userName) {
                        UtilsServer.redisClient.hset('player:' + this.userName, 'hunger', this.hunger.toString());
                    }
                }
                UtilsServer.io.to(this.id).emit('updateStatBars', {'status': 'success', 'statBarsDataArr': statBarsDataArr}); // Emit to specific player.
            }

            this.hasSprintedInLastTick = false;
        }
    }

}
