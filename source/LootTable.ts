const Utils = require('./Utils');
const Items = require('./public/generated/Items');
import {WorldServer} from './WorldServer';
import {UtilsServer} from './UtilsServer';
import {ItemId} from './HunkerInterfaces';

type AllTableRow = { itemId: ItemId, itemCount: number | number[] }[];
type WeightedTableRow = { chancePcnt: number, itemsArr: string }[];

export class LootTable {

    static allTablesArr: { [tableKey: string]: AllTableRow }                                             = {
        // @formatter:off
        'entityJunk':    [
            {'itemId': 'leather',           'itemCount': 1},
            {'itemId': 'twine',             'itemCount': 1},
            {'itemId': 'torch',             'itemCount': 1},
            {'itemId': 'coal',              'itemCount': 1},
            {'itemId': 'seeds',             'itemCount': 1},
            {'itemId': 'wheat',             'itemCount': 1},
            {'itemId': 'carrot',            'itemCount': 1},
            {'itemId': 'bread',             'itemCount': 1},
            {'itemId': 'mushroom_tan',      'itemCount': 1},
            {'itemId': 'mushroom_red',      'itemCount': 1},
            {'itemId': 'mushroom_brown',    'itemCount': 1},
            {'itemId': 'mushroom_blue',     'itemCount': 1},
            {'itemId': 'bowl',              'itemCount': 1},
        ],
        'junk':          [
            {'itemId': 'meat_rotten',       'itemCount': [3, 4]},
            {'itemId': 'slimeball',         'itemCount': [3, 4]},
            {'itemId': 'arrow',             'itemCount': [3, 4]},
            {'itemId': 'fireball',          'itemCount': [2, 3]},
            {'itemId': 'planks',            'itemCount': [2, 3]},
            {'itemId': 'stone',             'itemCount': [2, 3]},
            {'itemId': 'cobblestone',       'itemCount': [2, 3]},
            {'itemId': 'leather',           'itemCount': [2, 3]},
            {'itemId': 'twine',             'itemCount': [2, 3]},
            {'itemId': 'torch',             'itemCount': [2, 3]},
            {'itemId': 'coal',              'itemCount': [2, 3]},
            {'itemId': 'seeds',             'itemCount': [2, 3]},
            {'itemId': 'wheat',             'itemCount': [2, 3]},
            {'itemId': 'carrot',            'itemCount': [2, 3]},
            {'itemId': 'sugar_cane',        'itemCount': [2, 3]},
            {'itemId': 'sugar',             'itemCount': [2, 3]},
            {'itemId': 'bread',             'itemCount': [2, 3]},
            {'itemId': 'sapling',           'itemCount': [2, 3]},
            {'itemId': 'sapling_birch',     'itemCount': [2, 3]},
            {'itemId': 'wool',              'itemCount': [2, 3]},
            {'itemId': 'mushroom_tan',      'itemCount': [2, 3]},
            {'itemId': 'mushroom_red',      'itemCount': [2, 3]},
            {'itemId': 'mushroom_brown',    'itemCount': [2, 3]},
            {'itemId': 'mushroom_blue',     'itemCount': [2, 3]},
            {'itemId': 'bowl',              'itemCount': [2, 3]},
        ],
        'crudeArmour':   [
            {'itemId': 'helmet_crude',      'itemCount': 1},
            {'itemId': 'chestplate_crude',  'itemCount': 1},
            {'itemId': 'leggings_crude',    'itemCount': 1},
            {'itemId': 'boots_crude',       'itemCount': 1},
            {'itemId': 'earmuffs',          'itemCount': 1},
        ],
        'silverArmour':  [
            {'itemId': 'helmet_silver',     'itemCount': 1},
            {'itemId': 'chestplate_silver', 'itemCount': 1},
            {'itemId': 'leggings_silver',   'itemCount': 1},
            {'itemId': 'boots_silver',      'itemCount': 1},
            {'itemId': 'jacket',            'itemCount': 1},
        ],
        'goldArmour':  [
            {'itemId': 'helmet_gold',       'itemCount': 1},
            {'itemId': 'chestplate_gold',   'itemCount': 1},
            {'itemId': 'leggings_gold',     'itemCount': 1},
            {'itemId': 'boots_gold',        'itemCount': 1},
            {'itemId': 'coat',              'itemCount': 1},
        ],
        'diamondArmour':  [
            {'itemId': 'helmet_diamond',    'itemCount': 1},
            {'itemId': 'chestplate_diamond','itemCount': 1},
            {'itemId': 'leggings_diamond',  'itemCount': 1},
            {'itemId': 'boots_diamond',     'itemCount': 1},
        ],
        'woodTools':     [
            {'itemId': 'axe_wood',          'itemCount': 1},
            {'itemId': 'pick_wood',         'itemCount': 1},
            {'itemId': 'sword_wood',        'itemCount': 1},
            {'itemId': 'hoe_wood',          'itemCount': 1},
            {'itemId': 'shovel_wood',       'itemCount': 1},
        ],
        'stoneTools':    [
            {'itemId': 'axe_stone',         'itemCount': 1},
            {'itemId': 'pick_stone',        'itemCount': 1},
            {'itemId': 'sword_stone',       'itemCount': 1},
            {'itemId': 'hoe_stone',         'itemCount': 1},
            {'itemId': 'shovel_stone',      'itemCount': 1},
            {'itemId': 'bow',               'itemCount': 1},
        ],
        'silverTools':    [
            {'itemId': 'axe_silver',        'itemCount': 1},
            {'itemId': 'pick_silver',       'itemCount': 1},
            {'itemId': 'sword_silver',      'itemCount': 1},
            {'itemId': 'hoe_silver',        'itemCount': 1},
            {'itemId': 'shovel_silver',     'itemCount': 1},
        ],
        'goldTools':    [
            {'itemId': 'axe_gold',          'itemCount': 1},
            {'itemId': 'pick_gold',         'itemCount': 1},
            {'itemId': 'sword_gold',        'itemCount': 1},
            {'itemId': 'hoe_gold',          'itemCount': 1},
            {'itemId': 'shovel_gold',       'itemCount': 1},
        ],
        'diamondTools':    [
            {'itemId': 'axe_diamond',       'itemCount': 1},
            {'itemId': 'pick_diamond',      'itemCount': 1},
            {'itemId': 'sword_diamond',     'itemCount': 1},
            {'itemId': 'hoe_diamond',       'itemCount': 1},
            {'itemId': 'shovel_diamond',    'itemCount': 1},
        ],
        'misc':      [
            {'itemId': 'stew',              'itemCount': [2, 3]},
            {'itemId': 'hot_chocolate',     'itemCount': [1, 2]},
            {'itemId': 'snowball',          'itemCount': [7, 10]},
            {'itemId': 'chest',             'itemCount': [2, 3]},
            {'itemId': 'tnt',               'itemCount': [2, 3]},
            {'itemId': 'log',               'itemCount': [5, 7]},
            {'itemId': 'arrow',             'itemCount': [7, 10]},
            {'itemId': 'fireball',          'itemCount': [3, 4]},
            {'itemId': 'trap',              'itemCount': [2, 3]},
            {'itemId': 'bookshelf',         'itemCount': [2, 3]},
            {'itemId': 'workbench',         'itemCount': [2, 3]},
            {'itemId': 'hearth',            'itemCount': [2, 3]},
            {'itemId': 'fire',              'itemCount': [2, 3]},
            {'itemId': 'ring_flowers',      'itemCount': 1},
            {'itemId': 'ring_light',        'itemCount': 1},
            {'itemId': 'ring_speed',        'itemCount': 1},
            {'itemId': 'ring_regen',        'itemCount': 1},
            {'itemId': 'ring_thorns',       'itemCount': 1},
            // {'itemId': 'ring_copper',       'itemCount': 1},
            // {'itemId': 'ring_silver',       'itemCount': 1},
            {'itemId': 'ring_blood',        'itemCount': 1},
            {'itemId': 'bow',               'itemCount': 1},
        ],
        'entityOre': [
            {'itemId': 'silver',            'itemCount': 1},
        ],
        'ore':       [
            {'itemId': 'silver',            'itemCount': [2, 4]},
        ],
        'moreOre':       [
            {'itemId': 'silver',            'itemCount': [5, 7]},
            {'itemId': 'gold',              'itemCount': [2, 4]},
            {'itemId': 'diamond',           'itemCount': [2, 4]},
        ],
        // @formatter:on
    };
    static weightedTablesArr: { [weightedTableKey: string]: WeightedTableRow }                           = {
        'loot_wood':  [
            {'chancePcnt': 0.50, 'itemsArr': 'junk'},
            {'chancePcnt': 0.14, 'itemsArr': 'woodTools'},
            {'chancePcnt': 0.11, 'itemsArr': 'misc'},
            {'chancePcnt': 0.10, 'itemsArr': 'crudeArmour'},
            {'chancePcnt': 0.09, 'itemsArr': 'ore'},
            {'chancePcnt': 0.06, 'itemsArr': 'stoneTools'},
        ],
        'loot_stone': [
            {'chancePcnt': 0.29, 'itemsArr': 'misc'},
            {'chancePcnt': 0.23, 'itemsArr': 'ore'},
            {'chancePcnt': 0.17, 'itemsArr': 'crudeArmour'},
            {'chancePcnt': 0.14, 'itemsArr': 'stoneTools'},
            {'chancePcnt': 0.10, 'itemsArr': 'silverArmour'},
            {'chancePcnt': 0.07, 'itemsArr': 'silverTools'},
        ],
        'loot_blue':  [
            {'chancePcnt': 0.15, 'itemsArr': 'misc'},
            {'chancePcnt': 0.20, 'itemsArr': 'ore'},
            {'chancePcnt': 0.12, 'itemsArr': 'crudeArmour'},
            {'chancePcnt': 0.19, 'itemsArr': 'silverArmour'},
            {'chancePcnt': 0.14, 'itemsArr': 'silverTools'},
            {'chancePcnt': 0.11, 'itemsArr': 'goldArmour'},
            {'chancePcnt': 0.09, 'itemsArr': 'goldTools'},
        ],
        'loot_red':   [
            {'chancePcnt': 0.15, 'itemsArr': 'misc'},
            {'chancePcnt': 0.20, 'itemsArr': 'ore'},
            {'chancePcnt': 0.10, 'itemsArr': 'crudeArmour'},
            {'chancePcnt': 0.16, 'itemsArr': 'goldArmour'},
            {'chancePcnt': 0.14, 'itemsArr': 'goldTools'},
            {'chancePcnt': 0.11, 'itemsArr': 'diamondArmour'},
            {'chancePcnt': 0.09, 'itemsArr': 'diamondTools'},
            {'chancePcnt': 0.05, 'itemsArr': 'moreOre'},
        ],
        'loot_green': [
            {'chancePcnt': 0.15, 'itemsArr': 'misc'},
            {'chancePcnt': 0.26, 'itemsArr': 'ore'},
            {'chancePcnt': 0.16, 'itemsArr': 'goldArmour'},
            {'chancePcnt': 0.14, 'itemsArr': 'goldTools'},
            {'chancePcnt': 0.19, 'itemsArr': 'diamondArmour'},
            {'chancePcnt': 0.16, 'itemsArr': 'diamondTools'},
            {'chancePcnt': 0.24, 'itemsArr': 'moreOre'},
        ],
    };
    static tablesArr: { [tableKey: string]: { chancePcnt: number, itemsArr: string | AllTableRow }[][] } = {
        'zombie':  [
            [
                {'chancePcnt': 0.50, 'itemsArr': [{'itemId': 'meat_rotten', 'itemCount': [1, 2]}]},
                {'chancePcnt': 0.44, 'itemsArr': 'entityJunk'},
                {'chancePcnt': 0.03, 'itemsArr': 'woodTools'},
                {'chancePcnt': 0.02, 'itemsArr': 'entityOre'},
                {'chancePcnt': 0.01, 'itemsArr': 'crudeArmour'},
            ],
        ],
        'slime':   [
            [
                {'chancePcnt': 0.80, 'itemsArr': [{'itemId': 'slimeball', 'itemCount': [1, 2]}]},
                {'chancePcnt': 0.14, 'itemsArr': 'entityJunk'},
                {'chancePcnt': 0.03, 'itemsArr': 'woodTools'},
                {'chancePcnt': 0.02, 'itemsArr': 'entityOre'},
                {'chancePcnt': 0.01, 'itemsArr': 'crudeArmour'},
            ],
        ],
        'archer':  [
            [
                {'chancePcnt': 0.80, 'itemsArr': [{'itemId': 'arrow', 'itemCount': [1, 2]}]},
                {'chancePcnt': 0.14, 'itemsArr': 'entityJunk'},
                {'chancePcnt': 0.03, 'itemsArr': [{'itemId': 'twine', 'itemCount': [1, 2]}]},
                {'chancePcnt': 0.02, 'itemsArr': [{'itemId': 'bow', 'itemCount': 1}]},
                {'chancePcnt': 0.01, 'itemsArr': 'crudeArmour'},
            ],
        ],
        'axehead': [
            [{'chancePcnt': 1.00, 'itemsArr': [{'itemId': 'axe_giant', 'itemCount': 1}]}],
            LootTable.weightedTablesArr['loot_blue'],
            LootTable.weightedTablesArr['loot_blue'],
        ],
        'chest':           [
            LootTable.weightedTablesArr['loot_stone'],
            LootTable.weightedTablesArr['loot_wood'],
            LootTable.weightedTablesArr['loot_wood'],
            [{'chancePcnt': 1.00, 'itemsArr': [{'itemId': null, 'itemCount': null}]}],
            LootTable.weightedTablesArr['loot_wood'],
            LootTable.weightedTablesArr['loot_stone'],
            LootTable.weightedTablesArr['loot_wood'],
            [{'chancePcnt': 1.00, 'itemsArr': [{'itemId': null, 'itemCount': null}]}],
            LootTable.weightedTablesArr['loot_wood'],
        ],
        'present_red':     [LootTable.weightedTablesArr['loot_stone']],
        'present_blue':    [LootTable.weightedTablesArr['loot_stone']],
        'present_green':   [LootTable.weightedTablesArr['loot_stone']],
        'hearth':  [
            [
                {'chancePcnt': 0.25, 'itemsArr': [{'itemId': 'coal', 'itemCount': [2, 3]}]},
                {'chancePcnt': 0.25, 'itemsArr': [{'itemId': 'log', 'itemCount': [2, 3]}]},
            ],
            [{'chancePcnt': 1.00, 'itemsArr': [{'itemId': null, 'itemCount': null}]}],
            [
                {'chancePcnt': 0.50, 'itemsArr': 'ore'},
                {'chancePcnt': 0.50, 'itemsArr': [{'itemId': null, 'itemCount': null}]},
            ],
        ],
        'lootcrate_wood':  [LootTable.weightedTablesArr['loot_wood']],
        'lootcrate_stone': [LootTable.weightedTablesArr['loot_stone']],
        'lootcrate_blue':  [
            LootTable.weightedTablesArr['loot_blue'],
            LootTable.weightedTablesArr['loot_stone'],
        ],
        'lootcrate_red':   [
            LootTable.weightedTablesArr['loot_red'],
            LootTable.weightedTablesArr['loot_blue'],
        ],
        'lootcrate_green': [
            LootTable.weightedTablesArr['loot_green'],
            LootTable.weightedTablesArr['loot_green'],
            LootTable.weightedTablesArr['loot_red'],
        ],
    };

    static getLootItemsArr(lootKey: keyof typeof LootTable.tablesArr & string) {
        if (lootKey in LootTable.tablesArr) {
            let lootItemsArr = [];
            for (let i in LootTable.tablesArr[lootKey]) {
                const weightedItemsArr    = LootTable.tablesArr[lootKey][i];
                const chancePcnt          = Math.random();
                let itemChanceCounterPcnt = 0;
                for (let j in weightedItemsArr) {
                    const itemChancePcnt = weightedItemsArr[j]['chancePcnt'];
                    itemChanceCounterPcnt += itemChancePcnt;
                    if (chancePcnt <= itemChanceCounterPcnt) {
                        let itemsArr = weightedItemsArr[j]['itemsArr'];
                        if (typeof itemsArr == 'string') {
                            if (itemsArr in LootTable.allTablesArr) {
                                itemsArr = LootTable.allTablesArr[itemsArr];
                            } else {
                                throw new Error("Incorrect LootTable.allTablesArr index \"" + itemsArr + "\"");
                            }
                        }
                        const lootItemArr = Utils.randElFromArr(Utils.dereferenceObj(itemsArr));
                        if (lootItemArr['itemCount'] && Array.isArray(lootItemArr['itemCount'])) {
                            lootItemArr['itemCount'] = Utils.randBetween(lootItemArr['itemCount'][0], lootItemArr['itemCount'][1]);
                        }
                        if (lootItemArr['itemId'] && Items.itemsArr[lootItemArr['itemId']]['itemDurability']) {
                            lootItemArr['itemDurability'] = Utils.randBetween(Items.itemsArr[lootItemArr['itemId']]['itemDurability'] * 0.3, Items.itemsArr[lootItemArr['itemId']]['itemDurability'] * 0.7);
                        }
                        lootItemsArr.push(lootItemArr);
                        break;
                    }
                }
            }
            return lootItemsArr;
        }
        return [{'itemId': null, 'itemCount': null}];
    }

    static setContainerArrForBlock(blockItemId: ItemId, coordStr: string) {
        if (blockItemId in LootTable.tablesArr) {
            WorldServer.allContainersArr[coordStr] = Utils.dereferenceObj(LootTable.getLootItemsArr(blockItemId));
        } else {
            WorldServer.allContainersArr[coordStr] = [];
            for (let i = 0; i < (Items.itemsArr[blockItemId]['maxItemsInContainerNum'] || 1); i++) {
                WorldServer.allContainersArr[coordStr].push({'itemId': null, 'itemCount': null});
            }
        }
        if (UtilsServer.redisClient) {
            WorldServer.updateContainerRedis(coordStr, null);
        }
    }

}
