import {UtilsServer} from './UtilsServer';

export type WorldCell = [
    number, // Tile index.
    number, // Durability.
    (number | string)?, // Player username or id.
    number?, // Hearth fuel burn time.
    number?, // Hearth fuel burn item max time.
    number? // Hearth input smelting time.
];
export type WorldRow = (WorldCell | null)[];
export type WorldLayer = WorldRow[];
export type WorldArr = { [layerNum: string]: WorldLayer };

export interface JoinDetailsArr {
    name: string;
    image: string;
    gameModeStr: string;
    difficultyStr: string;
}

export interface Coord {
    x: number;
    y: number;
    layerNum?: number;
}

export type ItemEnchantmentsArr = {
    [enchantmentType: string]: number,
}

export interface HotbarSlot {
    itemId: ItemId | null;
    itemCount: number | null;
    itemDurability?: number | null;
    itemEnchantmentsArr?: ItemEnchantmentsArr | null;
}

export type EnchantmentType = keyof typeof Utils.allEnchantmentsArr & string;

export interface BoundsArr {
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}

export type ItemId = keyof typeof Items.itemsArr & string;
export type SoundEffectKey = keyof typeof SoundEffect.effectsArr & string;
export type ParticleEffectKey = keyof typeof ParticleEffect.effectsArr & string;
export type GameModeStr = keyof typeof Utils.playerGameModesArr & string;
export type DifficultyStr = keyof typeof Utils.playerDifficultiesArr & string;
export type PlayerImage = keyof typeof Utils.allPlayerImagesArr & string;
export type DirectionLetter = 'n' | 'e' | 's' | 'w';
export type ToolType = 'shovel' | 'axe' | 'pick' | 'hoe' | 'scythe' | 'sword';

export interface ItemRow {
    label: string,
    isObtainable: boolean,
    isStackable: boolean,
    isTool: boolean,
    isUsable: boolean,
    isContainer: boolean,
    canRemoveItems: boolean,
    isDisablingCollision: boolean,
    isRemovingContainerIfEmpty: boolean,
    canFeedEntities: boolean,
    canTill: boolean,
    isFloor: boolean,
    isProjectile: boolean,
    isFiringProjectiles: boolean,
    hasParticleEmitter: boolean,
    isDarkeningParticles: boolean,
    hexCode: string,
    isFlippingHeldSprite: boolean,
    isShowingBelowPlayer: boolean,
    heldSpriteOriginX: number,
    heldSpriteOriginY: number,
    heldSpriteWidthHeight: number,
    heldSpriteAngle: number,
    whichTool: ToolType,
    itemTier: keyof typeof UtilsServer.tierBlockMultArr,
    dropItemId: ItemId,
    itemToPlaceId: ItemId,
    entityToPlaceType: EntityType,
    returnItemId: ItemId,
    maxItemsInContainerNum: number,
    warmthValue: number,
    healthValue: number,
    hungerValue: number,
    heatLossReductionPcnt: number,
    damageReductionPcnt: number,
    additionalHotbarSlotsNum: number,
    entityDamageValue: number,
    playerReach: number,
    attackRangeDistance: number,
    attackRangeRadius: number,
    attackCooldownMillis: number,
    attackKnockbackMult: number,
    hasLightCircleWhenSelected: boolean,
    lightCircleRadius: number,
    lightCircleTint: string,
    heaterRadius: number,
    heaterWarmth: number,
    blockHardness: number,
    structureDurability: number,
    itemDurability: number,
    timeToLiveSecs: number,
    explosionPower: number,
    pointsGivenOnDestroy: number,
    pointsGivenOnCraft: number,
    craftingSpeedMillis: number,
    requiredItemsForCraftingArr: { [itemId in ItemId]: number },
    craftedItemsNum: number,
    requiredBlockForCraftingItemId: ItemId,
    smeltingOutputItemId: ItemId,
    burnTimeSecs: number,
    toggleItemId: ItemId,
    isEmittingCurrent: boolean,
    isConductingCurrent: boolean,
    hasActionOnCurrent: boolean,
    cropSeedId: ItemId,
    cropNextStageId: ItemId,
    cropYieldItemId: ItemId,
    cropRequiredFloorIdsArr: ItemId[],
    cropRequiredDistanceToWater: number,
    hitSoundEffectKey: SoundEffectKey,
    stepSoundEffectKey: SoundEffectKey,
    breakSoundEffectKey: SoundEffectKey,
    useSoundEffectKey: SoundEffectKey,
    projectileSoundEffectKey: SoundEffectKey,
    projectileAmmoId: ProjectileItemId,
    projectileAccuracy: number,
    projectileMaxCharge: number,
    speedMult: number,
    accessoriesSlotIndexesArr: number[],
    itemSrc: string,
    itemDesc: string,
    tileIndexesArr: number[],
    altTileIndexesArr: number[];
}

export type EntityType = 'wolf' | 'polar_bear' | 'cow' | 'pig' | 'sheep' | 'chicken' | 'zombie' | 'slime' | 'slime_fire' | 'slime_ice' | 'archer' | 'axehead' | 'arrowEntity' | 'fireballEntity' | 'snowballEntity' | 'eggEntity' | 'boatEntity';
export type ProjectileItemId = 'arrow' | 'fireball' | 'snowball' | 'egg';

export type AllEntityTypesArr = {
    [entityType: string]: {
        label: string;
        hasNameTag: boolean;
        tickIntervalMillis: number;
        initNum: number | null;
        minInWorld: number | null;
        isMonster: boolean;
        hasPanicMode: boolean;
        isSavingInRedis: boolean;
        cyclesUntilAdultNum: number | null;
        canBreed: boolean;
        feedingItemId: ItemId | null;
        spawnLayerNum: number | null;
        spawnFloorIdsArr: ItemId[] | null;
        idleFloorIdsArr: ItemId[] | null;
        moveFloorIdsArr: ItemId[] | null;
        health: number | null;
        pointsGivenWhenKilled: number | null;
        isPassive: boolean;
        isRideable: boolean;
        isProjectile: boolean;
        isColliding: boolean;
        isExplosive: boolean;
        projectileMaxDistance: number | null;
        projectileDespawnMillis: number | null;
        lightCircleRadius: number | null;
        lightCircleTint: string | null;
        explosionPower: number | null;
        explosionFloorPcnt: number | null;
        explosionFloorId: ItemId | null;
        fuseLengthNum: number | null;
        fuseAutoDecrementLengthNum: number | null;
        damage: number | null;
        attackCooldownTicksNum: number | null;
        alpha: number;
        speed: number;
        hitboxMult: number;
        knockbackMult: number;
        damageRadiusMult: number | null;
        width: number;
        height: number;
        babyWidth: number | null;
        babyHeight: number | null;
        minSizeMult: number;
        maxSizeMult: number;
        angleOffset: number;
        lootKey: string;
        babySpriteStr: string | null;
        hurtSoundEffectKey: SoundEffectKey | null;
        deathSoundEffectKey: SoundEffectKey | null;
        projectileHitParticleEffectKey: ParticleEffectKey | null;
        yieldItemsArr?: HotbarSlot[];
    };
}

export interface EntityDataArr {
    id?: string;
    type: EntityType;
    layerNum?: number;
    x: number;
    y: number;
    angle?: number;
    isBaby: boolean;
    hasWool?: boolean;
    sizeMult?: number;
    health?: number;
    boundsArr?: BoundsArr;
    growUpTime?: number;
    fromPlayerId?: string;
    fromEntityId?: string;
    fuseLengthNum?: number;
    archerProjectileCharge?: number;
    projectileChargeMult?: number;
    isKnockbackTick?: boolean;
    isRiding?: boolean;
}

export interface PublicPlayerData {
    id: string;
    name: string;
    image: string;
    currentLayerNum: number;
    x: number;
    y: number;
    angle: number;
    playerScore: number;
    isCrafting: boolean;
    isGhost: boolean;
    craftingItemId: ItemId | null;
    isPaused: boolean;
    selectedItemId: ItemId | null;
}

export interface PrivatePlayerData {
    maxHealth: number;
    maxWarmth: number;
    maxHunger: number;
    health: number;
    warmth: number;
    hunger: number;
    joinedTime: number;
    selectedItemIndex: number;
    hotbarItemsArr: HotbarSlot[];
    accessoryItemsArr: HotbarSlot[];
}

export interface BlockUpdateData {
    blockCoordArr: Coord;
    blockDataArr?: WorldCell | null;
    blockId?: ItemId;
}

export interface FloorUpdateData {
    floorCoordArr: Coord;
    floorDataArr: WorldCell | null;
}

export interface LoginResponse {
    status: 'success' | 'error';
    userName: string;
    userScore: number;
    errorMessage: string;
    errorMessageInputId: string;
}

export interface ServerResponse {
    status: 'success' | 'error';
    errorMessage?: string;
    allSoundEffectKeysArr?: SoundEffectKey[];
    allParticleEffectKeysArr?: ParticleEffectKey[];
    blockUpdateDataArr?: BlockUpdateData;
    blockUpdatesDataArr?: BlockUpdateData[];
    floorUpdateDataArr?: FloorUpdateData;
    floorUpdatesDataArr?: FloorUpdateData[];
    hotbarItemsArr?: HotbarSlot[];
    accessoryItemsArr?: HotbarSlot[];
    playerScore?: number;
    publicPlayerDataArr?: { [playerId: string]: PublicPlayerData };
    privatePlayerDataArr?: PrivatePlayerData;
    currentLayerNum?: number;
    floorArr?: WorldArr;
    worldArr?: WorldArr;
    statBarsDataArr?: {
        hunger?: number;
        warmth?: number;
        health?: number;
    };
    containerArr?: HotbarSlot[];
    selectedItemIndex?: number;
    itemCraftingTimeMillis?: number;
    isPaused?: boolean;
    biomeCoordsPerLayerArr?: { [layerNum: string]: BiomeName[][] };
    dayNightStage?: DayNightStage;
    nightsSurvivedNum?: number;
    gameStartMillis?: number;
    isGhost?: boolean;
    updatePlayerScoresArr?: PlayerScoresArr;
    explosionCoordArr?: Coord;
    explosionPower?: number;
    attackCooldownMillis?: number;
    ridingEntityId?: string;
}

export interface StructureDetailsArr {
    substitutionsArr: { [substitutionStr: string]: ItemId | null };
    structureArr: string[][];
    floorSubstitutionsArr: { [substitutionStr: string]: ItemId | null };
    structureFloorArr: string[][];
}

export type ItemRowType = 'hotbar' | 'container' | 'accessories';

export interface SwapItemData {
    swapItemContainerCoordArr?: Coord;
    swapFromItemType: ItemRowType;
    swapToItemType: ItemRowType;
    swapFromItemIndex: number;
    swapToItemIndex: number;
}

export interface DropItemData {
    itemRowType: ItemRowType;
    itemToDropIndex: number;
    layerNum: number;
    x: number;
    y: number;
    isDroppingWholeStack: boolean;
}

export interface TakeItemData {
    itemRowType: ItemRowType;
    itemToDropIndex: number;
    layerNum: number;
    x: number;
    y: number;
    isTakingWholeStack: boolean;
}

export type BiomeName = keyof typeof Utils.biomeDetailsArr & string;

export interface MovementData {
    x: number;
    y: number;
    angle: number;
    projectileCharge: number;
    isSprinting?: boolean;
    isGhost?: boolean;
    ridingEntityId?: string;
    ridingEntityAngle?: number;
}

export type DayNightStage = 'day' | 'night';

export type PlayerScoresArr = { [playerId: string]: number };

export interface StatBarsDataArr {
    maxHunger?: number;
    maxWarmth?: number;
    maxHealth?: number;
    hunger?: number;
    warmth?: number;
    health?: number;
}

export interface HeldSpritePosition {
    heldSpriteOriginX: number;
    heldSpriteOriginY: number;
    heldSpriteWidthHeight: number;
    heldSpriteAngle: number;
    isFlippingHeldSprite: boolean;
    isShowingBelowPlayer: boolean;
}

export interface PlayerDeathDetailsArr {
    playerId: string,
    deathSuffixText: string,
    deathKey: string,
    playerScore: number,
    hotbarItemsArr: HotbarSlot[],
    accessoryItemsArr: HotbarSlot[],
    nightsSurvivedNum: number,
    daysSurvivedNum: number,
}

export interface CraftingDataArr {
    isCrafting: boolean,
    craftingItemId: ItemId,
}

export interface MessageData {
    text: string;
    status?: 'success' | 'error';
    playerId?: string;
    hotbarItemsArr?: HotbarSlot[];
    soundEffectKey?: SoundEffectKey;
}

export interface UserState {
    isLoggedIn: boolean,
    userName?: string,
    userScore?: number,
}

export interface GenerateLayerResponse {
    worldLayerArr: WorldLayer;
    floorLayerArr: WorldLayer;
    seed: string;
}
