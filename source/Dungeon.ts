/// <reference path="Utils.ts" />
if (typeof require !== 'undefined') {
    // @ts-ignore
    Utils = require('./Utils');
}

interface RoomCoord {
    x1: number,
    x2: number,
    y1: number,
    y2: number,
    isMainRoom: boolean,
}

class Dungeon {

    private static lootcrateIdsArr: { [layerNum: string]: string[] } = {
        '-1': [
            'lw', // lootcrate_wood
            'lw', // lootcrate_wood
            'lw', // lootcrate_wood
            'lw', // lootcrate_wood
            'lw', // lootcrate_wood
            'c', // chest
            'h', // hearth
            'e', // workbench
            's', // bookshelf
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'lb', // lootcrate_blue
            'lb', // lootcrate_blue
            'lb', // lootcrate_blue
        ],
        '-2': [
            'lw', // lootcrate_wood
            'lw', // lootcrate_wood
            'c', // chest
            'h', // hearth
            'e', // workbench
            's', // bookshelf
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'lb', // lootcrate_blue
            'lb', // lootcrate_blue
            'lr', // lootcrate_red
            'lr', // lootcrate_red
        ],
        '-3': [
            'lw', // lootcrate_wood
            'c', // chest
            'h', // hearth
            'e', // workbench
            's', // bookshelf
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'ls', // lootcrate_stone
            'lb', // lootcrate_blue
            'lb', // lootcrate_blue
            'lr', // lootcrate_red
            'lr', // lootcrate_red
            'lg', // lootcrate_green
            'lg', // lootcrate_green
        ],
    };

    private readonly width: number;
    private readonly height: number;
    public readonly layerNum: number;
    private readonly roomExtraSize: number;
    private readonly numTiles: number;
    private readonly numRooms: number;

    public structureArr: string[][];
    public structureFloorArr: string[][];
    public roomsArr: RoomCoord[];

    static extraConnectorChance: number = 0.6;
    static windingPercent: number       = 1.00;

    constructor(width: number, height: number, layerNum: number) {
        if (width % 2 == 0 || height % 2 == 0) {
            throw new Error("The dungeon must be odd-sized.");
        }
        this.width         = width;
        this.height        = height;
        this.layerNum      = layerNum;
        this.roomExtraSize = 0;
        this.numTiles      = width * height;
        this.numRooms      = Math.round(this.numTiles * 0.002);
        // this.currentRegion        = -1;
        this.generateDungeon();
    }

    // http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
    //
    // The random dungeon generator.
    //
    // Starting with a stage of solid walls, it works like so:
    //
    // 1. Place a number of randomly sized and positioned rooms. If a room
    //    overlaps an existing room, it is discarded. Any remaining rooms are
    //    carved out.
    // 2. Any remaining solid areas are filled in with mazes. The maze generator
    //    will grow and fill in even odd-shaped areas, but will not touch any
    //    rooms.
    // 3. The result of the previous two steps is a series of unconnected rooms
    //    and mazes. We walk the stage and find every tile that can be a
    //    "connector". This is a solid tile that is adjacent to two unconnected
    //    regions.
    // 4. We randomly choose connectors and open them or place a door there until
    //    all of the unconnected regions have been joined. There is also a slight
    //    chance to carve a connector between two already-joined regions, so that
    //    the dungeon isn't single connected.
    // 5. The mazes will have a lot of dead ends. Finally, we remove those by
    //    repeatedly filling in any open tile that's closed on three sides. When
    //    this is done, every corridor in a maze actually leads somewhere.
    //
    // The end result of this is a multiply-connected dungeon with rooms and lots
    // of winding corridors.
    generateDungeon() {
        this.initDungeon();
        this.addRooms();
        this.addMaze();
        this.addDoorsToRooms();
        this.removeDeadEnds();
        this.addDecorations();
    }

    initDungeon() {
        this.structureArr      = [];
        this.structureFloorArr = [];
        for (let y = 0; y < this.height; y++) {
            const structureRow: string[] = [];
            for (let x = 0; x < this.width; x++) {
                structureRow.push('0');
            }
            this.structureArr.push(structureRow);
            this.structureFloorArr.push(structureRow);
        }
        this.structureFloorArr = Utils.dereferenceObj(this.structureFloorArr);
    }

    addRooms() {
        this.roomsArr = [];
        for (let i = 0; i < this.numRooms; i++) {
            this.addRoom(i == 0);
        }
    }

    addRoom(isMainRoom: boolean) {
        for (let numIterations = 0; numIterations < 100; numIterations++) {
            let size;
            let rectangularity;
            if (isMainRoom) {
                size           = Math.floor(this.width * 0.09) * 2 + 1;
                rectangularity = 0;
            } else {
                size           = Utils.randBetween(1, 2 + this.roomExtraSize) * 2 + 1;
                rectangularity = Utils.randBetween(0, 1 + Math.floor(size / 2)) * 2;
            }
            let width  = size;
            let height = size;
            if (Utils.randBool()) {
                width += rectangularity;
            } else {
                height += rectangularity;
            }
            let corner1X;
            let corner1Y;
            if (isMainRoom) {
                corner1X = Math.floor((this.width - width - 1) / 2 / 2) * 2 + 1;
                corner1Y = Math.floor((this.height - height - 1) / 2 / 2) * 2 + 1;
            } else {
                corner1X = Utils.randBetween(1, Math.floor((this.width - width - 1) / 2)) * 2 + 1;
                corner1Y = Utils.randBetween(1, Math.floor((this.height - height - 1) / 2)) * 2 + 1;
            }
            const corner2X    = corner1X + width;
            const corner2Y    = corner1Y + height;
            let isOverlapping = false;
            for (let i in this.roomsArr) {
                if (
                    !(
                        this.roomsArr[i]['x1'] > corner2X
                        ||
                        this.roomsArr[i]['x2'] < corner1X
                        ||
                        this.roomsArr[i]['y1'] > corner2Y
                        ||
                        this.roomsArr[i]['y2'] < corner1Y
                    )
                ) {
                    isOverlapping = true;
                    break;
                }
            }
            if (isOverlapping) {
                continue;
            }
            this.roomsArr.push({'x1': corner1X, 'y1': corner1Y, 'x2': corner2X, 'y2': corner2Y, 'isMainRoom': isMainRoom});
            for (let y = 0; y < height; y++) {
                for (let x = 0; x < width; x++) {
                    this.carve({
                        'x': corner1X + x,
                        'y': corner1Y + y,
                    });
                }
            }
            break;
        }
    }

    addMaze() {
        // Fill in all of the empty space with mazes.
        for (let y = 1; y < this.height; y += 2) {
            for (let x = 1; x < this.width; x += 2) {
                if (this.structureArr[y][x] != '0') {
                    continue;
                }
                this.growMaze({'x': x, 'y': y});
            }
        }
    }

    // Implementation of the "growing tree" algorithm from here:
    // http://www.astrolog.org/labyrnth/algrithm.htm.
    growMaze(startCoordArr: import('./HunkerInterfaces').Coord) {
        let cellCoordsArr: import('./HunkerInterfaces').Coord[]               = [];
        let lastDirectionLetter: import('./HunkerInterfaces').DirectionLetter = null;

        // this.startRegion();
        this.carve(startCoordArr);

        cellCoordsArr.push(startCoordArr);
        while (cellCoordsArr.length > 0) {
            let cellCoordArr = cellCoordsArr[cellCoordsArr.length - 1];

            // See which adjacent cells are open.
            let unmadeCellDirectionsArr: import('./HunkerInterfaces').DirectionLetter[] = [];

            for (let directionLetter of Utils.directionsArr) {
                if (this.canCarve(cellCoordArr, directionLetter)) {
                    unmadeCellDirectionsArr.push(directionLetter);
                }
            }

            if (unmadeCellDirectionsArr.length > 0) {
                // Based on how "windy" passages are, try to prefer carving in the
                // same direction.
                let directionLetter;
                if (lastDirectionLetter && unmadeCellDirectionsArr.indexOf(lastDirectionLetter) != -1 && (!Dungeon.windingPercent || Math.random() > Dungeon.windingPercent)) {
                    directionLetter = lastDirectionLetter;
                } else {
                    directionLetter = Utils.randElFromArr(unmadeCellDirectionsArr);
                }

                this.carve(Utils.getNewCoordFromDirection(cellCoordArr, directionLetter, 1, true));
                this.carve(Utils.getNewCoordFromDirection(cellCoordArr, directionLetter, 2, true));

                cellCoordsArr.push(Utils.getNewCoordFromDirection(cellCoordArr, directionLetter, 2, true));
                lastDirectionLetter = directionLetter;
            } else {
                // No adjacent uncarved cells.
                cellCoordsArr.pop();

                // This path has ended.
                lastDirectionLetter = null;
            }
        }
    }

    // Gets whether or not an opening can be carved from the given starting
    // [Cell] at [pos] to the adjacent Cell facing [direction]. Returns `true`
    // if the starting Cell is in bounds and the destination Cell is filled
    // (or out of bounds).
    canCarve(coordArr: import('./HunkerInterfaces').Coord, directionLetter: import('./HunkerInterfaces').DirectionLetter) {
        // Must end in bounds.
        const coordArrIn3 = Utils.getNewCoordFromDirection(coordArr, directionLetter, 3, true);
        if (coordArrIn3['x'] < 0 || coordArrIn3['y'] < 0 || coordArrIn3['x'] >= this.width || coordArrIn3['y'] >= this.height) {
            return false;
        }

        // Destination must not be open.
        const coordArrIn2 = Utils.getNewCoordFromDirection(coordArr, directionLetter, 2, true);
        return this.structureArr[coordArrIn2['y']][coordArrIn2['x']] != ' ';
    }

    carve(coordArr: import('./HunkerInterfaces').Coord) {
        this.structureArr[coordArr['y']][coordArr['x']]      = ' ';
        // this.structureFloorArr[coordArr['y']][coordArr['x']] = 'D';
        // this.regionsArr[coordArr['x'] + ',' + coordArr['y']] = this.currentRegion;
    }

    addDoorsToRooms() {
        for (let i in this.roomsArr) {
            let numDoors;
            if (this.roomsArr[i]['isMainRoom']) {
                numDoors = 4;
            } else {
                numDoors = (Math.random() < Dungeon.extraConnectorChance) ? 2 : 1;
            }
            for (let j = 0; j < numDoors; j++) {
                for (let numIterations = 0; numIterations < 100; numIterations++) {
                    let coordToCarveArr: import('./HunkerInterfaces').Coord;
                    let directionLetter: import('./HunkerInterfaces').DirectionLetter;
                    if (Utils.randBool()) {
                        let y;
                        if (Utils.randBool()) {
                            directionLetter = 'n';
                            y               = this.roomsArr[i]['y1'];
                        } else {
                            directionLetter = 's';
                            y               = this.roomsArr[i]['y2'];
                        }
                        coordToCarveArr = {
                            'x': Utils.randBetween(this.roomsArr[i]['x1'] + 1, this.roomsArr[i]['x2'] - 1),
                            'y': y,
                        };
                    } else {
                        let x;
                        if (Utils.randBool()) {
                            directionLetter = 'w';
                            x               = this.roomsArr[i]['x1'];
                        } else {
                            directionLetter = 'e';
                            x               = this.roomsArr[i]['x2'];
                        }
                        coordToCarveArr = {
                            'x': x,
                            'y': Utils.randBetween(this.roomsArr[i]['y1'] + 1, this.roomsArr[i]['y2'] - 1),
                        };
                    }
                    if (
                        coordToCarveArr['x'] >= 1
                        &&
                        coordToCarveArr['y'] >= 1
                        &&
                        coordToCarveArr['x'] < this.width - 1
                        &&
                        coordToCarveArr['y'] < this.height - 1
                        &&
                        this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] == '0'
                    ) {
                        const coordArrIn2 = Utils.getNewCoordFromDirection(coordToCarveArr, directionLetter, 1, true);
                        if (
                            coordArrIn2['x'] >= 1
                            &&
                            coordArrIn2['y'] >= 1
                            &&
                            coordArrIn2['x'] < this.width - 1
                            &&
                            coordArrIn2['y'] < this.height - 1
                            &&
                            this.structureArr[coordArrIn2['y']][coordArrIn2['x']] == ' '
                        ) {
                            this.structureFloorArr[coordToCarveArr['y']][coordToCarveArr['x']] = 'D';
                            if (this.roomsArr[i]['isMainRoom']) {
                                this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] = 'd'; // Always doors on main room.
                            } else {
                                if (Utils.randBetween(1, 4) === 1) {
                                    this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] = ' ';
                                } else {
                                    this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] = 'd';
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    removeDeadEnds() {
        let isDone = false;

        while (!isDone) {
            isDone = true;

            for (let y = 1; y < this.height - 1; y++) {
                for (let x = 1; x < this.width - 1; x++) {
                    if (this.structureArr[y][x] == '0') {
                        continue;
                    }

                    // If it only has one exit, it's a dead end.
                    let exitsNum = 0;
                    for (let directionLetter of Utils.directionsArr) {
                        const newCoordArr = Utils.getNewCoordFromDirection({'x': x, 'y': y}, directionLetter, 1, true);
                        if (this.structureArr[newCoordArr['y']][newCoordArr['x']] != '0') {
                            exitsNum++;
                        }
                    }

                    if (exitsNum != 1) {
                        continue;
                    }

                    isDone                       = false;
                    this.structureArr[y][x]      = '0';
                    this.structureFloorArr[y][x] = '0';
                }
            }
        }
    }

    addDecorations() {
        // Surround rooms with walls.
        for (let i in this.roomsArr) {
            const wallLetter = (this.roomsArr[i]['isMainRoom']) ? 'b' : 'w';
            for (let y = this.roomsArr[i]['y1'] - 1; y < this.roomsArr[i]['y2'] + 1; y++) {
                // @formatter:off
                if (this.structureArr[y][this.roomsArr[i]['x1'] - 1] == '0') { this.structureArr[y][this.roomsArr[i]['x1'] - 1] = wallLetter; }
                if (this.structureArr[y][this.roomsArr[i]['x2']    ] == '0') { this.structureArr[y][this.roomsArr[i]['x2']    ] = wallLetter; }
                // @formatter:on
            }
            for (let x = this.roomsArr[i]['x1'] - 1; x < this.roomsArr[i]['x2'] + 1; x++) {
                // @formatter:off
                if (this.structureArr[this.roomsArr[i]['y1'] - 1][x] == '0') { this.structureArr[this.roomsArr[i]['y1'] - 1][x] = wallLetter; }
                if (this.structureArr[this.roomsArr[i]['y2']    ][x] == '0') { this.structureArr[this.roomsArr[i]['y2']    ][x] = wallLetter; }
                // @formatter:on
            }

            // Add lootcrates.
            const roomTilesNum = (this.roomsArr[i]['x1'] - this.roomsArr[i]['x2']) * (this.roomsArr[i]['y1'] - this.roomsArr[i]['y2']);
            let numLootcrates  = Math.round(roomTilesNum * 0.05);
            numLootcrates      = Math.min(numLootcrates, 5);
            numLootcrates      = Math.max(numLootcrates, 1);
            if (this.roomsArr[i]['isMainRoom']) {
                numLootcrates *= 3;
            }
            for (let j = 0; j < numLootcrates; j++) {
                for (let numIterations = 0; numIterations < 100; numIterations++) {
                    const lootcrateCoordX = Utils.randBetween(this.roomsArr[i]['x1'] + 1, this.roomsArr[i]['x2'] - 1);
                    const lootcrateCoordY = Utils.randBetween(this.roomsArr[i]['y1'] + 1, this.roomsArr[i]['y2'] - 1);
                    let hasBlocksInRadius = false;
                    for (let y = Math.max(lootcrateCoordY - 1, 0); y <= Math.min(lootcrateCoordY + 1, Utils.worldHeight - 1) && !hasBlocksInRadius; y++) {
                        for (let x = Math.max(lootcrateCoordX - 1, 0); x <= Math.min(lootcrateCoordX + 1, Utils.worldWidth - 1) && !hasBlocksInRadius; x++) {
                            if (this.structureArr[y][x] != ' ') {
                                hasBlocksInRadius = true;
                            }
                        }
                    }
                    if (!hasBlocksInRadius) {
                        this.structureArr[lootcrateCoordY][lootcrateCoordX] = this.getLootcrateId();
                        break;
                    }
                }
            }
        }

        // Add red torches.
        let numTorches = Math.round(this.numTiles * 0.01);
        for (let i = 0; i < numTorches; i++) {
            for (let numIterations = 0; numIterations < 100; numIterations++) {
                const torchCoordX = Utils.randBetween(1, this.width - 2);
                const torchCoordY = Utils.randBetween(1, this.height - 2);
                if (this.structureArr[torchCoordY][torchCoordX] == ' ') {
                    this.structureArr[torchCoordY][torchCoordX] = 'r';
                    break;
                }
            }
        }
    }

    getLootcrateId(): string {
        return Utils.randElFromArr(Dungeon.lootcrateIdsArr[this.layerNum]);
    }

}

if (typeof module !== 'undefined') {
    module.exports = Dungeon;
}
