const Utils = require('./Utils');
import {UtilsServer} from './UtilsServer';
import {PlayerServer} from './PlayerServer';

export class DayNightCycleServer {

    static stage: 'day' | 'night'      = 'day';
    static gameStartMillis: number     = null;
    static nextTimeout: NodeJS.Timeout = null;

    static startDay() {
        const updatePlayerScoresArr: { [playerId: string]: number } = {};
        for (let playerId in PlayerServer.allPlayerObjs) {
            const playerObj = PlayerServer.allPlayerObjs[playerId];
            if (!playerObj.isGhost) {
                playerObj.playerScore += UtilsServer.playerSurviveNightPoints;
                updatePlayerScoresArr[playerId] = playerObj.playerScore;
                playerObj.nightsSurvivedNum++;
                UtilsServer.io.to(playerId).emit('nightSurvived', {
                    'nightsSurvivedNum': playerObj.nightsSurvivedNum,
                });
                if (playerObj.userName) {
                    UtilsServer.redisClient.hset('player:' + playerObj.userName, 'nightsSurvivedNum', playerObj.nightsSurvivedNum.toString());
                }
            }
        }
        UtilsServer.io.emit('startDay', {
            'status':                'success',
            'updatePlayerScoresArr': updatePlayerScoresArr,
        }); // Emit message to all players.
        console.log('startDay');
        DayNightCycleServer.stage       = 'day';
        DayNightCycleServer.nextTimeout = setTimeout(function () {
            DayNightCycleServer.startNight();
        }, Utils.dayLengthMillis);
    }

    static startNight() {
        UtilsServer.io.emit('startNight'); // Emit message to all players.
        console.log('startNight');
        DayNightCycleServer.stage       = 'night';
        DayNightCycleServer.nextTimeout = setTimeout(function () {
            DayNightCycleServer.startDay();
        }, Utils.nightLengthMillis);
    }

}
