/// <reference path="../Utils.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="ChatClient.ts" />
/// <reference path="DayNightCycleClient.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="Globals.ts" />
/// <reference path="../../node_modules/phaser/types/phaser.d.ts" />

// https://gamemechanicexplorer.com/#lighting-3
class LightCircle {

    static isBrightening: boolean;
    static shadowTexture: Phaser.Textures.CanvasTexture;
    static shadowSprite: Phaser.GameObjects.Image;
    static allObjsArr: { [id: string]: LightCircle } = {};
    static currentUpdateTickNum: number              = 0;
    static nextUpdateTickNum: number                 = 0;
    static shadowRgb: string                         = 'rgb(0, 0, 0)';

    static readonly defaultLightTick: number   = 3;
    static lightTick: number                   = LightCircle.defaultLightTick;
    static readonly updateShadowMillis: number = 2000;

    public layerNum: number;
    public x: number;
    public y: number;
    public isShowing: boolean;
    public lightCircleRadius: number;
    public lightCircleTint: string;
    private readonly id: string;

    constructor(layerNum: number, x: number, y: number, lightCircleRadius: number, lightCircleTint?: string, id: string = layerNum + ':' + x + ',' + y) {
        this.id                         = id;
        this.x                          = (x * Utils.tileSize) + (Utils.tileSize / 2);
        this.y                          = (y * Utils.tileSize) + (Utils.tileSize / 2);
        this.layerNum                   = layerNum;
        this.lightCircleRadius          = lightCircleRadius;
        this.lightCircleTint            = lightCircleTint;
        this.isShowing                  = true;
        LightCircle.allObjsArr[this.id] = this;
    }

    static init(): void {
        if (localStorage.getItem('isBrightening') == '1') {
            ChatClient.brighten();
        }
        LightCircle.shadowTexture      = Globals.game.textures.createCanvas('shadowTexture', window.innerWidth, window.innerHeight);
        LightCircle.shadowSprite       = Globals.game.add.image(0, 0, 'shadowTexture');
        LightCircle.shadowSprite.depth = UtilsClient.depthsArr['light'];
        LightCircle.shadowSprite.setOrigin(0);
        LightCircle.shadowSprite.setScrollFactor(0); // Fixes to camera
        LightCircle.shadowSprite.setBlendMode(Phaser.BlendModes.MULTIPLY);
    }

    static updateShadowTexture(): void {
        LightCircle.currentUpdateTickNum++;
        if (LightCircle.currentUpdateTickNum >= LightCircle.nextUpdateTickNum) {
            LightCircle.shadowTexture.context.fillStyle = LightCircle.shadowRgb;
            LightCircle.shadowTexture.context.fillRect(0, 0, window.innerWidth, window.innerHeight);
            if (parseInt(LightCircle.shadowRgb.slice(4, LightCircle.shadowRgb.indexOf(','))) <= 25) {
                for (const coordStr in LightCircle.allObjsArr) {
                    const lightCircleObj = LightCircle.allObjsArr[coordStr];
                    if (
                        lightCircleObj.isShowing
                        &&
                        lightCircleObj.layerNum == WorldClient.currentLayerNum
                        &&
                        lightCircleObj.lightCircleRadius
                        &&
                        lightCircleObj.x >= Globals.game.cameras.main.scrollX - lightCircleObj.lightCircleRadius
                        &&
                        lightCircleObj.y >= Globals.game.cameras.main.scrollY - lightCircleObj.lightCircleRadius
                        &&
                        lightCircleObj.x <= Globals.game.cameras.main.scrollX + window.innerWidth + lightCircleObj.lightCircleRadius
                        &&
                        lightCircleObj.y <= Globals.game.cameras.main.scrollY + window.innerHeight + lightCircleObj.lightCircleRadius
                    ) {
                        let lightCircleRadius = lightCircleObj.lightCircleRadius;
                        if (lightCircleObj.lightCircleTint != 'blue') {
                            lightCircleRadius += Utils.randBetween(1, 20);
                        }
                        const lightCircleObjRelX = lightCircleObj.x - Globals.game.cameras.main.scrollX;
                        const lightCircleObjRelY = lightCircleObj.y - Globals.game.cameras.main.scrollY;
                        const gradient           = LightCircle.shadowTexture.context.createRadialGradient(
                            lightCircleObjRelX, lightCircleObjRelY, 0,
                            lightCircleObjRelX, lightCircleObjRelY, lightCircleRadius
                        );
                        if (lightCircleObj.lightCircleTint == 'red') {
                            gradient.addColorStop(0.0, 'rgba(150, 50, 50, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 50, 15, 15, 0.0)');
                        } else if (lightCircleObj.lightCircleTint == 'orange') {
                            gradient.addColorStop(0.0, 'rgba(150, 75,  0, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 50, 15, 15, 0.0)');
                        } else if (lightCircleObj.lightCircleTint == 'cyan') {
                            gradient.addColorStop(0.0, 'rgba(100, 255, 255, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 15,  50,  50, 0.0)');
                        } else if (lightCircleObj.lightCircleTint == 'blue') {
                            gradient.addColorStop(0.0, 'rgba( 50, 100, 255, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 15,  50,  50, 0.0)');
                        } else {
                            gradient.addColorStop(0.0, 'rgba(150, 150, 150, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 50,  50,  50, 0.0)');
                        }

                        LightCircle.shadowTexture.context.beginPath();
                        LightCircle.shadowTexture.context.fillStyle = gradient;
                        LightCircle.shadowTexture.context.arc(lightCircleObjRelX, lightCircleObjRelY, lightCircleRadius, 0, Math.PI * 2);
                        LightCircle.shadowTexture.context.fill();
                    }
                }
            }

            LightCircle.shadowTexture.refresh();
            LightCircle.nextUpdateTickNum += LightCircle.lightTick;
        }
    }

    static updateShadowRgb(): void {
        if (LightCircle.isBrightening) {
            LightCircle.shadowRgb = 'rgb(255, 255, 255)';
        } else {
            if (WorldClient.currentLayerNum == 0) { // Overworld.
                const startNum = parseInt(LightCircle.shadowRgb.slice(4, LightCircle.shadowRgb.indexOf(',')));
                let endNum     = 25; // Nighttime.
                if (DayNightCycleClient.stage == 'day') { // Daytime.
                    if (Globals.currentPlayerObj && Globals.currentPlayerObj.isGhost) {
                        endNum = 25;
                    } else {
                        endNum = 255;
                    }
                }
                if (startNum != endNum) {
                    const shadowBlend = {'step': 0};
                    Globals.game.add.tween({
                        'targets':  shadowBlend,
                        'step':     100,
                        'duration': LightCircle.updateShadowMillis,
                        'onUpdate': function () {
                            const newColorObj     = Phaser.Display.Color.Interpolate.RGBWithRGB(startNum, startNum, startNum, endNum, endNum, endNum, 100, shadowBlend['step']);
                            LightCircle.shadowRgb = 'rgb(' + newColorObj.r + ', ' + newColorObj.g + ', ' + newColorObj.b + ')';
                        }
                    });
                    setTimeout(function () {
                        LightCircle.shadowRgb = 'rgb(' + endNum + ', ' + endNum + ', ' + endNum + ')';
                    }, LightCircle.updateShadowMillis);
                }
            } else { // Caves.
                LightCircle.shadowRgb = 'rgb(0, 0, 0)';
            }
        }
    }

    destroy(): void {
        delete LightCircle.allObjsArr[this.id];
    }

}
