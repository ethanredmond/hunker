/// <reference path="generated/Items.ts" />
/// <reference path="../Utils.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="InventoryModal.ts" />
/// <reference path="Crafting.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="Modals.ts" />

class HotbarClient {

    static readonly letterShortcutsArr: string[] = [
        'F',
        'R',
        'Z',
        'X',
        'C',
        'V',
        'Y',
        'G',
        'T',
    ];
    static $hotbarItems: JQuery;
    static prevPlayerItemsCountArr: { [itemId: string]: number };

    static init(): void {
        const $hotbarContainer = $("<div id=\"hotbarContainer\"></div>").appendTo('body');
        this.$hotbarItems      = $("<div id=\"hotbarItems\" class=\"itemRow\" data-item-row-type=\"hotbar\"></div>").appendTo($hotbarContainer);

        $(document).on('click', '#hotbarItems .item', function () {
            HotbarClient.changeItem($(this).parent().index());
        });

        $(document).on('mousedown', '.itemRow[data-item-row-type!="crafting"] .itemSlot:not(.disabled)', HotbarClient.shiftItemHandler);

        $('body').bind('mousewheel DOMMouseScroll', function (event) {
            if (!event.ctrlKey) {
                let itemToSelectIndex = $('#hotbarItems .itemSlot.highlighted').index();
                // @ts-ignore
                if (event.originalEvent.wheelDelta > 0) {
                    itemToSelectIndex--;
                    if (itemToSelectIndex < 0) {
                        itemToSelectIndex = 8;
                    }
                } else {
                    itemToSelectIndex++;
                    if (itemToSelectIndex > 8) {
                        itemToSelectIndex = 0;
                    }
                }
                HotbarClient.changeItem(itemToSelectIndex);
            }
            return false;
        });
    }

    static shiftItemHandler(event: JQuery.Event): void {
        if (event.shiftKey) {
            const $shiftFromItemSlot = $(this);
            const shiftFromItemId    = $shiftFromItemSlot.find('.item').data('itemId');
            if (shiftFromItemId) {
                let shiftFromItemType  = $shiftFromItemSlot.parent().data('itemRowType');
                let shiftFromItemIndex = $shiftFromItemSlot.index();
                if ($shiftFromItemSlot.parent().has('.enchantingTableButton').length) {
                    shiftFromItemIndex--;
                }
                if (shiftFromItemType == 'inventory') {
                    shiftFromItemType = 'hotbar';
                    shiftFromItemIndex += 9;
                }
                let shiftToItemType: import('../HunkerInterfaces').ItemRowType = null;
                let shiftToItemIndex                                           = null;
                if (shiftFromItemType == 'hotbar') {
                    const $containerItemRow = $('.itemRow[data-item-row-type="container"]');
                    // Shift from hotbar to container.
                    if ($containerItemRow.length && $containerItemRow.is(':visible')) {
                        shiftToItemType = 'container';
                        if (Globals.currentPlayerObj.markerTileId == 'hearth' || Globals.currentPlayerObj.markerTileId == 'hearth_lit') {
                            if ((!InventoryModal.containerArr[0]['itemId'] || InventoryModal.containerArr[0]['itemId'] == shiftFromItemId) && Items.itemsArr[shiftFromItemId]['burnTimeSecs']) {
                                shiftToItemIndex = 0;
                            } else if ((!InventoryModal.containerArr[1]['itemId'] || InventoryModal.containerArr[1]['itemId'] == shiftFromItemId) && Items.itemsArr[shiftFromItemId]['smeltingOutputItemId']) {
                                shiftToItemIndex = 1;
                            }
                        } else {
                            shiftToItemIndex = Utils.getNewItemIndex(InventoryModal.containerArr, shiftFromItemId);
                        }
                    }
                    // Shift from hotbar to accessories.
                    if (shiftToItemIndex == null && Items.itemsArr[shiftFromItemId]['accessoriesSlotIndexesArr']) {
                        for (const i in Items.itemsArr[shiftFromItemId]['accessoriesSlotIndexesArr']) {
                            const accessorySlotIndex = Items.itemsArr[shiftFromItemId]['accessoriesSlotIndexesArr'][i];
                            if (!Globals.currentPlayerObj.accessoryItemsArr[accessorySlotIndex]['itemId'] || Globals.currentPlayerObj.accessoryItemsArr[accessorySlotIndex]['itemCount'] <= 0) {
                                shiftToItemType  = 'accessories';
                                shiftToItemIndex = accessorySlotIndex;
                                break;
                            }
                        }
                    }
                    if (shiftToItemIndex == null) {
                        if (shiftFromItemIndex < 9) {
                            shiftToItemIndex = Utils.getNewItemIndex(Globals.currentPlayerObj.hotbarItemsArr.slice(9), shiftFromItemId);
                            if (shiftToItemIndex != null) {
                                shiftToItemIndex += 9;
                            }
                            shiftToItemType = 'hotbar'; // Shift from hotbar to "inventory".
                        } else {
                            shiftToItemIndex = Utils.getNewItemIndex(Globals.currentPlayerObj.hotbarItemsArr.slice(0, 9), shiftFromItemId);
                            shiftToItemType  = 'hotbar'; // Shift from "inventory" to hotbar.
                        }
                    }
                } else if ((shiftFromItemType == 'container' || shiftFromItemType == 'accessories') && Globals.currentPlayerObj) {
                    shiftToItemType  = 'hotbar'; // Shift from container or accessories to hotbar.
                    shiftToItemIndex = Utils.getNewItemIndex(Globals.currentPlayerObj.hotbarItemsArr, shiftFromItemId);
                }
                if (shiftFromItemType && shiftFromItemIndex != null && shiftToItemType && shiftToItemIndex != null) {
                    const swapItemData: import('../HunkerInterfaces').SwapItemData = {
                        'swapToItemType':    shiftToItemType,
                        'swapToItemIndex':   shiftToItemIndex,
                        'swapFromItemType':  shiftFromItemType,
                        'swapFromItemIndex': shiftFromItemIndex,
                    };
                    if (swapItemData['swapFromItemType'] == 'container' || swapItemData['swapToItemType'] == 'container') {
                        swapItemData['swapItemContainerCoordArr'] = {
                            'x': Globals.currentPlayerObj.markerTileX,
                            'y': Globals.currentPlayerObj.markerTileY,
                        };
                    }
                    Globals.socket.emit('swapItem', swapItemData, UtilsClient.handleSocketEmitResponse);
                }
            }
        }
    }

    static keydown(event: JQuery.KeyDownEvent): boolean {
        // @formatter:off
        if (event.key === '1' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[0]) { HotbarClient.changeItem(0); return false; }
        if (event.key === '2' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[1]) { HotbarClient.changeItem(1); return false; }
        if (event.key === '3' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[2]) { HotbarClient.changeItem(2); return false; }
        if (event.key === '4' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[3]) { HotbarClient.changeItem(3); return false; }
        if (event.key === '5' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[4]) { HotbarClient.changeItem(4); return false; }
        if (event.key === '6' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[5]) { HotbarClient.changeItem(5); return false; }
        if (event.key === '7' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[6]) { HotbarClient.changeItem(6); return false; }
        if (event.key === '8' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[7]) { HotbarClient.changeItem(7); return false; }
        if (event.key === '9' || event.key.toUpperCase() === HotbarClient.letterShortcutsArr[8]) { HotbarClient.changeItem(8); return false; }
        // @formatter:on
        if (event.key === 'q' || event.key === 'Q') {
            Globals.currentPlayerObj.dropItem(Globals.currentPlayerObj.selectedItemIndex, event.key === 'Q');
            return false;
        }
        // } else if (event.key === 'e' || event.key === 'E') {
        //     Globals.currentPlayerObj.takeItem(event.key === 'E');
        //     return false;
        // }
    }

    static changeItem(itemToSelectIndex: number, isSkippingEmit: boolean = false) {
        if (
            Globals.currentPlayerObj
            &&
            Globals.currentPlayerObj.hotbarItemsArr.length
            &&
            !Globals.currentPlayerObj.isCrafting // Not crafting.
            &&
            !Globals.currentPlayerObj.isGhost // Not a ghost.
            &&
            itemToSelectIndex < Globals.currentPlayerObj.hotbarItemsArr.length
            &&
            itemToSelectIndex in Globals.currentPlayerObj.hotbarItemsArr
        ) {
            const $itemToSelect = $('#hotbarItems .itemSlot:nth-child(' + (itemToSelectIndex + 1) + ')');
            $('#hotbarItems .itemSlot.highlighted').removeClass('highlighted');
            $itemToSelect.addClass('highlighted');
            if (isSkippingEmit) {
                Globals.currentPlayerObj.selectedItemIndex = itemToSelectIndex;
                $('#hotbarItems .itemSlot.selectedItem').removeClass('selectedItem');
                $itemToSelect.addClass('selectedItem');
            } else {
                Globals.socket.emit('selectItem', itemToSelectIndex, function (response: import('../HunkerInterfaces').ServerResponse) {
                    UtilsClient.handleSocketEmitResponse(response, function () {
                        Globals.currentPlayerObj.selectedItemIndex = response['selectedItemIndex'];
                        Globals.currentPlayerObj.selectItem(Globals.currentPlayerObj.hotbarItemsArr[Globals.currentPlayerObj.selectedItemIndex]['itemId']);
                        $('#hotbarItems .itemSlot.selectedItem').removeClass('selectedItem');
                        $itemToSelect.addClass('selectedItem');
                    });
                });
            }
        }
    }

    static update() {
        if (Globals.currentPlayerObj) {
            HotbarClient.addNewItemSprites();

            if (Globals.currentPlayerObj.selectedItemId != Globals.currentPlayerObj.hotbarItemsArr[Globals.currentPlayerObj.selectedItemIndex]['itemId']) {
                Globals.currentPlayerObj.selectItem(Globals.currentPlayerObj.hotbarItemsArr[Globals.currentPlayerObj.selectedItemIndex]['itemId']);
            }
            const hotbarItemRowHtml = HotbarClient.getItemRowHtml(Globals.currentPlayerObj.hotbarItemsArr.slice(0, 9), true);
            this.$hotbarItems.html(hotbarItemRowHtml);
            HotbarClient.changeItem(Globals.currentPlayerObj.selectedItemIndex, true);
            HotbarClient.updateDraggable('hotbarContainer');
            if (Modals.allModalElements['inventory'] && Modals.allModalElements['inventory'].style.display != 'none') {
                $('#inventoryModal .itemRow[data-item-row-type="crafting"]').html(InventoryModal.getCraftingItemsHtml());
                $('#inventoryModal .itemRow[data-item-row-type="accessories"]').html(HotbarClient.getItemRowHtml(Globals.currentPlayerObj.accessoryItemsArr));
                $('#inventoryModal .itemRow[data-item-row-type="inventory"]').html(HotbarClient.getItemRowHtml(Globals.currentPlayerObj.hotbarItemsArr.slice(9)));
                $('#inventoryModal .itemRow[data-item-row-type="hotbar"]').html(HotbarClient.getItemRowHtml(Globals.currentPlayerObj.hotbarItemsArr.slice(0, 9)));
                HotbarClient.updateDraggable('inventoryModal');
            }
        }
    }

    static addNewItemSprites() {
        let playerItemsCountArr = HotbarClient.getPlayerItemsCountArr();
        if (HotbarClient.prevPlayerItemsCountArr) {
            let newItemSpritesNum = 0;
            for (let itemId in playerItemsCountArr) {
                const itemCount     = playerItemsCountArr[itemId];
                const prevItemCount = (itemId in HotbarClient.prevPlayerItemsCountArr) ? HotbarClient.prevPlayerItemsCountArr[itemId] : 0;
                if (itemCount > prevItemCount) {
                    let newItemSprite = HotbarClient.getNewItemSprite();
                    let newItemText   = HotbarClient.getNewItemText();
                    HotbarClient.updateNewItemSprite(newItemSprite, newItemSpritesNum, itemId);
                    HotbarClient.updateNewItemText(newItemText, newItemSpritesNum, itemCount - prevItemCount);

                    setTimeout(function () {
                        newItemSprite.setActive(false).setVisible(false);
                        newItemText.setActive(false).setVisible(false);
                    }, 1000);

                    if (newItemSpritesNum) {
                        setTimeout(function () {
                            SoundEffect.play('pop');
                        }, 10 * newItemSpritesNum);
                    } else {
                        SoundEffect.play('pop');
                    }
                    newItemSpritesNum++;
                }
            }
        }
        HotbarClient.prevPlayerItemsCountArr = Utils.dereferenceObj(playerItemsCountArr);
    }

    static getPlayerItemsCountArr() {
        let playerItemsCountArr: { [itemId: string]: number } = {};
        const playerItemsArr                                  = Globals.currentPlayerObj.hotbarItemsArr.concat(Globals.currentPlayerObj.accessoryItemsArr);
        for (const slotIndex in playerItemsArr) {
            const slotArr = playerItemsArr[slotIndex];
            if (
                slotArr['itemId']
                &&
                slotArr['itemCount']
            ) {
                if (!(slotArr['itemId'] in playerItemsCountArr)) {
                    playerItemsCountArr[slotArr['itemId']] = 0;
                }
                playerItemsCountArr[slotArr['itemId']] += slotArr['itemCount'];
            }
        }
        return playerItemsCountArr;
    }

    static getNewItemSprite(): Phaser.GameObjects.Sprite {
        let newItemSprite: Phaser.GameObjects.Sprite = PlayerClient.newItemSpritesGroup.getFirstDead();
        if (newItemSprite) {
            newItemSprite.setActive(true).setVisible(true);
            newItemSprite.x = Globals.currentPlayerObj.sprite.x;
            newItemSprite.y = Globals.currentPlayerObj.sprite.y;
        } else {
            newItemSprite = Globals.game.add.sprite(Globals.currentPlayerObj.sprite.x, Globals.currentPlayerObj.sprite.y, '');
            PlayerClient.newItemSpritesGroup.add(newItemSprite);
            newItemSprite.depth = UtilsClient.depthsArr['newItem'];
        }
        return newItemSprite;
    }

    static getNewItemText(): Phaser.GameObjects.Text {
        let newItemText: Phaser.GameObjects.Text = PlayerClient.newItemTextsGroup.getFirstDead();
        if (newItemText) {
            newItemText.setActive(true).setVisible(true);
            newItemText.x = Globals.currentPlayerObj.sprite.x;
            newItemText.y = Globals.currentPlayerObj.sprite.y;
        } else {
            newItemText = Globals.game.add.text(Globals.currentPlayerObj.sprite.x, Globals.currentPlayerObj.sprite.y, '', {'font': '20px Germania One', 'fill': '#ffffff', 'align': 'left'});
            PlayerClient.newItemTextsGroup.add(newItemText);
            newItemText.depth = UtilsClient.depthsArr['newItem'];
        }
        return newItemText;
    }

    static updateNewItemSprite(newItemSprite: Phaser.GameObjects.Sprite, newItemSpritesNum: number, itemId: import('../HunkerInterfaces').ItemId): void {
        newItemSprite.setTexture(itemId);
        newItemSprite.alpha         = 1;
        newItemSprite.displayWidth  = 32;
        newItemSprite.displayHeight = 32;
        const originX               = 1.00;
        const originY               = 1.6 + newItemSpritesNum;
        newItemSprite.setOrigin(originX, originY);
        Globals.game.add.tween({
            'targets':  newItemSprite,
            'alpha':    0,
            'duration': 1000,
        });
        const newItemSpriteTween = {'step': 0};
        Globals.game.add.tween({
            'targets':  newItemSpriteTween,
            'step':     100,
            'duration': 1000,
            'onUpdate': function () {
                newItemSprite.setOrigin(originX, originY + (newItemSpriteTween['step'] / 100));
            },
        });
        // game.add.tween(newItemSprite.scale).to({x: 0.3, y: 0.3}, 250, Phaser.Easing.Bounce.In, true);
        // setTimeout(function () {
        //     game.add.tween(newItemSprite.scale).to({x: 0.25, y: 0.25}, 250, Phaser.Easing.Bounce.Out, true);
        // }, 250);
        // // game.add.tween(newItemSprite.scale).to({x: 1.5, y: 1.5}, 1000, Phaser.Easing.Bounce.None, true);
        // setTimeout(function () {
        //     game.add.tween(newItemSprite).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
        // }, 500);
    }

    static updateNewItemText(newItemText: Phaser.GameObjects.Text, newItemSpritesNum: number, newItemsCount: number): void {
        newItemText.alpha = 1;
        const originX     = -0.45;
        const originY     = 2.05 + newItemSpritesNum;
        newItemText.setOrigin(originX, originY);
        newItemText.setText('+' + newItemsCount);
        Globals.game.add.tween({
            'targets':  newItemText,
            'alpha':    0,
            'duration': 1000,
        });
        const newItemTextTween = {'step': 0};
        Globals.game.add.tween({
            'targets':  newItemTextTween,
            'step':     100,
            'duration': 1000,
            'onUpdate': function () {
                newItemText.setOrigin(originX, originY + (newItemTextTween['step'] / 100));
            },
        });
    }

    static getItemRowHtml(itemRowArr: import('../HunkerInterfaces').HotbarSlot[], isHotbar = false, isDisabled = false, isHearth = false, isGrindstone = false) {
        let out = '';
        let i   = 0;
        for (const slotIndex in itemRowArr) {
            let itemSlot = itemRowArr[slotIndex];
            out += HotbarClient.getItemHtml(
                (itemSlot ? itemSlot['itemId'] : null),
                (itemSlot ? itemSlot['itemCount'] : null),
                (itemSlot ? itemSlot['itemDurability'] : null),
                (itemSlot ? itemSlot['itemEnchantmentsArr'] : null),
                (Globals.currentPlayerObj.selectedItemIndex == i && isHotbar),
                isDisabled,
                (i < 9 && isHotbar ? HotbarClient.letterShortcutsArr[i] : ''),
                isHearth,
                isGrindstone,
                parseInt(slotIndex),
            );
            i++;
        }
        return out;
    }

    static updateDraggable(containerId: 'hotbarContainer' | 'inventoryModal') {
        const $draggableEl = $('#' + containerId + ' .itemRow[data-item-row-type!="crafting"] .itemSlot:not(.disabled) .item');
        $draggableEl.draggable({
            drag:   function () {
                if ((<HTMLElement>Modals.allModalObjs['gamePaused'].el).style.display == 'block') {
                    return false;
                }
            },
            revert: function (isDropSuccessful: boolean) {
                if (!isDropSuccessful) {
                    let itemToDropIndex = $(this).parent().index();
                    const itemRowType   = $(this).parent().parent().data('itemRowType');
                    if (itemRowType == 'inventory') {
                        itemToDropIndex += 9;
                    }
                    if (itemToDropIndex >= 0) {
                        Globals.currentPlayerObj.dropItem(itemToDropIndex, true, itemRowType);
                    }
                }
            },
            start:  function () {
                if (Globals.currentPlayerObj) {
                    Globals.currentPlayerObj.isDragging = true;
                }
            },
            stop:   function () {
                if (Globals.currentPlayerObj) {
                    Globals.currentPlayerObj.isDragging = false;
                }
            },
            helper:      function (event: JQuery.KeyDownEvent) {
                return $(event.currentTarget.outerHTML).addClass('dragItem');
            },
            containment: 'body',
            opacity:     1,
            zIndex:      1,
            scroll:      false,
            cursor:      'none',
            cursorAt:    {
                top:  10,
                left: 10,
            },
        });
        if (Globals.currentPlayerObj && (Globals.currentPlayerObj.isCrafting || Globals.currentPlayerObj.isGhost)) {
            $draggableEl.draggable('disable');
        }

        $('#' + containerId + ' .itemRow[data-item-row-type!="crafting"] .itemSlot:not(.disabled)').droppable({
            accept:      '.item',
            activeClass: 'dragActive',
            hoverClass:  'dragOver',
            drop:        function (event, ui) {
                const $swapToItemSlot   = $(this);
                const $swapToItem       = $swapToItemSlot.find('.item');
                const $swapFromItem     = ui.draggable;
                const $swapFromItemSlot = ui.draggable.parent();
                if ($swapToItem.length) {
                    $swapFromItemSlot.html($swapToItem.get(0).outerHTML);
                }
                $swapToItemSlot.html($swapFromItem.get(0).outerHTML);
                let swapFromItemType  = $swapFromItemSlot.parent().data('itemRowType');
                let swapFromItemIndex = $swapFromItemSlot.index();
                if ($swapFromItemSlot.parent().has('.enchantingTableButton').length) {
                    swapFromItemIndex--;
                }
                if (swapFromItemType == 'inventory') {
                    swapFromItemType = 'hotbar';
                    swapFromItemIndex += 9;
                }
                let swapToItemType  = $swapToItemSlot.parent().data('itemRowType');
                let swapToItemIndex = $swapToItemSlot.index();
                if ($swapToItemSlot.parent().has('.enchantingTableButton').length) {
                    swapToItemIndex--;
                }
                if (swapToItemType == 'inventory') {
                    swapToItemType = 'hotbar';
                    swapToItemIndex += 9;
                }
                const swapItemData: import('../HunkerInterfaces').SwapItemData = {
                    'swapFromItemType':  swapFromItemType,
                    'swapFromItemIndex': swapFromItemIndex,
                    'swapToItemType':    swapToItemType,
                    'swapToItemIndex':   swapToItemIndex,
                };
                if (swapItemData['swapFromItemType'] == 'container' || swapItemData['swapToItemType'] == 'container') {
                    swapItemData['swapItemContainerCoordArr'] = {
                        'x': Globals.currentPlayerObj.markerTileX,
                        'y': Globals.currentPlayerObj.markerTileY,
                    };
                }
                if (swapItemData['swapFromItemIndex'] != swapItemData['swapToItemIndex'] || swapItemData['swapFromItemType'] != swapItemData['swapToItemType']) {
                    Globals.socket.emit('swapItem', swapItemData, UtilsClient.handleSocketEmitResponse);
                } else {
                    HotbarClient.update();
                }
            },
        });
    }

    static getItemHtml(
        itemId: import('../HunkerInterfaces').ItemId,
        itemCount: number,
        itemDurability: number,
        itemEnchantmentsArr: import('../HunkerInterfaces').ItemEnchantmentsArr,
        isSelected: boolean,
        isDisabled: boolean          = false,
        hotbarShortcutLetter: string = null,
        isHearth: boolean            = false,
        isGrindstone: boolean        = false,
        slotIndex: number            = null,
    ) {
        const itemTooltip = Crafting.getItemTooltip(itemId, itemDurability, itemEnchantmentsArr);
        let itemHtml      = '';
        itemHtml += "<div class=\"itemSlot" + (isDisabled ? ' disabled' : '') + (isSelected ? ' selectedItem' : '') + "\">";
        if (hotbarShortcutLetter) {
            itemHtml += " <span class=\"hotbarShortcutLetter\">" + hotbarShortcutLetter + "</span>";
        }
        if (itemId && itemCount) {
            if (!(itemId in Items.itemsArr) || !Items.itemsArr[itemId]['itemSrc']) {
                console.log(itemId, 'invalid');
                return '';
            }
            itemHtml += " <div class=\"item btn-flat\" data-item-id=\"" + itemId + "\" title=\"" + itemTooltip + "\">";
            itemHtml += "  <img class=\"itemImg" + (itemEnchantmentsArr && Object.keys(itemEnchantmentsArr).length ? ' enchantedItem' : '') + "\" src=\"/public" + Items.itemsArr[itemId]['itemSrc'] + "\" alt=\"" + Items.itemsArr[itemId]['label'] + "\">";
            if (itemCount && itemCount > 1) {
                itemHtml += "  <span class=\"itemCount\">" + itemCount + "</span>";
            }
            itemHtml += " </div>";
            if (itemDurability != null && Items.itemsArr[itemId]['itemDurability'] && itemDurability < Items.itemsArr[itemId]['itemDurability']) {
                itemHtml += "<div class=\"progress\">";
                itemHtml += "<div class=\"determinate\" style=\"width: " + ((itemDurability / Items.itemsArr[itemId]['itemDurability']) * 100) + "%\"></div>";
                itemHtml += "</div>";
            }
        }
        if (isHearth && (slotIndex === 0 || slotIndex === 1)) {
            itemHtml += "<div class=\"progress\">";
            const worldCell = WorldClient.worldArr[WorldClient.currentLayerNum][Globals.currentPlayerObj.markerTileY][Globals.currentPlayerObj.markerTileX];
            if (slotIndex === 0) {
                itemHtml += "<div class=\"determinate\" style=\"width: " + ((worldCell[3] / worldCell[4]) * 100) + "%\"></div>";
            } else {
                itemHtml += "<div class=\"determinate\" style=\"width: " + ((worldCell[5] / 10) * 100) + "%\"></div>";
            }
            itemHtml += "</div>";
        }
        if (isGrindstone && slotIndex === 1) {
            itemHtml += "<div class=\"progress\">";
            const worldCell = WorldClient.worldArr[WorldClient.currentLayerNum][Globals.currentPlayerObj.markerTileY][Globals.currentPlayerObj.markerTileX];
            itemHtml += "<div class=\"determinate\" style=\"width: " + ((worldCell[3] / 20) * 100) + "%\"></div>";
            itemHtml += "</div>";
        }
        itemHtml += "</div>";
        return itemHtml;
    }

}
