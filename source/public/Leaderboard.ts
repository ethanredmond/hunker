/// <reference path="PlayerClient.ts" />

class Leaderboard {

    static $leaderboardContainer: JQuery;
    static $leaderboardTable: JQuery;

    static init() {
        Leaderboard.$leaderboardContainer = $("<div id=\"leaderboardContainer\"></div>").appendTo('body');
        Leaderboard.$leaderboardTable     = $("<table id=\"leaderboardTable\"></table>").appendTo(Leaderboard.$leaderboardContainer);
    }

    static updatePlayerScoresArr(updatePlayerScoresArr: import('../HunkerInterfaces').PlayerScoresArr) {
        for (let playerId in updatePlayerScoresArr) {
            if (playerId in PlayerClient.allPlayerObjs) {
                PlayerClient.allPlayerObjs[playerId].playerScore = updatePlayerScoresArr[playerId];
            }
        }
        Leaderboard.update();
    }

    static update () {
        let leaderboardHtml = '';
        if (PlayerClient.allPlayerObjs) {
            // Sort the connected players by score.
            let sortedPlayersArr = [];
            for (let playerId in PlayerClient.allPlayerObjs) {
                sortedPlayersArr.push({'playerId': playerId, 'playerScore': PlayerClient.allPlayerObjs[playerId].playerScore});
            }
            sortedPlayersArr.sort(function (a, b) {
                if (a['playerScore'] < b['playerScore']) {
                    return 1;
                } else if (a['playerScore'] > b['playerScore']) {
                    return -1;
                } else {
                    return 0;
                }
            });

            let i = 1;
            for (let index in sortedPlayersArr) {
                leaderboardHtml += "<tr><td>" + i + ".</td><td>" + PlayerClient.getNameTag(sortedPlayersArr[index]['playerId'], true) + "</td><td>" + Leaderboard.kFormatter(sortedPlayersArr[index]['playerScore']) + "</td></tr>";
                i++;
            }
        }
        Leaderboard.$leaderboardContainer.css('display', ((leaderboardHtml.length > 0) ? 'block' : 'none'));
        Leaderboard.$leaderboardTable.html(leaderboardHtml);
    }

    static kFormatter(num: number): string {
        if (num > 999) {
            return (num / 1000).toFixed(1) + 'k';
        } else {
            return num.toString();
        }
    }

}
