<?php
/*
php ~/mnt/ebs1/git/hunker/public/js/generated/generateItems.php
php /mnt/ebs1/git/hunker/public/js/generated/generateItems.php
wget -O - https://packages.sury.org/php/apt.gpg | sudo apt-key add -
echo "deb https://packages.sury.org/php/ stretch main" > /etc/apt/sources.list.d/php.list
apt-get update
apt-get install php7.3
apt-get install php7.3-gd
*/

$itemsSourcePath = 'https://docs.google.com/spreadsheets/d/1Qj_MLzNbfo9V1sF3anKPTHbQKqIX8r-gGmR-1MAMhrc/gviz/tq?tqx=out:csv&sheet=Items';
$itemsCsv        = file_get_contents($itemsSourcePath);
if (empty($itemsCsv)) {
    echo "Source empty.\n";
    exit;
}
$itemsArr    = array_map('str_getcsv', explode("\n", $itemsCsv));
$newItemsArr = [];
foreach ($itemsArr as $index => $itemRow) {
    if ($index > 0) {
        $newItemsArr[$itemRow[0]] = [];
        foreach ($itemRow as $rowIndex => $rowValue) {
            $newItemsArr[$itemRow[0]][explode(' ', $itemsArr[0][$rowIndex])[0]] = $rowValue;
        }
    }
}
$itemsArr = $newItemsArr;

/**
 * See also `Utils.getTileIndexFromCoord`.
 */
function getTileIndexFromCoord (int $tileX, int $tileY): int {
    return (20 * ($tileY - 1)) + ($tileX - 1);
}

$validCraftingStationItemIds   = ['workbench', 'fire', 'forge_table'];
$particleHexCodesToGenerateArr = [];
foreach ($itemsArr as $rowIndex => $itemRow) {
    unset($itemsArr[$rowIndex]['id']);
    foreach ($itemRow as $colKey => $colValue) {

        if ($colKey === 'requiredItemsForCraftingArr' && strpos($colValue, 'wood') !== false) {
            trigger_error("Use planks instead of wood in $itemRow[id].requiredItemsForCraftingArr");
        } elseif ($colKey === 'requiredBlockForCraftingItemId' && $colValue && !in_array($colValue, $validCraftingStationItemIds, true)) {
            trigger_error("$itemRow[id].requiredBlockForCraftingItemId must be one of \"" . implode("\", \"", $validCraftingStationItemIds) . "\" instead of \"$colValue\"");
        }

        if ($colKey === 'tileCoordsArr') {
            $itemsArr[$rowIndex]['tileIndexesArr'] = null;
            if ($colValue) {
                $tileCoordsArr                         = json_decode($colValue, true);
                $itemsArr[$rowIndex]['tileIndexesArr'] = [];
                foreach ($tileCoordsArr as $tileCoordArr) {
                    $itemsArr[$rowIndex]['tileIndexesArr'][] = getTileIndexFromCoord($tileCoordArr[0], $tileCoordArr[1]);
                }
            }
            unset($itemsArr[$rowIndex][$colKey]);
        } elseif ($colKey === 'altTileCoordsArr') {
            $itemsArr[$rowIndex]['altTileIndexesArr'] = null;
            if ($colValue) {
                $tileCoordsArr                            = json_decode($colValue, true);
                $itemsArr[$rowIndex]['altTileIndexesArr'] = [];
                foreach ($tileCoordsArr as $tileCoordArr) {
                    $itemsArr[$rowIndex]['altTileIndexesArr'][] = getTileIndexFromCoord($tileCoordArr[0], $tileCoordArr[1]);
                }
            }
            unset($itemsArr[$rowIndex][$colKey]);
        } elseif ($colValue === '') {
            $itemsArr[$rowIndex][$colKey] = null;
        } elseif (strpos($colKey, 'is') === 0 || strpos($colKey, 'can') === 0 || strpos($colKey, 'has') === 0) {
            $itemsArr[$rowIndex][$colKey] = ($colValue === 'TRUE');
        } elseif (strpos($colKey, 'Arr') !== false) {
            $itemsArr[$rowIndex][$colKey] = json_decode($colValue, true);
        } elseif ($colKey === 'hexCode') {
            $itemsArr[$rowIndex][$colKey] = (string) $colValue;
        } elseif (is_numeric($colValue)) {
            $itemsArr[$rowIndex][$colKey] = (float) $colValue;
        }
    }
    if (
        $itemsArr[$rowIndex]['hexCode'] && $itemsArr[$rowIndex]['hasParticleEmitter']
        &&
        !in_array($itemsArr[$rowIndex]['hexCode'], $particleHexCodesToGenerateArr, true)
    ) {
        $particleHexCodesToGenerateArr[] = $itemsArr[$rowIndex]['hexCode'];
    }
}

$itemsJson = json_encode($itemsArr, JSON_THROW_ON_ERROR, 512);

$hunkerPathStr = '~/mnt/ebs1/git/hunker';
if (file_exists('/mnt/ebs1/git/hunker')) {
    $hunkerPathStr = '/mnt/ebs1/git/hunker';
}
file_put_contents("$hunkerPathStr/source/public/generated/Items.ts", <<<TS
    class Items {
        // @formatter:off
        public static readonly itemsArr: {[itemId: string]: import('../../HunkerInterfaces').ItemRow} = $itemsJson;
        // @formatter:on
    }
    
    if (typeof module !== 'undefined') {
        module.exports = Items;
    }

    TS
);
echo "Success!\n";

if (extension_loaded('gd')) {
    array_map('unlink', array_filter((array) glob("$hunkerPathStr/public/assets/particles/2px/*")));
    foreach ($particleHexCodesToGenerateArr as $hexCode) {
        $outObj      = imagecreatetruecolor(2, 2);
        $newColorNum = imagecolorallocate($outObj, ...sscanf($hexCode, '%02x%02x%02x'));
        imagefill($outObj, 0, 0, $newColorNum);
        imagepng($outObj, "$hunkerPathStr/public/assets/particles/2px/$hexCode.png");
    }
    echo "Generated " . count($particleHexCodesToGenerateArr) . " particles!\n";
}
