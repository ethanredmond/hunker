/// <reference path="UtilsClient.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="LightCircle.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="ChatClient.ts" />
/// <reference path="Crafting.ts" />
/// <reference path="DayNightCycleClient.ts" />
/// <reference path="InventoryModal.ts" />
/// <reference path="ParticleEffect.ts" />
/// <reference path="Leaderboard.ts" />
/// <reference path="Minimap.ts" />
/// <reference path="StatBar.ts" />
/// <reference path="Modals.ts" />
/// <reference path="Globals.ts" />
/// <reference path="./generated/Items.ts" />

class PlayerClient {

    static allPlayerObjs: { [id: string]: PlayerClient }                        = {};
    static allPlayerEntitiesGroup: Phaser.GameObjects.Group;
    static newItemSpritesGroup: Phaser.GameObjects.Group;
    static newItemTextsGroup: Phaser.GameObjects.Group;
    static readonly selfColor: string                                           = '#ffffff';
    static readonly enemyColor: string                                          = '#d32f2f';
    static isPlayerColliding: boolean                                           = true;
    static clickRailTimeoutsArr: { [index: number]: NodeJS.Timeout }            = {};
    static isClickingRail: boolean                                              = false;
    static escKey: Phaser.Input.Keyboard.Key                                    = null;
    static spaceKey: Phaser.Input.Keyboard.Key                                  = null;
    static shiftKey: Phaser.Input.Keyboard.Key                                  = null;
    static altKey: Phaser.Input.Keyboard.Key                                    = null;
    static arrowKeys: { [keyName: string]: Phaser.Input.Keyboard.Key }          = null;
    static wasdKeys: { [keyName: string]: Phaser.Input.Keyboard.Key }           = null;
    static bowHeldPositionArr: import('../HunkerInterfaces').HeldSpritePosition = {
        'isShowingBelowPlayer':  false,
        'isFlippingHeldSprite':  false,
        'heldSpriteOriginX':     0.21,
        'heldSpriteOriginY':     0.57,
        'heldSpriteWidthHeight': 113,
        'heldSpriteAngle':       -52,
    };
    static deathHelperTextArr: { [deathKey: string]: string[] }                 = {
        'hunger':     ["So frustrating", "Where's an apple tree when you need it?"],
        'warmth':     ["Better than being burned to death", "Lol"],
        'wolf':       ["Don't pet the wolves", "Aww... so cute &#128525;"],
        'polar_bear': ["You weren't fast enough", "Running never works"],
        'zombie':     ["He said your brains were tasty", "He ate your legs first, then your arms"],
        'axehead':    ["Watch out for the axe", "Better luck next time..."],
        'player':     ["\"I thought we were friends...\" &#128546;", "Humans are very territorial"],
        'lava':       ["Mmm, crispy human", "Try water next time"],
        'explosion':  ["What an explosive ending", "At least you went out with a bang"],
        'arrow':      ["Death by pointy things", "Turns out you can't outrun arrows"],
    };

    static directionsArr: string[]                                                                                                                        = ['up', 'down', 'left', 'right'];
    static isSprinting: boolean                                                                                                                           = false;
    static keyDownDataArr: { [directionLetter: string]: { timeOnLastKeypress: number, isSprinting: boolean, isDown: boolean, isDownLastFrame: boolean } } = {
        'up':    {'timeOnLastKeypress': 0, 'isSprinting': false, 'isDown': false, 'isDownLastFrame': false},
        'down':  {'timeOnLastKeypress': 0, 'isSprinting': false, 'isDown': false, 'isDownLastFrame': false},
        'left':  {'timeOnLastKeypress': 0, 'isSprinting': false, 'isDown': false, 'isDownLastFrame': false},
        'right': {'timeOnLastKeypress': 0, 'isSprinting': false, 'isDown': false, 'isDownLastFrame': false},
    };
    static sprintDoubleTapTimeoutMillis: number                                                                                                           = 500;
    static attackCooldownRectangleWidthPx: number                                                                                                         = 50;

    static gameModeStr: import('../HunkerInterfaces').GameModeStr;
    static difficultyStr: import('../HunkerInterfaces').DifficultyStr;

    public readonly name: string;
    public readonly id: string;
    public playerScore: number;
    public isCrafting: boolean;
    public craftingItemId: import('../HunkerInterfaces').ItemId;
    public isPaused: boolean;
    public isDragging: boolean;
    public isGhost: boolean;
    public selectedItemId: import('../HunkerInterfaces').ItemId;
    private wasRightButtonDownLastUpdate: boolean = false;

    private maxHealth: number;
    private maxWarmth: number;
    private maxHunger: number;
    private health: number;
    private warmth: number;
    private hunger: number;
    public healthBar: StatBar;
    public warmthBar: StatBar;
    public hungerBar: StatBar;

    private projectileCharge: number;
    public currentLayerNum: number;
    public readonly image: import('../HunkerInterfaces').PlayerImage;
    public readonly sprite: Phaser.Physics.Arcade.Sprite;
    public spriteCollider: Phaser.Physics.Arcade.Collider;
    public attackCooldownRectangle: Phaser.GameObjects.Rectangle;
    public attackCooldownTween: Phaser.Tweens.Tween;
    private nameTag: Phaser.GameObjects.Text;
    private lightCircleObj: LightCircle;
    public readonly selectedItemSprite: Phaser.GameObjects.Sprite;
    private readonly indicatorSprite: Phaser.GameObjects.Sprite;
    private readonly indicatorChildSprite: Phaser.GameObjects.Sprite;
    private indicatorSpriteRotateTween: Phaser.Tweens.Tween;
    public selectedItemIndex: number;
    public hotbarItemsArr: import('../HunkerInterfaces').HotbarSlot[];
    public accessoryItemsArr: import('../HunkerInterfaces').HotbarSlot[];
    public markerCrackSprite: Phaser.GameObjects.Sprite;
    public markerSprite: Phaser.GameObjects.Graphics;
    public markerTileX: number;
    public markerTileY: number;
    private markerTileIndex: number;
    public markerTileId: string;
    private markerFloorTileId: string;
    private oldPosition: {
        x: number,
        y: number,
        angle: number,
        projectileCharge: number,
        markerTileX: number,
        markerTileY: number,
    };
    private nightsSurvivedNum: number;
    private lastSwingTimeMillis: number;
    private lastUseTimeMillis: number;
    private lastPlaceTimeMillis: number;
    private lastPlayStepTimeMillis: number;
    public ridingEntityId: string;
    private readonly $minimapPlayer: JQuery;

    constructor(playerInfo: import('../HunkerInterfaces').PublicPlayerData, privatePlayerInfo?: import('../HunkerInterfaces').PrivatePlayerData) {
        this.name             = playerInfo['name'];
        this.id               = playerInfo['id'];
        this.playerScore      = playerInfo['playerScore'];
        this.isCrafting       = playerInfo['isCrafting'];
        this.craftingItemId   = playerInfo['craftingItemId'];
        this.isPaused         = playerInfo['isPaused'];
        this.isDragging       = false;
        this.projectileCharge = 0;
        this.currentLayerNum  = playerInfo['currentLayerNum'];
        this.image            = playerInfo['image'];
        this.sprite           = Globals.game.physics.add.sprite(playerInfo['x'], playerInfo['y'], playerInfo['image']);
        this.sprite.angle     = playerInfo['angle'];
        this.sprite.setCollideWorldBounds(true);
        this.sprite.displayWidth  = Utils.allPlayerImagesArr[playerInfo['image']]['width'] * (53.75 / Utils.allPlayerImagesArr[playerInfo['image']]['height']);
        this.sprite.displayHeight = Utils.allPlayerImagesArr[playerInfo['image']]['height'] * (53.75 / Utils.allPlayerImagesArr[playerInfo['image']]['height']);
        this.sprite.visible       = (this.currentLayerNum == WorldClient.currentLayerNum);
        this.sprite.depth         = UtilsClient.depthsArr['player'];
        this.nameTag              = Globals.game.add.text(playerInfo['x'], playerInfo['y'], this.name, {font: '20px Germania One', fill: '#ffffff'});
        this.nameTag.setOrigin(0.5, -1.25);
        this.nameTag.depth      = UtilsClient.depthsArr['playerNameTag'];
        this.nameTag.visible    = (this.currentLayerNum == WorldClient.currentLayerNum);
        this.lightCircleObj     = new LightCircle(this.currentLayerNum, -1, -1, 0, '', this.id);
        this.selectedItemSprite = Globals.game.add.sprite(0, 0, '');

        this.indicatorSprite         = Globals.game.add.sprite(playerInfo['x'], playerInfo['y'], '');
        this.indicatorChildSprite    = Globals.game.add.sprite(0, 0, '');
        this.indicatorSprite.visible = false;
        this.updateIndicatorSprite();

        if (typeof privatePlayerInfo !== 'undefined') { // Is current player.
            this.sprite.setDrag(1250, 1250);
            Globals.game.cameras.main.startFollow(this.sprite, true, 0.1, 0.1);
            this.selectedItemIndex = privatePlayerInfo['selectedItemIndex'];
            this.hotbarItemsArr    = privatePlayerInfo['hotbarItemsArr'];
            this.accessoryItemsArr = privatePlayerInfo['accessoryItemsArr'];
            this.initStatBars(privatePlayerInfo);

            this.attackCooldownRectangle = Globals.game.add.rectangle(playerInfo['x'], playerInfo['y'], 50, 5, 0xe0e0e0);
            this.attackCooldownRectangle.setOrigin(0.5, -12);
            this.attackCooldownRectangle.depth = UtilsClient.depthsArr['playerCooldownBar'];
            this.attackCooldownRectangle.setVisible(false);

            this.markerCrackSprite       = Globals.game.add.sprite(0, 0, 'destroyStage0');
            this.markerCrackSprite.depth = UtilsClient.depthsArr['marker'];
            this.markerCrackSprite.setOrigin(0);
            this.markerSprite       = Globals.game.add.graphics();
            this.markerSprite.depth = UtilsClient.depthsArr['marker'];
            this.markerSprite.lineStyle(2, 0x000000, 1);
            this.markerSprite.strokeRect(0, 0, Utils.tileSize, Utils.tileSize);
            this.markerTileX     = null;
            this.markerTileY     = null;
            this.markerTileIndex = null;
            this.markerTileId    = null;

            this.nightsSurvivedNum      = 0;
            this.lastSwingTimeMillis    = null;
            this.lastUseTimeMillis      = null;
            this.lastPlaceTimeMillis    = null;
            this.lastPlayStepTimeMillis = null;
        }
        this.selectItem(playerInfo['selectedItemId']);
        this.updateIsGhost(playerInfo['isGhost']);

        PlayerClient.allPlayerObjs[this.id] = this;
        PlayerClient.allPlayerEntitiesGroup.add(this.sprite);
        this.$minimapPlayer = Minimap.initPlayer(this.id, playerInfo);

        if (Utils.isTakingScreenshots) {
            Utils.playerSpeed                  = 1300;
            this.sprite.alpha                  = 0;
            this.attackCooldownRectangle.alpha = 0;
            this.nameTag.alpha                 = 0;
            this.markerSprite.alpha            = 0;
        }
    }

    updateIndicatorSprite() {
        if (this.isCrafting || this.isPaused) {
            this.indicatorSprite.x = this.sprite.x;
            this.indicatorSprite.y = this.sprite.y - 60;
            if (this.isCrafting) {
                this.indicatorSprite.setTexture(this.craftingItemId);
                this.indicatorSprite.displayWidth  = 32;
                this.indicatorSprite.displayHeight = 32;
                this.indicatorChildSprite.setTexture('craftingIndicator');
                this.indicatorChildSprite.displayWidth  = 64;
                this.indicatorChildSprite.displayHeight = 64;
                this.indicatorSpriteRotateTween         = Globals.game.add.tween({'targets': this.indicatorSprite, 'angle': 365, 'duration': 2000, 'loop': -1});
                this.indicatorSprite.visible            = true;
                this.indicatorChildSprite.visible       = true;
            } else if (this.isPaused) {
                this.indicatorSprite.setTexture('pausedIndicator');
                this.indicatorSprite.displayWidth  = 32;
                this.indicatorSprite.displayHeight = 32;
                this.indicatorSprite.visible       = true;
                this.indicatorChildSprite.visible  = false;
            }
        } else {
            this.indicatorSprite.visible = false;
            if (this.indicatorSpriteRotateTween) {
                Globals.game.tweens.remove(this.indicatorSpriteRotateTween);
                this.indicatorSpriteRotateTween = null;
            }
        }
    }

    initStatBars(statBarsDataArr: import('../HunkerInterfaces').StatBarsDataArr) {
        if (this.id != Globals.socket.id) {
            throw new Error("Only call initStatBars on the current player.");
        }

        this.maxHealth = statBarsDataArr['maxHealth'];
        this.maxWarmth = statBarsDataArr['maxWarmth'];
        this.maxHunger = statBarsDataArr['maxHunger'];
        this.health    = statBarsDataArr['health'];
        this.warmth    = statBarsDataArr['warmth'];
        this.hunger    = statBarsDataArr['hunger'];
        this.healthBar = new StatBar('health', this.health, this.maxHealth, Utils.playerGameModesArr[PlayerClient.gameModeStr]['hasHealth']);
        this.warmthBar = new StatBar('warmth', this.warmth, this.maxWarmth, Utils.playerGameModesArr[PlayerClient.gameModeStr]['hasWarmth']);
        this.hungerBar = new StatBar('hunger', this.hunger, this.maxHunger, Utils.playerGameModesArr[PlayerClient.gameModeStr]['hasHunger']);
    }

    updateStatBars(statBarsDataArr: import('../HunkerInterfaces').StatBarsDataArr) {
        if (this.id != Globals.socket.id) {
            throw new Error("Only call updateStatBars on the current player.");
        }

        if (typeof statBarsDataArr['maxHealth'] != 'undefined' && statBarsDataArr['maxHealth'] != this.maxHealth) {
            this.maxHealth = statBarsDataArr['maxHealth'];
            this.healthBar.updateMax(this.maxHealth);
        }
        if (typeof statBarsDataArr['maxWarmth'] != 'undefined' && statBarsDataArr['maxWarmth'] != this.maxWarmth) {
            this.maxWarmth = statBarsDataArr['maxWarmth'];
            this.warmthBar.updateMax(this.maxWarmth);
        }
        if (typeof statBarsDataArr['maxHunger'] != 'undefined' && statBarsDataArr['maxHunger'] != this.maxHunger) {
            this.maxHunger = statBarsDataArr['maxHunger'];
            this.hungerBar.updateMax(this.maxHunger);
        }
        if (typeof statBarsDataArr['health'] != 'undefined' && statBarsDataArr['health'] != this.health) {
            if (statBarsDataArr['health'] < this.health) { // Play sound effect when lost health.
                const xRounded         = Math.floor(this.sprite.x / Utils.tileSize);
                const yRounded         = Math.floor(this.sprite.y / Utils.tileSize);
                let playerFloorBlockId = null;
                if (
                    WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded] != null
                    &&
                    WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0] in Utils.tileIndexToItemIdArr
                    &&
                    Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0]] in Items.itemsArr
                ) {
                    playerFloorBlockId = Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0]];
                }
                if (playerFloorBlockId == 'lava') {
                    SoundEffect.play('player_fire_damage');
                } else {
                    SoundEffect.play('player_damage');
                }
                UtilsClient.healthTweenTint(this.sprite);
                Globals.game.cameras.main.shake(100, 0.005);
                if (this.currentLayerNum == 0) {
                    Globals.game.cameras.main.fadeIn(100, 255, 82, 82);
                } else {
                    Globals.game.cameras.main.fadeIn(100, 213, 0, 0);
                }
            } else {
                UtilsClient.healthTweenTint(this.sprite, true);
            }
            this.health = statBarsDataArr['health'];
            this.healthBar.update(this.health);
        }
        if (typeof statBarsDataArr['warmth'] != 'undefined' && statBarsDataArr['warmth'] != this.warmth) {
            this.warmth = statBarsDataArr['warmth'];
            this.warmthBar.update(this.warmth);
        }
        if (typeof statBarsDataArr['hunger'] != 'undefined' && statBarsDataArr['hunger'] != this.hunger) {
            this.hunger = statBarsDataArr['hunger'];
            this.hungerBar.update(this.hunger);
        }
    }

    selectItem(itemId: import('../HunkerInterfaces').ItemId) {
        if (this.isGhost) {
            itemId = null;
        }
        this.selectedItemId = itemId;
        if (itemId == 'axe_giant') {
            this.selectedItemSprite.setTexture('axe_giant_held');
        } else {
            this.selectedItemSprite.setTexture(this.selectedItemId);
            if (this.selectedItemId && this.currentLayerNum == WorldClient.currentLayerNum) {
                this.selectedItemSprite.setVisible(true);
            } else {
                this.selectedItemSprite.setVisible(false);
            }
        }
        if (this.selectedItemId && this.selectedItemSprite) {
            PlayerClient.updateSelectedItemSpritePosition(this.selectedItemSprite, this.sprite, Items.itemsArr[this.selectedItemId]);
        }
        this.projectileCharge = 0;
        this.updateLightCircle();
    }

    static updateSelectedItemSpritePosition(sprite: Phaser.GameObjects.Sprite, parentSprite: Phaser.GameObjects.Sprite, newPositionArr: import('../HunkerInterfaces').HeldSpritePosition): void {
        sprite.setOrigin(newPositionArr['heldSpriteOriginX'], newPositionArr['heldSpriteOriginY']);
        sprite.displayWidth  = newPositionArr['heldSpriteWidthHeight'];
        sprite.displayHeight = newPositionArr['heldSpriteWidthHeight'];
        sprite.setData('angleOffset', newPositionArr['heldSpriteAngle']);
        sprite.angle = parentSprite.angle + newPositionArr['heldSpriteAngle'];
        sprite.flipY = newPositionArr['isFlippingHeldSprite'] || false;
        sprite.depth = parentSprite.depth + ((newPositionArr['isShowingBelowPlayer'] || false) ? -0.1 : 0.1);
    }

    updateLightCircle() {
        let lightCircleItemId;
        if (this.selectedItemId && Items.itemsArr[this.selectedItemId]['hasLightCircleWhenSelected'] && Items.itemsArr[this.selectedItemId]['lightCircleRadius'] != null) {
            lightCircleItemId = this.selectedItemId;
        }
        if (!lightCircleItemId) {
            for (let i in this.accessoryItemsArr) {
                if (this.accessoryItemsArr[i]['itemId'] && Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['hasLightCircleWhenSelected'] && Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['lightCircleRadius'] != null) {
                    lightCircleItemId = this.accessoryItemsArr[i]['itemId'];
                    break;
                }
            }
        }
        if (this.isGhost) {
            lightCircleItemId = 'torch';
        }
        if (lightCircleItemId) {
            this.lightCircleObj.isShowing         = true;
            this.lightCircleObj.lightCircleRadius = Items.itemsArr[lightCircleItemId]['lightCircleRadius'];
            this.lightCircleObj.lightCircleTint   = Items.itemsArr[lightCircleItemId]['lightCircleTint'];
        } else {
            this.lightCircleObj.isShowing         = false;
            this.lightCircleObj.lightCircleRadius = null;
            this.lightCircleObj.lightCircleTint   = null;
        }
    }

    dropItem(itemToDropIndex: number, isDroppingWholeStack: boolean, itemRowType: string = null) {
        if (this.id != Globals.socket.id) {
            throw new Error("Only call dropItem on the current player.");
        }
        if (this.hotbarItemsArr[itemToDropIndex]['itemId'] != null && this.hotbarItemsArr[itemToDropIndex]['itemCount'] != null && itemRowType != 'container') {
            Globals.socket.emit('dropItem', {
                'itemToDropIndex':      itemToDropIndex,
                'itemRowType':          itemRowType,
                'x':                    this.markerTileX,
                'y':                    this.markerTileY,
                'isDroppingWholeStack': isDroppingWholeStack,
            }, UtilsClient.handleSocketEmitResponse);
        }
    }

    takeItem(isTakingWholeStack: boolean) {
        if (this.id != Globals.socket.id) {
            throw new Error("Only call takeItem on the current player.");
        }
        if (this.markerTileId && this.markerTileId in Items.itemsArr && Items.itemsArr[this.markerTileId]['isContainer']) {
            Globals.socket.emit('takeItem', {
                'x':                  this.markerTileX,
                'y':                  this.markerTileY,
                'isTakingWholeStack': isTakingWholeStack,
            }, UtilsClient.handleSocketEmitResponse);
        }
    }

    updateIsGhost(isGhost: boolean) {
        this.isGhost = isGhost;
        if (isGhost) {
            this.sprite.alpha = 0.5;
        } else {
            this.sprite.alpha = 1.0;
        }
        if (this.id == Globals.socket.id) {
            LightCircle.updateShadowRgb();
            this.updateLightCircle();
            if (isGhost) {
                $('html').addClass('ghostGrayscale');
            } else {
                $('html').removeClass('ghostGrayscale');
            }
        }
    }

    killClient(playerDeathDetailsArr: import('../HunkerInterfaces').PlayerDeathDetailsArr) {
        ChatClient.addMessage({
            'playerId': this.id,
            'text':     playerDeathDetailsArr['deathSuffixText'],
        });
        if (playerDeathDetailsArr['deathKey'] == 'disconnected') {
            this.$minimapPlayer.remove();
            this.sprite.destroy();
            if (this.indicatorSprite) {
                this.indicatorSprite.destroy();
            }
            if (this.selectedItemSprite) {
                this.selectedItemSprite.destroy();
            }
            if (this.attackCooldownRectangle) {
                this.attackCooldownRectangle.destroy();
            }
            this.nameTag.destroy();
            this.lightCircleObj.destroy();
            delete PlayerClient.allPlayerObjs[this.id];
        } else {
            this.updateIsGhost(true);
        }
    }

    static killCurrentPlayer(playerDeathDetailsArr: import('../HunkerInterfaces').PlayerDeathDetailsArr) {
        // Minimap.addMinimapDeathMarker();
        // Globals.currentPlayerObj.markerSprite.destroy();
        // Globals.currentPlayerObj.markerCrackSprite.destroy();
        // game.camera.follow(null);
        $('#playerDiedText').html("You " + playerDeathDetailsArr['deathSuffixText']);
        if (playerDeathDetailsArr['deathKey'] && playerDeathDetailsArr['deathKey'] in PlayerClient.deathHelperTextArr) {
            $('#playerHelperText').html(Utils.randElFromArr(PlayerClient.deathHelperTextArr[playerDeathDetailsArr['deathKey']]));
        } else {
            $('#playerHelperText').html('');
        }
        $('#playerScore').html(playerDeathDetailsArr['playerScore'].toString());
        $('#daysSurvivedNum').html(playerDeathDetailsArr['daysSurvivedNum'].toString());
        Globals.currentPlayerObj.updateStatBars({
            'health': 0,
            'hunger': 0,
            'warmth': 0,
        });
        Modals.closeAll();
        Modals.allModalObjs['playerDied'].open();
        Globals.currentPlayerObj.playerScore = 0;
        Leaderboard.update();
        UtilsClient.updateHotbarAndAccessories(playerDeathDetailsArr);
        Globals.currentPlayerObj.killClient(playerDeathDetailsArr);
        clearInterval(DayNightCycleClient.updateDayNightIndicatorInterval);
    }

    update() {
        if (this.id != Globals.socket.id) {
            Modals.allModalObjs['disconnected'].open();
            throw new Error("Only call update on the current player.");
        }
        // Don't move while disabling input.
        if (!Globals.isInputEnabled) {
            this.sprite.body.velocity.x = 0;
            this.sprite.body.velocity.y = 0;
            return;
        }
        if (this.spriteCollider) {
            this.spriteCollider.active = (PlayerClient.isPlayerColliding && !this.isGhost);
        }

        const xRounded  = Math.floor(this.sprite.x / Utils.tileSize);
        const yRounded  = Math.floor(this.sprite.y / Utils.tileSize);
        let playerReach = 1;
        if (this.selectedItemId && Items.itemsArr[this.selectedItemId]['playerReach']) {
            playerReach = Items.itemsArr[this.selectedItemId]['playerReach'];
        }
        this.markerTileX         = Math.floor((Globals.game.input.activePointer.x + Globals.game.cameras.main.scrollX) / Utils.tileSize);
        this.markerTileX         = Math.min(this.markerTileX, Math.ceil(this.sprite.x / Utils.tileSize) + (playerReach - 1));
        this.markerTileX         = Math.max(this.markerTileX, Math.floor(this.sprite.x / Utils.tileSize) - playerReach);
        this.markerTileY         = Math.floor((Globals.game.input.activePointer.y + Globals.game.cameras.main.scrollY) / Utils.tileSize);
        this.markerTileY         = Math.min(this.markerTileY, Math.ceil(this.sprite.y / Utils.tileSize) + (playerReach - 1));
        this.markerTileY         = Math.max(this.markerTileY, Math.floor(this.sprite.y / Utils.tileSize) - playerReach);
        const markerTile         = WorldClient.worldTilemap.getTileAt(this.markerTileX, this.markerTileY);
        this.markerSprite.x      = this.markerTileX * Utils.tileSize;
        this.markerSprite.y      = this.markerTileY * Utils.tileSize;
        this.markerCrackSprite.x = this.markerSprite.x;
        this.markerCrackSprite.y = this.markerSprite.y;

        const currentTimeMillis     = Utils.time();
        let swingTimeMillis: number = Utils.playerSwingTimeMillis;
        let angleOffsetDeg: number  = 0;
        let didRightClickAction     = false;
        if (!this.isCrafting) { // Not crafting.
            if (markerTile.index && markerTile.index != UtilsClient.transparentTileIndex) {
                this.markerTileIndex = markerTile.index;
                if (this.markerTileIndex != null && this.markerTileIndex in Utils.tileIndexToItemIdArr) {
                    this.markerTileId = Utils.tileIndexToItemIdArr[this.markerTileIndex];
                } else {
                    this.markerTileId = null;
                }
            } else {
                this.markerTileIndex = null;
                this.markerTileId    = null;
            }
            if (WorldClient.floorTilemap.getTileAt(this.markerTileX, this.markerTileY)) {
                const markerFloorTileIndex = WorldClient.floorTilemap.getTileAt(this.markerTileX, this.markerTileY).index;
                if (markerFloorTileIndex != null && markerFloorTileIndex in Utils.tileIndexToItemIdArr) {
                    this.markerFloorTileId = Utils.tileIndexToItemIdArr[markerFloorTileIndex];
                } else {
                    this.markerFloorTileId = null;
                }
            } else {
                this.markerFloorTileId = null;
            }
            if (this.isGhost) {
                this.markerSprite.visible = (this.markerTileId == 'rail');
            } else {
                this.markerSprite.visible = Boolean(this.markerTileId);
            }
            // Update show marker sprite.
            if (this.oldPosition && (this.markerTileX != this.oldPosition.markerTileX || this.markerTileY != this.oldPosition.markerTileY)) {
                this.updateMarkerCrackSprite();
            }
            if (
                !this.isDragging
                &&
                !$('.modal-overlay').is(':visible')
            ) {
                if (
                    !this.isGhost
                    &&
                    Globals.game.input.activePointer.leftButtonDown() // Left button is down.
                    &&
                    this.lastSwingTimeMillis + swingTimeMillis <= currentTimeMillis
                ) {
                    this.lastSwingTimeMillis = currentTimeMillis;
                    if (this.markerTileIndex == null || this.markerTileIndex == UtilsClient.transparentTileIndex) { // Play attack sounds if no block at marker.
                        if (
                            this.selectedItemId
                            &&
                            (
                                this.selectedItemId.indexOf('sword_') == 0
                                ||
                                this.selectedItemId.indexOf('axe_') == 0
                            )
                            &&
                            !Globals.currentPlayerObj.attackCooldownRectangle.visible
                        ) {
                            SoundEffect.play('attack_sweep');
                        } else if (
                            this.selectedItemId
                            &&
                            this.selectedItemId.indexOf('hoe_') == 0
                            &&
                            this.markerFloorTileId
                            &&
                            Items.itemsArr[this.markerFloorTileId]['canTill']
                        ) {
                            SoundEffect.play('hoe_till');
                        } else {
                            SoundEffect.play('attack_weak');
                        }
                    }
                    const _this = this;
                    if (
                        !PlayerClient.altKey.isDown
                        &&
                        this.selectedItemId
                        &&
                        this.selectedItemId.indexOf('scythe_') == 0
                    ) {
                        // @formatter:off
                        if        (xRounded     == this.markerTileX && yRounded - 1 == this.markerTileY) { // North.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX - 1, 'y': this.markerTileY}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX + 1, 'y': this.markerTileY}, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded     == this.markerTileX && yRounded + 1 == this.markerTileY) { // South.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX - 1, 'y': this.markerTileY}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX + 1, 'y': this.markerTileY}, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded - 1 == this.markerTileX && yRounded     == this.markerTileY) { // West.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY - 1}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY + 1}, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded + 1 == this.markerTileX && yRounded     == this.markerTileY) { // East.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY - 1}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY + 1}, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded - 1 == this.markerTileX && yRounded - 1 == this.markerTileY) { // Northwest.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY + 1}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX + 1, 'y': this.markerTileY    }, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded - 1 == this.markerTileX && yRounded + 1 == this.markerTileY) { // Southwest.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY - 1}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX + 1, 'y': this.markerTileY    }, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded + 1 == this.markerTileX && yRounded - 1 == this.markerTileY) { // Northeast.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY + 1}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX - 1, 'y': this.markerTileY    }, UtilsClient.handleSocketEmitResponse);
                        } else if (xRounded + 1 == this.markerTileX && yRounded + 1 == this.markerTileY) { // Southeast.
                            Globals.socket.emit('startSwing', {'x': this.markerTileX,     'y': this.markerTileY - 1}, UtilsClient.handleSocketEmitResponse);
                            Globals.socket.emit('startSwing', {'x': this.markerTileX - 1, 'y': this.markerTileY    }, UtilsClient.handleSocketEmitResponse);
                        }
                        // @formatter:on
                    }
                    Globals.socket.emit('startSwing', {
                        'x': this.markerTileX,
                        'y': this.markerTileY,
                    }, function (response: import('../HunkerInterfaces').ServerResponse) {
                        if (
                            (
                                _this.markerTileIndex == null
                                ||
                                _this.markerTileIndex == UtilsClient.transparentTileIndex
                            )
                            &&
                            _this.selectedItemId
                            &&
                            (
                                _this.selectedItemId.indexOf('shovel_') == 0
                                ||
                                _this.selectedItemId.indexOf('hoe_') == 0
                            )
                            &&
                            _this.markerFloorTileId
                        ) {
                            if (_this.selectedItemId.indexOf('shovel_') == 0 && Items.itemsArr[_this.markerFloorTileId]['hitSoundEffectKey']) {
                                SoundEffect.play(Items.itemsArr[_this.markerFloorTileId]['hitSoundEffectKey']);
                            }
                            if (Items.itemsArr[_this.markerFloorTileId]['hexCode'] && Items.itemsArr[_this.markerFloorTileId]['hasParticleEmitter']) {
                                ParticleEffect.play(Items.itemsArr[_this.markerFloorTileId]['hexCode']);
                            }
                        } else if (_this.markerTileId && _this.markerTileId in Items.itemsArr) {
                            if (Items.itemsArr[_this.markerTileId]['hitSoundEffectKey']) {
                                SoundEffect.play(Items.itemsArr[_this.markerTileId]['hitSoundEffectKey']);
                            }
                            if (Items.itemsArr[_this.markerTileId]['hexCode'] && Items.itemsArr[_this.markerTileId]['hasParticleEmitter']) {
                                ParticleEffect.play(Items.itemsArr[_this.markerTileId]['hexCode']);
                            }
                        }
                        UtilsClient.handleSocketEmitResponse(response);
                    });
                }
                if (
                    !this.isGhost
                    &&
                    (
                        Globals.game.input.activePointer.leftButtonDown()
                        ||
                        PlayerClient.escKey.isDown
                    )
                ) {
                    this.projectileCharge = 0;
                    this.updateProjectileCharge();
                }

                if (Globals.game.input.activePointer.rightButtonDown()) {
                    const isDisableRightClick = (this.selectedItemId == 'bow' || this.selectedItemId == 'fireball');
                    if (this.lastUseTimeMillis + UtilsClient.playerUseTimeMillis <= currentTimeMillis) {
                        if (!this.isGhost && this.selectedItemId == 'map') {
                            didRightClickAction = true;
                            Modals.closeAll();
                            Minimap.update();
                            Minimap.bigMinimapModal.open();
                        } else if (!this.isGhost && this.selectedItemId == 'compass') {
                            didRightClickAction = true;
                            Minimap.openCompassModal();
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            (
                                this.markerTileId == 'workbench'
                                ||
                                this.markerTileId == 'forge_table'
                                ||
                                this.markerTileId == 'fire'
                            )
                        ) {
                            didRightClickAction      = true;
                            this.lastSwingTimeMillis = currentTimeMillis;
                            InventoryModal.open();
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            this.markerTileId
                            &&
                            this.markerTileId in Items.itemsArr
                            &&
                            Items.itemsArr[this.markerTileId]['isContainer']
                        ) {
                            didRightClickAction      = true;
                            this.lastSwingTimeMillis = currentTimeMillis;
                            Modals.closeAll();
                            InventoryModal.open('container');
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] != null
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0] in Utils.tileIndexToItemIdArr
                            &&
                            Utils.tileIndexToItemIdArr[WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]] in Items.itemsArr
                            &&
                            Items.itemsArr[Utils.tileIndexToItemIdArr[WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]]]['toggleItemId']
                        ) {
                            didRightClickAction      = true;
                            this.lastSwingTimeMillis = currentTimeMillis;
                            if (Utils.tileIndexToItemIdArr[WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]] == 'door_open') {
                                SoundEffect.play('door_close');
                            } else {
                                SoundEffect.play('door_open');
                            }
                            Globals.socket.emit('toggleBlock', {
                                'x': this.markerTileX,
                                'y': this.markerTileY,
                            }, UtilsClient.handleSocketEmitResponse);
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] != null
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0] in Utils.tileIndexToItemIdArr
                            &&
                            Items.itemsArr[Utils.tileIndexToItemIdArr[WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]]]['explosionPower']
                        ) {
                            didRightClickAction      = true;
                            this.lastSwingTimeMillis = currentTimeMillis;
                            Globals.socket.emit('triggerExplosive', {
                                'x': this.markerTileX,
                                'y': this.markerTileY,
                            }, UtilsClient.handleSocketEmitResponse);
                        } else if (
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] != null
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0] in Utils.tileIndexToItemIdArr
                            &&
                            Utils.tileIndexToItemIdArr[WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]].indexOf('rail') == 0
                        ) {
                            didRightClickAction = true;
                            this.sprite.x       = this.markerTileX * Utils.tileSize + (Utils.tileSize / 2);
                            this.sprite.y       = this.markerTileY * Utils.tileSize + (Utils.tileSize / 2);
                            Globals.disableInput();
                            setTimeout(function () {
                                PlayerClient.clickRail();
                            }, 0);
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            this.selectedItemId == 'bucket'
                            &&
                            WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] == null
                            &&
                            WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX] != null
                            &&
                            WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0] in Utils.tileIndexToItemIdArr
                            &&
                            (
                                Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]] == 'water'
                                ||
                                Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]] == 'lava'
                            )
                        ) {
                            didRightClickAction = true;
                            Globals.socket.emit('fillBucket', {
                                'x': this.markerTileX,
                                'y': this.markerTileY,
                            }, UtilsClient.handleSocketEmitResponse);
                            this.lastPlaceTimeMillis = currentTimeMillis + UtilsClient.playerUseTimeMillis;
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            this.selectedItemId
                            &&
                            Items.itemsArr[this.selectedItemId]['entityToPlaceType']
                            &&
                            Items.itemsArr[this.selectedItemId]['entityToPlaceType'] in Utils.allEntityTypesArr
                        ) {
                            didRightClickAction = true;
                            Globals.socket.emit('placeEntity', {
                                'x': this.markerTileX,
                                'y': this.markerTileY,
                            }, UtilsClient.handleSocketEmitResponse);
                            this.lastPlaceTimeMillis = currentTimeMillis + UtilsClient.playerUseTimeMillis;
                        } else if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            this.selectedItemId
                            &&
                            this.selectedItemId in Items.itemsArr // Selected item is valid.
                        ) {
                            if (
                                Items.itemsArr[this.selectedItemId]['isUsable']
                                &&
                                (
                                    !Items.itemsArr[this.selectedItemId]['hungerValue']
                                    ||
                                    Items.itemsArr[this.selectedItemId]['warmthValue']
                                    ||
                                    Items.itemsArr[this.selectedItemId]['healthValue']
                                    ||
                                    this.hunger < this.maxHunger
                                )
                            ) {
                                didRightClickAction = true;
                                if (Items.itemsArr[this.selectedItemId]['useSoundEffectKey']) {
                                    if (Items.itemsArr[this.selectedItemId]['hungerValue']) {
                                        if (this.hunger < this.maxHunger) {
                                            SoundEffect.play(Items.itemsArr[this.selectedItemId]['useSoundEffectKey']);
                                        }
                                    } else {
                                        SoundEffect.play(Items.itemsArr[this.selectedItemId]['useSoundEffectKey']);
                                    }
                                }
                                Globals.socket.emit('useItem', UtilsClient.handleSocketEmitResponse);
                            } else if (Items.itemsArr[this.selectedItemId]['canFeedEntities']) {
                                didRightClickAction      = true;
                                this.lastSwingTimeMillis = currentTimeMillis;
                                Globals.socket.emit('feedEntities', UtilsClient.handleSocketEmitResponse);
                            }
                        }
                        if (
                            !this.isGhost
                            &&
                            !this.projectileCharge
                            &&
                            !isDisableRightClick
                            &&
                            !didRightClickAction
                            &&
                            this.lastUseTimeMillis + UtilsClient.playerUseTimeMillis <= currentTimeMillis
                        ) {
                            didRightClickAction    = true;
                            this.lastUseTimeMillis = currentTimeMillis + UtilsClient.playerUseTimeMillis;
                            Globals.socket.emit('rideEntities', UtilsClient.handleSocketEmitResponse);
                        }
                        if (didRightClickAction) {
                            this.lastUseTimeMillis = currentTimeMillis;
                        }
                    }

                    if (
                        !this.isGhost
                        &&
                        !this.projectileCharge
                        &&
                        this.lastPlaceTimeMillis + UtilsClient.playerPlaceTimeMillis <= currentTimeMillis
                        &&
                        this.selectedItemId
                        &&
                        Items.itemsArr[this.selectedItemId]['itemToPlaceId'] // Can place selected item.
                        &&
                        Items.itemsArr[Items.itemsArr[this.selectedItemId]['itemToPlaceId']]['tileIndexesArr'] // Item has tile indexes.
                        &&
                        WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] == null // No item where going to place.
                        &&
                        (
                            // No item where going to place.
                            !this.markerTileIndex
                            ||
                            this.markerTileIndex == UtilsClient.transparentTileIndex
                        )
                    ) {
                        const itemToPlaceId = Items.itemsArr[this.selectedItemId]['itemToPlaceId'];
                        const worldCell     = WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX];
                        const floorCell     = WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX];
                        if (
                            (
                                Items.itemsArr[itemToPlaceId]['isFloor'] // Is floor, or,
                                ||
                                !worldCell // Isn't floor, and no block at cell, and,
                            )
                            &&
                            (
                                !Items.itemsArr[itemToPlaceId]['isFloor'] // Isn't floor, or,
                                ||
                                !floorCell // Is floor, and cell isn't set, or,
                                ||
                                Items.itemsArr[itemToPlaceId]['tileIndexesArr'].indexOf(parseInt(floorCell[0].toString())) == -1 // Is floor, floor is set, and isn't same floor as place item, and,
                            )
                            &&
                            (
                                !Items.itemsArr[itemToPlaceId]['cropRequiredFloorIdsArr'] // No crop required floors, or,
                                ||
                                Items.itemsArr[itemToPlaceId]['cropRequiredFloorIdsArr'].indexOf(Utils.tileIndexToItemIdArr[floorCell[0]]) != -1 // Has crop required floors, and is correct floor.
                            )
                        ) {
                            didRightClickAction = true;
                            Globals.socket.emit('placeBlock', {
                                'x': this.markerTileX,
                                'y': this.markerTileY,
                            }, UtilsClient.handleSocketEmitResponse);
                            this.lastUseTimeMillis   = currentTimeMillis;
                            this.lastPlaceTimeMillis = currentTimeMillis;
                        }
                    }
                }

                if (
                    !this.isGhost
                    &&
                    !this.projectileCharge
                    &&
                    this.ridingEntityId
                    &&
                    PlayerClient.spaceKey.isDown
                ) {
                    Globals.socket.emit('dismountEntity', UtilsClient.handleSocketEmitResponse);
                }

                if (
                    !this.isGhost
                    &&
                    !didRightClickAction
                    &&
                    this.selectedItemId
                    &&
                    Items.itemsArr[this.selectedItemId]['isFiringProjectiles']
                    &&
                    Utils.hasAmmoForProjectile(this) != null
                    &&
                    this.lastSwingTimeMillis + swingTimeMillis <= currentTimeMillis // Don't fire while swinging.
                ) {
                    const projectileMaxCharge = Items.itemsArr[this.selectedItemId]['projectileMaxCharge'];
                    if (
                        this.wasRightButtonDownLastUpdate
                        &&
                        !Globals.game.input.activePointer.rightButtonDown()
                        &&
                        (
                            (
                                this.selectedItemId != 'fireball'
                                &&
                                this.projectileCharge
                            )
                            ||
                            (
                                this.selectedItemId == 'fireball'
                                &&
                                this.projectileCharge >= projectileMaxCharge
                            )
                        )
                    ) { // Release projectile.
                        SoundEffect.play(Items.itemsArr[this.selectedItemId]['projectileSoundEffectKey']);
                        Globals.socket.emit('fireProjectile', {'projectileCharge': this.projectileCharge}, UtilsClient.handleSocketEmitResponse);
                        this.projectileCharge = 0;
                        this.updateProjectileCharge();
                    }
                    this.wasRightButtonDownLastUpdate = Globals.game.input.activePointer.rightButtonDown();
                    if (Globals.game.input.activePointer.rightButtonDown()) {
                        angleOffsetDeg = 90 * Math.min(Math.max(this.projectileCharge / projectileMaxCharge, 0), 1);
                        if (this.projectileCharge <= projectileMaxCharge) {
                            this.projectileCharge++;
                            if (this.projectileCharge == 1) {
                                UtilsClient.showAttackCooldown(projectileMaxCharge * Globals.delta);
                            }
                            this.updateProjectileCharge();
                        }
                    }
                }
            }
        }

        // Set speed.
        let playerFloorBlockId = null;
        if (
            WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded] != null
            &&
            WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0] in Utils.tileIndexToItemIdArr
            &&
            Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0]] in Items.itemsArr
        ) {
            playerFloorBlockId = Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0]];
        }

        let playerSpeed = Utils.playerSpeed;
        let ridingEntityType;
        if (this.ridingEntityId) {
            if (this.ridingEntityId in EntityClient.allEntityObjs) {
                ridingEntityType = EntityClient.allEntityObjs[this.ridingEntityId].type;
                playerSpeed      = Utils.allEntityTypesArr[ridingEntityType]['speed'];
            } else {
                this.ridingEntityId = null;
            }
        }
        if (this.isGhost) {
            playerSpeed *= 2;
        } else {
            if (this.selectedItemId && Items.itemsArr[this.selectedItemId]['speedMult'] && !Items.itemsArr[this.selectedItemId]['isFloor']) {
                playerSpeed *= Items.itemsArr[this.selectedItemId]['speedMult'];
            } else {
                for (let i in this.accessoryItemsArr) {
                    if (
                        this.accessoryItemsArr[i]['itemId']
                        &&
                        this.accessoryItemsArr[i]['itemId'] in Items.itemsArr
                        &&
                        Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['speedMult']
                        &&
                        !Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['isFloor']
                    ) {
                        playerSpeed *= Items.itemsArr[this.accessoryItemsArr[i]['itemId']]['speedMult'];
                        break;
                    }
                }
            }
            if (this.projectileCharge) {
                playerSpeed *= 0.5;
            }
        }
        if (playerFloorBlockId) {
            let speedMult = Items.itemsArr[playerFloorBlockId]['speedMult'] || 1;
            playerSpeed *= speedMult;
            // @ts-ignore
            if (speedMult > 1 && this.sprite.body.drag.x != 750) {
                // this.sprite.setDrag(750, 750);
                // @ts-ignore
            } else if (this.sprite.body.drag.x != 1250) {
                // this.sprite.setDrag(1250, 1250);
            }
            // @ts-ignore
        } else if (this.sprite.body.drag.x != 1250) {
            // this.sprite.setDrag(1250, 1250);
        }

        // Handle sprinting.
        for (const i in PlayerClient.directionsArr) {
            const directionStr                                  = PlayerClient.directionsArr[i];
            PlayerClient.keyDownDataArr[directionStr]['isDown'] = (PlayerClient.arrowKeys[directionStr].isDown || PlayerClient.wasdKeys[directionStr].isDown);
            if (!PlayerClient.keyDownDataArr[directionStr]['isDownLastFrame'] && PlayerClient.keyDownDataArr[directionStr]['isDown']) {
                if (currentTimeMillis - PlayerClient.keyDownDataArr[directionStr]['timeOnLastKeypress'] <= PlayerClient.sprintDoubleTapTimeoutMillis) {
                    PlayerClient.keyDownDataArr[directionStr]['isSprinting'] = true;
                }
                PlayerClient.keyDownDataArr[directionStr]['timeOnLastKeypress'] = currentTimeMillis;
                PlayerClient.keyDownDataArr[directionStr]['isDownLastFrame']    = true;
            }

            // Released key.
            if (PlayerClient.keyDownDataArr[directionStr]['isDownLastFrame'] && !PlayerClient.keyDownDataArr[directionStr]['isDown']) {
                PlayerClient.keyDownDataArr[directionStr]['isSprinting']     = false;
                PlayerClient.keyDownDataArr[directionStr]['isDownLastFrame'] = false;
            }
        }
        if (this.ridingEntityId) {
            PlayerClient.isSprinting = (
                PlayerClient.shiftKey.isDown
                ||
                PlayerClient.keyDownDataArr['up']['isSprinting']
            );
        } else {
            PlayerClient.isSprinting = (
                PlayerClient.shiftKey.isDown
                ||
                PlayerClient.keyDownDataArr['left']['isSprinting']
                ||
                PlayerClient.keyDownDataArr['right']['isSprinting']
                ||
                PlayerClient.keyDownDataArr['up']['isSprinting']
                ||
                PlayerClient.keyDownDataArr['down']['isSprinting']
            );
        }
        if (PlayerClient.isSprinting) {
            playerSpeed *= 1.5;
        }

        // Handle movement.
        let ridingEntitySprite;
        if (this.ridingEntityId) {
            // @ts-ignore
            ridingEntitySprite = EntityClient.allEntityObjs[this.ridingEntityId].sprite;
            if (PlayerClient.keyDownDataArr['up']['isDown']) {
                Globals.game.physics.velocityFromAngle(
                    ridingEntitySprite.angle - Utils.allEntityTypesArr[ridingEntityType]['angleOffset'],
                    // @ts-ignore
                    Math.min(ridingEntitySprite.body.speed + 10, playerSpeed),
                    ridingEntitySprite.body.velocity,
                );
            } else if (PlayerClient.keyDownDataArr['down']['isDown']) {
                Globals.game.physics.velocityFromAngle(
                    ridingEntitySprite.angle - Utils.allEntityTypesArr[ridingEntityType]['angleOffset'] + 180,
                    // @ts-ignore
                    Math.min(ridingEntitySprite.body.speed + 10, playerSpeed * 0.5),
                    ridingEntitySprite.body.velocity,
                );
            }

            if (PlayerClient.keyDownDataArr['left']['isDown']) {
                // @ts-ignore
                ridingEntitySprite.setAngularVelocity(Math.max(ridingEntitySprite.body.angularVelocity - 100, -150));
            } else if (PlayerClient.keyDownDataArr['right']['isDown']) {
                // @ts-ignore
                ridingEntitySprite.setAngularVelocity(Math.min(ridingEntitySprite.body.angularVelocity + 100, 150));
            }

            this.sprite.x = ridingEntitySprite.x;
            this.sprite.y = ridingEntitySprite.y;
        }
        if (!this.ridingEntityId) {
            let movementPoint = {'x': 0, 'y': 0};
            // @formatter:off
            if (PlayerClient.keyDownDataArr['left' ]['isDown']) { movementPoint['x'] =  1; }
            if (PlayerClient.keyDownDataArr['right']['isDown']) { movementPoint['x'] = -1; }
            if (PlayerClient.keyDownDataArr['up'   ]['isDown']) { movementPoint['y'] =  1; }
            if (PlayerClient.keyDownDataArr['down' ]['isDown']) { movementPoint['y'] = -1; }
            // @formatter:on
            if (movementPoint['x'] || movementPoint['y']) {
                const movementAngle = Utils.toDegrees(Utils.rotation({'x': 0, 'y': 0}, movementPoint));
                Globals.game.physics.velocityFromAngle(movementAngle, playerSpeed, this.sprite.body.velocity);
            }
        }
        if (this.lastSwingTimeMillis + swingTimeMillis >= currentTimeMillis) {
            const sinceStartSwingMillis = currentTimeMillis - this.lastSwingTimeMillis;
            if (sinceStartSwingMillis <= swingTimeMillis / 2) {
                angleOffsetDeg = (sinceStartSwingMillis / (swingTimeMillis / 2)) * -90;
            } else {
                angleOffsetDeg = ((swingTimeMillis - sinceStartSwingMillis) / (swingTimeMillis / 2)) * -90;
            }
        }
        this.sprite.angle = (Phaser.Math.RAD_TO_DEG * parseFloat(Phaser.Math.Angle.Between(this.sprite.x, this.sprite.y, Globals.game.input.activePointer.x + Globals.game.cameras.main.scrollX, Globals.game.input.activePointer.y + Globals.game.cameras.main.scrollY).toFixed(2))) + angleOffsetDeg;

        // Emit player movement.
        const currentXRounded     = Math.round(this.sprite.x * 10) / 10;
        const currentYRounded     = Math.round(this.sprite.y * 10) / 10;
        const currentAngleRounded = Math.round(this.sprite.angle * 10) / 10;
        if (UtilsClient.byId('compassModal').style.display == 'block') {
            Minimap.updateCompassModal();
        }
        if (
            this.oldPosition
            &&
            (
                currentXRounded !== this.oldPosition.x
                ||
                currentYRounded !== this.oldPosition.y
                ||
                currentAngleRounded !== this.oldPosition.angle
                ||
                this.projectileCharge !== this.oldPosition.projectileCharge
            )
        ) {
            this.moveSpritesWithPlayer();
            const movementData: import('../HunkerInterfaces').MovementData = {
                'x':                currentXRounded,
                'y':                currentYRounded,
                'angle':            Math.round(this.sprite.angle * 10) / 10,
                'projectileCharge': this.projectileCharge,
            };
            if (PlayerClient.isSprinting) {
                movementData['isSprinting'] = true;
            }
            if (this.ridingEntityId) {
                movementData['ridingEntityAngle'] = ridingEntitySprite.angle - Utils.allEntityTypesArr[EntityClient.allEntityObjs[this.ridingEntityId].type]['angleOffset'];
            }
            Globals.socket.emit('playerMovement', movementData);
            if (UtilsClient.byId('bigMinimapModal').style.display == 'block') {
                document.querySelector('#bigMinimapModal .minimapDot.minimapPlayer.playerSelf').setAttribute('style', Minimap.getPlayerPositionCss(Globals.currentPlayerObj));
            }
            if (
                currentXRounded !== this.oldPosition.x
                ||
                currentYRounded !== this.oldPosition.y
            ) {
                // Step sounds.
                let nextPlayStepTimeMillis;
                let boatSpeedMult;
                if (ridingEntityType == 'boatEntity') {
                    // @ts-ignore
                    boatSpeedMult          = Math.max(Utils.allEntityTypesArr[ridingEntityType]['speed'] / ridingEntitySprite.body.speed, 0.25);
                    nextPlayStepTimeMillis = this.lastPlayStepTimeMillis + 1000 * boatSpeedMult;

                    if (Math.random() > boatSpeedMult - 0.2) {
                        ParticleEffect.play('boatTrail', this.sprite.x, this.sprite.y);
                    }
                } else {
                    nextPlayStepTimeMillis = this.lastPlayStepTimeMillis + 500 * (Utils.playerDefaultSpeed / playerSpeed);
                }
                if (
                    !this.lastPlayStepTimeMillis
                    ||
                    currentTimeMillis >= nextPlayStepTimeMillis
                ) {
                    this.lastPlayStepTimeMillis = currentTimeMillis;
                    if (ridingEntityType == 'boatEntity') {
                        SoundEffect.play('paddle_water');
                        for (let i = 0; i < 5 / boatSpeedMult; i++) {
                            ParticleEffect.play('boatPaddle', this.sprite.x, this.sprite.y);
                        }
                    } else {
                        if (
                            playerFloorBlockId
                            &&
                            Items.itemsArr[playerFloorBlockId]['isDisablingCollision']
                            &&
                            Items.itemsArr[playerFloorBlockId]['stepSoundEffectKey']
                        ) {
                            SoundEffect.play(Items.itemsArr[playerFloorBlockId]['stepSoundEffectKey']);
                        } else {
                            if (WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0] in Utils.tileIndexToItemIdArr) {
                                const floorItemId = Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][yRounded][xRounded][0]];
                                SoundEffect.play(Items.itemsArr[floorItemId]['stepSoundEffectKey']);
                            } else {
                                SoundEffect.play('step_sand');
                            }
                        }
                    }
                }

                // Alpha trees.
                for (const blockCoordStr in WorldClient.allLeavesOverlaySpritesArr) {
                    const blockLayerNum = parseInt(blockCoordStr.split(':')[0]);
                    if (this.currentLayerNum == blockLayerNum) {
                        const blockX = parseInt(blockCoordStr.split(':')[1].split(',')[0]);
                        const blockY = parseInt(blockCoordStr.split(':')[1].split(',')[1]);
                        if (Math.abs(blockX - xRounded) <= 1 && Math.abs(blockY - yRounded) <= 1) {
                            if (Math.round(WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].alpha * 10) / 10 == 1) {
                                Globals.game.add.tween({
                                    'targets':  WorldClient.allLeavesOverlaySpritesArr[blockCoordStr],
                                    'alpha':    0.2,
                                    'duration': 100,
                                });
                            }
                        } else {
                            if (Math.round(WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].alpha * 10) / 10 == 0.2) {
                                Globals.game.add.tween({
                                    'targets':  WorldClient.allLeavesOverlaySpritesArr[blockCoordStr],
                                    'alpha':    1,
                                    'duration': 1000,
                                });
                            }
                        }
                    }
                }
            }
        }
        // Save old position data.
        this.oldPosition = {
            'x':                currentXRounded,
            'y':                currentYRounded,
            'angle':            currentAngleRounded,
            'projectileCharge': this.projectileCharge,
            'markerTileX':      this.markerTileX,
            'markerTileY':      this.markerTileY,
        };
    }

    moveSpritesWithPlayer() {
        if (this.indicatorSprite && this.indicatorSprite.visible) {
            this.indicatorSprite.x = this.sprite.x;
            this.indicatorSprite.y = this.sprite.y - 60;
        }
        this.selectedItemSprite.x     = this.sprite.x;
        this.selectedItemSprite.y     = this.sprite.y;
        this.selectedItemSprite.angle = this.sprite.angle + this.selectedItemSprite.getData('angleOffset');
        if (this.attackCooldownRectangle) {
            this.attackCooldownRectangle.x = this.sprite.x;
            this.attackCooldownRectangle.y = this.sprite.y;
        }
        this.nameTag.x        = this.sprite.x;
        this.nameTag.y        = this.sprite.y;
        this.lightCircleObj.x = this.sprite.x;
        this.lightCircleObj.y = this.sprite.y;
        if (this.id == Globals.socket.id) {
            const _this = this;
            PlayerClient.newItemSpritesGroup.getChildren().forEach(function (childSprite: Phaser.GameObjects.Sprite) {
                if (childSprite.visible && childSprite.alpha) {
                    childSprite.x = _this.sprite.x;
                    childSprite.y = _this.sprite.y;
                }
            });
            PlayerClient.newItemTextsGroup.getChildren().forEach(function (childText: Phaser.GameObjects.Sprite) {
                if (childText.visible && childText.alpha) {
                    childText.x = _this.sprite.x;
                    childText.y = _this.sprite.y;
                }
            });
        }
        const currentXRounded = Math.round(this.sprite.x * 10) / 10;
        const currentYRounded = Math.round(this.sprite.y * 10) / 10;
        if (
            this.oldPosition
            &&
            (
                currentXRounded !== this.oldPosition.x
                ||
                currentYRounded !== this.oldPosition.y
            )
        ) {
            Minimap.updatePos(this.$minimapPlayer, this.sprite);
        }
        const xRounded        = Math.floor(this.sprite.x / Utils.tileSize);
        const yRounded        = Math.floor(this.sprite.y / Utils.tileSize);
        const currentBiomeStr = Minimap.$currentBiomeDiv.text();
        let newBiomeStr       = 'Ocean';
        if (
            WorldClient.biomeCoordsPerLayerArr[this.currentLayerNum]
            &&
            WorldClient.biomeCoordsPerLayerArr[this.currentLayerNum][yRounded]
            &&
            WorldClient.biomeCoordsPerLayerArr[this.currentLayerNum][yRounded][xRounded]
        ) {
            const biomeName = WorldClient.biomeCoordsPerLayerArr[this.currentLayerNum][yRounded][xRounded];
            newBiomeStr     = Utils.biomeDetailsArr[this.currentLayerNum][biomeName]['label'];
        } else if (Utils.tileIndexToItemIdArr[WorldClient.floorArr[0][yRounded][xRounded][0]] == 'sand') {
            newBiomeStr = 'Beach';
        }
        if (newBiomeStr != currentBiomeStr) {
            Minimap.$currentBiomeDiv.text(newBiomeStr);
        }
    }

    enchantingTableButtonClick() {
        Globals.socket.emit('enchantItem', {
            'x': this.markerTileX,
            'y': this.markerTileY,
        }, function (response: import('../HunkerInterfaces').ServerResponse) {
            UtilsClient.handleSocketEmitResponse(response);
            if (response['status'] == 'success') {
                SoundEffect.play('enchant');
            }
        });
    }

    static clickRail() {
        Globals.socket.emit('clickRail', function (response: import('../HunkerInterfaces').ServerResponse) {
            UtilsClient.handleSocketEmitResponse(response);
            if (response['status'] == 'success') {
                // SoundEffect.play('rail');
                Globals.game.cameras.main.fade(500, 0, 0, 0);
                SoundEffect.play('inside');
                SoundEffect.audioObjsArr['inside'].volume = SoundEffect.effectsArr['inside']['volume'] * 0.2;
                PlayerClient.clickRailTimeoutsArr[1]      = setTimeout(function () {
                    delete PlayerClient.clickRailTimeoutsArr[1];
                    SoundEffect.audioObjsArr['inside'].volume = SoundEffect.effectsArr['inside']['volume'] * 0.5;
                }, 250);
                PlayerClient.isClickingRail               = true;
                PlayerClient.clickRailTimeoutsArr[0]      = setTimeout(function () {
                    delete PlayerClient.clickRailTimeoutsArr[0];
                    PlayerClient.isClickingRail               = false;
                    SoundEffect.audioObjsArr['inside'].volume = SoundEffect.effectsArr['inside']['volume'] * 0.7;
                    PlayerClient.clickRailTimeoutsArr[2]      = setTimeout(function () {
                        delete PlayerClient.clickRailTimeoutsArr[2];
                        SoundEffect.audioObjsArr['inside'].volume = SoundEffect.effectsArr['inside']['volume'] * 0.5;
                        PlayerClient.clickRailTimeoutsArr[3]      = setTimeout(function () {
                            delete PlayerClient.clickRailTimeoutsArr[3];
                            SoundEffect.audioObjsArr['inside'].volume = SoundEffect.effectsArr['inside']['volume'] * 0.2;
                        }, 500);
                        PlayerClient.clickRailTimeoutsArr[4]      = setTimeout(function () {
                            delete PlayerClient.clickRailTimeoutsArr[4];
                            SoundEffect.audioObjsArr['inside'].stop();
                        }, 1000);
                        Globals.game.cameras.main.fadeIn(500, 0, 0, 0);
                        Globals.enableInput();
                    }, 500);
                }, 500);
            } else {
                Globals.enableInput();
            }
        });
    }

    static cancelClickRail() {
        if (PlayerClient.isClickingRail) {
            Globals.socket.emit('cancelClickRail', function (response: import('../HunkerInterfaces').ServerResponse) {
                UtilsClient.handleSocketEmitResponse(response);
                if (response['status'] == 'success') {
                    PlayerClient.isClickingRail = false;
                    SoundEffect.audioObjsArr['inside'].stop();
                    SoundEffect.audioObjsArr['rail'].stop();
                    for (let i in PlayerClient.clickRailTimeoutsArr) {
                        clearTimeout(PlayerClient.clickRailTimeoutsArr[i]);
                        delete PlayerClient.clickRailTimeoutsArr[i];
                    }
                    Globals.enableInput();
                    Globals.game.cameras.main.fadeIn(500, 0, 0, 0);
                }
            });
        }
    }

    playerRideEntity(ridingEntityId: string): void {
        this.ridingEntityId                                     = ridingEntityId;
        EntityClient.allEntityObjs[ridingEntityId].sprite.depth = UtilsClient.depthsArr['playerRidingEntity'];
        EntityClient.allEntityObjs[ridingEntityId].sprite.enableBody(false, 0, 0, true, false);
        EntityClient.allEntityObjs[ridingEntityId].sprite.setDrag(200);
        EntityClient.allEntityObjs[ridingEntityId].sprite.setAngularDrag(500);
        EntityClient.allEntityObjs[ridingEntityId].sprite.setCollideWorldBounds(true);
        EntityClient.allEntityObjs[ridingEntityId].sprite.setFriction(0);
        Globals.game.physics.add.collider(EntityClient.allEntityObjs[ridingEntityId].sprite, WorldClient.layerFloor);
        Globals.game.physics.add.collider(EntityClient.allEntityObjs[ridingEntityId].sprite, WorldClient.layerCollide);
        EntityClient.allEntityObjs[ridingEntityId].sprite.body.setSize(Utils.tileSize - 10, Utils.tileSize - 10);
    }

    playerDismountEntity(): void {
        EntityClient.allEntityObjs[this.ridingEntityId].sprite.body.velocity.x = 0;
        EntityClient.allEntityObjs[this.ridingEntityId].sprite.body.velocity.y = 0;
        this.ridingEntityId                                                    = null;
    }

    updatePos(movementData: import('../HunkerInterfaces').MovementData) {
        this.sprite.x = movementData['x'];
        this.sprite.y = movementData['y'];
        if ('angle' in movementData) {
            this.sprite.angle = movementData['angle'];
        }
        this.ridingEntityId = movementData['ridingEntityId'];
        if (this.ridingEntityId && this.ridingEntityId in EntityClient.allEntityObjs) {
            EntityClient.allEntityObjs[this.ridingEntityId].sprite.depth = UtilsClient.depthsArr['playerRidingEntity'];
            EntityClient.allEntityObjs[this.ridingEntityId].sprite.x     = this.sprite.x;
            EntityClient.allEntityObjs[this.ridingEntityId].sprite.y     = this.sprite.y;
            EntityClient.allEntityObjs[this.ridingEntityId].sprite.angle = movementData['ridingEntityAngle'] + Utils.allEntityTypesArr[EntityClient.allEntityObjs[this.ridingEntityId].type]['angleOffset'];
        }
        this.moveSpritesWithPlayer();
        this.projectileCharge = movementData['projectileCharge'];
        this.updateProjectileCharge();
        if ('isGhost' in movementData) {
            this.updateIsGhost(movementData['isGhost']);
        }
    }

    updateProjectileCharge() {
        if (this.selectedItemId == 'bow') {
            PlayerClient.updateBowProjectileCharge(this);
        }
    }

    static updateBowProjectileCharge(obj: PlayerClient | EntityClient) {
        let projectileCharge;
        let projectileMaxCharge;
        if (obj instanceof EntityClient) {
            if (obj.type == 'archer') {
                projectileCharge    = obj.archerProjectileCharge;
                projectileMaxCharge = obj.archerProjectileMaxCharge;
            } else {
                throw new Error("Only run updateBowProjectileCharge on archers or players");
            }
        } else {
            projectileCharge    = obj.projectileCharge;
            projectileMaxCharge = Items.itemsArr['bow']['projectileMaxCharge'];
        }
        if (projectileCharge >= 0 && projectileCharge < Math.floor(projectileMaxCharge * (1 / 8))) {
            obj.selectedItemSprite.setTexture('bow');
            PlayerClient.updateSelectedItemSpritePosition(obj.selectedItemSprite, obj.sprite, Items.itemsArr['bow']);
        } else {
            if (obj.selectedItemSprite.x != PlayerClient.bowHeldPositionArr['heldSpriteOriginX'] || obj.selectedItemSprite.y != PlayerClient.bowHeldPositionArr['heldSpriteOriginY']) {
                PlayerClient.updateSelectedItemSpritePosition(obj.selectedItemSprite, obj.sprite, PlayerClient.bowHeldPositionArr);
            }
            if (projectileCharge >= Math.floor(projectileMaxCharge * (1 / 8)) && projectileCharge < Math.floor(projectileMaxCharge * (2 / 8))) {
                obj.selectedItemSprite.setTexture('bow1');
            } else if (projectileCharge >= Math.floor(projectileMaxCharge * (2 / 8)) && projectileCharge < Math.floor(projectileMaxCharge * (3 / 8))) {
                obj.selectedItemSprite.setTexture('bow2');
            } else if (projectileCharge >= Math.floor(projectileMaxCharge * (3 / 8)) && projectileCharge < Math.floor(projectileMaxCharge * (4 / 8))) {
                obj.selectedItemSprite.setTexture('bow3');
            } else if (projectileCharge >= Math.floor(projectileMaxCharge * (4 / 8)) && projectileCharge < Math.floor(projectileMaxCharge * (5 / 8))) {
                obj.selectedItemSprite.setTexture('bow4');
            } else if (projectileCharge >= Math.floor(projectileMaxCharge * (5 / 8)) && projectileCharge < Math.floor(projectileMaxCharge * (6 / 8))) {
                obj.selectedItemSprite.setTexture('bow5');
            } else if (projectileCharge >= Math.floor(projectileMaxCharge * (6 / 8)) && projectileCharge < Math.floor(projectileMaxCharge * (7 / 8))) {
                obj.selectedItemSprite.setTexture('bow6');
            } else if (projectileCharge >= Math.floor(projectileMaxCharge * (7 / 8)) && projectileCharge < projectileMaxCharge) {
                obj.selectedItemSprite.setTexture('bow7');
            } else if (projectileCharge >= projectileMaxCharge) {
                obj.selectedItemSprite.setTexture('bow8');
            }
        }
    }

    updateCurrentLayerNum(currentLayerNum: number): void {
        this.currentLayerNum         = currentLayerNum;
        this.lightCircleObj.layerNum = currentLayerNum;
        if (this.id == Globals.socket.id) {
            WorldClient.updateCurrentLayerNum(currentLayerNum);
            WorldClient.initWorld();
            WorldClient.initFloor();
            DayNightCycleClient.manageBackgroundAudio();

            // Update other players relative to current player.
            for (let playerId in PlayerClient.allPlayerObjs) {
                if (playerId != this.id) {
                    const playerObj = PlayerClient.allPlayerObjs[playerId];
                    playerObj.updateCurrentLayerNum(playerObj.currentLayerNum);
                }
            }
        }
        this.sprite.visible             = (currentLayerNum == WorldClient.currentLayerNum);
        this.selectedItemSprite.visible = (currentLayerNum == WorldClient.currentLayerNum);
        this.nameTag.visible            = (currentLayerNum == WorldClient.currentLayerNum);
    }

    updateMarkerCrackSprite() {
        if (
            (
                this.markerTileId
                &&
                this.markerTileId in Items.itemsArr
                &&
                Items.itemsArr[this.markerTileId]['structureDurability']
                &&
                WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] != null
            )
            ||
            (
                this.selectedItemId
                &&
                (
                    this.selectedItemId.indexOf('hoe_') == 0
                    ||
                    this.selectedItemId.indexOf('shovel_') == 0
                )
                &&
                this.selectedItemId in Items.itemsArr
            )
        ) {
            this.markerCrackSprite.visible = true;
            if (WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX] != null && this.markerTileId && this.markerTileId in Items.itemsArr) {
                this.markerCrackSprite.setTexture(WorldClient.getDestroyStageSprite(WorldClient.worldArr[this.currentLayerNum][this.markerTileY][this.markerTileX][1], Items.itemsArr[this.markerTileId]['structureDurability']));
                this.markerCrackSprite.alpha = 0.75;
            } else {
                const floorBlockId = Utils.tileIndexToItemIdArr[WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX][0]];
                this.markerCrackSprite.setTexture(WorldClient.getDestroyStageSprite(WorldClient.floorArr[this.currentLayerNum][this.markerTileY][this.markerTileX][1], Items.itemsArr[floorBlockId]['structureDurability']));
                this.markerCrackSprite.alpha = 0.25;
            }
            this.markerCrackSprite.displayWidth  = Utils.tileSize;
            this.markerCrackSprite.displayHeight = Utils.tileSize;
        } else {
            this.markerCrackSprite.visible = false;
        }
    }

    /**
     * Create various groups and objects. Run inside the Phaser create function.
     */
    static init() {
        PlayerClient.isPlayerColliding      = (localStorage.getItem('isPlayerColliding') != '0');
        PlayerClient.allPlayerEntitiesGroup = Globals.game.add.group();
        PlayerClient.newItemSpritesGroup    = Globals.game.add.group();
        PlayerClient.newItemTextsGroup      = Globals.game.add.group();
        PlayerClient.arrowKeys              = Globals.game.input.keyboard.createCursorKeys();
        PlayerClient.escKey                 = Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ESC);
        PlayerClient.spaceKey               = Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        PlayerClient.shiftKey               = Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SHIFT);
        PlayerClient.altKey                 = Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ALT);
        PlayerClient.wasdKeys               = {
            'up':    Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
            'left':  Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
            'down':  Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
            'right': Globals.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
        };
        $(
            "<div id=\"pauseGameBtn\" class=\"itemSlot\" title=\"Pause game\">" +
            " <div class=\"item btn-flat\">" +
            "  <img class=\"itemImg\" src=\"/public/assets/ui/pause.png\" alt=\"Pause\">" +
            " </div>" +
            "</div>",
        ).appendTo('body').on('click', function () {
            if (Globals.currentPlayerObj.isPaused) {
                Globals.socket.emit('playGame', UtilsClient.handleSocketEmitResponse);
            } else {
                Globals.socket.emit('pauseGame', UtilsClient.handleSocketEmitResponse);
            }
        });
        $('#playGameBtn').on('click', function () {
            Globals.socket.emit('playGame', UtilsClient.handleSocketEmitResponse);
        });
    }

    static getNameTag(playerId: string, useFullName = false): string {
        if (playerId == Globals.socket.id) {
            return "<b style=\"color: " + PlayerClient.selfColor + ";\">" + ((useFullName) ? PlayerClient.allPlayerObjs[playerId].name : 'YOU') + "</b>";
        } else {
            return "<b style=\"color: " + PlayerClient.enemyColor + ";\">" + PlayerClient.allPlayerObjs[playerId].name + "</b>";
        }
    }

}
