/// <reference path="../Chat.ts" />
/// <reference path="TimeSpent.ts" />
/// <reference path="Globals.ts" />

class ChatClient {

    static readonly messagesToKeepNum: number     = 5;
    static readonly fadeOutDurationMillis: number = 500;
    static readonly fadeOutDelayMillis: number    = 5000;
    static $messagesList: JQuery;
    static $messageInput: JQuery;

    static initElements(): void {
        $(
            // "<div id=\"messagesContainer\">\n" +
            " <div id=\"messagesList\"></div>\n" +
            " <div id=\"messageInputContainer\">\n" +
            "  <input placeholder=\"Type a message...\" id=\"messageInput\" type=\"text\" maxlength=\"100\">\n" +
            // " </div>\n" +
            "</div>",
        ).appendTo('body');
        ChatClient.$messagesList = $('#messagesList');
        ChatClient.$messageInput = $('#messageInput');
        ChatClient.$messageInput.focus(function () {
            Globals.disableInput();
        });
        ChatClient.$messageInput.blur(function () {
            Globals.enableInput();
        });
        ChatClient.$messageInput.keyup(function (event) {
            const $this = $(this);
            if (event.key === 'Enter') {
                const messageText = $this.val();
                if (typeof messageText === 'string' && messageText !== '') {
                    if (messageText.indexOf('/') == 0) {
                        let isValidCommand          = false;
                        let didExecuteLocalFunction = false;
                        for (let commandKey in Chat.allCommandsArr) {
                            if (messageText.indexOf(commandKey) == 0) {
                                if (Chat.allCommandsArr[commandKey]['isLocal']) {
                                    // @ts-ignore
                                    ChatClient[Chat.allCommandsArr[commandKey]['functionName']](messageText);
                                    didExecuteLocalFunction = true;
                                }
                                isValidCommand = true;
                                break;
                            }
                        }
                        if (!isValidCommand) {
                            ChatClient.showHelpMessage(messageText);
                        } else if (isValidCommand && !didExecuteLocalFunction) {
                            Globals.socket.emit('playerMessage', messageText); // Emit command to server.
                        }
                    } else {
                        Globals.socket.emit('playerMessage', messageText);
                    }
                    $this.val('');
                }
                $this.blur();
                return false;
            } else if (event.key === 'Escape') {
                $this.blur();
                return false;
            }
        });

        $(document).keyup(function (event) {
            if (!ChatClient.$messageInput.is(':focus')) {
                if (event.key === 'Enter') {
                    ChatClient.$messageInput.focus();
                } else if (event.key === '/') {
                    ChatClient.$messageInput.focus();
                    ChatClient.$messageInput.val('/');
                }
            }
        });
    }

    static setPlayerSpeed(messageText: string) {
        let speedMultiplier  = parseFloat(messageText.slice('/speed '.length));
        speedMultiplier      = Math.max(speedMultiplier, 0);
        speedMultiplier      = Math.min(speedMultiplier, (Utils.isDev ? 20 : 3));
        const newPlayerSpeed = Utils.playerDefaultSpeed * speedMultiplier;
        if (speedMultiplier && newPlayerSpeed) {
            Utils.playerSpeed = newPlayerSpeed;
            if (newPlayerSpeed == Utils.playerDefaultSpeed) {
                localStorage.removeItem('playerSpeed');
            } else {
                localStorage.setItem('playerSpeed', JSON.stringify(newPlayerSpeed));
            }
            ChatClient.addMessage({
                'status': 'success',
                'text':   "&#x2714; Speed set to " + speedMultiplier,
            });
        } else {
            ChatClient.addMessage({
                'status': 'error',
                'text':   "Invalid command, try <code>/speed 2</code>",
            });
        }
    }

    static showHelpMessage(messageText: string) {
        let helpText = '';
        if (messageText && messageText.indexOf('/help') != 0) {
            helpText += "<span class=\"commandError\">Invalid command \"" + messageText + "\"</span><br>";
        }
        helpText += "List of commands:<br>";
        helpText += "<ul>";
        for (let commandKey in Chat.allCommandsArr) {
            helpText += "<li><code style=\"width: 100px;\">" + Chat.allCommandsArr[commandKey]['exampleCommandStr'] + "</code>" + Chat.allCommandsArr[commandKey]['descriptionText'] + "</li>";
        }
        helpText += "</ul>";
        ChatClient.addMessage({
            'text': helpText,
        });
    }

    static addMessage(messageData: import('../HunkerInterfaces').MessageData) {
        if (Globals.currentPlayerObj && typeof messageData['hotbarItemsArr'] !== 'undefined') {
            Globals.currentPlayerObj.hotbarItemsArr = messageData['hotbarItemsArr'];
            HotbarClient.update();
        }
        if ('playerId' in messageData && messageData['playerId'] != Globals.socket.id && 'soundEffectKey' in messageData) {
            SoundEffect.play(messageData['soundEffectKey']);
        }
        const $messagesListItems = ChatClient.$messagesList.children('div');
        if ($messagesListItems.length >= ChatClient.messagesToKeepNum) {
            // Remove the first items, so that there's always <=messagesToKeepNum messages.
            $messagesListItems.slice(0, $messagesListItems.length - (ChatClient.messagesToKeepNum - 1)).fadeOut(ChatClient.fadeOutDurationMillis, function () {
                $(this).remove();
            });
        }
        let html = '';
        html += "<div class=\"message";
        if ('status' in messageData) {
            html += ` command${Utils.ucfirst(messageData['status'])}`;
        }
        html += "\">";
        if ('playerId' in messageData) {
            html += PlayerClient.getNameTag(messageData['playerId']) + " ";
        }
        html += messageData['text'];
        html += "</div>";
        const $message = $(html).appendTo(ChatClient.$messagesList);
        setTimeout(function () {
            if ($message.length) {
                $message.fadeOut(ChatClient.fadeOutDurationMillis, function () {
                    $(this).remove();
                });
            }
        }, ChatClient.fadeOutDelayMillis);
    }

    static setLightTick(messageText: string) {
        LightCircle.lightTick = parseInt(messageText.slice('/lighttick '.length));
        LightCircle.lightTick = Math.max(LightCircle.lightTick, 1);
        LightCircle.lightTick = Math.min(LightCircle.lightTick, 10);
        if (LightCircle.lightTick == LightCircle.defaultLightTick) {
            localStorage.removeItem('lightTick');
        } else {
            localStorage.setItem('lightTick', JSON.stringify(LightCircle.lightTick));
        }
        ChatClient.addMessage({
            'status': 'success',
            'text':   "&#x2714; Light tick set to " + LightCircle.lightTick,
        });
    }

    static brighten() {
        LightCircle.isBrightening = true;
        LightCircle.updateShadowRgb();
        localStorage.setItem('isBrightening', '1');
        ChatClient.addMessage({
            'status': 'success',
            'text':   "&#x2714; Brightened",
        });
    }

    static darken() {
        LightCircle.isBrightening = false;
        LightCircle.updateShadowRgb();
        localStorage.setItem('isBrightening', '0');
        ChatClient.addMessage({
            'status': 'success',
            'text':   "&#x2714; Darkened",
        });
    }

    static collide(messageText: string) {
        PlayerClient.isPlayerColliding = (messageText.slice('/collide '.length) == '1');
        localStorage.setItem('isPlayerColliding', (PlayerClient.isPlayerColliding ? '1' : '0'));
        ChatClient.addMessage({
            'status': 'success',
            'text':   "&#x2714; " + (PlayerClient.isPlayerColliding ? "Enabled" : "Disabled") + " collisions",
        });
    }

    static timespent(messageText: string) {
        const newTimeSpentSecs = parseInt(messageText.slice('/timespent '.length));
        if (newTimeSpentSecs || newTimeSpentSecs === 0) {
            TimeSpent.secs = newTimeSpentSecs;
            localStorage.setItem('timeSpentSecs', TimeSpent.secs.toString());
            ChatClient.addMessage({
                'status': 'success',
                'text':   "&#x2714; Set to " + newTimeSpentSecs,
            });
        } else {
            ChatClient.addMessage({
                'status': 'error',
                'text':   "&#x2714; Invalid arguments",
            });
        }
    }

    static showHeldSpriteAdjuster() {
        let out = '';
        out += "<table id=\"heldSpriteAdjuster\">";
        out += "  <tr>";
        out += "    <td>X: </td>";
        out += "    <td><input type=\"number\" id=\"heldSpriteOriginX\" value=\"0\" style=\"width: 50px;\"></td>";
        out += "    <td><input type=\"range\" min=\"-3\" max=\"3\" value=\"0\" step=\"0.01\" id=\"heldSpriteOriginXSlider\"></td>";
        out += "  </tr>";
        out += "  <tr>";
        out += "    <td>Y: </td>";
        out += "    <td><input type=\"number\" id=\"heldSpriteOriginY\" value=\"0\" style=\"width: 50px;\"></td>";
        out += "    <td><input type=\"range\" min=\"-3\" max=\"3\" value=\"0\" step=\"0.01\" id=\"heldSpriteOriginYSlider\"></td>";
        out += "  </tr>";
        out += "  <tr>";
        out += "    <td>Size: </td>";
        out += "    <td><input type=\"number\" id=\"heldSpriteWidthHeight\" value=\"0\" style=\"width: 50px;\"></td>";
        out += "    <td><input type=\"range\" min=\"-60\" max=\"60\" value=\"0\" id=\"heldSpriteWidthHeightSlider\"></td>";
        out += "  </tr>";
        out += "  <tr>";
        out += "    <td>Angle: </td>";
        out += "    <td><input type=\"number\" id=\"heldSpriteAngle\" value=\"0\" style=\"width: 50px;\"></td>";
        out += "    <td><input type=\"range\" min=\"-180\" max=\"180\" value=\"0\" id=\"heldSpriteAngleSlider\"></td>";
        out += "  </tr>";
        out += "</table>";
        $(out).appendTo('body');
        $('#heldSpriteOriginXSlider').on('input', function () {
            ChatClient.updateHeldSpriteAdjuster();
        }).on('mousedown', function () {
            Globals.disableInput();
        }).on('mouseup', function () {
            Globals.enableInput();
        });
        $('#heldSpriteOriginYSlider').on('input', function () {
            ChatClient.updateHeldSpriteAdjuster();
        }).on('mousedown', function () {
            Globals.disableInput();
        }).on('mouseup', function () {
            Globals.enableInput();
        });
        $('#heldSpriteWidthHeightSlider').on('input', function () {
            ChatClient.updateHeldSpriteAdjuster();
        }).on('mousedown', function () {
            Globals.disableInput();
        }).on('mouseup', function () {
            Globals.enableInput();
        });
        $('#heldSpriteAngleSlider').on('input', function () {
            ChatClient.updateHeldSpriteAdjuster();
        }).on('mousedown', function () {
            Globals.disableInput();
        }).on('mouseup', function () {
            Globals.enableInput();
        });
    }

    static updateHeldSpriteAdjuster() {
        const heldSpriteOriginX     = parseFloat(<string>$('#heldSpriteOriginXSlider').val());
        const heldSpriteOriginY     = parseFloat(<string>$('#heldSpriteOriginYSlider').val());
        const heldSpriteWidthHeight = parseInt(<string>$('#heldSpriteWidthHeightSlider').val());
        const heldSpriteAngle       = parseInt(<string>$('#heldSpriteAngleSlider').val());
        $('#heldSpriteOriginX').val(heldSpriteOriginX);
        $('#heldSpriteOriginY').val(heldSpriteOriginY);
        $('#heldSpriteWidthHeight').val(heldSpriteWidthHeight);
        $('#heldSpriteAngle').val(heldSpriteAngle);
        PlayerClient.updateSelectedItemSpritePosition(Globals.currentPlayerObj.selectedItemSprite, Globals.currentPlayerObj.sprite, {
            'heldSpriteOriginX':     Items.itemsArr[Globals.currentPlayerObj.selectedItemId]['heldSpriteOriginX'] + heldSpriteOriginX,
            'heldSpriteOriginY':     Items.itemsArr[Globals.currentPlayerObj.selectedItemId]['heldSpriteOriginY'] + heldSpriteOriginY,
            'heldSpriteWidthHeight': Items.itemsArr[Globals.currentPlayerObj.selectedItemId]['heldSpriteWidthHeight'] + heldSpriteWidthHeight,
            'heldSpriteAngle':       Items.itemsArr[Globals.currentPlayerObj.selectedItemId]['heldSpriteAngle'] + heldSpriteAngle,
            'isFlippingHeldSprite':  Items.itemsArr[Globals.currentPlayerObj.selectedItemId]['isFlippingHeldSprite'],
            'isShowingBelowPlayer':  Items.itemsArr[Globals.currentPlayerObj.selectedItemId]['isShowingBelowPlayer'],
        });
    }

}
