/// <reference path="WorldClient.ts" />
/// <reference path="../Utils.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="Modals.ts" />
/// <reference path="./generated/Items.ts" />

class Minimap {

    static readonly bigMinimapWidthPx: number                                 = 800;
    static readonly bigMinimapHeightPx: number                                = 800;
    static readonly blockIdsToShowArr: import('../HunkerInterfaces').ItemId[] = ['sign_arrow_se', 'sign_arrow_ne', 'rail', 'torch'];

    static $minimapDiv: JQuery;
    static $minimapCurrentLayerNumDiv: JQuery;
    static $bigMinimapModal: JQuery;
    static bigMinimapCanvas: HTMLCanvasElement;
    static bigMinimapContext: CanvasRenderingContext2D;
    static bigMinimapModal: M.Modal;
    static $compassModal: JQuery;
    static compassModal: M.Modal;
    static $currentBiomeDiv: JQuery;

    static init() {
        Minimap.$minimapDiv                = $("<div id=\"minimapDiv\"></div>").appendTo('body');
        Minimap.$minimapCurrentLayerNumDiv = $("<div id=\"minimapCurrentLayerNum\">" + WorldClient.currentLayerNum + "</div>").appendTo('body');
        if (Utils.isDev) {
            $("<div id=\"minimapLayerBtns\"></div>").appendTo('body');
            for (let layerNum = Utils.startLayerNum; layerNum > Utils.startLayerNum - Utils.layersNum; layerNum--) {
                let minimapLayerBtnOnclickJsStr = "Globals.socket.emit('playerMessage', '/layer " + layerNum + "');";
                if (Utils.isWorldPage) {
                    minimapLayerBtnOnclickJsStr = "WorldClient.updateCurrentLayerNum(" + layerNum + "); WorldPreview.generateLayer();";
                }
                $(
                    "<div class=\"itemSlot minimapLayerBtn\" title=\"Layer " + layerNum + "\" onclick=\"" + minimapLayerBtnOnclickJsStr + "\">" +
                    " <div class=\"item btn-flat\">" + layerNum + "</div>" +
                    "</div>",
                ).appendTo('#minimapLayerBtns');
            }
        }
        Minimap.$bigMinimapModal  = $(
            "<div id=\"bigMinimapModal\" class=\"modal\">" +
            " <div class=\"modal-content\">" +
            "  <canvas id=\"bigMinimapCanvas\" width=\"800\" height=\"800\"></canvas>" +
            "  <div id=\"bigMinimapElements\"></div>" +
            " </div>" +
            "</div>",
        ).appendTo('body');
        Minimap.bigMinimapCanvas  = <HTMLCanvasElement>UtilsClient.byId('bigMinimapCanvas');
        Minimap.bigMinimapContext = Minimap.bigMinimapCanvas.getContext("2d");
        Minimap.bigMinimapModal   = M.Modal.init(Minimap.$bigMinimapModal[0], {
            'inDuration':  0,
            'outDuration': 0,
        });
        Minimap.$compassModal     = $(
            "<div id=\"compassModal\" class=\"modal\">" +
            " <div class=\"modal-background\">" +
            " </div>" +
            " <div class=\"modal-content\">" +
            "  <img id=\"compassNeedle\" src=\"/public/assets/ui/needle.png\">" +
            " </div>" +
            "</div>",
        ).appendTo('body');
        Minimap.compassModal      = M.Modal.init(Minimap.$compassModal[0], {
            'inDuration':  0,
            'outDuration': 0,
        });
        Minimap.$currentBiomeDiv  = $("<div id=\"currentBiomeDiv\"></div>").appendTo('body');
    }

    static keydown(event: JQuery.KeyDownEvent) {
        if (PlayerClient.gameModeStr == 'creative' && event.key === 'm') {
            if (Minimap.$bigMinimapModal.css('display') === 'block') {
                Minimap.bigMinimapModal.close();
            } else {
                Modals.closeAll();
                Minimap.update();
                Minimap.bigMinimapModal.open();
            }
        }
    }

    static initPlayer(playerId: string, posObj: import('../HunkerInterfaces').Coord): JQuery {
        let $minimapPlayer = $("<div class=\"minimapDot minimapPlayer" + ((playerId == Globals.socket.id) ? ' playerSelf' : '') + "\" title=\"" + ((playerId == Globals.socket.id) ? "You" : PlayerClient.allPlayerObjs[playerId]['name']) + "\"></div>").appendTo(Minimap.$minimapDiv);
        Minimap.updatePos($minimapPlayer, posObj);
        return $minimapPlayer;
    }

    static updatePos($minimapDot: JQuery, posObj: import('../HunkerInterfaces').Coord) {
        if (Globals.currentPlayerObj && $minimapDot.hasClass('playerSelf')) {
            console.log(Utils.toDegrees(Utils.rotation(Globals.currentPlayerObj.sprite, posObj)));
            console.log(Utils.distance(Globals.currentPlayerObj.sprite, posObj));
        }
        $minimapDot.css({
            'top':  (Math.round((posObj.y / (Utils.worldHeight * Utils.tileSize)) * 100) - 1) + '%',
            'left': (Math.round((posObj.x / (Utils.worldWidth * Utils.tileSize)) * 100) - 1) + '%',
        });
    }

    static update() {
        if (Minimap.$bigMinimapModal) {
            let bigMinimapElementsHtml = '';
            Minimap.bigMinimapContext.clearRect(0, 0, 800, 800);
            const cellWidth  = Minimap.bigMinimapWidthPx / Utils.worldWidth;
            const cellHeight = Minimap.bigMinimapHeightPx / Utils.worldHeight;
            Minimap.$bigMinimapModal.find('.modal-content').attr('data-current-layer', WorldClient.currentLayerNum);
            const blockIdsToShowArr = Minimap.blockIdsToShowArr;

            // Show markers if has compass.
            let hasCompass = false;
            if (typeof Globals.currentPlayerObj != 'undefined') {
                for (let slotIndex in Globals.currentPlayerObj.hotbarItemsArr) {
                    if (Globals.currentPlayerObj.hotbarItemsArr[slotIndex]['itemId'] == 'compass') {
                        hasCompass = true;
                        break;
                    }
                }
            }
            if (hasCompass) {
                blockIdsToShowArr.push('marker');
            }

            const isShowingTerrain = (Utils.isWorldPage || Utils.playerGameModesArr[PlayerClient.gameModeStr]['isShowingTerrain']);
            for (let y = 0; y < WorldClient.worldArr[WorldClient.currentLayerNum].length; y++) {
                for (let x = 0; x < WorldClient.worldArr[WorldClient.currentLayerNum][y].length; x++) {
                    let itemId;
                    const floorCell = WorldClient.floorArr[WorldClient.currentLayerNum][y][x];
                    if (floorCell) {
                        let floorItemId = Utils.tileIndexToItemIdArr[floorCell[0]];
                        if (isShowingTerrain || blockIdsToShowArr.indexOf(floorItemId) != -1) {
                            itemId = floorItemId;
                        }
                    }
                    const worldCell = WorldClient.worldArr[WorldClient.currentLayerNum][y][x];
                    if (worldCell && (typeof WorldPreview === 'undefined' || !WorldPreview.isOnlyShowingFloor)) {
                        const worldItemId = Utils.tileIndexToItemIdArr[worldCell[0]];
                        if (isShowingTerrain || blockIdsToShowArr.indexOf(worldItemId) != -1) {
                            itemId = worldItemId;
                        }
                    }
                    if (itemId) {
                        if (
                            typeof Globals.currentPlayerObj != 'undefined'
                            &&
                            Globals.currentPlayerObj.isGhost
                            &&
                            (
                                itemId == 'spawn'
                                ||
                                (
                                    itemId == 'bed'
                                    &&
                                    (
                                        worldCell[2] == $('#loggedInAsUserName').html()
                                        ||
                                        worldCell[2] == Globals.currentPlayerObj.id
                                    )
                                )
                            )
                        ) {
                            let styleAttrCss = '';
                            styleAttrCss += "left: " + (x * cellWidth) + "px; ";
                            styleAttrCss += "top: " + (y * cellHeight) + "px; ";
                            bigMinimapElementsHtml += "<div class=\"minimapDivCell pulse\" title=\"" + Items.itemsArr[itemId]['label'] + "\" style=\"" + styleAttrCss + "\"></div>";
                        }
                        if (isShowingTerrain || blockIdsToShowArr.indexOf(itemId) != -1) {
                            const hexCode = '#' + Items.itemsArr[itemId]['hexCode'];
                            if (itemId == 'sign_arrow_se') {
                                Minimap.bigMinimapContext.fillStyle = hexCode;
                                Minimap.bigMinimapContext.moveTo(x * cellWidth, y * cellHeight);
                                Minimap.bigMinimapContext.lineTo(x * cellWidth + cellWidth, y * cellHeight);
                                Minimap.bigMinimapContext.lineTo(x * cellWidth + cellWidth / 2, y * cellHeight + cellHeight);
                                Minimap.bigMinimapContext.fill();
                            } else if (itemId == 'sign_arrow_ne') {
                                Minimap.bigMinimapContext.fillStyle = hexCode;
                                Minimap.bigMinimapContext.moveTo(x * cellWidth, y * cellHeight + cellHeight);
                                Minimap.bigMinimapContext.lineTo(x * cellWidth + cellWidth, y * cellHeight + cellHeight);
                                Minimap.bigMinimapContext.lineTo(x * cellWidth + cellWidth / 2, y * cellHeight);
                                Minimap.bigMinimapContext.fill();
                            } else if (Items.itemsArr[itemId]['isDisablingCollision']) {
                                // Checkered pattern for non-solid blocks.
                                for (let i = 0; i < cellHeight; i++) {
                                    for (let j = 0; j < cellWidth; j++) {
                                        if (
                                            (i % 2 == 0 && j % 2 == 1)
                                            ||
                                            (i % 2 == 1 && j % 2 == 0)
                                        ) {
                                            Minimap.bigMinimapContext.beginPath();
                                            Minimap.bigMinimapContext.rect(x * cellWidth + i, y * cellHeight + j, 1, 1);
                                            Minimap.bigMinimapContext.fillStyle = hexCode;
                                            Minimap.bigMinimapContext.fill();
                                        }
                                    }
                                }
                            } else {
                                Minimap.bigMinimapContext.beginPath();
                                Minimap.bigMinimapContext.rect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
                                Minimap.bigMinimapContext.fillStyle = hexCode;
                                Minimap.bigMinimapContext.fill();
                            }
                        }
                    }
                }
            }
            if (typeof PlayerClient != 'undefined') {
                for (let playerId in PlayerClient.allPlayerObjs) {
                    const playerObj = PlayerClient.allPlayerObjs[playerId];
                    bigMinimapElementsHtml += "<img src=\"/public/assets/player/" + playerObj.image + ".png\" class=\"minimapDot minimapPlayer" + ((playerId == Globals.socket.id) ? ' playerSelf' : '') + "\" title=\"" + ((playerId == Globals.socket.id) ? "You" : playerObj.name) + "\" style=\"" + Minimap.getPlayerPositionCss(playerObj) + "\">";
                }
            }
            $('#bigMinimapElements').html(bigMinimapElementsHtml);
        }
    }

    static getPlayerPositionCss(playerObj: PlayerClient): string {
        let styleAttrCss = '';
        styleAttrCss += 'top:  ' + ((playerObj.sprite.y / (Utils.worldHeight * Utils.tileSize) * 100) - 1) + '%; ';
        styleAttrCss += 'left: ' + ((playerObj.sprite.x / (Utils.worldWidth * Utils.tileSize) * 100) - 1) + '%; ';
        styleAttrCss += 'transform: rotate(' + playerObj.sprite.angle + 'deg);';
        return styleAttrCss;
    }

    static openCompassModal(): void {
        Modals.closeAll();
        Minimap.updateCompassModal();
        Minimap.compassModal.open();
    }

    static updateCompassModal(): void {
        let closestMarkerDistance = 10000;
        let closestMarkerCoord    = null;
        for (let markerCoordStr in WorldClient.allMarkerCoords) {
            let markerCoord = Utils.dereferenceObj(WorldClient.allMarkerCoords[markerCoordStr]);
            if (markerCoord.layerNum == WorldClient.currentLayerNum) {
                markerCoord['x']       = (markerCoord['x'] * Utils.tileSize) + (Utils.tileSize / 2);
                markerCoord['y']       = (markerCoord['y'] * Utils.tileSize) + (Utils.tileSize / 2);
                const distanceToMarker = Utils.distance(Globals.currentPlayerObj.sprite, markerCoord);
                if (distanceToMarker < closestMarkerDistance) {
                    closestMarkerCoord    = markerCoord;
                    closestMarkerDistance = distanceToMarker;
                }
            }
        }
        if (!closestMarkerCoord && WorldClient.spawnCoord) {
            closestMarkerCoord = {
                'x': (WorldClient.spawnCoord['x'] * Utils.tileSize) + (Utils.tileSize / 2),
                'y': (WorldClient.spawnCoord['y'] * Utils.tileSize) + (Utils.tileSize / 2),
            };
        }
        if (closestMarkerCoord) {
            const angleToClosestMarkerDeg = Utils.toDegrees(Utils.rotation(Globals.currentPlayerObj.sprite, closestMarkerCoord));
            let needleOffsetDeg           = -90;
            needleOffsetDeg += Utils.randBetween(-20, 20);
            $('#compassNeedle').css('transform', 'translateX(-50%) rotate(' + (angleToClosestMarkerDeg + needleOffsetDeg) + 'deg)');
        }
    }

}
