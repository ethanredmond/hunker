/// <reference path="PlayerClient.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="LightCircle.ts" />
/// <reference path="NotificationMsg.ts" />
/// <reference path="Globals.ts" />

class DayNightCycleClient {

    static stage: 'day' | 'night'                          = 'day';
    static gameStartMillis: number                         = null;
    static $dayNightIndicator: JQuery                      = null;
    static $dayIndicatorImg: JQuery                        = null;
    static $nightIndicatorImg: JQuery                      = null;
    static backgroundAudioDayObj: Phaser.Sound.BaseSound   = null;
    static backgroundAudioNightObj: Phaser.Sound.BaseSound = null;
    static backgroundAudioCaveObj: Phaser.Sound.BaseSound  = null;
    static updateDayNightIndicatorInterval: NodeJS.Timeout = null;

    static initSockets(): void {
        Globals.socket.on('nightSurvived', function (response: import('../HunkerInterfaces').ServerResponse) {
            UtilsClient.handleSocketEmitResponse(response);
            if (Globals.currentPlayerObj && response['nightsSurvivedNum']) {
                NotificationMsg.show("You've survived " + response['nightsSurvivedNum'] + " night" + (response['nightsSurvivedNum'] == 1 ? '' : 's'));
            }
        });
        Globals.socket.on('startDay', function (response: import('../HunkerInterfaces').ServerResponse) {
            UtilsClient.handleSocketEmitResponse(response);
            DayNightCycleClient.stage = 'day';
            DayNightCycleClient.updateBackground();
            LightCircle.updateShadowRgb();
            DayNightCycleClient.manageBackgroundAudio();
            DayNightCycleClient.updateDayNightIndicatorImg();
        });
        Globals.socket.on('startNight', function (): void {
            DayNightCycleClient.stage = 'night';
            DayNightCycleClient.updateBackground();
            LightCircle.updateShadowRgb();
            DayNightCycleClient.manageBackgroundAudio();
            DayNightCycleClient.updateDayNightIndicatorImg();
        });
    }

    static updateBackground(): void {
        if (DayNightCycleClient.stage == 'night' || WorldClient.currentLayerNum < 0) {
            Globals.game.cameras.main.setBackgroundColor('#000000');
        } else {
            Globals.game.cameras.main.setBackgroundColor('#b0e9fc');
        }
    }

    static manageBackgroundAudio(): void {
        if (WorldClient.currentLayerNum == 0) {
            if (DayNightCycleClient.stage == 'night') {
                DayNightCycleClient.playBackgroundAudioNight();
            } else {
                DayNightCycleClient.playBackgroundAudioDay();
            }
        } else {
            DayNightCycleClient.playBackgroundAudioCaves();
        }
    }

    static playBackgroundAudioDay(): void {
        if (!DayNightCycleClient.backgroundAudioDayObj || !DayNightCycleClient.backgroundAudioDayObj.isPlaying) {
            if (DayNightCycleClient.backgroundAudioNightObj && DayNightCycleClient.backgroundAudioNightObj.isPlaying) {
                Globals.game.tweens.add({
                    'targets':  DayNightCycleClient.backgroundAudioNightObj,
                    'volume':   0,
                    'duration': 2000,
                });
            }
            if (DayNightCycleClient.backgroundAudioCaveObj && DayNightCycleClient.backgroundAudioCaveObj.isPlaying) {
                Globals.game.tweens.add({
                    'targets':  DayNightCycleClient.backgroundAudioCaveObj,
                    'volume':   0,
                    'duration': 2000,
                });
            }
            setTimeout(function (): void {
                Globals.game.sound.stopAll();
                DayNightCycleClient.backgroundAudioDayObj = SoundEffect.play('background_menu');
            }, 2000);
        }
    }

    static playBackgroundAudioNight(): void {
        if (!DayNightCycleClient.backgroundAudioNightObj || !DayNightCycleClient.backgroundAudioNightObj.isPlaying) {
            if (DayNightCycleClient.backgroundAudioDayObj && DayNightCycleClient.backgroundAudioDayObj.isPlaying) {
                Globals.game.tweens.add({
                    'targets':  DayNightCycleClient.backgroundAudioDayObj,
                    'volume':   0,
                    'duration': 2000,
                });
            }
            if (DayNightCycleClient.backgroundAudioCaveObj && DayNightCycleClient.backgroundAudioCaveObj.isPlaying) {
                Globals.game.tweens.add({
                    'targets':  DayNightCycleClient.backgroundAudioCaveObj,
                    'volume':   0,
                    'duration': 2000,
                });
            }
            setTimeout(function (): void {
                Globals.game.sound.stopAll();
                DayNightCycleClient.backgroundAudioNightObj = SoundEffect.play('background_calm');
            }, 2000);
            SoundEffect.play('background_thunder');
            DayNightCycleClient.scheduleSoundEffect(function (): boolean {
                return (DayNightCycleClient.stage == 'night' && WorldClient.currentLayerNum == 0);
            }, 'background_thunder');
        }
    }

    static playBackgroundAudioCaves(): void {
        if (!DayNightCycleClient.backgroundAudioCaveObj || !DayNightCycleClient.backgroundAudioCaveObj.isPlaying) {
            if (DayNightCycleClient.backgroundAudioDayObj && DayNightCycleClient.backgroundAudioDayObj.isPlaying) {
                Globals.game.tweens.add({
                    'targets':  DayNightCycleClient.backgroundAudioDayObj,
                    'volume':   0,
                    'duration': 2000,
                });
            }
            if (DayNightCycleClient.backgroundAudioNightObj && DayNightCycleClient.backgroundAudioNightObj.isPlaying) {
                Globals.game.tweens.add({
                    'targets':  DayNightCycleClient.backgroundAudioNightObj,
                    'volume':   0,
                    'duration': 2000,
                });
            }
            setTimeout(function (): void {
                Globals.game.sound.stopAll();
                DayNightCycleClient.backgroundAudioCaveObj = SoundEffect.play('background_nether');
            }, 2000);
            SoundEffect.play('background_cave');
            DayNightCycleClient.scheduleSoundEffect(function (): boolean {
                return (WorldClient.currentLayerNum < 0);
            }, 'background_cave');
        }
    }

    static scheduleSoundEffect(conditionFunc: () => boolean, soundEffectKey: import('../HunkerInterfaces').SoundEffectKey) {
        setTimeout(function (): void {
            if (conditionFunc()) {
                SoundEffect.play(soundEffectKey);
                DayNightCycleClient.scheduleSoundEffect(conditionFunc, soundEffectKey);
            }
        }, Utils.randBetween(10000, 40000));
    }

    static initClient(dayNightStage: import('../HunkerInterfaces').DayNightStage, gameStartMillis: number): void {
        DayNightCycleClient.stage = dayNightStage;
        if (DayNightCycleClient.stage == 'day') {
            DayNightCycleClient.playBackgroundAudioDay();
        } else {
            DayNightCycleClient.playBackgroundAudioNight();
        }
        DayNightCycleClient.updateBackground();
        LightCircle.updateShadowRgb();
        DayNightCycleClient.initDayNightIndicator(gameStartMillis);
    }

    static initDayNightIndicator(gameStartMillis: number) {
        DayNightCycleClient.gameStartMillis = gameStartMillis;
        if (!DayNightCycleClient.$dayNightIndicator || !DayNightCycleClient.$dayNightIndicator.length) {
            $(
                "<div id=\"dayNightIndicatorContainer\">" +
                " <input type=\"text\" value=\"0\" id=\"dayNightIndicator\">" +
                " <img id=\"dayIndicatorImg\" src=\"../public/assets/ui/sun.png\" alt=\"Day\" title=\"Day\">" +
                " <img id=\"nightIndicatorImg\" src=\"../public/assets/ui/moon.png\" alt=\"Night\" title=\"Night\">" +
                "</div>",
            ).appendTo('body');
            DayNightCycleClient.$dayNightIndicator = $('#dayNightIndicator');
            DayNightCycleClient.$dayIndicatorImg   = $('#dayIndicatorImg');
            DayNightCycleClient.$nightIndicatorImg = $('#nightIndicatorImg');
            // @ts-ignore
            DayNightCycleClient.$dayNightIndicator.knob({
                'min':          0,
                'max':          1000,
                'width':        '100%',
                'bgColor':      'transparent',
                'lineCap':      'round',
                'readOnly':     true,
                'displayInput': false,
                'angleOffset':  -90,
            });
        }
        DayNightCycleClient.updateDayNightIndicatorImg();
        DayNightCycleClient.updateDayNightIndicator();
        clearInterval(DayNightCycleClient.updateDayNightIndicatorInterval);
        DayNightCycleClient.updateDayNightIndicatorInterval = setInterval(function (): void {
            DayNightCycleClient.updateDayNightIndicator();
        }, 500);
    }

    static updateDayNightIndicatorImg(): void {
        let dayNightIndicatorColor;
        if (DayNightCycleClient.stage == 'day') {
            DayNightCycleClient.$dayIndicatorImg.show();
            DayNightCycleClient.$nightIndicatorImg.hide();
            dayNightIndicatorColor = '#42a5f5'; // blue lighten-1
        } else {
            DayNightCycleClient.$dayIndicatorImg.hide();
            DayNightCycleClient.$nightIndicatorImg.show();
            dayNightIndicatorColor = '#1a237e'; // indigo darken-4
        }
        DayNightCycleClient.$dayNightIndicator.trigger(
            'configure',
            {
                'fgColor': dayNightIndicatorColor,
            },
        );
    }

    static updateDayNightIndicator(): void {
        let cycleProgressMillis = (Utils.time() - DayNightCycleClient.gameStartMillis) % Utils.cycleLengthMillis;
        let dayNightFraction;
        if (DayNightCycleClient.stage == 'day') {
            dayNightFraction = cycleProgressMillis / Utils.dayLengthMillis;
        } else {
            dayNightFraction = (cycleProgressMillis - Utils.dayLengthMillis) / Utils.nightLengthMillis;
        }
        DayNightCycleClient.$dayNightIndicator.val(Math.round(dayNightFraction * 1000)).trigger('change');
    }

}
