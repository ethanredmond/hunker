/// <reference path="../Utils.ts" />
/// <reference path="UtilsClient.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="NotificationMsg.ts" />
/// <reference path="Modals.ts" />

class TimeSpent {

    static secs: number;
    static $counterDiv: JQuery;
    static interval: NodeJS.Timeout;

    static init() {
        let todayDateStr = (new Date()).toDateString();
        if (localStorage.getItem('timeSpentDate') === todayDateStr) {
            TimeSpent.secs = parseInt(localStorage.getItem('timeSpentSecs'));
        } else {
            TimeSpent.secs = 0;
            localStorage.setItem('timeSpentDate', todayDateStr);
            localStorage.setItem('timeSpentSecs', TimeSpent.secs.toString());
        }
        TimeSpent.$counterDiv = $("<div id=\"timeSpentCounterDiv\"></div>").appendTo('body');
        TimeSpent.updateCounter();
        TimeSpent.startInterval();
    }

    static startInterval() {
        TimeSpent.interval = setInterval(function () {
            if (Globals.currentPlayerObj && !Globals.currentPlayerObj.isPaused) {
                TimeSpent.secs++;
                localStorage.setItem('timeSpentSecs', TimeSpent.secs.toString());
                TimeSpent.updateCounter();
            }
        }, 1000);
    }

    static updateCounter() {
        const hours   = Math.round(TimeSpent.secs / 3600 * 10) / 10;
        const minutes = Math.floor(TimeSpent.secs / 60);
        let counterText;
        if (hours >= 1) {
            counterText = `${hours} hr${hours == 1 ? '' : "s"}`;
        } else if (minutes >= 1) {
            counterText = `${minutes} min${minutes == 1 ? '' : "s"}`;
        } else {
            counterText = `${TimeSpent.secs} sec${TimeSpent.secs == 1 ? '' : "s"}`;
        }
        TimeSpent.$counterDiv.html(counterText);
        if (
            TimeSpent.secs == 180 * 60
            ||
            TimeSpent.secs == 150 * 60
            ||
            TimeSpent.secs == 120 * 60
            ||
            TimeSpent.secs == 90 * 60
            ||
            TimeSpent.secs == 60 * 60
            ||
            TimeSpent.secs == 30 * 60
        ) {
            NotificationMsg.show(`Playing for ${counterText}`);
        }
    }

}
