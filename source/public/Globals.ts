/// <reference path="PlayerClient.ts" />

class Globals {

    static game: Phaser.Scene;
    static socket: SocketIOClient.Socket;
    static currentPlayerObj: PlayerClient;
    static isInputEnabled: boolean;
    static isGameCreated: boolean = false;
    static delta: number;

    static disableInput(): void {
        Globals.isInputEnabled = false;
        if (Globals.game) {
            Globals.game.input.keyboard.clearCaptures();
        }
    }

    static enableInput(): void {
        Globals.isInputEnabled = true;
    }
}
