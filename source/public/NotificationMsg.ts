class NotificationMsg {

    private static $notificationMsg: JQuery;
    private static hideMsgTimeout: NodeJS.Timeout;

    static init(): void {
        NotificationMsg.$notificationMsg = $("<div id=\"notificationMsg\"></div>").appendTo('body');
    }

    static show(notificationText: string): void {
        if (NotificationMsg.$notificationMsg) {
            if (notificationText) {
                NotificationMsg.$notificationMsg.text(notificationText).fadeIn(200);
                clearTimeout(NotificationMsg.hideMsgTimeout);
                NotificationMsg.hideMsgTimeout = setTimeout(function () {
                    NotificationMsg.$notificationMsg.fadeOut(1000);
                }, 1000);
            } else {
                console.trace();
                console.error("notificationText not set");
            }
        }
    }

}

