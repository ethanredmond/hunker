/// <reference path="../Utils.ts" />
/// <reference path="./generated/Items.ts" />

class SoundEffect {

    static readonly effectsArr: { [effectKey: string]: { volume: number, loop: boolean, numFiles: number, srcPath: string } } = {
        // @formatter:off
        'eat':                      {'volume': 0.08, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/player/eat.ogg'},
        'grindstone':               {'volume': 0.08, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/player/grindstone.ogg'},
        'drink':                    {'volume': 0.08, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/player/drink.ogg'},
        'pop':                      {'volume': 0.08, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/player/pop.ogg'},
        'rail':                     {'volume': 0.12, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/player/trigger.ogg'},
        'inside':                   {'volume': 0.10, 'loop': true,  'numFiles':  1, 'srcPath': '/public/assets/audio/player/inside.ogg'},
        'attack_sweep':             {'volume': 0.08, 'loop': false, 'numFiles':  7, 'srcPath': '/public/assets/audio/attack/sweep.ogg'},
        'attack_strong':            {'volume': 0.08, 'loop': false, 'numFiles':  6, 'srcPath': '/public/assets/audio/attack/strong.ogg'},
        'attack_weak':              {'volume': 0.12, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/attack/weak.ogg'},
        'hit_wood':                 {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/wood.ogg'},
        'hit_stone':                {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/stone.ogg'},
        'hit_grass':                {'volume': 0.08, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/grass.ogg'},
        'hit_sand':                 {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/sand.ogg'},
        'hit_snow':                 {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/snow.ogg'},
        'hit_dirt':                 {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/gravel.ogg'},
        'hit_mushroom':             {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/coral.ogg'},
        'hit_carpet':               {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hit/cloth.ogg'},
        'step_sand':                {'volume': 0.04, 'loop': false, 'numFiles':  5, 'srcPath': '/public/assets/audio/step/sand.ogg'},
        'step_snow':                {'volume': 0.16, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/step/snow.ogg'},
        'step_grass':               {'volume': 0.06, 'loop': false, 'numFiles':  6, 'srcPath': '/public/assets/audio/step/grass.ogg'},
        'step_dirt':                {'volume': 0.04, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/step/gravel.ogg'},
        'step_stone':               {'volume': 0.10, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/step/stone.ogg'},
        'step_carpet':              {'volume': 0.10, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/step/cloth.ogg'},
        'paddle_water':             {'volume': 0.35, 'loop': false, 'numFiles':  8, 'srcPath': '/public/assets/audio/entity/boat/paddle_water.ogg'},
        'break_glass':              {'volume': 0.10, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/break/glass.ogg'},
        'background_menu':          {'volume': 0.10, 'loop': true,  'numFiles':  4, 'srcPath': '/public/assets/audio/background/menu.ogg'},
        'background_calm':          {'volume': 0.10, 'loop': true,  'numFiles':  3, 'srcPath': '/public/assets/audio/background/calm.ogg'},
        'background_cave':          {'volume': 0.05, 'loop': false, 'numFiles': 19, 'srcPath': '/public/assets/audio/background/cave.ogg'},
        'background_nether':        {'volume': 0.10, 'loop': true,  'numFiles':  4, 'srcPath': '/public/assets/audio/background/nether.ogg'},
        'background_thunder':       {'volume': 0.03, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/background/thunder.ogg'},
        'hoe_till':                 {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/hoe/till.ogg'},
        'equip_leather':            {'volume': 0.10, 'loop': false, 'numFiles':  6, 'srcPath': '/public/assets/audio/player/equip_leather.ogg'},
        'player_fire_damage':       {'volume': 0.10, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/player/fire_hurt.ogg'},
        'player_damage':            {'volume': 0.10, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/player/hit.ogg'},
        'door_close':               {'volume': 0.07, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/door/close.ogg'},
        'door_open':                {'volume': 0.07, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/door/open.ogg'},
        'chest_close':              {'volume': 0.07, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/chest/close.ogg'},
        'chest_open':               {'volume': 0.07, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/chest/open.ogg'},
        'cow_hurt':                 {'volume': 0.05, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/cow/hurt1.ogg'},
        'cow_death':                {'volume': 0.05, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/cow/hurt2.ogg'},
        'pig_hurt':                 {'volume': 0.05, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/pig/death.ogg'},
        'sheep_hurt':               {'volume': 0.15, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/sheep/say.ogg'},
        'sheep_death':              {'volume': 0.15, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/sheep/say3.ogg'},
        'chicken_hurt':             {'volume': 0.05, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/chicken/hurt.ogg'},
        'polar_bear_hurt':          {'volume': 0.07, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/entity/polar_bear/hurt.ogg'},
        'polar_bear_death':         {'volume': 0.07, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/entity/polar_bear/death.ogg'},
        'wolf_hurt':                {'volume': 0.05, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/entity/wolf/hurt.ogg'},
        'wolf_death':               {'volume': 0.05, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/wolf/death.ogg'},
        'zombie_hurt':              {'volume': 0.10, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/zombie/hurt.ogg'},
        'zombie_death':             {'volume': 0.10, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/zombie/death.ogg'},
        'slime_hurt':               {'volume': 0.10, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/slime/attack.ogg'},
        'slime_death':              {'volume': 0.10, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/slime/death.ogg'},
        'slime_hiss':               {'volume': 0.10, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/entity/slime/say.ogg'},
        'slime_splat':              {'volume': 0.10, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/slime/big.ogg'},
        'archer_hurt':              {'volume': 0.10, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/entity/archer/hurt.ogg'},
        'archer_death':             {'volume': 0.10, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/archer/death.ogg'},
        'axehead_hurt':             {'volume': 0.15, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/entity/axehead/hurt.ogg'},
        'axehead_death':            {'volume': 0.15, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/entity/axehead/death.ogg'},
        'axehead_celebrate':        {'volume': 0.15, 'loop': false, 'numFiles':  2, 'srcPath': '/public/assets/audio/entity/axehead/celebrate.ogg'},
        'axehead_step':             {'volume': 0.15, 'loop': false, 'numFiles':  5, 'srcPath': '/public/assets/audio/entity/axehead/step.ogg'},
        'explode':                  {'volume': 0.20, 'loop': false, 'numFiles':  4, 'srcPath': '/public/assets/audio/explode/explode.ogg'},
        'fuse':                     {'volume': 0.60, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/explode/fuse.ogg'},
        'break':                    {'volume': 0.20, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/player/break.ogg'},
        'shear':                    {'volume': 0.15, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/entity/sheep/shear.ogg'},
        'milk':                     {'volume': 0.35, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/entity/cow/milk.ogg'},
        'bow':                      {'volume': 0.20, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/bow/bow.ogg'},
        'fireball':                 {'volume': 0.20, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/player/fireball.ogg'},
        'snowball':                 {'volume': 0.20, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/player/throw.ogg'},
        'snowball_hit':             {'volume': 0.20, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/player/snowball.ogg'},
        'bucket_empty':             {'volume': 0.20, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/bucket/empty.ogg'},
        'bucket_fill_water':        {'volume': 0.20, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/bucket/fill.ogg'},
        'bucket_fill_lava':         {'volume': 0.20, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/bucket/fill_lava.ogg'},
        'notification':             {'volume': 0.25, 'loop': false, 'numFiles':  1, 'srcPath': '/public/assets/audio/player/notification.mp3'},
        'enchant':                  {'volume': 0.25, 'loop': false, 'numFiles':  3, 'srcPath': '/public/assets/audio/enchant/enchant.ogg'},
        // @formatter:on
    };
    static audioObjsArr: { [soundEffectKey in import('../HunkerInterfaces').SoundEffectKey]: Phaser.Sound.HTML5AudioSound }   = {};

    static preload() {
        for (let soundEffectKey in SoundEffect.effectsArr) {
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] == 1) {
                Globals.game.load.audio(soundEffectKey, SoundEffect.effectsArr[soundEffectKey]['srcPath']);
            } else {
                for (let i = 1; i <= SoundEffect.effectsArr[soundEffectKey]['numFiles']; i++) {
                    let srcPath       = SoundEffect.effectsArr[soundEffectKey]['srcPath'];
                    const insertIndex = srcPath.indexOf('.ogg');
                    srcPath           = srcPath.slice(0, insertIndex) + i + srcPath.slice(insertIndex);
                    Globals.game.load.audio(soundEffectKey + i, srcPath);
                }
            }
        }
    }

    static create() {
        for (let soundEffectKey in SoundEffect.effectsArr) {
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] == 1) {
                SoundEffect.audioObjsArr[soundEffectKey] = <Phaser.Sound.HTML5AudioSound>Globals.game.sound.add(soundEffectKey, {'volume': SoundEffect.effectsArr[soundEffectKey]['volume'], 'loop': SoundEffect.effectsArr[soundEffectKey]['loop']});
            } else {
                for (let i = 1; i <= SoundEffect.effectsArr[soundEffectKey]['numFiles']; i++) {
                    SoundEffect.audioObjsArr[soundEffectKey + i] = <Phaser.Sound.HTML5AudioSound>Globals.game.sound.add(soundEffectKey + i, {'volume': SoundEffect.effectsArr[soundEffectKey]['volume'], 'loop': SoundEffect.effectsArr[soundEffectKey]['loop']});
                }
            }
        }
    }

    static play(soundEffectKey: import('../HunkerInterfaces').SoundEffectKey): Phaser.Sound.HTML5AudioSound | null {
        if (soundEffectKey in SoundEffect.effectsArr) {
            let audioKey = soundEffectKey;
            if (SoundEffect.effectsArr[soundEffectKey]['numFiles'] > 1) {
                audioKey += Utils.randBetween(1, SoundEffect.effectsArr[soundEffectKey]['numFiles']);
            }
            if (audioKey in SoundEffect.audioObjsArr) {
                SoundEffect.audioObjsArr[audioKey].stop();
                SoundEffect.audioObjsArr[audioKey].play();
                return SoundEffect.audioObjsArr[audioKey];
            } else {
                console.log("Audio key \"" + audioKey + "\" is invalid");
            }
        } else {
            console.log("SoundEffect key \"" + soundEffectKey + "\" is invalid");
        }
        return null;
    }

}
