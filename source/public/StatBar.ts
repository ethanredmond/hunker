class StatBar {

    private static $statBarContainer: JQuery;

    private barName: string;
    private barMax: number;
    private barValue: number;
    private $innerBar: JQuery;

    static init() {
        StatBar.$statBarContainer = $("<div class=\"statBarContainer\"></div>").appendTo('body');
    }

    constructor(barName: string, barValue: number, barMax: number, isShowing: boolean) {
        this.barName   = barName;
        this.barMax    = barMax;
        this.barValue  = barValue;
        this.$innerBar = $('#' + barName + 'Bar .innerBar');
        if (!this.$innerBar.length) {
            const $statBar = $("<div id=\"" + barName + "Bar\" class=\"statBar\"></div>").appendTo(StatBar.$statBarContainer);
            this.$innerBar = $("<div class=\"innerBar\"></div>").appendTo($statBar);
        }
        this.updateIsShowing(isShowing);
        this.update(barValue);
    }

    public update(barValue: number): void {
        this.barValue = barValue;
        this.$innerBar.css('width', Math.min(Math.round((this.barValue / this.barMax) * 100), 100) + '%');
        this.$innerBar.toggleClass('flashStatBar', (this.barValue <= 10));
    }

    public updateMax(barMax: number): void {
        this.barMax = barMax;
        this.update(this.barValue);
    }

    public updateIsShowing(isShowing: boolean): void {
        $(`#${this.barName}Bar`).toggleClass('hideStatBar', !isShowing);
    }

}
