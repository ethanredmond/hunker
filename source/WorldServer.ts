const Utils = require('./Utils');
const Items = require('./public/generated/Items');
import {DayNightCycleServer} from './DayNightCycleServer';
import {UtilsServer} from './UtilsServer';
import {PlayerServer} from './PlayerServer';
import {EntityServer} from './EntityServer';
import {Timer} from './Timer';
import {BiomeName, BlockUpdateData, FloorUpdateData, BoundsArr, Coord, HotbarSlot, ItemId, WorldArr, WorldCell} from './HunkerInterfaces';
import {HotbarServer} from './HotbarServer';
import {LayerGenerator} from './LayerGenerator';

export class WorldServer {

    static worldSeed: string                                                         = null;//'DQZcRL3X';
    static spawnLayerNum: number                                                     = Utils.startLayerNum;
    static spawnX: number                                                            = Utils.worldWidth / 2;
    static spawnY: number                                                            = Utils.worldHeight / 2;
    static worldArr: WorldArr                                                        = {};
    static floorArr: WorldArr                                                        = {};
    static biomeCoordsPerLayerArr: { [layerNum: string]: BiomeName[][] }             = {};
    static layerRailCoordsArr: { [layerNum: string]: Coord[] }                       = {};
    static cropsToGrowCoordsArr: { [blockCoordStr: string]: Coord }                  = {};
    static mushroomsToSpreadCoordsArr: { [blockCoordStr: string]: Coord }            = {};
    static allHeaterCoords: { [blockCoordStr: string]: Coord }                       = {};
    static allWorkbenchCoords: { [blockCoordStr: string]: Coord }                    = {};
    static allHearthCoords: { [blockCoordStr: string]: Coord }                       = {};
    static allGrindstoneCoords: { [blockCoordStr: string]: Coord }                   = {};
    static allContainersArr: { [blockCoordStr: string]: HotbarSlot[] }               = {};
    static allHearthTimeoutsArr: { [blockCoordStr: string]: NodeJS.Timeout }         = {};
    static allDungeonObjsArr: { dungeonObj: Dungeon, dungeonBoundsArr: BoundsArr }[] = [];

    static init(callbackFn: () => void) {
        WorldServer.biomeCoordsPerLayerArr = {};
        for (let layerNum = Utils.startLayerNum; layerNum > Utils.startLayerNum - Utils.layersNum; layerNum--) {
            if (UtilsServer.redisClient) {
                UtilsServer.redisClient.hgetall(`biomeCoords:${layerNum}`, function (error, layerBiomeCoordsArr) {
                    if (error) {
                        throw error;
                    } else if (layerBiomeCoordsArr) {
                        WorldServer.biomeCoordsPerLayerArr[layerNum] = [];
                        for (let y = 0; y < Utils.worldHeight; y++) {
                            for (let x = 0; x < Utils.worldWidth; x++) {
                                const redisCoordStr = x + ',' + y;
                                if (redisCoordStr in layerBiomeCoordsArr && layerBiomeCoordsArr[redisCoordStr] in Utils.biomeDetailsArr[layerNum]) {
                                    if (!(y in WorldServer.biomeCoordsPerLayerArr[layerNum])) {
                                        WorldServer.biomeCoordsPerLayerArr[layerNum][y] = [];
                                    }
                                    WorldServer.biomeCoordsPerLayerArr[layerNum][y][x] = <BiomeName>layerBiomeCoordsArr[redisCoordStr];
                                }
                            }
                        }
                    }
                });
            }
            [WorldServer.worldArr[layerNum], WorldServer.floorArr[layerNum]] = LayerGenerator.initLayer(layerNum);
            if (UtilsServer.redisClient) {
                UtilsServer.redisClient.hgetall('world:' + layerNum, function (error, redisWorldArr) {
                    if (error) {
                        throw error;
                    } else if (!redisWorldArr) { // If world isn't set in Redis.
                        WorldServer.generateNewLayer(layerNum, callbackFn);
                    } else {
                        WorldServer.loadLayerFromRedis(redisWorldArr, layerNum, callbackFn);
                    }
                });
            } else {
                WorldServer.generateNewLayer(layerNum, callbackFn);
            }
        }
    }

    static initSpawn() {
        if (UtilsServer.redisClient) {
            UtilsServer.redisClient.get('spawn', function (error, spawnCoordStr) {
                if (error) {
                    throw error;
                } else if (!spawnCoordStr || spawnCoordStr.indexOf(':') == -1 || spawnCoordStr.indexOf(',') == -1) {
                    [WorldServer.spawnX, WorldServer.spawnY] = WorldServer.findEmptySpawnPosSpot(WorldServer.spawnLayerNum, Utils.playerSpawnFloorIdsArr, 500);
                    UtilsServer.redisClient.set('spawn', WorldServer.spawnLayerNum + ':' + WorldServer.spawnX + ',' + WorldServer.spawnY);
                    WorldServer.addSpawnBlock();
                } else {
                    WorldServer.spawnLayerNum = parseInt(spawnCoordStr.split(':')[0]);
                    WorldServer.spawnX        = parseInt(spawnCoordStr.split(':')[1].split(',')[0]);
                    WorldServer.spawnY        = parseInt(spawnCoordStr.split(':')[1].split(',')[1]);
                    WorldServer.addSpawnBlock();
                }
            });
        } else {
            [WorldServer.spawnX, WorldServer.spawnY] = WorldServer.findEmptySpawnPosSpot(WorldServer.spawnLayerNum, Utils.playerSpawnFloorIdsArr, 500);
            WorldServer.addSpawnBlock();
        }
    }

    static addSpawnBlock() {
        WorldServer.worldArr[WorldServer.spawnLayerNum][WorldServer.spawnY][WorldServer.spawnX] = [Utils.randElFromArr(Items.itemsArr['spawn']['tileIndexesArr']), Items.itemsArr['spawn']['structureDurability']];
        if (UtilsServer.redisClient) {
            WorldServer.updateSquareRedis({
                'layerNum': WorldServer.spawnLayerNum,
                'x':        WorldServer.spawnX,
                'y':        WorldServer.spawnY,
            });
        }
    }

    static generateNewLayer(layerNum: number, callbackFn: () => void) {
        const layerObj                                            = new LayerGenerator(layerNum, WorldServer.worldSeed);
        WorldServer.worldArr[layerNum]                            = layerObj.worldLayerArr;
        WorldServer.floorArr[layerNum]                            = layerObj.floorLayerArr;
        // Save biomes in Redis.
        WorldServer.biomeCoordsPerLayerArr[layerNum]              = LayerGenerator.layerBiomeCoordsArr;
        const redisBiomeCoordsArr: { [coordStr: string]: string } = {};
        for (let y = 0; y < Utils.worldHeight; y++) {
            for (let x = 0; x < Utils.worldWidth; x++) {
                if (WorldServer.biomeCoordsPerLayerArr[layerNum][y][x]) {
                    redisBiomeCoordsArr[x + ',' + y] = WorldServer.biomeCoordsPerLayerArr[layerNum][y][x];
                }
            }
        }
        if (Object.keys(redisBiomeCoordsArr).length) {
            UtilsServer.redisClient.hmset(`biomeCoords:${layerNum}`, redisBiomeCoordsArr);
        }
        WorldServer.allDungeonObjsArr = WorldServer.allDungeonObjsArr.concat(layerObj.allDungeonObjsArr);
        WorldServer.addRailsToLayer(layerNum);

        if (UtilsServer.redisClient) {
            // Save new world to Redis.
            const redisWorldArr: { [coordStr: string]: string } = {};
            const redisFloorArr: { [coordStr: string]: string } = {};
            for (let y = 0; y < Utils.worldHeight; y++) {
                for (let x = 0; x < Utils.worldWidth; x++) {
                    if (WorldServer.worldArr[layerNum][y][x]) {
                        redisWorldArr[x + ',' + y] = WorldServer.formatSquareForRedis(WorldServer.worldArr[layerNum][y][x]);
                    }
                    // Save if,
                    if (
                        WorldServer.floorArr[layerNum][y][x] // Has a value, and,
                        &&
                        (
                            (
                                layerNum == 0 // Is first layer, and,
                                &&
                                Items.itemsArr['water']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum][y][x][0]) == -1 // Isn't water, or,
                            )
                            ||
                            (
                                layerNum != 0 // Isn't first layer, and,
                                &&
                                Items.itemsArr['cave']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum][y][x][0]) == -1 // Isn't cave.
                            )
                        )
                    ) {
                        redisFloorArr[x + ',' + y] = WorldServer.formatSquareForRedis(WorldServer.floorArr[layerNum][y][x]);
                    }
                }
            }
            UtilsServer.redisClient.hmset('world:' + layerNum, redisWorldArr);
            if (Object.keys(redisFloorArr).length !== 0) {
                UtilsServer.redisClient.hmset('floor:' + layerNum, redisFloorArr);
            }
        }

        if (layerNum == Utils.endLayerNum) { // Is last layer.
            callbackFn();
        }
    }

    static formatSquareForRedis(worldCell: WorldCell): string {
        let worldCellStr = worldCell[0] + ',' + worldCell[1];
        if (2 in worldCell) {
            worldCellStr += ',' + worldCell[2];
        }
        if (3 in worldCell) {
            worldCellStr += ',' + worldCell[3];
        }
        return worldCellStr;
    }

    static addRailsToLayer(layerNum: number): void {
        if (layerNum <= -1) {
            let railsPerLayerNum = Utils.worldBlocksNum * 0.0010;
            if (layerNum == -1) {
                railsPerLayerNum = Utils.worldBlocksNum * 0.0020; // For overworld.
            }
            WorldServer.layerRailCoordsArr[layerNum] = [];
            for (let i = 0; i < railsPerLayerNum; i++) {
                let numIterations = 0;
                let x             = null;
                let y             = null;
                for (; numIterations < 100; numIterations++) {
                    let xLoop = Utils.randBetween(0, Utils.worldWidth - 1);
                    let yLoop = Utils.randBetween(0, Utils.worldHeight - 1);
                    if (
                        !(
                            WorldServer.worldArr[layerNum][yLoop][xLoop] != null // Block at this layer, or,
                            ||
                            Items.itemsArr['cave']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum][yLoop][xLoop][0]) == -1 // Floor at this layer isn't cave,
                            ||
                            WorldServer.worldArr[layerNum + 1][yLoop][xLoop] != null // Block at layer above isn't empty, or,
                            ||
                            (
                                layerNum + 1 == 0 // Layer above is overworld, and,
                                &&
                                Items.itemsArr['sand']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum + 1][yLoop][xLoop][0]) == -1 // Floor at layer above isn't sand, and,
                                &&
                                Items.itemsArr['snow']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum + 1][yLoop][xLoop][0]) == -1 // Floor at layer above isn't snow, or,
                                &&
                                Items.itemsArr['grass']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum + 1][yLoop][xLoop][0]) == -1 // Floor at layer above isn't grass, or,
                            )
                            ||
                            (
                                layerNum + 1 <= -1 // Layer above is cave, and,
                                &&
                                Items.itemsArr['cave']['tileIndexesArr'].indexOf(WorldServer.floorArr[layerNum + 1][yLoop][xLoop][0]) == -1 // Floor at layer above isn't cave.
                            )
                        )
                    ) {
                        const thisRailCoordArr = {
                            'x': xLoop,
                            'y': yLoop,
                        };
                        let isTooCloseToRail   = false;
                        if (layerNum < 0 && layerNum + 1) {
                            for (let coordArr of WorldServer.layerRailCoordsArr[layerNum + 1]) {
                                if (Utils.distance(thisRailCoordArr, coordArr) <= 7) {
                                    isTooCloseToRail = true;
                                    break;
                                }
                            }
                        }
                        for (let coordArr of WorldServer.layerRailCoordsArr[layerNum]) {
                            if (Utils.distance(thisRailCoordArr, coordArr) <= 7) {
                                isTooCloseToRail = true;
                                break;
                            }
                        }
                        if (!isTooCloseToRail) {
                            x = xLoop;
                            y = yLoop;
                            WorldServer.layerRailCoordsArr[layerNum].push(Utils.dereferenceObj(thisRailCoordArr));
                            break;
                        }
                    }
                }
                if (x != null && y != null) {
                    // @formatter:off
                    WorldServer.worldArr[layerNum + 1][y    ][x    ] = [Items.itemsArr['rail'         ]['tileIndexesArr'][0], Items.itemsArr['rail'         ]['structureDurability']];
                    WorldServer.worldArr[layerNum    ][y    ][x    ] = [Items.itemsArr['rail'         ]['tileIndexesArr'][0], Items.itemsArr['rail'         ]['structureDurability']];
                    WorldServer.worldArr[layerNum + 1][y    ][x + 1] = [Items.itemsArr['torch'        ]['tileIndexesArr'][0], Items.itemsArr['torch'        ]['structureDurability']];
                    WorldServer.worldArr[layerNum    ][y    ][x + 1] = [Items.itemsArr['torch'        ]['tileIndexesArr'][0], Items.itemsArr['torch'        ]['structureDurability']];
                    WorldServer.worldArr[layerNum + 1][y - 1][x    ] = [Items.itemsArr['sign_arrow_se']['tileIndexesArr'][0], Items.itemsArr['sign_arrow_se']['structureDurability']];
                    WorldServer.worldArr[layerNum    ][y - 1][x    ] = [Items.itemsArr['sign_arrow_ne']['tileIndexesArr'][0], Items.itemsArr['sign_arrow_ne']['structureDurability']];
                    // Add torches to heaters array.
                    WorldServer.allHeaterCoords[(layerNum + 1) + ':' + (x + 1) + ',' + y] = {'layerNum': layerNum + 1, 'x': x + 1, 'y': y};
                    WorldServer.allHeaterCoords[ layerNum      + ':' + (x + 1) + ',' + y] = {'layerNum': layerNum,     'x': x + 1, 'y': y};
                    if (UtilsServer.redisClient) {
                        // Update layer above (this layer will be update automatically).
                        UtilsServer.redisClient.hset('world:' + (layerNum + 1),  x      + ',' +  y,      WorldServer.formatSquareForRedis(WorldServer.worldArr[layerNum + 1][y][x]));
                        UtilsServer.redisClient.hset('world:' + (layerNum + 1), (x + 1) + ',' +  y,      WorldServer.formatSquareForRedis(WorldServer.worldArr[layerNum + 1][y][x + 1]));
                        UtilsServer.redisClient.hset('world:' + (layerNum + 1),  x      + ',' + (y - 1), WorldServer.formatSquareForRedis(WorldServer.worldArr[layerNum + 1][y - 1][x]));
                    }
                    // @formatter:on
                }
            }
        }
    }

    static loadLayerFromRedis(redisWorldArr: { [coordStr: string]: string }, layerNum: number, callbackFn: () => void): void {
        for (let y = 0; y < Utils.worldHeight; y++) {
            for (let x = 0; x < Utils.worldWidth; x++) {
                const redisCoordStr = x + ',' + y;
                if (redisCoordStr in redisWorldArr && redisWorldArr[redisCoordStr]) {
                    const blockCoordStr                  = layerNum + ':' + x + ',' + y;
                    WorldServer.worldArr[layerNum][y][x] = WorldServer.processRedisWorldCell(redisWorldArr[redisCoordStr]);
                    const coordArr                       = {'layerNum': layerNum, 'x': x, 'y': y};
                    // Add block coord to the right arrays.
                    if (WorldServer.worldArr[layerNum][y][x][0] in Utils.tileIndexToItemIdArr) {
                        const blockItemId = Utils.tileIndexToItemIdArr[WorldServer.worldArr[layerNum][y][x][0]];
                        if (blockItemId == 'workbench') {
                            WorldServer.allWorkbenchCoords[blockCoordStr] = coordArr;
                        } else if (blockItemId == 'hearth' || blockItemId == 'hearth_lit') {
                            WorldServer.allHearthCoords[blockCoordStr] = coordArr;
                        } else if (blockItemId == 'grindstone') {
                            WorldServer.allGrindstoneCoords[blockCoordStr] = coordArr;
                        } else if (Items.itemsArr[blockItemId]['cropSeedId'] && Items.itemsArr[blockItemId]['cropNextStageId']) {
                            WorldServer.cropsToGrowCoordsArr[blockCoordStr] = coordArr;
                        } else if (blockItemId.indexOf('mushroom_') == 0) {
                            WorldServer.mushroomsToSpreadCoordsArr[blockCoordStr] = coordArr;
                        }
                        if (Items.itemsArr[blockItemId]['heaterWarmth'] != null) {
                            WorldServer.allHeaterCoords[blockCoordStr] = coordArr;
                        }
                        if (Items.itemsArr[blockItemId]['isContainer']) {
                            UtilsServer.redisClient.hgetall('container:' + blockCoordStr, function (error, containerArr) {
                                if (error) {
                                    throw error;
                                } else if (containerArr == null) { // If container isn't set in Redis.
                                    if (blockItemId.indexOf('lootcrate_') == -1 && blockItemId.indexOf('present_') == -1) {
                                        WorldServer.allContainersArr[blockCoordStr] = [];
                                        for (let i = 0; i < (Items.itemsArr[blockItemId]['maxItemsInContainerNum'] || 1); i++) {
                                            WorldServer.allContainersArr[blockCoordStr].push({'itemId': null, 'itemCount': null});
                                        }
                                    }
                                } else {
                                    // Set container from Redis.
                                    WorldServer.allContainersArr[blockCoordStr] = PlayerServer.processRedisItemsArr(containerArr);
                                }
                            });
                        }
                    }
                }
            }
        }

        // Get floor from Redis.
        UtilsServer.redisClient.hgetall('floor:' + layerNum, function (error, redisFloorArr) {
            if (error) {
                throw error;
            } else if (redisFloorArr != null) {
                for (let floorCoordStr in redisFloorArr) {
                    if (floorCoordStr) {
                        const floorCoordArr                                                                    = floorCoordStr.split(',');
                        WorldServer.floorArr[layerNum][parseInt(floorCoordArr[1])][parseInt(floorCoordArr[0])] = WorldServer.processRedisWorldCell(redisFloorArr[floorCoordStr]);
                    }
                }
                if (layerNum == Utils.endLayerNum) { // Is last layer.
                    callbackFn();
                }
            }
        });
    }

    static processRedisWorldCell(redisWorldCellStr: string): WorldCell {
        const redisWorldCellArr    = redisWorldCellStr.split(',');
        const worldCell: WorldCell = [
            parseInt(redisWorldCellArr[0]),
            parseInt(redisWorldCellArr[1]),
        ];
        if (redisWorldCellArr[2]) {
            if (isNaN(parseInt(redisWorldCellArr[2]))) {
                worldCell.push(redisWorldCellArr[2].toString());
            } else {
                worldCell.push(parseInt(redisWorldCellArr[2]));
            }
        }
        if (redisWorldCellArr[3]) {
            worldCell.push(parseInt(redisWorldCellArr[3]));
        }
        if (redisWorldCellArr[4]) {
            worldCell.push(parseInt(redisWorldCellArr[4]));
        }
        return worldCell;
    }

    /**
     * The world is stored as a hash in Redis like this:
     *     world
     *         0,0: '1,100,1'
     *         ...
     * Each key in the hash is a coordinate string of the block.
     */
    static updateSquareRedis(coordArr: Coord): void {
        if (coordArr['layerNum'] in WorldServer.worldArr && coordArr['y'] in WorldServer.worldArr[coordArr['layerNum']] && coordArr['x'] in WorldServer.worldArr[coordArr['layerNum']][coordArr['y']]) {
            if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) {
                UtilsServer.redisClient.hset('world:' + coordArr['layerNum'], coordArr['x'] + ',' + coordArr['y'], WorldServer.formatSquareForRedis(WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]));
            } else {
                UtilsServer.redisClient.hdel('world:' + coordArr['layerNum'], coordArr['x'] + ',' + coordArr['y']);
            }
        } else {
            console.log("Weird, coordArr:", coordArr);
        }
    }

    /**
     * The floor is stored as a hash in Redis like this:
     *     floor
     *         0,0: '1,5',
     *         ...
     * Each key in the hash is a coordinate string of the block.
     */
    static updateFloorSquareRedis(coordArr: Coord): void {
        if (coordArr['layerNum'] in WorldServer.floorArr && coordArr['y'] in WorldServer.floorArr[coordArr['layerNum']] && coordArr['x'] in WorldServer.floorArr[coordArr['layerNum']][coordArr['y']]) {
            if (WorldServer.floorArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) {
                UtilsServer.redisClient.hset('floor:' + coordArr['layerNum'], coordArr['x'] + ',' + coordArr['y'], WorldServer.formatSquareForRedis(WorldServer.floorArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]));
            } else {
                UtilsServer.redisClient.hdel('floor:' + coordArr['layerNum'], coordArr['x'] + ',' + coordArr['y']);
            }
        } else {
            console.log("Weird, coordArr:", coordArr);
        }
    }

    /**
     * Containers are stored as hashes in Redis like this:
     *     container:0,0
     *         0: 'wood:5',
     *         1: '',
     *         ...
     * The id of the container is a coordinate string of the block the container is on.
     * Each key in the hash is the index in the container.
     * If a value is '', then that slot is empty.
     */
    static updateContainerRedis(coordStr: string, prevContainerArr: HotbarSlot[] | null): void {
        if (coordStr in WorldServer.allContainersArr) {
            if (prevContainerArr == null) {
                // Container didn't exist before, so create a new container hash in Redis.
                UtilsServer.redisClient.hmset('container:' + coordStr, WorldServer.formatContentsForRedis(WorldServer.allContainersArr[coordStr]));
            } else {
                // Container existed before, so update Redis with what's changed.
                for (let slotIndex in WorldServer.allContainersArr[coordStr]) {
                    let slotDetails     = WorldServer.allContainersArr[coordStr][slotIndex];
                    let prevSlotDetails = prevContainerArr[slotIndex];
                    if (
                        prevSlotDetails['itemId'] != slotDetails['itemId']
                        ||
                        prevSlotDetails['itemCount'] != slotDetails['itemCount']
                        ||
                        prevSlotDetails['itemDurability'] != slotDetails['itemDurability']
                        ||
                        (
                            (
                                'itemEnchantmentsArr' in prevSlotDetails
                                ||
                                'itemEnchantmentsArr' in slotDetails
                            )
                            &&
                            PlayerServer.itemEnchantmentsArrToStr(prevSlotDetails['itemEnchantmentsArr'] ?? {}) != PlayerServer.itemEnchantmentsArrToStr(slotDetails['itemEnchantmentsArr'] ?? {})
                        )
                    ) {
                        if (slotDetails['itemId'] && slotDetails['itemCount']) {
                            UtilsServer.redisClient.hset(
                                'container:' + coordStr,
                                slotIndex,
                                slotDetails['itemId'] + ':' + slotDetails['itemCount'] +
                                (slotDetails['itemDurability'] ? ',' + slotDetails['itemDurability'] : '') +
                                ('itemEnchantmentsArr' in slotDetails && slotDetails['itemEnchantmentsArr'] ? ',' + PlayerServer.itemEnchantmentsArrToStr(slotDetails['itemEnchantmentsArr']) : ''),
                            );
                        } else {
                            UtilsServer.redisClient.hset('container:' + coordStr, slotIndex, 'null');
                        }
                    }
                }
            }
        } else if (prevContainerArr && !(coordStr in WorldServer.allContainersArr)) { // Container was removed.
            UtilsServer.redisClient.del('container:' + coordStr);
        } else {
            console.log("Weird, coordStr:", coordStr);
        }
    }

    static formatContentsForRedis(contentsArr: HotbarSlot[]): { [slotIndex: number]: string } {
        const contentsRedisArr: { [slotIndex: number]: string } = {};
        for (let slotIndex in contentsArr) {
            let slotDetails = contentsArr[slotIndex];
            if (slotDetails['itemId'] && slotDetails['itemCount']) {
                contentsRedisArr[slotIndex] = slotDetails['itemId'] + ':' + slotDetails['itemCount'] + (slotDetails['itemDurability'] ? ',' + slotDetails['itemDurability'] : '');
            } else {
                contentsRedisArr[slotIndex] = 'null';
            }
        }
        return contentsRedisArr;
    }

    static updateAllHearths() {
        for (let coordStr in WorldServer.allHearthCoords) {
            const coordArr = WorldServer.allHearthCoords[coordStr];
            if (
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']] == null // No block at coord.
                ||
                !(coordStr in WorldServer.allContainersArr) // No container at coord.
            ) {
                delete WorldServer.allHearthCoords[coordStr];
                delete WorldServer.allHeaterCoords[coordStr];
                continue;
            }

            const prevContainerArr  = Utils.dereferenceObj(WorldServer.allContainersArr[coordStr]);
            const hasInput          = (WorldServer.allContainersArr[coordStr][1]['itemCount'] > 0);
            const prevWorldCellJson = JSON.stringify(WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]);

            // @formatter:off
            if (!(3 in WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] == null/* || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] == 'null'*/) { WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] = 0; }
            if (!(4 in WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][4] == null/* || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][4] == 'null'*/) { WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][4] = 0; }
            if (!(5 in WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] == null/* || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] == 'null'*/) { WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] = 0; }
            // @formatter:on

            if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] > 0) { // Is burning.
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3]--; // Decrement burn time.
            }

            if (
                hasInput
                &&
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] <= 0 // Burned out.
                &&
                WorldServer.allContainersArr[coordStr][0]['itemId'] in Items.itemsArr // Has item in fuel slot.
                &&
                Items.itemsArr[WorldServer.allContainersArr[coordStr][0]['itemId']]['burnTimeSecs'] // Item has a burn time.
                &&
                WorldServer.allContainersArr[coordStr][0]['itemCount'] > 0 // Has items in fuel slot.
            ) {
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] = Items.itemsArr[WorldServer.allContainersArr[coordStr][0]['itemId']]['burnTimeSecs'];
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][4] = Items.itemsArr[WorldServer.allContainersArr[coordStr][0]['itemId']]['burnTimeSecs'];
                WorldServer.allHearthCoords[coordStr]                                       = coordArr;
                WorldServer.allHeaterCoords[coordStr]                                       = coordArr;
                const prevContainerArr                                                      = Utils.dereferenceObj(WorldServer.allContainersArr[coordStr]);
                HotbarServer.removeItemsAtIndex(WorldServer.allContainersArr[coordStr], 0, 1); // Remove one fuel.
                WorldServer.updateContainerRedis(coordStr, prevContainerArr);
            }

            let blockId;
            if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] > 0) { // Is still burning.
                blockId = 'hearth_lit';
                if (hasInput) {
                    WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5]++; // Increase smelting time.
                    if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] >= 10) {
                        // Smelted.
                        WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] = 0; // Reset smelting time.

                        HotbarServer.addItemsAtIndex(WorldServer.allContainersArr[coordStr], {'itemId': Items.itemsArr[WorldServer.allContainersArr[coordStr][1]['itemId']]['smeltingOutputItemId'], 'itemCount': 1}, 2); // Add one output item.
                        HotbarServer.removeItemsAtIndex(WorldServer.allContainersArr[coordStr], 1, 1); // Remove one input item.
                    }
                } else {
                    WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] = 0; // Reset smelting time.
                }
            } else {
                blockId                                                                     = 'hearth';
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][5] = 0; // Reset smelting time.
            }
            WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][0] = Items.itemsArr[blockId]['tileIndexesArr'][0];
            if (prevWorldCellJson != JSON.stringify(WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']])) {
                UtilsServer.io.emit('blockUpdate', {
                    'blockCoordArr': coordArr,
                    'blockDataArr':  WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']],
                    'blockId':       blockId,
                }); // Emit to all players.
                WorldServer.updateSquareRedis(coordArr);
            }
            WorldServer.updateContainerRedis(coordStr, prevContainerArr);
        }
    }

    static updateAllGrindstones() {
        for (let coordStr in WorldServer.allGrindstoneCoords) {
            const coordArr = WorldServer.allGrindstoneCoords[coordStr];
            if (
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']] == null // No block at coord.
                ||
                !(coordStr in WorldServer.allContainersArr) // No container at coord.
            ) {
                delete WorldServer.allGrindstoneCoords[coordStr];
                continue;
            }

            const prevContainerArr  = Utils.dereferenceObj(WorldServer.allContainersArr[coordStr]);
            const hasInput          = (WorldServer.allContainersArr[coordStr][0]['itemCount'] > 0 && WorldServer.allContainersArr[coordStr][0]['itemId'] == 'wheat');
            const prevWorldCellJson = JSON.stringify(WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]);

            // @formatter:off
            // if (!(2 in WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][2] == null/* || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][2] == 'null'*/) { WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][2] = 0; }
            if (!(3 in WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']]) || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] == null/* || WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] == 'null'*/) { WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] = 0; }
            // @formatter:on

            // if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] > 0) { // Is still grinding.
            if (hasInput) {
                UtilsServer.checkRadiusForBlock(coordArr['x'], coordArr['y'], 1, (xLoop: number, yLoop: number) => {
                    if (coordArr['x'] != xLoop || coordArr['y'] != yLoop) { // Not center block.
                        if (
                            WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop]
                            &&
                            Items.itemsArr['windmill']['tileIndexesArr'].indexOf(WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][0]) != -1
                        ) {
                            WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3]++; // Increase grinding time.
                            return true;
                        }
                    }
                    return false;
                });
                if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] >= 20) {
                    // Finished grinding.
                    WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] = 0; // Reset grinding time.

                    HotbarServer.addItemsAtIndex(WorldServer.allContainersArr[coordStr], {'itemId': 'flour', 'itemCount': 1}, 1); // Add one output item.
                    HotbarServer.removeItemsAtIndex(WorldServer.allContainersArr[coordStr], 0, 1); // Remove one input item.
                }
            } else {
                WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][3] = 0; // Reset grinding time.
            }
            if (prevWorldCellJson != JSON.stringify(WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']])) {
                UtilsServer.io.emit('blockUpdate', {
                    'blockCoordArr': coordArr,
                    'blockDataArr':  WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']],
                    'blockId':       'grindstone',
                }); // Emit to all players.
                WorldServer.updateSquareRedis(coordArr);
            }
            WorldServer.updateContainerRedis(coordStr, prevContainerArr);
        }
    }

    static findEmptySpawnPosSpot(layerNum: number, floorIdsArr: string[], maxIterations: number = 50) {
        let x             = null;
        let y             = null;
        let numIterations = 0;
        while (
            numIterations < maxIterations
            &&
            (
                x == null
                ||
                y == null
                ||
                WorldServer.worldArr[layerNum][y][x] != null
                ||
                (
                    floorIdsArr
                    &&
                    floorIdsArr.indexOf(Utils.tileIndexToItemIdArr[WorldServer.floorArr[layerNum][y][x][0]]) == -1)
                ||
                WorldServer.isAnyPlayerCloserThanX({'layerNum': layerNum, 'x': x, 'y': y}, 10)
            )
            ) {
            x = Utils.randBetween(0, Utils.worldWidth - 1);
            y = Utils.randBetween(0, Utils.worldHeight - 1);
            numIterations++;
        }
        return [x, y, numIterations];
    }

    static isAnyPlayerCloserThanX(posArr: Coord, minDistance: number): boolean {
        if (typeof PlayerServer == 'undefined') {
            return false;
        }
        for (let playerId in PlayerServer.allPlayerObjs) {
            if (PlayerServer.allPlayerObjs[playerId].currentLayerNum == posArr['layerNum']) {
                const distanceToPlayer = Utils.distance(posArr, PlayerServer.allPlayerObjs[playerId]);
                if (distanceToPlayer <= minDistance) {
                    return true;
                }
            }
        }
        return false;
    }

    static destroyBlock(blockCoordArr: Coord, blockCoordStr: string) {
        WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']] = null;
        WorldServer.updateSquareRedis(blockCoordArr);
        delete WorldServer.cropsToGrowCoordsArr[blockCoordArr['layerNum'] + ':' + blockCoordStr];
        delete WorldServer.allHeaterCoords[blockCoordStr];
        delete WorldServer.allWorkbenchCoords[blockCoordStr];
        delete WorldServer.allHearthCoords[blockCoordStr];
        delete WorldServer.allGrindstoneCoords[blockCoordStr];
        if (blockCoordStr in WorldServer.allContainersArr) {
            const prevContainerArr = Utils.dereferenceObj(WorldServer.allContainersArr[blockCoordStr]);
            delete WorldServer.allContainersArr[blockCoordStr];
            WorldServer.updateContainerRedis(blockCoordStr, prevContainerArr);
        }
        delete WorldServer.allHearthTimeoutsArr[blockCoordStr];
        Timer.removeTimersForBlock(blockCoordStr);
    }

    static getMostCommonNeighbourFloorItemId(layerNum: number, x: number, y: number): ItemId | null {
        const floorItemIdsArr: { [itemId in ItemId]: number } = {};
        UtilsServer.checkRadiusForBlock(x, y, 1, function (xLoop: number, yLoop: number): boolean {
            const floorCell = WorldServer.floorArr[layerNum][yLoop][xLoop];
            if (floorCell && floorCell[0] in Utils.tileIndexToItemIdArr) {
                const floorItemId = Utils.tileIndexToItemIdArr[floorCell[0]];
                if (!(floorItemId in floorItemIdsArr)) {
                    floorItemIdsArr[floorItemId] = 0;
                }
                floorItemIdsArr[floorItemId]++;
            }
            return false;
        });
        let mostCommonNeighbourFloorItemId = null;
        let numNeighbours                  = -1;
        for (let floorItemId in floorItemIdsArr) {
            if (numNeighbours < floorItemIdsArr[floorItemId]) {
                mostCommonNeighbourFloorItemId = floorItemId;
                numNeighbours                  = floorItemIdsArr[floorItemId];
            }
        }
        return mostCommonNeighbourFloorItemId;
    }

    static findEmptySquareInRadius(x: number, y: number, layerNum: number): Coord {
        const allSquareCoordsArr: Coord[]   = [];
        const emptySquareCoordsArr: Coord[] = [];
        UtilsServer.checkRadiusForBlock(x, y, 1, function (xLoop: number, yLoop: number): boolean {
            if (!(xLoop == x && yLoop == y)) {
                const coordArr = {
                    'layerNum': layerNum,
                    'x':        xLoop,
                    'y':        yLoop,
                };
                allSquareCoordsArr.push(coordArr);
                if (!WorldServer.worldArr[layerNum][yLoop][xLoop]) {
                    emptySquareCoordsArr.push(coordArr);
                }
            }
            return false;
        });
        const emptySquareCoordArr = Utils.randElFromArr((emptySquareCoordsArr.length) ? emptySquareCoordsArr : allSquareCoordsArr);
        return {
            'layerNum': emptySquareCoordArr['layerNum'],
            'x':        (emptySquareCoordArr['x'] * Utils.tileSize) + (Utils.tileSize / 2),
            'y':        (emptySquareCoordArr['y'] * Utils.tileSize) + (Utils.tileSize / 2),
        };
    }

    // static addWalls () {
    //     let wallX              = 0;
    //     let wallY              = 0;
    //     let changeBlockAtIndex = null;
    //     let tileIdToAdd        = null;
    //     let tileIndexToAdd     = null;
    //     for (let i = 0; i < Utils.worldWidth + Utils.worldHeight + Utils.worldWidth + Utils.worldHeight; i++) {
    //         if (changeBlockAtIndex == null || changeBlockAtIndex < i) {
    //             tileIdToAdd        = Utils.randElFromArr(['tree', 'stone']);
    //             tileIndexToAdd     = Utils.randElFromArr(Items.itemsArr[tileIdToAdd]['tileIndexesArr']);
    //             changeBlockAtIndex = i + Utils.randElFromArr([5, 6, 7]);
    //         }
    //         WorldServer.worldArr[wallY][wallX] = [tileIndexToAdd, Items.itemsArr[tileIdToAdd]['structureDurability']];
    //         if (i < Utils.worldWidth - 1) {
    //             wallX++;
    //         } else if (i > Utils.worldWidth && i < Utils.worldWidth + Utils.worldHeight) {
    //             wallY++;
    //         } else if (i >= Utils.worldWidth + Utils.worldHeight && i < Utils.worldWidth + Utils.worldHeight + Utils.worldWidth - 1) {
    //             wallX--;
    //         } else if (i > Utils.worldWidth + Utils.worldHeight + Utils.worldWidth) {
    //             wallY--;
    //         }
    //     }
    // }

    static findEmptyMonsterSpawnPosSpot(centerX: number, centerY: number, layerNum: number, floorIdsArr: string[]): number[] {
        let x             = null;
        let y             = null;
        let numIterations = 0;
        const maxDist     = 20;
        const minDist     = 5;
        while (
            numIterations < 100
            &&
            (
                x == null
                ||
                y == null
                ||
                x < 0
                ||
                x >= Utils.worldWidth
                ||
                y < 0
                ||
                y >= Utils.worldHeight
                ||
                UtilsServer.isCoordCollidingWithWorld(WorldServer.worldArr[layerNum], x, y)[0]
                ||
                floorIdsArr.indexOf(Utils.tileIndexToItemIdArr[WorldServer.floorArr[layerNum][y][x][0]]) == -1
                ||
                UtilsServer.checkRadiusForBlock(x, y, 3, function (xLoop, yLoop) {
                    const worldCell = WorldServer.worldArr[layerNum][yLoop][xLoop];
                    return (worldCell && Items.itemsArr['torch']['tileIndexesArr'].indexOf(worldCell[0]) != -1);
                })
            )
            ) {
            if (Utils.randBool()) {
                if (Utils.randBool()) {
                    x = centerX + Utils.randBetween(-maxDist, -minDist);
                    y = centerY + Utils.randBetween(-maxDist, maxDist);
                } else {
                    x = centerX + Utils.randBetween(minDist, maxDist);
                    y = centerY + Utils.randBetween(-maxDist, maxDist);
                }
            } else {
                if (Utils.randBool()) {
                    x = centerX + Utils.randBetween(-maxDist, maxDist);
                    y = centerY + Utils.randBetween(-maxDist, -minDist);
                } else {
                    x = centerX + Utils.randBetween(-maxDist, maxDist);
                    y = centerY + Utils.randBetween(minDist, maxDist);
                }
            }
            numIterations++;
        }
        return [x, y, numIterations];
    }

    static growCrops() {
        // Don't grow crops at night.
        if (DayNightCycleServer.stage == 'night') {
            return;
        }
        for (let i in WorldServer.cropsToGrowCoordsArr) {
            const coordArr = WorldServer.cropsToGrowCoordsArr[i];
            if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']] == null) {
                delete WorldServer.cropsToGrowCoordsArr[i];
                continue;
            }
            if (Math.random() >= 0.1) {
                continue;
            }
            const blockTileIndex = WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][0];
            if (!(blockTileIndex in Utils.tileIndexToItemIdArr)) {
                delete WorldServer.cropsToGrowCoordsArr[i];
                continue;
            }
            const blockId = Utils.tileIndexToItemIdArr[blockTileIndex];
            if (!(blockId in Items.itemsArr)) {
                delete WorldServer.cropsToGrowCoordsArr[i];
                continue;
            }
            if (
                (
                    blockId == 'sapling'
                    ||
                    blockId == 'sapling_birch'
                )
                &&
                (
                    !UtilsServer.isCoord3x3Empty(WorldServer.worldArr[coordArr['layerNum']], coordArr['x'], coordArr['y'])
                    ||
                    Math.random() > 0.1 // Extra randomness on growing sapling on average every eight minutes `100 / (0.1 * 0.1 * 100) * 5 / 60`.
                )
            ) {
                continue;
            }
            if (
                blockId == 'tree_apple_0'
                &&
                Math.random() > 0.3 // Extra randomness on regenerating apple tree.
            ) {
                continue;
            }
            if (
                (blockId.indexOf('carrot_') == 0 || blockId.indexOf('sugar_cane_') == 0)
                &&
                Math.random() > 0.5 // Extra randomness on growing carrots.
            ) {
                continue;
            }
            if (
                blockId.indexOf('bud_') == 0
                &&
                Math.random() > 0.25 // Extra randomness on growing buds.
            ) {
                continue;
            }
            if (Items.itemsArr[blockId]['cropRequiredDistanceToWater'] && !UtilsServer.checkBlockNearWater(WorldServer.floorArr[coordArr['layerNum']], coordArr['x'], coordArr['y'], Items.itemsArr[blockId]['cropRequiredDistanceToWater'])) {
                continue;
            }
            const cropNextStageId = Items.itemsArr[blockId]['cropNextStageId'];
            if (!cropNextStageId) {
                delete WorldServer.cropsToGrowCoordsArr[i];
                continue;
            }
            WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']] = [Utils.randElFromArr(Items.itemsArr[cropNextStageId]['tileIndexesArr']), Items.itemsArr[cropNextStageId]['structureDurability']];
            WorldServer.updateSquareRedis(coordArr);
            UtilsServer.io.emit('blockUpdate', {
                'blockId':       cropNextStageId,
                'blockCoordArr': coordArr,
                'blockDataArr':  WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']],
            });
            if (!Items.itemsArr[cropNextStageId]['cropNextStageId']) { // If has finished growing, remove from cropsToGrowCoordsArr.
                delete WorldServer.cropsToGrowCoordsArr[i];
            }
        }

        WorldServer.spreadMushrooms();
    }

    static spreadMushrooms(): void {
        for (let i in WorldServer.mushroomsToSpreadCoordsArr) {
            const coordArr: Coord = WorldServer.mushroomsToSpreadCoordsArr[i];
            if (WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']] == null) {
                delete WorldServer.mushroomsToSpreadCoordsArr[i];
                continue;
            }
            if (Math.random() >= 0.1) {
                continue;
            }
            const blockTileIndex = WorldServer.worldArr[coordArr['layerNum']][coordArr['y']][coordArr['x']][0];
            if (!(blockTileIndex in Utils.tileIndexToItemIdArr)) {
                delete WorldServer.mushroomsToSpreadCoordsArr[i];
                continue;
            }
            const blockId = Utils.tileIndexToItemIdArr[blockTileIndex];
            if (!(blockId in Items.itemsArr)) {
                delete WorldServer.mushroomsToSpreadCoordsArr[i];
                continue;
            }

            const boundsArr: BoundsArr = {
                'x1': Math.max(coordArr['x'] - 2, 0),
                'x2': Math.min(coordArr['x'] + 2, Utils.worldWidth - 1),
                'y1': Math.max(coordArr['y'] - 2, 0),
                'y2': Math.min(coordArr['y'] + 2, Utils.worldHeight - 1),
            };
            let newCoordArr;
            for (let i = 0; i < 50; i++) { // 50 iterations.
                const loopCoordArr: Coord = {
                    'x':        Utils.randBetween(boundsArr['x1'], boundsArr['x2']),
                    'y':        Utils.randBetween(boundsArr['y1'], boundsArr['y2']),
                    'layerNum': coordArr['layerNum'],
                };

                // No blocks in radius of 1.
                if (
                    UtilsServer.checkRadiusForBlock(loopCoordArr['x'], loopCoordArr['y'], 1, function (xLoop: number, yLoop: number) {
                        return !!WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop];
                    })
                ) {
                    continue;
                }

                // Has required floor.
                if (Items.itemsArr[blockId]['cropRequiredFloorIdsArr'].indexOf(Utils.tileIndexToItemIdArr[WorldServer.floorArr[loopCoordArr['layerNum']][loopCoordArr['y']][loopCoordArr['x']][0]]) == -1) {
                    continue;
                }

                newCoordArr = loopCoordArr;
            }
            if (newCoordArr) {
                WorldServer.mushroomsToSpreadCoordsArr[newCoordArr['layerNum'] + ': ' + newCoordArr['x'] + ',' + newCoordArr['y']] = newCoordArr;
                WorldServer.worldArr[newCoordArr['layerNum']][newCoordArr['y']][newCoordArr['x']]                                  = [Utils.randElFromArr(Items.itemsArr[blockId]['tileIndexesArr']), Items.itemsArr[blockId]['structureDurability']];
                WorldServer.updateSquareRedis(newCoordArr);
                UtilsServer.io.emit('blockUpdate', {
                    'blockId':       blockId,
                    'blockCoordArr': newCoordArr,
                    'blockDataArr':  WorldServer.worldArr[newCoordArr['layerNum']][newCoordArr['y']][newCoordArr['x']],
                });
            }
        }
    }

    static createExplosion(coordArr: Coord, explosionPower: number, addFloorPcnt: number = null, addFloorId: string = null): void {
        const blockUpdatesDataArr: BlockUpdateData[] = [];
        const floorUpdatesDataArr: FloorUpdateData[] = [];
        const coordStr                               = coordArr['layerNum'] + ':' + coordArr['x'] + ',' + coordArr['y'];
        // Damage nearby blocks.
        UtilsServer.checkRadiusForBlock(coordArr['x'], coordArr['y'], Math.ceil(explosionPower * 3), function (xLoop, yLoop) {
            const coordArrLoop    = {
                'layerNum': coordArr['layerNum'],
                'x':        xLoop,
                'y':        yLoop,
            };
            const distanceToBlock = Utils.distance(coordArr, coordArrLoop);
            if (distanceToBlock < explosionPower * 2) {
                if (WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop]) {
                    const coordStrLoop = coordArr['layerNum'] + ':' + xLoop + ',' + yLoop;
                    if (
                        coordStrLoop != coordStr
                        &&
                        WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][0] in Utils.tileIndexToItemIdArr
                        &&
                        Utils.tileIndexToItemIdArr[WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][0]] in Items.itemsArr
                        &&
                        Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][0]]]['explosionPower']
                    ) {
                        setTimeout(function () {
                            if (WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop]) {
                                WorldServer.createExplosion(coordArrLoop, Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][0]]]['explosionPower']);
                            }
                        }, 2000);
                    } else if (WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][1] > 0) {
                        WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][1] -= explosionPower * 50 / distanceToBlock;
                        if (WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop][1] <= 0) {
                            WorldServer.destroyBlock(coordArrLoop, coordStrLoop);
                        }
                        blockUpdatesDataArr.push({
                            'blockCoordArr': coordArrLoop,
                            'blockDataArr':  WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop],
                        });
                    }
                }
                if (
                    !WorldServer.worldArr[coordArr['layerNum']][yLoop][xLoop]
                    &&
                    addFloorPcnt
                    &&
                    addFloorId
                    &&
                    Math.random() <= addFloorPcnt
                ) {
                    WorldServer.floorArr[coordArr['layerNum']][yLoop][xLoop] = [Utils.randElFromArr(Items.itemsArr[addFloorId]['tileIndexesArr']), Items.itemsArr[addFloorId]['structureDurability']];
                    floorUpdatesDataArr.push({
                        'floorCoordArr': coordArrLoop,
                        'floorDataArr':  WorldServer.floorArr[coordArr['layerNum']][yLoop][xLoop],
                    });
                }
            }
            return false;
        });
        // Emit message to all players.
        UtilsServer.io.emit('explosion', {
            'blockUpdatesDataArr': blockUpdatesDataArr,
            'floorUpdatesDataArr': floorUpdatesDataArr,
            'explosionCoordArr':   coordArr,
            'explosionPower':      explosionPower,
        });
        // Damage nearby players.
        const maxDistance = explosionPower * 2 * Utils.tileSize;
        for (let playerId in PlayerServer.allPlayerObjs) {
            if (PlayerServer.allPlayerObjs[playerId].currentLayerNum == coordArr['layerNum']) {
                const distance = Utils.distance(PlayerServer.allPlayerObjs[playerId], {
                    'x': (coordArr['x'] * Utils.tileSize) + (Utils.tileSize / 2),
                    'y': (coordArr['y'] * Utils.tileSize) + (Utils.tileSize / 2),
                });
                if (distance <= maxDistance) {
                    const playerDamage = -explosionPower * 25 * ((maxDistance - distance) / maxDistance);
                    PlayerServer.allPlayerObjs[playerId].updateHealth(playerDamage, true, 'explosion');
                }
            }
        }
        // Damage nearby entities.
        for (let entityId in EntityServer.allEntityObjs) {
            const entityObj = EntityServer.allEntityObjs[entityId];
            if (entityObj.layerNum == coordArr['layerNum']) {
                const blockCoordArr = {
                    'x': (coordArr['x'] * Utils.tileSize) + (Utils.tileSize / 2),
                    'y': (coordArr['y'] * Utils.tileSize) + (Utils.tileSize / 2),
                };
                const distance      = Utils.distance(entityObj, blockCoordArr);
                if (distance <= maxDistance) {
                    if (Utils.allEntityTypesArr[entityObj.type]['explosionPower']) {
                        entityObj.fuseLengthNum = Utils.allEntityTypesArr[entityObj.type]['fuseAutoDecrementLengthNum'];
                    } else {
                        const entityDamage = -explosionPower * 25 * ((maxDistance - distance) / maxDistance);
                        entityObj.updateHealth(entityDamage);
                    }
                }
                if (entityObj && distance <= maxDistance * 2) {
                    const knockbackMult = explosionPower * 4 * Math.max(((maxDistance * 2) - distance) / (maxDistance * 2), 0.5);
                    entityObj.addKnockback(Utils.rotation(entityObj, blockCoordArr), knockbackMult);
                }
            }
        }
    }

    static updateCopperCurrent(blockCoordArr: Coord, hasCurrent: boolean, isFirstBlock: boolean = true, visitedCoordStrsArr: string[] = [], isPreviousConductingCurrent: boolean = false): string[] {
        if (
            WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']] != null
            &&
            WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0] in Utils.tileIndexToItemIdArr
            &&
            Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]] in Items.itemsArr
        ) {
            if (!visitedCoordStrsArr) {
                visitedCoordStrsArr = [blockCoordArr['layerNum'] + ':' + blockCoordArr['x'] + ',' + blockCoordArr['y']];
            }
            const blockId = Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]];
            if (!isFirstBlock) {
                if (Items.itemsArr[blockId]['hasActionOnCurrent']) {
                    if (Items.itemsArr[blockId]['toggleItemId']) {
                        if (hasCurrent && blockId.indexOf('_open') == -1) {
                            WorldServer.toggleBlock(blockCoordArr);
                        } else if (!hasCurrent && blockId.indexOf('_open') != -1) {
                            WorldServer.toggleBlock(blockCoordArr);
                        }
                    }
                    if (Items.itemsArr[blockId]['explosionPower']) {
                        setTimeout(function () {
                            if (WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']]) {
                                console.log('createExplosion');
                                WorldServer.createExplosion(blockCoordArr, Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]]]['explosionPower']);
                            }
                        }, 2000);
                    }
                }

                let newBlockId = blockId;
                if (hasCurrent) {
                    if (newBlockId.indexOf('_on') == -1) {
                        newBlockId = newBlockId + '_on';
                    }
                } else {
                    newBlockId = newBlockId.replace('_on', '');
                }
                if (!(newBlockId in Items.itemsArr)) {
                    newBlockId = blockId;
                }
                if (blockId != newBlockId) {
                    WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0] = Items.itemsArr[newBlockId]['tileIndexesArr'][0];
                    WorldServer.updateSquareRedis(blockCoordArr);
                    UtilsServer.io.emit('blockUpdate', {
                        'blockCoordArr': blockCoordArr,
                        'blockDataArr':  WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']],
                    }); // Emit to all players.
                }
            }

            if (
                isFirstBlock
                ||
                Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]]]['isConductingCurrent']
                ||
                (
                    isPreviousConductingCurrent
                    &&
                    !Items.itemsArr[blockId]['isDisablingCollision']
                )
            ) {
                for (let i in Utils.directionsArr) {
                    let directionLetter  = Utils.directionsArr[i];
                    const newCoord       = Utils.getNewCoordFromDirection(blockCoordArr, directionLetter, 1);
                    newCoord['layerNum'] = blockCoordArr['layerNum'];
                    const newCoordStr    = newCoord['layerNum'] + ':' + newCoord['x'] + ',' + newCoord['y'];
                    if (visitedCoordStrsArr.indexOf(newCoordStr) == -1) {
                        visitedCoordStrsArr.push(newCoordStr);
                        visitedCoordStrsArr = WorldServer.updateCopperCurrent(newCoord, hasCurrent, false, visitedCoordStrsArr, (
                            isFirstBlock
                            ||
                            Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]]]['isConductingCurrent']
                        ));
                    }
                }
            }
        }
        return visitedCoordStrsArr;
    }

    static toggleBlock(blockCoordArr: Coord): void {
        if (
            WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']] != null
            &&
            WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0] in Utils.tileIndexToItemIdArr // Is a valid block, and,
            &&
            Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]] in Items.itemsArr
            &&
            Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]]]['toggleItemId'] // Block is toggleable.
        ) {
            let newBlockId                                                                             = Items.itemsArr[Utils.tileIndexToItemIdArr[WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0]]]['toggleItemId'];
            WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']][0] = Items.itemsArr[newBlockId]['tileIndexesArr'][0];
            WorldServer.updateSquareRedis(blockCoordArr);
            UtilsServer.io.emit('blockUpdate', {
                'blockCoordArr': blockCoordArr,
                'blockDataArr':  WorldServer.worldArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']],
            }); // Emit to all players.
            if (
                Items.itemsArr[newBlockId]['isEmittingCurrent']
                ||
                (
                    newBlockId + '_down' in Items.itemsArr
                    &&
                    Items.itemsArr[newBlockId + '_down']['isEmittingCurrent']
                )
            ) {
                WorldServer.updateCopperCurrent(blockCoordArr, Items.itemsArr[newBlockId]['isEmittingCurrent']);
            }
        }
    }

}
