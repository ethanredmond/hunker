const Utils = require('./Utils');
const Items = require('./public/generated/Items');
const Chat  = require('./Chat');
import {UtilsServer} from './UtilsServer';
import {PlayerServer} from './PlayerServer';
import {EntityServer} from './EntityServer';
import {WorldServer} from './WorldServer';
import {HotbarServer} from './HotbarServer';
import {DayNightCycleServer} from './DayNightCycleServer';
import SocketIO = require('socket.io');
import {EntityType, StatBarsDataArr} from './HunkerInterfaces';

export class ChatServer {

    static handleCommandServer(messageText: string, socket: SocketIO.Socket): void {
        for (let commandKey in Chat.allCommandsArr) {
            if (messageText.indexOf(commandKey) == 0 && !Chat.allCommandsArr[commandKey]['isLocal']) {
                // @ts-ignore
                ChatServer[Chat.allCommandsArr[commandKey]['functionName']](messageText, socket);
                break;
            }
        }
    }

    static givePlayerItems(messageText: string, socket: SocketIO.Socket): void {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj          = PlayerServer.allPlayerObjs[socket.id];
        const commandArgsArr     = messageText.substr('/give '.length).split(' ');
        let itemToGiveId         = commandArgsArr[0];
        const prevSelectedItemId = playerObj.selectedItemId;
        const prevHotbarArr      = Utils.dereferenceObj(playerObj.hotbarItemsArr);
        let playerMessageText;
        if (itemToGiveId == 'gear_diamond' || itemToGiveId == 'gear_gold' || itemToGiveId == 'gear_silver' || itemToGiveId == 'gear_crude' || itemToGiveId == 'gear_stone' || itemToGiveId == 'gear_wood') {
            let itemsToGiveArr;
            if (itemToGiveId == 'gear_diamond') {
                itemsToGiveArr = [
                    {'itemId': 'sword_diamond', 'itemCount': 1},
                    {'itemId': 'pick_diamond', 'itemCount': 1},
                    {'itemId': 'hoe_diamond', 'itemCount': 1},
                    {'itemId': 'axe_diamond', 'itemCount': 1},
                    {'itemId': 'shovel_diamond', 'itemCount': 1},
                    {'itemId': 'planks', 'itemCount': 64},
                    {'itemId': 'stone', 'itemCount': 64},
                    {'itemId': 'stew', 'itemCount': 64},
                    {'itemId': 'torch', 'itemCount': 64},
                    {'itemId': 'helmet_diamond', 'itemCount': 1},
                    {'itemId': 'chestplate_diamond', 'itemCount': 1},
                    {'itemId': 'leggings_diamond', 'itemCount': 1},
                    {'itemId': 'boots_diamond', 'itemCount': 1},
                    {'itemId': 'bag', 'itemCount': 1},
                    {'itemId': 'backpack', 'itemCount': 1},
                ];
            } else if (itemToGiveId == 'gear_gold') {
                itemsToGiveArr = [
                    {'itemId': 'sword_gold', 'itemCount': 1},
                    {'itemId': 'pick_gold', 'itemCount': 1},
                    {'itemId': 'hoe_gold', 'itemCount': 1},
                    {'itemId': 'axe_gold', 'itemCount': 1},
                    {'itemId': 'shovel_gold', 'itemCount': 1},
                    {'itemId': 'planks', 'itemCount': 64},
                    {'itemId': 'stone', 'itemCount': 64},
                    {'itemId': 'stew', 'itemCount': 64},
                    {'itemId': 'torch', 'itemCount': 64},
                    {'itemId': 'helmet_gold', 'itemCount': 1},
                    {'itemId': 'chestplate_gold', 'itemCount': 1},
                    {'itemId': 'leggings_gold', 'itemCount': 1},
                    {'itemId': 'boots_gold', 'itemCount': 1},
                    {'itemId': 'bag', 'itemCount': 1},
                    {'itemId': 'backpack', 'itemCount': 1},
                ];
            } else if (itemToGiveId == 'gear_silver') {
                itemsToGiveArr = [
                    {'itemId': 'sword_silver', 'itemCount': 1},
                    {'itemId': 'pick_silver', 'itemCount': 1},
                    {'itemId': 'hoe_silver', 'itemCount': 1},
                    {'itemId': 'axe_silver', 'itemCount': 1},
                    {'itemId': 'shovel_silver', 'itemCount': 1},
                    {'itemId': 'planks', 'itemCount': 64},
                    {'itemId': 'stone', 'itemCount': 64},
                    {'itemId': 'stew', 'itemCount': 64},
                    {'itemId': 'torch', 'itemCount': 64},
                    {'itemId': 'helmet_silver', 'itemCount': 1},
                    {'itemId': 'chestplate_silver', 'itemCount': 1},
                    {'itemId': 'leggings_silver', 'itemCount': 1},
                    {'itemId': 'boots_silver', 'itemCount': 1},
                    {'itemId': 'bag', 'itemCount': 1},
                    {'itemId': 'backpack', 'itemCount': 1},
                ];
            } else if (itemToGiveId == 'gear_crude') {
                itemsToGiveArr = [
                    {'itemId': 'sword_stone', 'itemCount': 1},
                    {'itemId': 'pick_stone', 'itemCount': 1},
                    {'itemId': 'hoe_stone', 'itemCount': 1},
                    {'itemId': 'axe_stone', 'itemCount': 1},
                    {'itemId': 'shovel_stone', 'itemCount': 1},
                    {'itemId': 'planks', 'itemCount': 64},
                    {'itemId': 'stone', 'itemCount': 64},
                    {'itemId': 'stew', 'itemCount': 64},
                    {'itemId': 'torch', 'itemCount': 64},
                    {'itemId': 'helmet_crude', 'itemCount': 1},
                    {'itemId': 'chestplate_crude', 'itemCount': 1},
                    {'itemId': 'leggings_crude', 'itemCount': 1},
                    {'itemId': 'boots_crude', 'itemCount': 1},
                    {'itemId': 'bag', 'itemCount': 1},
                    {'itemId': 'backpack', 'itemCount': 1},
                ];
            } else if (itemToGiveId == 'gear_stone') {
                itemsToGiveArr = [
                    {'itemId': 'pick_stone', 'itemCount': 1},
                    {'itemId': 'hoe_wood', 'itemCount': 1},
                    {'itemId': 'axe_stone', 'itemCount': 1},
                    {'itemId': 'apple', 'itemCount': 4},
                    {'itemId': 'torch', 'itemCount': 1},
                ];
            } else if (itemToGiveId == 'gear_wood') {
                itemsToGiveArr = [
                    {'itemId': 'pick_wood', 'itemCount': 1},
                    {'itemId': 'axe_wood', 'itemCount': 1},
                    {'itemId': 'torch', 'itemCount': 1},
                ];
            }
            playerObj.hotbarItemsArr = HotbarServer.addItemsFromArr(playerObj.hotbarItemsArr, itemsToGiveArr);
            playerMessageText        = "Gave " + itemToGiveId;
        } else {
            if (!itemToGiveId || !(itemToGiveId in Items.itemsArr) || !Items.itemsArr[itemToGiveId]['isObtainable']) {
                UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text':   "\"" + itemToGiveId + "\" not found, try <code>/give wood 5</code>",
                });
                return;
            }
            let numItemsToGive = 1;
            if (1 in commandArgsArr) {
                numItemsToGive = parseInt(commandArgsArr[1]);
            }
            if (!numItemsToGive) {
                UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text':   "Invalid items num, try <code>/give " + itemToGiveId + " 5</code>",
                });
                return;
            }
            numItemsToGive     = Math.min(numItemsToGive, 64);
            numItemsToGive     = Math.max(numItemsToGive, 1);
            const newItemIndex = Utils.getNewItemIndex(playerObj.hotbarItemsArr, itemToGiveId);
            if (newItemIndex == null) {
                UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text':   "Hotbar full",
                });
                return;
            }

            // Add items to hotbar.
            playerObj.hotbarItemsArr = HotbarServer.addItemsAtIndex(playerObj.hotbarItemsArr, {'itemId': itemToGiveId, 'itemCount': numItemsToGive}, newItemIndex);
            playerMessageText        = "Gave " + numItemsToGive + "x " + Utils.lcfirst(Items.itemsArr[itemToGiveId]['label']);
        }

        playerObj.selectedItemId = playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemId'];
        playerObj.updateHotbarRedis(prevHotbarArr);
        if (prevSelectedItemId != playerObj.selectedItemId) {
            socket.broadcast.emit('selectItem', playerObj.id, playerObj.selectedItemId); // Emit to all other players.
        }

        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status':         'success',
            'text':           playerMessageText,
            'hotbarItemsArr': playerObj.hotbarItemsArr,
        });
    }

    static enchantItem(messageText: string, socket: SocketIO.Socket): void {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj = PlayerServer.allPlayerObjs[socket.id];
        if (!playerObj.selectedItemId) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Nothing selected",
            });
            return;
        }
        const commandArgsArr = messageText.substr('/enchant '.length).split(' ');
        let enchantmentType  = commandArgsArr[0];
        const prevHotbarArr  = Utils.dereferenceObj(playerObj.hotbarItemsArr);
        let playerMessageText;
        if (!enchantmentType || !(enchantmentType in Utils.allEnchantmentsArr)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "\"" + enchantmentType + "\" not found, try <code>/enchant durability</code>",
            });
            return;
        }
        let tierNum = 1;
        if (1 in commandArgsArr) {
            tierNum = parseInt(commandArgsArr[1]);
        }
        if (!tierNum) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Invalid tier num, try <code>/enchant " + enchantmentType + " 2</code>",
            });
            return;
        }
        if (!Utils.playerGameModesArr[playerObj.gameModeStr]['hasInfiniteBlocks']) {
            tierNum = Math.min(tierNum, Utils.allEnchantmentsArr[enchantmentType]['tierMax']);
        }
        tierNum = Math.max(tierNum, 1);
        if (!('itemEnchantmentsArr' in playerObj.hotbarItemsArr[playerObj.selectedItemIndex])) {
            playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemEnchantmentsArr'] = {};
        }
        playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemEnchantmentsArr'][enchantmentType] = tierNum;
        playerMessageText                                                                             = "Enchanted with " + Utils.allEnchantmentsArr[enchantmentType]['label'] + ' ' + Utils.toRomanNumerals(tierNum);
        console.log('enchantmentType', enchantmentType);
        playerObj.updateHotbarRedis(prevHotbarArr);
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status':         'success',
            'text':           playerMessageText,
            'hotbarItemsArr': playerObj.hotbarItemsArr,
        });
    }

    static setPlayerLayer(messageText: string, socket: SocketIO.Socket): void {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj = PlayerServer.allPlayerObjs[socket.id];
        const layerNum  = parseInt(messageText.substr('/layer '.length));
        playerObj.setCurrentLayerNum(layerNum, function (response) {
            if (response['status'] == 'error') {
                UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text':   response['errorMessage'],
                });
            } else {
                UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'success',
                    'text':   "Set layer to " + layerNum,
                });
            }
        });
    }

    static killPlayer(messageText: string, socket: SocketIO.Socket): void {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj = PlayerServer.allPlayerObjs[socket.id];
        playerObj.kill('command');
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Killed",
        });
    }

    static revivePlayer(messageText: string, socket: SocketIO.Socket): void {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj = PlayerServer.allPlayerObjs[socket.id];
        playerObj.revive();
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Revived",
        });
    }

    static startDay(messageText: string, socket: SocketIO.Socket): void {
        DayNightCycleServer.startDay();
        clearTimeout(DayNightCycleServer.nextTimeout);
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Started day",
        });
    }

    static startNight(messageText: string, socket: SocketIO.Socket): void {
        DayNightCycleServer.startNight();
        clearTimeout(DayNightCycleServer.nextTimeout);
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Started night",
        });
    }

    static setPlayerStatBar(messageText: string, socket: SocketIO.Socket) {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj     = PlayerServer.allPlayerObjs[socket.id];
        const whichStatBar  = messageText.substr(1, 6);
        let newStatBarValue = parseInt(messageText.substr(8));
        if (!newStatBarValue && newStatBarValue !== 0) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   messageText.substr(8) + " is invalid",
            });
            return;
        }
        if (whichStatBar != 'health' && whichStatBar != 'warmth' && whichStatBar != 'hunger') {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   messageText.substr(8) + " is invalid",
            });
            return;
        }
        newStatBarValue         = Math.max(newStatBarValue, 0);
        newStatBarValue         = Math.min(newStatBarValue, 20);
        playerObj[whichStatBar] = newStatBarValue;
        if (playerObj.userName) {
            UtilsServer.redisClient.hset('player:' + playerObj.userName, whichStatBar, playerObj[whichStatBar].toString());
        }
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Set your " + whichStatBar + " to " + playerObj[whichStatBar],
        });
        const statBarsDataArr: StatBarsDataArr = {};
        statBarsDataArr[whichStatBar]          = playerObj[whichStatBar];
        UtilsServer.io.to(socket.id).emit('updateStatBars', {'status': 'success', 'statBarsDataArr': statBarsDataArr});
    }

    static summonEntity(messageText: string, socket: SocketIO.Socket) {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj = PlayerServer.allPlayerObjs[socket.id];
        let entityType  = messageText.substr('/summon '.length);
        let summonNum   = 1;
        if (entityType.indexOf(' ') != -1) {
            summonNum  = parseInt(entityType.split(' ')[1]);
            entityType = entityType.split(' ')[0];
        }
        if (!entityType || !(entityType in Utils.allEntityTypesArr)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   entityType + " is invalid",
            });
            return;
        }
        if (!summonNum || isNaN(summonNum)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   summonNum + " is invalid",
            });
            return;
        }
        for (let i = 0; i < summonNum; i++) {
            new EntityServer({
                'type':     <EntityType>entityType,
                'layerNum': playerObj.currentLayerNum,
                'x':        playerObj.x,
                'y':        playerObj.y,
                'isBaby':   false,
            });
        }
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Summoned " + entityType,
        });
    }

    static setNightsSurvivedNum(messageText: string, socket: SocketIO.Socket) {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj       = PlayerServer.allPlayerObjs[socket.id];
        let nightsSurvivedNum = parseInt(messageText.slice('/nights '.length));
        if (!nightsSurvivedNum && nightsSurvivedNum !== 0) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   messageText.substr(8) + " is invalid",
            });
            return;
        }
        nightsSurvivedNum           = Math.max(nightsSurvivedNum, 0);
        playerObj.nightsSurvivedNum = nightsSurvivedNum;
        if (playerObj.userName) {
            UtilsServer.redisClient.hset('player:' + playerObj.userName, 'nightsSurvivedNum', nightsSurvivedNum.toString());
        }
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Set your nights survived to " + nightsSurvivedNum,
        });
    }

    // static setDifficulty(messageText, socket) {
    //     if (!(socket.id in PlayerServer.allPlayerObjs)) {
    //         UtilsServer.io.to(socket.id).emit('playerMessage', {
    //             'status': 'error',
    //             'text':   "Player not found",
    //         });
    //         return;
    //     }
    //     const playerObj      = PlayerServer.allPlayerObjs[socket.id];
    //     let difficultyLetter = messageText.slice('/difficulty '.length);
    //     if (!(difficultyLetter in Utils.playerDifficultyLettersArr)) {
    //         UtilsServer.io.to(socket.id).emit('playerMessage', {
    //             'status': 'error',
    //             'text':   messageText.substr(8) + " is invalid",
    //         });
    //         return;
    //     }
    //     playerObj.difficultyLetter = difficultyLetter;
    //     if (playerObj.userName) {
    //         UtilsServer.redisClient.hset('player:' + playerObj.userName, 'difficultyLetter', difficultyLetter);
    //     }
    //     // Emit to specific player.
    //     UtilsServer.io.to(socket.id).emit('playerMessage', {
    //         'status': 'success',
    //         'text':   "Set your difficulty to \"" + difficultyLetter + "\"",
    //     });
    // }

    static setSpawn(messageText: string, socket: SocketIO.Socket) {
        if (!(socket.id in PlayerServer.allPlayerObjs)) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Player not found",
            });
            return;
        }
        const playerObj = PlayerServer.allPlayerObjs[socket.id];
        if (playerObj.currentLayerNum != 0) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Can only set on overworld",
            });
            return;
        }
        const playerXRounded = Math.floor(playerObj.x / Utils.tileSize);
        const playerYRounded = Math.floor(playerObj.y / Utils.tileSize);
        if (WorldServer.worldArr[0][playerYRounded][playerXRounded]) {
            UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text':   "Block at spawn",
            });
            return;
        }
        WorldServer.worldArr[WorldServer.spawnLayerNum][WorldServer.spawnY][WorldServer.spawnX] = null;
        WorldServer.updateSquareRedis({
            'layerNum': WorldServer.spawnLayerNum,
            'x':        WorldServer.spawnX,
            'y':        WorldServer.spawnY,
        });
        UtilsServer.io.emit('blockUpdate', {
            'blockId':       null,
            'blockCoordArr': {
                'layerNum': WorldServer.spawnLayerNum,
                'x':        WorldServer.spawnX,
                'y':        WorldServer.spawnY,
            },
            'blockDataArr':  WorldServer.worldArr[WorldServer.spawnLayerNum][WorldServer.spawnY][WorldServer.spawnX],
        }); // Emit to all players.
        WorldServer.spawnLayerNum = playerObj.currentLayerNum;
        WorldServer.spawnX        = playerXRounded;
        WorldServer.spawnY        = playerYRounded;
        const spawnCoordStr       = playerObj.currentLayerNum + ':' + playerXRounded + ',' + playerYRounded;
        console.log(spawnCoordStr);
        UtilsServer.redisClient.set('spawn', spawnCoordStr);
        WorldServer.addSpawnBlock();
        UtilsServer.io.emit('blockUpdate', {
            'blockId':       'spawn',
            'blockCoordArr': {
                'layerNum': WorldServer.spawnLayerNum,
                'x':        WorldServer.spawnX,
                'y':        WorldServer.spawnY,
            },
            'blockDataArr':  WorldServer.worldArr[WorldServer.spawnLayerNum][WorldServer.spawnY][WorldServer.spawnX],
        }); // Emit to all players.
        // Emit to specific player.
        UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text':   "Set spawn to " + spawnCoordStr,
        });
    }

}
