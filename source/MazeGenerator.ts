export class MazeGenerator {

    /**
     * Don't really know how this works, just copied and then modified from http://rosettacode.org/wiki/Maze_generation#JavaScript
     */
    static generateMaze(width: number, height: number): string[][] {
        let n     = width * height - 1;
        let horiz = [];
        for (let x = 0; x < width + 1; x++) {
            horiz[x] = [];
        }
        let verti = [];
        for (let y = 0; y < height + 1; y++) {
            verti[y] = [];
        }
        let here      = [Math.floor(Math.random() * width), Math.floor(Math.random() * height)];
        let path      = [here];
        let unvisited = [];
        for (let x = 0; x < width + 2; x++) {
            unvisited[x] = [];
            for (let y = 0; y < height + 1; y++) {
                unvisited[x].push(x > 0 && x < width + 1 && y > 0 && (x != here[0] + 1 || y != here[1] + 1));
            }
        }
        while (0 < n) {
            const potential = [
                [here[0] + 1, here[1]], [here[0], here[1] + 1],
                [here[0] - 1, here[1]], [here[0], here[1] - 1]
            ];
            const neighbors = [];
            for (let x = 0; x < 4; x++) {
                if (unvisited[potential[x][0] + 1][potential[x][1] + 1]) {
                    neighbors.push(potential[x]);
                }
            }
            if (neighbors.length) {
                n--;
                const next                          = neighbors[Math.floor(Math.random() * neighbors.length)];
                unvisited[next[0] + 1][next[1] + 1] = false;
                if (next[0] == here[0]) {
                    horiz[next[0]][(next[1] + here[1] - 1) / 2] = true;
                } else {
                    verti[(next[0] + here[0] - 1) / 2][next[1]] = true;
                }
                here = next;
                path.push(here);
            } else {
                here = path.pop();
            }
        }
        const structureArr: string[][] = [];
        for (let x = 0; x < width * 2 + 1; x++) {
            structureArr[x] = [];
            if (0 == x % 2) {
                for (let y = 0; y < height * 2 + 1; y++) {
                    if (0 == y % 2) {
                        structureArr[x][y] = 'w';
                    } else if (x > 0 && verti[x / 2 - 1][Math.floor(y / 2)]) {
                        structureArr[x][y] = ' ';
                    } else {
                        structureArr[x][y] = 'w';
                    }
                }
            } else {
                for (let y = 0; y < height * 2 + 1; y++) {
                    if (0 == y % 2) {
                        if (y > 0 && horiz[(x - 1) / 2][y / 2 - 1]) {
                            structureArr[x][y] = ' ';
                        } else {
                            structureArr[x][y] = 'w';
                        }
                    } else {
                        structureArr[x][y] = ' ';
                    }
                }
            }
        }
        return structureArr;
    }

}
