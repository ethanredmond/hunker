/// <reference path="Utils.ts" />
if (typeof require !== 'undefined') {
    // @ts-ignore
    Utils = require('./Utils');
}

class Chat {

    static allCommandsArr: { [commandStr: string]: { exampleCommandStr: string, descriptionText: string, isLocal: boolean, functionName: keyof typeof import('./ChatServer').ChatServer | keyof typeof ChatClient } } = {};

    static init(): void {
        Chat.allCommandsArr                = {};
        // @formatter:off
        Chat.allCommandsArr['/speed']      = {'exampleCommandStr': '/speed 0-3',           'descriptionText': "Sets your speed",                 'isLocal': true,  'functionName': 'setPlayerSpeed'};
        Chat.allCommandsArr['/lighttick']  = {'exampleCommandStr': '/lighttick 1-10',      'descriptionText': "Sets lights update frequency",    'isLocal': true,  'functionName': 'setLightTick'};
        Chat.allCommandsArr['/help']       = {'exampleCommandStr': '/help',                'descriptionText': "Outputs this message",            'isLocal': true,  'functionName': 'showHelpMessage'};
        Chat.allCommandsArr['/setspawn']   = {'exampleCommandStr': '/setspawn',            'descriptionText': "Sets spawn to current pos",       'isLocal': false, 'functionName': 'setSpawn'};
        if (Utils.isDev) {
            Chat.allCommandsArr['/summon']          = {'exampleCommandStr': '/summon pig',          'descriptionText': "Summons an entity",               'isLocal': false, 'functionName': 'summonEntity'};
            Chat.allCommandsArr['/give']            = {'exampleCommandStr': '/give wood 5',         'descriptionText': "Gives you items",                 'isLocal': false, 'functionName': 'givePlayerItems'};
            Chat.allCommandsArr['/layer']           = {'exampleCommandStr': '/layer -1',            'descriptionText': "Changes your layer",              'isLocal': false, 'functionName': 'setPlayerLayer'};
            Chat.allCommandsArr['/kill']            = {'exampleCommandStr': '/kill',                'descriptionText': "Kills you",                       'isLocal': false, 'functionName': 'killPlayer'};
            Chat.allCommandsArr['/revive']          = {'exampleCommandStr': '/revive',              'descriptionText': "Revives you",                     'isLocal': false, 'functionName': 'revivePlayer'};
            Chat.allCommandsArr['/nights']          = {'exampleCommandStr': '/nights 4',            'descriptionText': "Sets nights survived",            'isLocal': false, 'functionName': 'setNightsSurvivedNum'};
            Chat.allCommandsArr['/day']             = {'exampleCommandStr': '/day',                 'descriptionText': "Starts day",                      'isLocal': false, 'functionName': 'startDay'};
            Chat.allCommandsArr['/night']           = {'exampleCommandStr': '/night',               'descriptionText': "Starts night",                    'isLocal': false, 'functionName': 'startNight'};
            Chat.allCommandsArr['/health']          = {'exampleCommandStr': '/health 0-20',         'descriptionText': "Sets your health",                'isLocal': false, 'functionName': 'setPlayerStatBar'};
            Chat.allCommandsArr['/hunger']          = {'exampleCommandStr': '/hunger 0-20',         'descriptionText': "Sets your hunger",                'isLocal': false, 'functionName': 'setPlayerStatBar'};
            Chat.allCommandsArr['/warmth']          = {'exampleCommandStr': '/warmth 0-20',         'descriptionText': "Sets your warmth",                'isLocal': false, 'functionName': 'setPlayerStatBar'};
            Chat.allCommandsArr['/brighten']        = {'exampleCommandStr': '/brighten',            'descriptionText': "Hides darkness",                  'isLocal': true,  'functionName': 'brighten'};
            Chat.allCommandsArr['/darken']          = {'exampleCommandStr': '/darken',              'descriptionText': "Resets /brighten",                'isLocal': true,  'functionName': 'darken'};
            Chat.allCommandsArr['/collide']         = {'exampleCommandStr': '/collide 0',           'descriptionText': "Toggles collisions",              'isLocal': true,  'functionName': 'collide'};
            Chat.allCommandsArr['/heldsprite']      = {'exampleCommandStr': '/heldsprite',          'descriptionText': "Held sprite adjuster",            'isLocal': true,  'functionName': 'showHeldSpriteAdjuster'};
            Chat.allCommandsArr['/enchant']         = {'exampleCommandStr': '/enchant durability',  'descriptionText': "Adds enchantments",               'isLocal': false, 'functionName': 'enchantItem'};
            // Chat.allCommandsArr['/timespent']    = {'exampleCommandStr': '/timespent 0',         'descriptionText': "Resets time spent counter",       'isLocal': true,  'functionName': 'timespent'};
            // @formatter:on
        }
    }

}

Chat.init();

if (typeof module !== 'undefined') {
    module.exports = Chat;
}
