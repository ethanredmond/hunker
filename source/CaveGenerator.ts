const Utils = require('./Utils');
const Items = require('./public/generated/Items');
import {BiomeName, WorldCell, WorldLayer, WorldRow} from './HunkerInterfaces';

// https://gamedevelopment.tutsplus.com/tutorials/generate-random-cave-levels-using-cellular-automata--gamedev-9664
export class CaveGenerator {

    private readonly chanceToStartAlive: number;
    private readonly deathLimit: number;
    private readonly birthLimit: number;
    private readonly numberOfSteps: number;
    private readonly cellAliveValuesArr: (WorldCell | null)[];
    private readonly cellDeadValuesArr: (WorldCell | null)[];
    private readonly isGeneratingPools: boolean;
    private readonly layerNum: number;
    private readonly originalWorldLayerArr: WorldLayer;
    private readonly originalFloorLayerArr: WorldLayer;
    private readonly layerBiomeCoordsArr: BiomeName[][];
    public layerArr: WorldLayer;

    constructor(
        chanceToStartAlive: number,
        deathLimit: number,
        birthLimit: number,
        numberOfSteps: number,
        cellAliveValuesArr: ([number, number] | null)[],
        cellDeadValuesArr: ([number, number] | null)[],
        isGeneratingPools: boolean,
        layerNum: number,
        originalWorldLayerArr: WorldLayer,
        originalFloorLayerArr: WorldLayer,
        layerBiomeCoordsArr: BiomeName[][] = null,
    ) {
        this.chanceToStartAlive    = chanceToStartAlive;
        this.deathLimit            = deathLimit;
        this.birthLimit            = birthLimit;
        this.numberOfSteps         = numberOfSteps;
        this.cellAliveValuesArr    = cellAliveValuesArr;
        this.cellDeadValuesArr     = cellDeadValuesArr;
        this.isGeneratingPools     = isGeneratingPools;
        this.layerNum              = layerNum;
        this.originalWorldLayerArr = Utils.dereferenceObj(originalWorldLayerArr);
        this.originalFloorLayerArr = Utils.dereferenceObj(originalFloorLayerArr);
        this.layerBiomeCoordsArr   = layerBiomeCoordsArr;
    }

    generateLayerArr(layerArr: WorldLayer) {
        this.layerArr = Utils.dereferenceObj(layerArr);
        this.initMap();
        for (let i = 0; i < this.numberOfSteps; i++) {
            this.doSimulationStep();
        }
        if (!this.isGeneratingPools) {
            this.addLootcrates();
        }
    }

    initMap() {
        for (let y = 0; y < this.layerArr.length; y++) {
            for (let x = 0; x < this.layerArr[y].length; x++) {
                if (
                    this.isGeneratingPools
                    &&
                    (
                        this.originalWorldLayerArr[y][x] != null // Block on world layer above.
                        ||
                        Items.itemsArr['cave']['tileIndexesArr'].indexOf(this.originalFloorLayerArr[y][x][0]) == -1 // Isn't cave floor, don't mess with it.
                    )
                ) {
                    // Leave cell as is.
                    continue;
                }
                let chanceToStartAlive = this.chanceToStartAlive;
                if (this.layerBiomeCoordsArr && this.layerBiomeCoordsArr[y] && this.layerBiomeCoordsArr[y][x] == 'glaciers') {
                    chanceToStartAlive -= 0.11;
                }
                if (Math.random() < chanceToStartAlive) {
                    this.layerArr[y][x] = Utils.randElFromArr(this.cellAliveValuesArr);
                } else {
                    this.layerArr[y][x] = Utils.randElFromArr(this.cellDeadValuesArr);
                }
            }
        }
    }

    doSimulationStep(): void {
        let newLayerArr: WorldLayer = [];
        for (let y = 0; y < this.layerArr.length; y++) {
            let newLayerRow: WorldRow = [];
            for (let x = 0; x < this.layerArr[y].length; x++) {
                if (
                    this.isGeneratingPools
                    &&
                    (
                        this.originalWorldLayerArr[y][x] != null // Block on world layer above.
                        ||
                        Items.itemsArr['cave']['tileIndexesArr'].indexOf(this.originalFloorLayerArr[y][x][0]) == -1 // Isn't cave floor, don't mess with it.
                    )
                ) {
                    // Leave cell as is.
                    newLayerRow.push(this.layerArr[y][x]);
                    continue;
                }
                let aliveNeighboursNum = this.countAliveNeighbours(x, y);
                if (this.cellAliveValuesArr.indexOf(this.layerArr[y][x]) != -1) { // If the tile is currently alive.
                    if (aliveNeighboursNum < this.deathLimit) { // See if it should die.
                        newLayerRow.push(Utils.randElFromArr(this.cellDeadValuesArr));
                    } else { // Otherwise keep it alive.
                        newLayerRow.push(Utils.randElFromArr(this.cellAliveValuesArr));
                    }
                } else { // If the tile is currently dead.
                    // See if it should become alive.
                    if (aliveNeighboursNum > this.birthLimit) {
                        newLayerRow.push(Utils.randElFromArr(this.cellAliveValuesArr));
                    } else {
                        newLayerRow.push(Utils.randElFromArr(this.cellDeadValuesArr));
                    }
                }
            }
            newLayerArr.push(newLayerRow);
        }
        this.layerArr = newLayerArr;
    }

    countAliveNeighbours(x: number, y: number): number {
        let count = 0;
        for (let i = -1; i < 2; i++) {
            for (let j = -1; j < 2; j++) {
                let x_loop = i + x;
                let y_loop = j + y;
                if (i == 0 && j == 0) {
                    // Is current block.
                } else if ( // If it's at the edges, consider it to be alive.
                    y_loop < 0
                    ||
                    x_loop < 0
                    ||
                    y_loop >= this.layerArr.length
                    ||
                    x_loop >= this.layerArr[y_loop].length
                    ||
                    (
                        this.isGeneratingPools
                        &&
                        (
                            this.originalWorldLayerArr[y_loop][x_loop] != null // Is alive if block on world layer.
                            ||
                            Items.itemsArr['cave']['tileIndexesArr'].indexOf(this.originalFloorLayerArr[y_loop][x_loop][0]) == -1 // Isn't cave floor, don't mess with it.
                        )
                    )
                ) {
                    count++;
                } else if (this.cellAliveValuesArr.indexOf(this.layerArr[y_loop][x_loop]) != -1) {
                    count++;
                }
            }
        }
        return count;
    }

    addLootcrates() {
        for (let y = 0; y < this.layerArr.length; y++) {
            for (let x = 0; x < this.layerArr[y].length; x++) {
                if (!this.layerArr[y][x]) {
                    let neighboursNum = this.countAliveNeighbours(x, y);
                    if (neighboursNum >= 6) {
                        this.layerArr[y][x] = [Items.itemsArr['lootcrate_wood']['tileIndexesArr'][0], Items.itemsArr['lootcrate_wood']['structureDurability']];
                    }
                }
            }
        }
    }

}
