/// <reference path="Utils.ts" />
if (typeof require !== 'undefined') {
    // @ts-ignore
    Utils = require('./Utils');
}
var Dungeon = /** @class */ (function () {
    function Dungeon(width, height, layerNum) {
        if (width % 2 == 0 || height % 2 == 0) {
            throw new Error("The dungeon must be odd-sized.");
        }
        this.width = width;
        this.height = height;
        this.layerNum = layerNum;
        this.roomExtraSize = 0;
        this.numTiles = width * height;
        this.numRooms = Math.round(this.numTiles * 0.002);
        // this.currentRegion        = -1;
        this.generateDungeon();
    }
    // http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
    //
    // The random dungeon generator.
    //
    // Starting with a stage of solid walls, it works like so:
    //
    // 1. Place a number of randomly sized and positioned rooms. If a room
    //    overlaps an existing room, it is discarded. Any remaining rooms are
    //    carved out.
    // 2. Any remaining solid areas are filled in with mazes. The maze generator
    //    will grow and fill in even odd-shaped areas, but will not touch any
    //    rooms.
    // 3. The result of the previous two steps is a series of unconnected rooms
    //    and mazes. We walk the stage and find every tile that can be a
    //    "connector". This is a solid tile that is adjacent to two unconnected
    //    regions.
    // 4. We randomly choose connectors and open them or place a door there until
    //    all of the unconnected regions have been joined. There is also a slight
    //    chance to carve a connector between two already-joined regions, so that
    //    the dungeon isn't single connected.
    // 5. The mazes will have a lot of dead ends. Finally, we remove those by
    //    repeatedly filling in any open tile that's closed on three sides. When
    //    this is done, every corridor in a maze actually leads somewhere.
    //
    // The end result of this is a multiply-connected dungeon with rooms and lots
    // of winding corridors.
    Dungeon.prototype.generateDungeon = function () {
        this.initDungeon();
        this.addRooms();
        this.addMaze();
        this.addDoorsToRooms();
        this.removeDeadEnds();
        this.addDecorations();
    };
    Dungeon.prototype.initDungeon = function () {
        this.structureArr = [];
        this.structureFloorArr = [];
        for (var y = 0; y < this.height; y++) {
            var structureRow = [];
            for (var x = 0; x < this.width; x++) {
                structureRow.push('0');
            }
            this.structureArr.push(structureRow);
            this.structureFloorArr.push(structureRow);
        }
        this.structureFloorArr = Utils.dereferenceObj(this.structureFloorArr);
    };
    Dungeon.prototype.addRooms = function () {
        this.roomsArr = [];
        for (var i = 0; i < this.numRooms; i++) {
            this.addRoom(i == 0);
        }
    };
    Dungeon.prototype.addRoom = function (isMainRoom) {
        for (var numIterations = 0; numIterations < 100; numIterations++) {
            var size = void 0;
            var rectangularity = void 0;
            if (isMainRoom) {
                size = Math.floor(this.width * 0.09) * 2 + 1;
                rectangularity = 0;
            }
            else {
                size = Utils.randBetween(1, 2 + this.roomExtraSize) * 2 + 1;
                rectangularity = Utils.randBetween(0, 1 + Math.floor(size / 2)) * 2;
            }
            var width = size;
            var height = size;
            if (Utils.randBool()) {
                width += rectangularity;
            }
            else {
                height += rectangularity;
            }
            var corner1X = void 0;
            var corner1Y = void 0;
            if (isMainRoom) {
                corner1X = Math.floor((this.width - width - 1) / 2 / 2) * 2 + 1;
                corner1Y = Math.floor((this.height - height - 1) / 2 / 2) * 2 + 1;
            }
            else {
                corner1X = Utils.randBetween(1, Math.floor((this.width - width - 1) / 2)) * 2 + 1;
                corner1Y = Utils.randBetween(1, Math.floor((this.height - height - 1) / 2)) * 2 + 1;
            }
            var corner2X = corner1X + width;
            var corner2Y = corner1Y + height;
            var isOverlapping = false;
            for (var i in this.roomsArr) {
                if (!(this.roomsArr[i]['x1'] > corner2X
                    ||
                        this.roomsArr[i]['x2'] < corner1X
                    ||
                        this.roomsArr[i]['y1'] > corner2Y
                    ||
                        this.roomsArr[i]['y2'] < corner1Y)) {
                    isOverlapping = true;
                    break;
                }
            }
            if (isOverlapping) {
                continue;
            }
            this.roomsArr.push({ 'x1': corner1X, 'y1': corner1Y, 'x2': corner2X, 'y2': corner2Y, 'isMainRoom': isMainRoom });
            for (var y = 0; y < height; y++) {
                for (var x = 0; x < width; x++) {
                    this.carve({
                        'x': corner1X + x,
                        'y': corner1Y + y,
                    });
                }
            }
            break;
        }
    };
    Dungeon.prototype.addMaze = function () {
        // Fill in all of the empty space with mazes.
        for (var y = 1; y < this.height; y += 2) {
            for (var x = 1; x < this.width; x += 2) {
                if (this.structureArr[y][x] != '0') {
                    continue;
                }
                this.growMaze({ 'x': x, 'y': y });
            }
        }
    };
    // Implementation of the "growing tree" algorithm from here:
    // http://www.astrolog.org/labyrnth/algrithm.htm.
    Dungeon.prototype.growMaze = function (startCoordArr) {
        var cellCoordsArr = [];
        var lastDirectionLetter = null;
        // this.startRegion();
        this.carve(startCoordArr);
        cellCoordsArr.push(startCoordArr);
        while (cellCoordsArr.length > 0) {
            var cellCoordArr = cellCoordsArr[cellCoordsArr.length - 1];
            // See which adjacent cells are open.
            var unmadeCellDirectionsArr = [];
            for (var _i = 0, _a = Utils.directionsArr; _i < _a.length; _i++) {
                var directionLetter = _a[_i];
                if (this.canCarve(cellCoordArr, directionLetter)) {
                    unmadeCellDirectionsArr.push(directionLetter);
                }
            }
            if (unmadeCellDirectionsArr.length > 0) {
                // Based on how "windy" passages are, try to prefer carving in the
                // same direction.
                var directionLetter = void 0;
                if (lastDirectionLetter && unmadeCellDirectionsArr.indexOf(lastDirectionLetter) != -1 && (!Dungeon.windingPercent || Math.random() > Dungeon.windingPercent)) {
                    directionLetter = lastDirectionLetter;
                }
                else {
                    directionLetter = Utils.randElFromArr(unmadeCellDirectionsArr);
                }
                this.carve(Utils.getNewCoordFromDirection(cellCoordArr, directionLetter, 1, true));
                this.carve(Utils.getNewCoordFromDirection(cellCoordArr, directionLetter, 2, true));
                cellCoordsArr.push(Utils.getNewCoordFromDirection(cellCoordArr, directionLetter, 2, true));
                lastDirectionLetter = directionLetter;
            }
            else {
                // No adjacent uncarved cells.
                cellCoordsArr.pop();
                // This path has ended.
                lastDirectionLetter = null;
            }
        }
    };
    // Gets whether or not an opening can be carved from the given starting
    // [Cell] at [pos] to the adjacent Cell facing [direction]. Returns `true`
    // if the starting Cell is in bounds and the destination Cell is filled
    // (or out of bounds).
    Dungeon.prototype.canCarve = function (coordArr, directionLetter) {
        // Must end in bounds.
        var coordArrIn3 = Utils.getNewCoordFromDirection(coordArr, directionLetter, 3, true);
        if (coordArrIn3['x'] < 0 || coordArrIn3['y'] < 0 || coordArrIn3['x'] >= this.width || coordArrIn3['y'] >= this.height) {
            return false;
        }
        // Destination must not be open.
        var coordArrIn2 = Utils.getNewCoordFromDirection(coordArr, directionLetter, 2, true);
        return this.structureArr[coordArrIn2['y']][coordArrIn2['x']] != ' ';
    };
    Dungeon.prototype.carve = function (coordArr) {
        this.structureArr[coordArr['y']][coordArr['x']] = ' ';
        // this.structureFloorArr[coordArr['y']][coordArr['x']] = 'D';
        // this.regionsArr[coordArr['x'] + ',' + coordArr['y']] = this.currentRegion;
    };
    Dungeon.prototype.addDoorsToRooms = function () {
        for (var i in this.roomsArr) {
            var numDoors = void 0;
            if (this.roomsArr[i]['isMainRoom']) {
                numDoors = 4;
            }
            else {
                numDoors = (Math.random() < Dungeon.extraConnectorChance) ? 2 : 1;
            }
            for (var j = 0; j < numDoors; j++) {
                for (var numIterations = 0; numIterations < 100; numIterations++) {
                    var coordToCarveArr = void 0;
                    var directionLetter = void 0;
                    if (Utils.randBool()) {
                        var y = void 0;
                        if (Utils.randBool()) {
                            directionLetter = 'n';
                            y = this.roomsArr[i]['y1'];
                        }
                        else {
                            directionLetter = 's';
                            y = this.roomsArr[i]['y2'];
                        }
                        coordToCarveArr = {
                            'x': Utils.randBetween(this.roomsArr[i]['x1'] + 1, this.roomsArr[i]['x2'] - 1),
                            'y': y,
                        };
                    }
                    else {
                        var x = void 0;
                        if (Utils.randBool()) {
                            directionLetter = 'w';
                            x = this.roomsArr[i]['x1'];
                        }
                        else {
                            directionLetter = 'e';
                            x = this.roomsArr[i]['x2'];
                        }
                        coordToCarveArr = {
                            'x': x,
                            'y': Utils.randBetween(this.roomsArr[i]['y1'] + 1, this.roomsArr[i]['y2'] - 1),
                        };
                    }
                    if (coordToCarveArr['x'] >= 1
                        &&
                            coordToCarveArr['y'] >= 1
                        &&
                            coordToCarveArr['x'] < this.width - 1
                        &&
                            coordToCarveArr['y'] < this.height - 1
                        &&
                            this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] == '0') {
                        var coordArrIn2 = Utils.getNewCoordFromDirection(coordToCarveArr, directionLetter, 1, true);
                        if (coordArrIn2['x'] >= 1
                            &&
                                coordArrIn2['y'] >= 1
                            &&
                                coordArrIn2['x'] < this.width - 1
                            &&
                                coordArrIn2['y'] < this.height - 1
                            &&
                                this.structureArr[coordArrIn2['y']][coordArrIn2['x']] == ' ') {
                            this.structureFloorArr[coordToCarveArr['y']][coordToCarveArr['x']] = 'D';
                            if (this.roomsArr[i]['isMainRoom']) {
                                this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] = 'd'; // Always doors on main room.
                            }
                            else {
                                if (Utils.randBetween(1, 4) === 1) {
                                    this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] = ' ';
                                }
                                else {
                                    this.structureArr[coordToCarveArr['y']][coordToCarveArr['x']] = 'd';
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    };
    Dungeon.prototype.removeDeadEnds = function () {
        var isDone = false;
        while (!isDone) {
            isDone = true;
            for (var y = 1; y < this.height - 1; y++) {
                for (var x = 1; x < this.width - 1; x++) {
                    if (this.structureArr[y][x] == '0') {
                        continue;
                    }
                    // If it only has one exit, it's a dead end.
                    var exitsNum = 0;
                    for (var _i = 0, _a = Utils.directionsArr; _i < _a.length; _i++) {
                        var directionLetter = _a[_i];
                        var newCoordArr = Utils.getNewCoordFromDirection({ 'x': x, 'y': y }, directionLetter, 1, true);
                        if (this.structureArr[newCoordArr['y']][newCoordArr['x']] != '0') {
                            exitsNum++;
                        }
                    }
                    if (exitsNum != 1) {
                        continue;
                    }
                    isDone = false;
                    this.structureArr[y][x] = '0';
                    this.structureFloorArr[y][x] = '0';
                }
            }
        }
    };
    Dungeon.prototype.addDecorations = function () {
        // Surround rooms with walls.
        for (var i in this.roomsArr) {
            var wallLetter = (this.roomsArr[i]['isMainRoom']) ? 'b' : 'w';
            for (var y = this.roomsArr[i]['y1'] - 1; y < this.roomsArr[i]['y2'] + 1; y++) {
                // @formatter:off
                if (this.structureArr[y][this.roomsArr[i]['x1'] - 1] == '0') {
                    this.structureArr[y][this.roomsArr[i]['x1'] - 1] = wallLetter;
                }
                if (this.structureArr[y][this.roomsArr[i]['x2']] == '0') {
                    this.structureArr[y][this.roomsArr[i]['x2']] = wallLetter;
                }
                // @formatter:on
            }
            for (var x = this.roomsArr[i]['x1'] - 1; x < this.roomsArr[i]['x2'] + 1; x++) {
                // @formatter:off
                if (this.structureArr[this.roomsArr[i]['y1'] - 1][x] == '0') {
                    this.structureArr[this.roomsArr[i]['y1'] - 1][x] = wallLetter;
                }
                if (this.structureArr[this.roomsArr[i]['y2']][x] == '0') {
                    this.structureArr[this.roomsArr[i]['y2']][x] = wallLetter;
                }
                // @formatter:on
            }
            // Add lootcrates.
            var roomTilesNum = (this.roomsArr[i]['x1'] - this.roomsArr[i]['x2']) * (this.roomsArr[i]['y1'] - this.roomsArr[i]['y2']);
            var numLootcrates = Math.round(roomTilesNum * 0.05);
            numLootcrates = Math.min(numLootcrates, 5);
            numLootcrates = Math.max(numLootcrates, 1);
            if (this.roomsArr[i]['isMainRoom']) {
                numLootcrates *= 3;
            }
            for (var j = 0; j < numLootcrates; j++) {
                for (var numIterations = 0; numIterations < 100; numIterations++) {
                    var lootcrateCoordX = Utils.randBetween(this.roomsArr[i]['x1'] + 1, this.roomsArr[i]['x2'] - 1);
                    var lootcrateCoordY = Utils.randBetween(this.roomsArr[i]['y1'] + 1, this.roomsArr[i]['y2'] - 1);
                    var hasBlocksInRadius = false;
                    for (var y = Math.max(lootcrateCoordY - 1, 0); y <= Math.min(lootcrateCoordY + 1, Utils.worldHeight - 1) && !hasBlocksInRadius; y++) {
                        for (var x = Math.max(lootcrateCoordX - 1, 0); x <= Math.min(lootcrateCoordX + 1, Utils.worldWidth - 1) && !hasBlocksInRadius; x++) {
                            if (this.structureArr[y][x] != ' ') {
                                hasBlocksInRadius = true;
                            }
                        }
                    }
                    if (!hasBlocksInRadius) {
                        this.structureArr[lootcrateCoordY][lootcrateCoordX] = this.getLootcrateId();
                        break;
                    }
                }
            }
        }
        // Add red torches.
        var numTorches = Math.round(this.numTiles * 0.01);
        for (var i = 0; i < numTorches; i++) {
            for (var numIterations = 0; numIterations < 100; numIterations++) {
                var torchCoordX = Utils.randBetween(1, this.width - 2);
                var torchCoordY = Utils.randBetween(1, this.height - 2);
                if (this.structureArr[torchCoordY][torchCoordX] == ' ') {
                    this.structureArr[torchCoordY][torchCoordX] = 'r';
                    break;
                }
            }
        }
    };
    Dungeon.prototype.getLootcrateId = function () {
        return Utils.randElFromArr(Dungeon.lootcrateIdsArr[this.layerNum]);
    };
    Dungeon.lootcrateIdsArr = {
        '-1': [
            'lw',
            'lw',
            'lw',
            'lw',
            'lw',
            'c',
            'h',
            'e',
            's',
            'ls',
            'ls',
            'ls',
            'lb',
            'lb',
            'lb',
        ],
        '-2': [
            'lw',
            'lw',
            'c',
            'h',
            'e',
            's',
            'ls',
            'ls',
            'ls',
            'ls',
            'ls',
            'ls',
            'lb',
            'lb',
            'lr',
            'lr',
        ],
        '-3': [
            'lw',
            'c',
            'h',
            'e',
            's',
            'ls',
            'ls',
            'ls',
            'lb',
            'lb',
            'lr',
            'lr',
            'lg',
            'lg',
        ],
    };
    Dungeon.extraConnectorChance = 0.6;
    Dungeon.windingPercent = 1.00;
    return Dungeon;
}());
if (typeof module !== 'undefined') {
    module.exports = Dungeon;
}
//# sourceMappingURL=Dungeon.js.map