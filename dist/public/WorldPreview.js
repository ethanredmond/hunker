var WorldPreview = /** @class */ (function () {
    function WorldPreview() {
    }
    WorldPreview.init = function () {
        Utils.isWorldPage = true;
        Minimap.init();
        WorldClient.updateCurrentLayerNum(WorldPreview.defaultLayerNum);
        WorldPreview.generateLayer();
        $(document).keydown(function (event) {
            if ($(event.target).closest('input').length // An input isn't focused.
                ||
                    $(event.target).closest('textarea').length // An textarea isn't focused.
            ) {
                return;
            }
            if (!event.metaKey && !event.ctrlKey && !event.shiftKey && !event.altKey) {
                Minimap.keydown(event);
            }
            if (!event.metaKey && !event.ctrlKey && !event.shiftKey && event.altKey && event.key === 'r') {
                WorldPreview.generateLayer();
            }
        });
        $("<div class=\"itemSlot minimapLayerBtn\" title=\"Layer -3\" onclick=\"WorldPreview.openFalloffMap();\"><div class=\"item btn-flat\">F</div></div>").appendTo('#minimapLayerBtns');
        $("<a class=\"btn\" onclick=\"WorldPreview.generateLayer();\">Regenerate</a><br><br><div id=\"worldSeed\"></div>").appendTo('#buttonsContainer');
        Modals.closeAll();
        Minimap.bigMinimapModal.open();
    };
    WorldPreview.generateLayer = function () {
        Globals.socket.emit('generateLayer', WorldClient.currentLayerNum, function (responseData) {
            WorldClient.worldArr[WorldClient.currentLayerNum] = responseData['worldLayerArr'];
            WorldClient.floorArr[WorldClient.currentLayerNum] = responseData['floorLayerArr'];
            $('#worldSeed').html(responseData['seed']);
            Minimap.update();
        });
    };
    WorldPreview.openFalloffMap = function () {
        var cellWidth = Minimap.bigMinimapWidthPx / Utils.worldWidth;
        var cellHeight = Minimap.bigMinimapHeightPx / Utils.worldHeight;
        var minimapDivHtml = '';
        // let mapArr = NoiseMap.generate(60);
        var mapArr = Utils.generateFalloffMap();
        for (var y = 0; y < mapArr.length; y++) {
            for (var x = 0; x < mapArr[y].length; x++) {
                minimapDivHtml += "<div class=\"minimapDivCell\" title=\"" + mapArr[y][x].toFixed(2) + "\" style=\"left: " + (x * cellWidth) + "px; top: " + (y * cellHeight) + "px; opacity: " + mapArr[y][x].toFixed(2) + ";\"></div>";
            }
        }
        Minimap.$bigMinimapModal.find('.modal-content').attr('data-current-layer', 'falloff');
        Minimap.$bigMinimapModal.find('.modal-content').html(minimapDivHtml);
        Minimap.$minimapCurrentLayerNumDiv.html('F');
        Modals.closeAll();
        Minimap.bigMinimapModal.open();
    };
    WorldPreview.defaultLayerNum = -1;
    WorldPreview.isOnlyShowingFloor = false;
    return WorldPreview;
}());
//# sourceMappingURL=WorldPreview.js.map