/// <reference path="./generated/Items.ts" />
/// <reference path="../Utils.ts" />
/// <reference path="UtilsClient.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="ParticleEffect.ts" />
/// <reference path="Minimap.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="LightCircle.ts" />
/// <reference path="WorldClient.ts" />
var EntityClient = /** @class */ (function () {
    function EntityClient(entityId, entityDetailsArr) {
        this.fireballTickNum = 0;
        this.id = entityId;
        this.type = entityDetailsArr['type'];
        this.isBaby = entityDetailsArr['isBaby'];
        this.isExplosive = Utils.allEntityTypesArr[this.type]['isExplosive'];
        this.fuseLengthNum = Utils.allEntityTypesArr[this.type]['fuseLengthNum'];
        var textureName = (this.isBaby && Utils.allEntityTypesArr[this.type]['babySpriteStr']) ? Utils.allEntityTypesArr[this.type]['babySpriteStr'] : this.type;
        if (this.type == 'slime') {
            this.targetX = entityDetailsArr['x'];
            this.targetY = entityDetailsArr['y'];
        }
        this.layerNum = WorldClient.currentLayerNum;
        this.sprite = Globals.game.physics.add.sprite(entityDetailsArr['x'], entityDetailsArr['y'], textureName);
        if (this.type == 'sheep') {
            this.hasWool = entityDetailsArr['hasWool'];
            if (!this.hasWool) {
                this.sprite.setTexture('sheep_sheared');
            }
        }
        this.sprite.alpha = Utils.allEntityTypesArr[this.type]['alpha'];
        this.sprite.angle = entityDetailsArr['angle'];
        if (this.isBaby && Utils.allEntityTypesArr[this.type]['babySpriteStr']) {
            this.sprite.displayWidth = Utils.allEntityTypesArr[this.type]['babyWidth'];
            this.sprite.displayHeight = Utils.allEntityTypesArr[this.type]['babyHeight'];
        }
        else {
            this.sprite.displayWidth = Utils.allEntityTypesArr[this.type]['width'];
            this.sprite.displayHeight = Utils.allEntityTypesArr[this.type]['height'];
        }
        if (this.isBaby) {
            this.sprite.displayWidth *= 2 / 3;
            this.sprite.displayHeight *= 2 / 3;
        }
        this.sprite.displayWidth *= entityDetailsArr['sizeMult'];
        this.sprite.displayHeight *= entityDetailsArr['sizeMult'];
        this.sprite.depth = UtilsClient.depthsArr['entities'];
        if (Utils.allEntityTypesArr[this.type]['lightCircleRadius']) {
            this.lightCircleObj = new LightCircle(WorldClient.currentLayerNum, -1, -1, Utils.allEntityTypesArr[this.type]['lightCircleRadius'], Utils.allEntityTypesArr[this.type]['lightCircleTint'], this.id);
        }
        this.maxHealth = Utils.allEntityTypesArr[this.type]['health'];
        if (Utils.allEntityTypesArr[this.type]['hasNameTag']) {
            this.nameTag = Globals.game.add.text(entityDetailsArr['x'], entityDetailsArr['y'], Utils.allEntityTypesArr[this.type]['label'], { font: '20px Germania One', fill: '#ffffff' });
            this.nameTag.setOrigin(0.5, -2.75);
        }
        if (this.type == 'archer') {
            this.archerProjectileMaxCharge = Math.round(12 * entityDetailsArr['sizeMult']);
            this.selectedItemSprite = Globals.game.add.sprite(0, 0, '');
            this.selectedItemSprite.setTexture('bow');
            PlayerClient.updateSelectedItemSpritePosition(this.selectedItemSprite, this.sprite, Items.itemsArr['bow']);
        }
        EntityClient.allEntityObjs[this.id] = this;
        if (Utils.isTakingScreenshots) {
            this.sprite.visible = false;
        }
    }
    EntityClient.init = function () {
        EntityClient.allEntitySpritesGroup = Globals.game.add.group();
    };
    EntityClient.prototype.updateClient = function (entityDetailsArr) {
        this.updateHealth(entityDetailsArr);
        if (entityDetailsArr['isRiding']) {
            return;
        }
        this.updateFuseLengthNum(entityDetailsArr);
        if (this.selectedItemSprite) {
            Globals.game.add.tween({
                'targets': this.selectedItemSprite,
                'ease': 'Linear',
                'x': entityDetailsArr['x'],
                'y': entityDetailsArr['y'],
                'angle': this.selectedItemSprite.angle + UtilsClient.getShortestAngle(entityDetailsArr['angle'], this.selectedItemSprite.angle - this.selectedItemSprite.getData('angleOffset')),
                'duration': Utils.allEntityTypesArr[this.type]['tickIntervalMillis'],
            });
        }
        if (this.nameTag) {
            this.updateNameTag(entityDetailsArr);
        }
        this.playProjectileHitEffects(entityDetailsArr);
        this.updatePosition(entityDetailsArr);
        this.playStepSounds(entityDetailsArr);
        if (this.isBaby && !entityDetailsArr['isBaby']) { // Baby grew up.
            this.isBaby = entityDetailsArr['isBaby'];
            this.sprite.setTexture(this.type);
            this.sprite.displayWidth = Utils.allEntityTypesArr[this.type]['width'] * entityDetailsArr['sizeMult'];
            this.sprite.displayHeight = Utils.allEntityTypesArr[this.type]['height'] * entityDetailsArr['sizeMult'];
        }
        if (this.type == 'fireballEntity') {
            if (this.fireballTickNum % 2 == 0) {
                ParticleEffect.play('fireballTrail', entityDetailsArr['x'], entityDetailsArr['y']);
            }
            this.fireballTickNum++;
        }
        if (this.type == 'snowballEntity' && this.projectileHasMoved && !this.hasShownProjectileHitParticles) {
            if (this.fireballTickNum % 2 == 0) {
                ParticleEffect.play('snowballTrail', entityDetailsArr['x'], entityDetailsArr['y']);
            }
            this.fireballTickNum++;
        }
        if (this.type == 'archer') {
            if (Globals.currentPlayerObj && Utils.distance(entityDetailsArr, Globals.currentPlayerObj.sprite) < 6 * Utils.tileSize) {
                if (entityDetailsArr['archerProjectileCharge'] == 0 && this.archerProjectileCharge >= this.archerProjectileMaxCharge) {
                    SoundEffect.play('bow');
                }
                this.archerProjectileCharge = entityDetailsArr['archerProjectileCharge'];
                PlayerClient.updateBowProjectileCharge(this);
            }
        }
        if (this.type == 'sheep' && !this.isBaby) {
            if (this.hasWool != entityDetailsArr['hasWool']) {
                this.hasWool = entityDetailsArr['hasWool'];
                if (!this.hasWool) {
                    ParticleEffect.play('snowballHit', entityDetailsArr['x'], entityDetailsArr['y']);
                }
                this.sprite.setTexture((this.hasWool) ? 'sheep' : 'sheep_sheared');
            }
        }
    };
    EntityClient.prototype.updateHealth = function (entityDetailsArr) {
        if (entityDetailsArr['health'] < this.health) {
            UtilsClient.healthTweenTint(this.sprite);
            this.playSoundEffectKey(Utils.allEntityTypesArr[this.type]['hurtSoundEffectKey']);
            ParticleEffect.showDamageNumber(Math.round(this.health - entityDetailsArr['health']), this.sprite.x, this.sprite.y);
        }
        this.health = entityDetailsArr['health'];
    };
    EntityClient.prototype.updateFuseLengthNum = function (entityDetailsArr) {
        if (this.isExplosive) {
            if (this.type == 'slime') {
                if (this.fuseLengthNum != entityDetailsArr['fuseLengthNum'] && entityDetailsArr['fuseLengthNum'] % 2 == 0) {
                    SoundEffect.play('slime_hiss');
                }
                if (this.fuseLengthNum != entityDetailsArr['fuseLengthNum'] && this.fuseLengthNum % 2) {
                    this.sprite.setTexture('slime_white');
                }
                else if (this.sprite.texture.key != 'slime') {
                    this.sprite.setTexture('slime');
                }
                if (this.fuseLengthNum != entityDetailsArr['fuseLengthNum'] && this.fuseLengthNum == Utils.allEntityTypesArr[this.type]['fuseAutoDecrementLengthNum']) {
                    Globals.game.add.tween({
                        'targets': this.sprite,
                        'scaleX': '+' + (this.sprite.scaleX * 0.3),
                        'scaleY': '+' + (this.sprite.scaleY * 0.3),
                        'duration': Utils.allEntityTypesArr[this.type]['fuseAutoDecrementLengthNum'] * Utils.entityTickIntervalMillis,
                        'ease': 'Bounce.easeOut',
                    });
                    SoundEffect.play('fuse');
                }
            }
            this.fuseLengthNum = entityDetailsArr['fuseLengthNum'];
        }
    };
    EntityClient.prototype.updateNameTag = function (entityDetailsArr) {
        Globals.game.add.tween({
            'targets': this.nameTag,
            'ease': 'Linear',
            'x': entityDetailsArr['x'],
            'y': entityDetailsArr['y'],
            'duration': Utils.allEntityTypesArr[this.type]['tickIntervalMillis'],
        });
        var startColor = Phaser.Display.Color.HexStringToColor('#ffffff');
        var endColor = Phaser.Display.Color.HexStringToColor('#ff8a80'); // red accent-1
        var tweenColor = Phaser.Display.Color.Interpolate.ColorWithColor(startColor, endColor, 100, Math.floor(100 - (entityDetailsArr['health'] / this.maxHealth) * 100));
        this.nameTag.setTint(Phaser.Display.Color.GetColor(tweenColor.r, tweenColor.g, tweenColor.b));
    };
    EntityClient.prototype.playProjectileHitEffects = function (entityDetailsArr) {
        if (Utils.allEntityTypesArr[this.type]['projectileHitParticleEffectKey']
            &&
                this.projectileHasMoved
            &&
                !this.hasShownProjectileHitParticles
            &&
                this.sprite.x == entityDetailsArr['x']
            &&
                this.sprite.y == entityDetailsArr['y']
            &&
                this.layerNum == WorldClient.currentLayerNum) {
            this.hasShownProjectileHitParticles = true;
            ParticleEffect.play(Utils.allEntityTypesArr[this.type]['projectileHitParticleEffectKey'], this.sprite.x, this.sprite.y);
            if (this.type == 'snowballEntity' && Utils.distance(Globals.currentPlayerObj.sprite, entityDetailsArr) <= Utils.tileSize * 7) {
                SoundEffect.play('snowball_hit');
            }
        }
        if (Utils.allEntityTypesArr[this.type]['projectileHitParticleEffectKey']
            &&
                !this.projectileHasMoved
            &&
                this.sprite.x != entityDetailsArr['x']
            &&
                this.sprite.y != entityDetailsArr['y']) {
            this.projectileHasMoved = true;
        }
    };
    EntityClient.prototype.updatePosition = function (entityDetailsArr) {
        // For debugging: disables entity transition on move.
        // this.sprite.x        = entityDetailsArr['x'];
        // this.sprite.y        = entityDetailsArr['y'];
        // this.sprite.angle = entityDetailsArr['angle'] + Utils.allEntityTypesArr[this.type]['angleOffset'];
        var easingType = 'Linear';
        if (this.type == 'slime' || entityDetailsArr['isKnockbackTick']) {
            easingType = 'Cubic.easeOut';
        }
        Globals.game.add.tween({
            'targets': this.sprite,
            'x': entityDetailsArr['x'],
            'y': entityDetailsArr['y'],
            'angle': this.sprite.angle + UtilsClient.getShortestAngle(entityDetailsArr['angle'], this.sprite.angle - Utils.allEntityTypesArr[this.type]['angleOffset']),
            'duration': Utils.allEntityTypesArr[this.type]['tickIntervalMillis'] * (this.type == 'slime' ? 2 : 1),
            'ease': easingType,
        });
        if (this.lightCircleObj) {
            this.lightCircleObj.x = entityDetailsArr['x'];
            this.lightCircleObj.y = entityDetailsArr['y'];
        }
    };
    EntityClient.prototype.playStepSounds = function (entityDetailsArr) {
        if ((this.type == 'slime'
            ||
                this.type == 'axehead')
            &&
                (this.targetX != entityDetailsArr['x']
                    ||
                        this.targetY != entityDetailsArr['y'])
            &&
                Globals.currentPlayerObj
            &&
                Utils.distance(entityDetailsArr, Globals.currentPlayerObj.sprite) < 5 * Utils.tileSize) {
            if (this.type == 'slime') {
                SoundEffect.play('slime_splat');
            }
            else if (this.type == 'axehead' && Utils.distance(entityDetailsArr, { 'x': this.targetX, 'y': this.targetY }) > 20) {
                SoundEffect.play('axehead_step');
            }
            this.targetX = entityDetailsArr['x'];
            this.targetY = entityDetailsArr['y'];
        }
    };
    EntityClient.prototype.playSoundEffectKey = function (soundEffectKey) {
        if (soundEffectKey && Globals.currentPlayerObj && this.layerNum == WorldClient.currentLayerNum) {
            var distance = Utils.distance(this.sprite, Globals.currentPlayerObj.sprite);
            var maxDistance = Utils.tileSize * 10;
            if (distance < maxDistance) {
                var soundEffect = SoundEffect.play(soundEffectKey);
                soundEffect.detune = (this.isBaby) ? 350 : 0;
                var defaultVolume = SoundEffect.effectsArr[soundEffectKey]['volume'];
                var halfMaxDistance = maxDistance / 2;
                if (distance <= halfMaxDistance) {
                    soundEffect.volume = defaultVolume;
                }
                else {
                    var volumeMult = 1 - (distance - halfMaxDistance) / halfMaxDistance;
                    soundEffect.volume = defaultVolume * volumeMult;
                }
            }
        }
    };
    EntityClient.prototype.killClient = function () {
        if (Utils.allEntityTypesArr[this.type]['projectileHitParticleEffectKey'] && this.projectileHasMoved && !this.hasShownProjectileHitParticles) {
            ParticleEffect.play(Utils.allEntityTypesArr[this.type]['projectileHitParticleEffectKey'], this.sprite.x, this.sprite.y);
            if (this.type == 'snowballEntity') {
                SoundEffect.play('snowball_hit');
            }
        }
        if (this.health && this.health < this.maxHealth && this.sprite.visible) {
            if (!Utils.allEntityTypesArr[this.type]['isProjectile']) {
                ParticleEffect.play('swirl_red', this.sprite.x, this.sprite.y);
            }
            this.playSoundEffectKey(Utils.allEntityTypesArr[this.type]['deathSoundEffectKey']);
            ParticleEffect.showDamageNumber(Math.round(this.health), this.sprite.x, this.sprite.y);
        }
        this.sprite.destroy();
        if (this.lightCircleObj) {
            this.lightCircleObj.destroy();
        }
        if (this.selectedItemSprite) {
            this.selectedItemSprite.destroy();
        }
        if (this.nameTag) {
            this.nameTag.destroy();
        }
        delete EntityClient.allEntityObjs[this.id];
    };
    EntityClient.allEntitiesUpdate = function (responseData) {
        // console.log(Object.keys(allEntityObjs).length);
        // console.log(allEntityObjs);
        for (var entityId in responseData['entityDataToEmitArr']) {
            if (!(entityId in EntityClient.allEntityObjs)) {
                console.log("Create entity", entityId);
                new EntityClient(entityId, responseData['entityDataToEmitArr'][entityId]);
            }
            else {
                // console.log("Update entity", entityId);
                EntityClient.allEntityObjs[entityId].updateClient(responseData['entityDataToEmitArr'][entityId]);
            }
        }
        for (var entityId in EntityClient.allEntityObjs) {
            if (!(entityId in responseData['entityDataToEmitArr']) && (responseData['isEntityUpdateInterval'] || Utils.allEntityTypesArr[EntityClient.allEntityObjs[entityId].type]['isProjectile'])) {
                console.log("Kill entity", entityId);
                EntityClient.allEntityObjs[entityId].killClient();
            }
        }
    };
    EntityClient.initSockets = function () {
        Globals.socket.on('allEntitiesUpdate', EntityClient.allEntitiesUpdate);
    };
    EntityClient.allEntityObjs = {};
    return EntityClient;
}());
//# sourceMappingURL=EntityClient.js.map