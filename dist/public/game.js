/// <reference path="generated/Items.ts" />
/// <reference path="UtilsClient.ts" />
/// <reference path="DayNightCycleClient.ts" />
/// <reference path="ChatClient.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="NotificationMsg.ts" />
/// <reference path="Leaderboard.ts" />
/// <reference path="Minimap.ts" />
/// <reference path="HotbarClient.ts" />
/// <reference path="Crafting.ts" />
/// <reference path="LightCircle.ts" />
/// <reference path="ParticleEffect.ts" />
/// <reference path="TimeSpent.ts" />
/// <reference path="StatBar.ts" />
/// <reference path="InventoryModal.ts" />
/// <reference path="EntityClient.ts" />
/// <reference path="Modals.ts" />
var $playerImageChooser;
var $nameInput;
var currentPlayerImage;
var $loginScreen;
var $loginSuccessScreen;
var $loggedInAsUserName;
var $userScore;
var $userNameInput;
var $passwordCodeInput;
var $logoutBtn;
var $deleteAccountBtn;
var $loginBtn;
var $registerBtn;
window.onload = function () {
    UtilsClient.byId('loadingBackground').style.backgroundImage = 'url(/public/assets/backgrounds/' + Utils.randBetween(1, 5) + '.png)';
};
$(function () {
    M.AutoInit();
    // @ts-ignore
    $.widget.bridge('uitooltip', $.ui.tooltip); // https://github.com/Dogfalo/materialize/issues/3653
    // @ts-ignore
    $(document).uitooltip({
        content: function () {
            return $(this).prop('title');
        },
        track: true,
        show: false,
        hide: false,
    });
    Modals.init();
    Utils.playerSpeed = JSON.parse(localStorage.getItem('playerSpeed')) || Utils.playerDefaultSpeed;
    currentPlayerImage = localStorage.getItem('playerImage') || 'manBrown';
    $('#changePlayerImageModal .playerImageBtn[data-id=' + currentPlayerImage + ']').append($('<div id="selectedPlayerImage">✓</div>'));
    $playerImageChooser = $('#playerImageChooser');
    $playerImageChooser.find('img').attr('src', '/public/assets/player/' + currentPlayerImage + '.png');
    $('#changePlayerImageModal .playerImageBtn').on('click', function () {
        $('#selectedPlayerImage').remove();
        $(this).append($('<div id="selectedPlayerImage">✓</div>'));
        $playerImageChooser.find('img').attr('src', $(this).find('img').attr('src'));
        $playerImageChooser.data('id', $(this).data('id'));
        Modals.allModalObjs['changePlayerImage'].close();
        currentPlayerImage = $(this).data('id');
        localStorage.setItem('playerImage', currentPlayerImage);
    });
    $nameInput = $('#nameInput');
    $nameInput.val(JSON.parse(localStorage.getItem('name')));
    $nameInput.on('keyup', function (event) {
        if (event.key === 'Enter') {
            $('#joinGameBtn').trigger('click');
            return false;
        }
    });
    $('#joinGameBtn').on('click', function () {
        $('#nameInputContainer .progress').show();
        if (Globals.isGameCreated && Globals.socket) {
            $nameInput.trigger('focus');
            var joinDetailsArr = {
                'name': $nameInput.val().toString(),
                'image': currentPlayerImage,
                'gameModeStr': PlayerClient.gameModeStr,
                'difficultyStr': PlayerClient.difficultyStr,
            };
            Globals.socket.emit('joinGame', joinDetailsArr, function (response) {
                if ('errorMessage' in response) {
                    $('.helper-text').html('');
                    $nameInput.siblings('.helper-text').html(response['errorMessage']);
                }
                else {
                    WorldClient.updateCurrentLayerNum(response['currentLayerNum']);
                    if (!PlayerClient.allPlayerEntitiesGroup) {
                        PlayerClient.init();
                        EntityClient.init();
                        ChatClient.initElements();
                        InventoryModal.init();
                        Leaderboard.init();
                        Minimap.init();
                        TimeSpent.init();
                        HotbarClient.init();
                        StatBar.init();
                        NotificationMsg.init();
                        LightCircle.init();
                        initSockets();
                        for (var playerId in response['publicPlayerDataArr']) {
                            if (playerId == Globals.socket.id) {
                                Globals.currentPlayerObj = new PlayerClient(response['publicPlayerDataArr'][playerId], response['privatePlayerDataArr']);
                                HotbarClient.update();
                            }
                            else {
                                new PlayerClient(response['publicPlayerDataArr'][playerId]);
                            }
                        }
                    }
                    else {
                        Globals.currentPlayerObj = new PlayerClient(response['publicPlayerDataArr'][Globals.socket.id], response['privatePlayerDataArr']);
                        HotbarClient.update();
                    }
                    WorldClient.biomeCoordsPerLayerArr = response['biomeCoordsPerLayerArr'];
                    WorldClient.worldArr = response['worldArr'];
                    WorldClient.initWorld();
                    WorldClient.floorArr = response['floorArr'];
                    WorldClient.initFloor();
                    Leaderboard.update();
                    localStorage.setItem('name', JSON.stringify($nameInput.val()));
                    DayNightCycleClient.initClient(response['dayNightStage'], response['gameStartMillis']);
                    Modals.allModalObjs['joinGame'].close();
                    $('#loadingBackground').hide();
                    $('#nameInputContainer .progress').hide();
                }
            });
        }
        else {
            setTimeout(function () {
                $('#joinGameBtn').trigger('click');
            }, 200);
        }
    });
    $(document).on('keydown', function (event) {
        if ($(event.target).closest('input').length // An input isn't focused.
            ||
                $(event.target).closest('textarea').length // An textarea isn't focused.
        ) {
            return;
        }
        if (!event.metaKey && !event.ctrlKey && !event.altKey) {
            if (event.key === '?') {
                if ($('#howToPlayModal').css('display') == 'block') {
                    Modals.allModalObjs['howToPlay'].close();
                }
                else {
                    Modals.closeAll();
                    Modals.allModalObjs['howToPlay'].open();
                }
            }
            else if (event.key === 'Escape') {
                PlayerClient.cancelClickRail();
            }
            HotbarClient.keydown(event);
            Minimap.keydown(event);
            InventoryModal.keydown(event);
        }
    });
    $loginScreen = $('#loginScreen');
    $loginSuccessScreen = $('#loginSuccessScreen');
    $loggedInAsUserName = $('#loggedInAsUserName');
    $userScore = $('#userScore');
    $userNameInput = $('#userNameInput');
    $passwordCodeInput = $('#passwordCodeInput');
    $logoutBtn = $('#logoutBtn');
    $deleteAccountBtn = $('#deleteAccountBtn');
    $loginBtn = $('#loginBtn');
    $registerBtn = $('#registerBtn');
    $userNameInput.on('keyup', function (event) {
        if (event.key === 'Enter') {
            $loginBtn.trigger('click');
            return false;
        }
    });
    $passwordCodeInput.on('keyup', function (event) {
        if (event.key === 'Enter') {
            $loginBtn.trigger('click');
            return false;
        }
    });
    $logoutBtn.on('click', function () {
        if (Globals.socket && Globals.socket.id) {
            $.post('logout', {
                'socketId': Globals.socket.id,
            }, function (response) {
                console.log(response);
                $loginScreen.show();
                $loginSuccessScreen.hide();
            });
        }
        else {
            console.log('Socket not loaded');
        }
    });
    $deleteAccountBtn.on('click', function () {
        if (Globals.socket && Globals.socket.id) {
            $.post('deleteAccount', {
                'socketId': Globals.socket.id,
            }, function (response) {
                console.log(response);
                $loginScreen.show();
                $loginSuccessScreen.hide();
            });
        }
        else {
            console.log('Socket not loaded');
        }
    });
    $loginBtn.on('click', function () {
        if (Globals.socket && Globals.socket.id) {
            $.post('login', {
                'userName': $userNameInput.val(),
                'passwordCode': $passwordCodeInput.val(),
                'socketId': Globals.socket.id,
            }, handleLoginResponse);
        }
        else {
            console.log('Socket not loaded');
        }
    });
    $registerBtn.on('click', function () {
        if (Globals.socket && Globals.socket.id) {
            $.post('register', {
                'userName': $userNameInput.val(),
                'passwordCode': $passwordCodeInput.val(),
                'socketId': Globals.socket.id,
            }, handleLoginResponse);
        }
        else {
            console.log('Socket not loaded');
        }
    });
    function handleLoginResponse(response) {
        if (response['status'] == 'success') {
            $('.helper-text').html('');
            $loginScreen.hide();
            $loginSuccessScreen.show();
            $loggedInAsUserName.html(response['userName']);
            $userScore.html(Leaderboard.kFormatter(response['userScore']));
            $userScore.attr('title', response['userScore'].toLocaleString());
            $userNameInput.val('');
            $passwordCodeInput.val('');
        }
        else if ('errorMessage' in response) {
            $('.helper-text').html('');
            $('#' + response['errorMessageInputId'] + 'Input').trigger('focus').siblings('.helper-text').html(response['errorMessage']);
        }
    }
});
new Phaser.Game({
    type: Phaser.AUTO,
    parent: 'hunker',
    width: window.innerWidth,
    height: window.innerHeight,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false,
        },
    },
    scene: {
        preload: function () {
            Globals.game = this;
            Globals.socket = io();
            Globals.socket.on('userState', function (response) {
                if (response['isLoggedIn']) {
                    $loginScreen.hide();
                    $loginSuccessScreen.show();
                    $loggedInAsUserName.html(response['userName']);
                    $userScore.html(Leaderboard.kFormatter(response['userScore']));
                    $userScore.attr('title', response['userScore'].toLocaleString());
                }
                else {
                    $loginScreen.show();
                    $loginSuccessScreen.hide();
                }
            });
            for (var i = 0; i <= 10; i++) {
                Globals.game.load.image('destroyStage' + i, '/public/assets/destroyStages/destroyStage' + i + '.png');
            }
            // @formatter:off
            Globals.game.load.image('spritesheet_tiles', '/public/assets/spritesheet_tiles.png');
            Globals.game.load.image('windmill_top', '/public/assets/leaves/windmill_top.png');
            Globals.game.load.image('leaves_tree', '/public/assets/leaves/tree.png');
            Globals.game.load.image('leaves_tree_birch', '/public/assets/leaves/tree_birch.png');
            Globals.game.load.image('leaves_tree_christmas_1', '/public/assets/leaves/tree_christmas_1.png');
            Globals.game.load.image('leaves_tree_christmas_2', '/public/assets/leaves/tree_christmas_2.png');
            Globals.game.load.image('leaves_tree_christmas_3', '/public/assets/leaves/tree_christmas_3.png');
            Globals.game.load.image('leaves_tree_christmas_4', '/public/assets/leaves/tree_christmas_4.png');
            Globals.game.load.image('craftingIndicator', '/public/assets/ui/crafting_icon.png');
            Globals.game.load.image('pausedIndicator', '/public/assets/ui/pause.png');
            Globals.game.load.image('slime_white', '/public/assets/entity/slime_white.png');
            Globals.game.load.image('sheep_sheared', '/public/assets/entity/sheep_sheared.png');
            Globals.game.load.image('axe_giant_held', '/public/assets/items/axe_giant_held.png');
            Globals.game.load.image('bow1', '/public/assets/items/bow/1.png');
            Globals.game.load.image('bow2', '/public/assets/items/bow/2.png');
            Globals.game.load.image('bow3', '/public/assets/items/bow/3.png');
            Globals.game.load.image('bow4', '/public/assets/items/bow/4.png');
            Globals.game.load.image('bow5', '/public/assets/items/bow/5.png');
            Globals.game.load.image('bow6', '/public/assets/items/bow/6.png');
            Globals.game.load.image('bow7', '/public/assets/items/bow/7.png');
            Globals.game.load.image('bow8', '/public/assets/items/bow/8.png');
            // @formatter:on
            for (var entityType in Utils.allEntityTypesArr) {
                Globals.game.load.image(entityType, '/public/assets/entity/' + entityType + '.png');
                if (Utils.allEntityTypesArr[entityType]['babySpriteStr']) {
                    Globals.game.load.image(Utils.allEntityTypesArr[entityType]['babySpriteStr'], '/public/assets/entity/' + Utils.allEntityTypesArr[entityType]['babySpriteStr'] + '.png');
                }
            }
            for (var playerImage in Utils.allPlayerImagesArr) {
                Globals.game.load.image(playerImage, Utils.allPlayerImagesArr[playerImage]['src']);
            }
            for (var itemId in Items.itemsArr) {
                if (Items.itemsArr[itemId]['isObtainable']) {
                    if (Items.itemsArr[itemId]['itemSrc']) {
                        Globals.game.load.image(itemId, '/public' + Items.itemsArr[itemId]['itemSrc']);
                    }
                    else {
                        console.error("Item \"" + itemId + "\" should have itemSrc set");
                    }
                }
            }
            SoundEffect.preload();
            ParticleEffect.preload();
            Globals.disableInput();
        },
        create: function () {
            Globals.game.cameras.main.setBounds(0, 0, Utils.worldWidth * Utils.tileSize, Utils.worldHeight * Utils.tileSize);
            Globals.game.physics.world.setBounds(0, 0, Utils.worldWidth * Utils.tileSize, Utils.worldHeight * Utils.tileSize);
            SoundEffect.create();
            ParticleEffect.create();
            document.querySelector('canvas').addEventListener('click', function () {
                if (document.activeElement) {
                    document.activeElement.blur();
                }
                this.focus();
                Globals.enableInput();
            });
            Globals.game.cameras.main.setDeadzone(window.innerWidth * 0.35, window.innerHeight * 0.35);
            window.addEventListener('resize', function () {
                Globals.game.scale.resize(window.innerWidth, window.innerHeight);
                Globals.game.cameras.main.setDeadzone(window.innerWidth * 0.35, window.innerHeight * 0.35);
                LightCircle.shadowTexture.setSize(window.innerWidth, window.innerHeight);
            }, false);
            Globals.game.sound.pauseOnBlur = false;
            Globals.isGameCreated = true;
        },
        update: function (time, delta) {
            if (Globals.currentPlayerObj) {
                Globals.delta = delta;
                Globals.currentPlayerObj.update();
            }
            if (LightCircle.shadowTexture) {
                LightCircle.updateShadowTexture();
            }
        },
    }
});
function initSockets() {
    Globals.socket.on('playerJoined', function (response) {
        console.log("Player \"" + response['playerInfo']['name'] + "\" joined.");
        var playerObj = new PlayerClient(response['playerInfo']);
        ChatClient.addMessage({
            'playerId': playerObj.id,
            'text': "joined",
        });
        Leaderboard.update();
    });
    Globals.socket.on('playerDied', function (playerDeathDetailsArr) {
        if (playerDeathDetailsArr['playerId'] in PlayerClient.allPlayerObjs && playerDeathDetailsArr['playerId'] != Globals.socket.id) {
            console.log("Player \"" + PlayerClient.allPlayerObjs[playerDeathDetailsArr['playerId']].name + "\" died.");
            PlayerClient.allPlayerObjs[playerDeathDetailsArr['playerId']].killClient(playerDeathDetailsArr);
        }
    });
    Globals.socket.on('youDied', function (playerDeathDetailsArr) {
        if (playerDeathDetailsArr['playerId'] in PlayerClient.allPlayerObjs) {
            console.log("Player \"" + PlayerClient.allPlayerObjs[playerDeathDetailsArr['playerId']].name + "\" (you) died.");
            PlayerClient.killCurrentPlayer(playerDeathDetailsArr);
        }
    });
    Globals.socket.on('playerMovement', function (playerId, movementData) {
        if (playerId in PlayerClient.allPlayerObjs) {
            PlayerClient.allPlayerObjs[playerId].updatePos(movementData);
        }
    });
    Globals.socket.on('playerCurrentLayerNum', function (playerId, currentLayerNum) {
        if (playerId in PlayerClient.allPlayerObjs) {
            PlayerClient.allPlayerObjs[playerId].updateCurrentLayerNum(currentLayerNum);
        }
    });
    Globals.socket.on('selectItem', function (playerId, itemId) {
        if (playerId in PlayerClient.allPlayerObjs) {
            PlayerClient.allPlayerObjs[playerId].selectItem(itemId);
        }
    });
    Globals.socket.on('isCrafting', function (playerId, craftingDataArr) {
        if (playerId in PlayerClient.allPlayerObjs) {
            PlayerClient.allPlayerObjs[playerId].isCrafting = craftingDataArr['isCrafting'];
            PlayerClient.allPlayerObjs[playerId].craftingItemId = craftingDataArr['craftingItemId'];
            PlayerClient.allPlayerObjs[playerId].updateIndicatorSprite();
        }
    });
    Globals.socket.on('isPaused', function (playerId, isPaused) {
        if (playerId in PlayerClient.allPlayerObjs) {
            PlayerClient.allPlayerObjs[playerId].isPaused = isPaused;
            PlayerClient.allPlayerObjs[playerId].updateIndicatorSprite();
        }
    });
    Globals.socket.on('playerMessage', function (messageData) {
        ChatClient.addMessage(messageData);
    });
    Globals.socket.on('updateStatBars', UtilsClient.handleSocketEmitResponse);
    Globals.socket.on('updatePlayerScoresArr', function (updatePlayerScoresArr) {
        Leaderboard.updatePlayerScoresArr(updatePlayerScoresArr);
    });
    Globals.socket.on('explosion', UtilsClient.handleSocketEmitResponse);
    Globals.socket.on('handleMiscEmit', UtilsClient.handleSocketEmitResponse);
    Globals.socket.on('blockUpdate', function (blockUpdateDataArr) {
        WorldClient.blockUpdate(blockUpdateDataArr);
    });
    Globals.socket.on('floorUpdate', function (floorUpdateDataArr) {
        WorldClient.floorUpdate(floorUpdateDataArr);
    });
    Globals.socket.on('disconnect', function () {
        Modals.allModalObjs['disconnected'].open();
    });
    EntityClient.initSockets();
    DayNightCycleClient.initSockets();
    Crafting.initSockets();
}
document.addEventListener('contextmenu', function (event) { return event.preventDefault(); }); // Disable right click menu.
//# sourceMappingURL=game.js.map