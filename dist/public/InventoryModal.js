/// <reference path="./generated/Items.ts" />
/// <reference path="../Utils.ts" />
/// <reference path="UtilsClient.ts" />
/// <reference path="Crafting.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="HotbarClient.ts" />
/// <reference path="Modals.ts" />
var InventoryModal = /** @class */ (function () {
    function InventoryModal() {
    }
    InventoryModal.init = function () {
        $(document).on('mouseup', function (event) {
            if (event.which == 3) {
                clearInterval(InventoryModal.multiCraftInterval);
            }
        });
        $(document).on('mousedown', '.itemRow[data-item-row-type="crafting"] .itemSlot:not(.disabled) .item', function (event) {
            if (event.which == 1) {
                Crafting.startCraftingItem($(this));
            }
            else if (event.which == 3) {
                var $item_1 = $(this);
                Crafting.startCraftingItem($item_1);
                InventoryModal.multiCraftInterval = setInterval(function () {
                    Crafting.startCraftingItem($item_1);
                }, 100);
            }
        });
    };
    InventoryModal.initInventoryModal = function () {
        Modals.allModalElements['inventory'] = UtilsClient.byId('inventoryModal');
        M.Modal.init(Modals.allModalElements['inventory'], {
            'inDuration': 0,
            'outDuration': 0,
            'onOpenStart': function () {
                Globals.disableInput();
            },
            'onCloseStart': function () {
                Globals.enableInput();
                if (InventoryModal.topRowType == 'container'
                    &&
                        Globals.currentPlayerObj.markerTileId
                    &&
                        Globals.currentPlayerObj.markerTileId in Items.itemsArr
                    &&
                        Items.itemsArr[Globals.currentPlayerObj.markerTileId]['isContainer']) {
                    SoundEffect.play('chest_close');
                }
            },
        });
        Modals.allModalObjs['inventory'] = M.Modal.getInstance(Modals.allModalElements['inventory']);
    };
    InventoryModal.open = function (topRowType) {
        if (topRowType == 'container' && Items.itemsArr[Globals.currentPlayerObj.markerTileId]['isContainer']) {
            SoundEffect.play('chest_open');
        }
        InventoryModal.update(topRowType);
        Modals.closeAll();
        Modals.allModalObjs['inventory'].open();
    };
    InventoryModal.close = function () {
        InventoryModal.topRowType = null;
        InventoryModal.containerArr = null;
        Modals.allModalObjs['inventory'].close();
    };
    InventoryModal.update = function (topRowType) {
        InventoryModal.containerArr = null;
        InventoryModal.topRowType = topRowType || 'crafting';
        var out = '';
        out += "<div class=\"itemRow\" data-item-row-type=\"" + InventoryModal.topRowType + "\">";
        if (InventoryModal.topRowType == 'container') {
            var emptyContainerArr = [];
            for (var i = 0; i < (Items.itemsArr[Globals.currentPlayerObj.markerTileId]['maxItemsInContainerNum'] || 1); i++) {
                emptyContainerArr.push({ 'itemId': null, 'itemCount': null });
            }
            out += HotbarClient.getItemRowHtml(emptyContainerArr);
            InventoryModal.updateInventoryContainer();
        }
        else {
            out += InventoryModal.getCraftingItemsHtml();
        }
        if (InventoryModal.topRowType == 'crafting') {
            out += "<div class=\"itemRowDividers\">";
            out += "<hr>";
            out += "<a class=\"expandCollapseCraftingButton\" id=\"collapseCraftingButton\" onclick=\"$(this).hide(); $('#expandCraftingButton').show(); $('.itemRow[data-item-row-type=crafting]').removeClass('expandedCrafting');\" style=\"display: none;\"><img src=\"../../public/assets/ui/arrow_up.png\"></a>";
            out += "</div>";
        }
        out += "</div>";
        if (InventoryModal.topRowType == 'crafting') {
            out += "<div class=\"itemRowDividers\">";
            out += "<hr>";
            out += "<a class=\"expandCollapseCraftingButton\" id=\"expandCraftingButton\" onclick=\"$(this).hide(); $('#collapseCraftingButton').show(); $('.itemRow[data-item-row-type=crafting]').addClass('expandedCrafting');\"><img src=\"../../public/assets/ui/arrow_down.png\"></a>";
            out += "</div>";
        }
        out += "<div class=\"itemRow\" data-item-row-type=\"accessories\">";
        var accessoryItemsArr = Globals.currentPlayerObj.accessoryItemsArr;
        out += HotbarClient.getItemRowHtml(accessoryItemsArr);
        out += "</div>";
        out += "<hr>";
        out += "<div class=\"itemRow\" data-item-row-type=\"inventory\">";
        out += HotbarClient.getItemRowHtml(Globals.currentPlayerObj.hotbarItemsArr.slice(9));
        out += "</div>";
        out += "<hr>";
        out += "<div class=\"itemRow\" data-item-row-type=\"hotbar\">";
        out += HotbarClient.getItemRowHtml(Globals.currentPlayerObj.hotbarItemsArr.slice(0, 9));
        out += "</div>";
        var craftingScrollTop;
        if (InventoryModal.topRowType == 'crafting') {
            // noinspection JSJQueryEfficiency
            craftingScrollTop = $('#inventoryModal .itemRow[data-item-row-type="crafting"]').scrollTop();
        }
        $('#inventoryModal .modal-content').html(out);
        if (InventoryModal.topRowType == 'crafting') {
            // Scroll crafting menu.
            // noinspection JSJQueryEfficiency
            $('#inventoryModal .itemRow[data-item-row-type="crafting"]').bind('mousewheel DOMMouseScroll', function (event) {
                event.stopPropagation();
            }).scrollTop(craftingScrollTop);
        }
        HotbarClient.updateDraggable('inventoryModal');
    };
    InventoryModal.updateInventoryContainer = function () {
        Globals.socket.emit('openContainer', {
            'layerNum': Globals.currentPlayerObj.currentLayerNum,
            'x': Globals.currentPlayerObj.markerTileX,
            'y': Globals.currentPlayerObj.markerTileY,
        }, function (response) {
            UtilsClient.handleSocketEmitResponse(response, null, function () {
                Modals.allModalObjs['inventory'].close();
            });
        });
    };
    InventoryModal.getCraftingItemsHtml = function () {
        if (Utils.playerGameModesArr[PlayerClient.gameModeStr]['hasInfiniteBlocks']) {
            return InventoryModal.getAllItemsHtml();
        }
        var out = '';
        for (var itemId in Items.itemsArr) {
            var itemRow = Items.itemsArr[itemId];
            if (itemRow['requiredItemsForCraftingArr'] != null
                &&
                    (!itemRow['requiredBlockForCraftingItemId']
                        ||
                            itemRow['requiredBlockForCraftingItemId'] == Globals.currentPlayerObj.markerTileId)) {
                out += HotbarClient.getItemHtml(itemId, Items.itemsArr[itemId]['craftedItemsNum'], null, null, false, !Utils.hasRequiredItemsForCrafting(Globals.currentPlayerObj.hotbarItemsArr, Items.itemsArr[itemId]['requiredItemsForCraftingArr']));
            }
        }
        return out;
    };
    InventoryModal.getAllItemsHtml = function () {
        var out = '';
        for (var itemId in Items.itemsArr) {
            var itemRow = Items.itemsArr[itemId];
            if (itemRow['isObtainable']) {
                out += HotbarClient.getItemHtml(itemId, 1, null, null, false);
            }
        }
        return out;
    };
    InventoryModal.keydown = function (event) {
        if (event.key === 'e') {
            InventoryModal.toggleInventoryModal();
            return false;
        }
    };
    InventoryModal.toggleInventoryModal = function () {
        if (Modals.allModalElements['inventory'].style.display == 'block') {
            InventoryModal.close();
        }
        else {
            Modals.closeAll();
            InventoryModal.open();
        }
    };
    return InventoryModal;
}());
//# sourceMappingURL=InventoryModal.js.map