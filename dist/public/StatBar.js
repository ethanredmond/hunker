var StatBar = /** @class */ (function () {
    function StatBar(barName, barValue, barMax, isShowing) {
        this.barName = barName;
        this.barMax = barMax;
        this.barValue = barValue;
        this.$innerBar = $('#' + barName + 'Bar .innerBar');
        if (!this.$innerBar.length) {
            var $statBar = $("<div id=\"" + barName + "Bar\" class=\"statBar\"></div>").appendTo(StatBar.$statBarContainer);
            this.$innerBar = $("<div class=\"innerBar\"></div>").appendTo($statBar);
        }
        this.updateIsShowing(isShowing);
        this.update(barValue);
    }
    StatBar.init = function () {
        StatBar.$statBarContainer = $("<div class=\"statBarContainer\"></div>").appendTo('body');
    };
    StatBar.prototype.update = function (barValue) {
        this.barValue = barValue;
        this.$innerBar.css('width', Math.min(Math.round((this.barValue / this.barMax) * 100), 100) + '%');
        this.$innerBar.toggleClass('flashStatBar', (this.barValue <= 10));
    };
    StatBar.prototype.updateMax = function (barMax) {
        this.barMax = barMax;
        this.update(this.barValue);
    };
    StatBar.prototype.updateIsShowing = function (isShowing) {
        $("#" + this.barName + "Bar").toggleClass('hideStatBar', !isShowing);
    };
    return StatBar;
}());
//# sourceMappingURL=StatBar.js.map