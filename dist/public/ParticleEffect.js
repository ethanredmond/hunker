/// <reference path="./generated/Items.ts" />
/// <reference path="../Utils.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="PlayerClient.ts" />
var ParticleEffect = /** @class */ (function () {
    function ParticleEffect() {
    }
    ParticleEffect.preload = function () {
        for (var particleEffectKey in ParticleEffect.effectsArr) {
            for (var spriteKey in ParticleEffect.effectsArr[particleEffectKey]['spritesArr']) {
                Globals.game.load.image(spriteKey, ParticleEffect.effectsArr[particleEffectKey]['spritesArr'][spriteKey]);
            }
        }
        Globals.game.load.spritesheet('explosion', '/public/assets/particles/explosion.png', { 'frameWidth': 126, 'frameHeight': 126 });
    };
    ParticleEffect.create = function () {
        ParticleEffect.explosionsGroup = Globals.game.add.group();
        ParticleEffect.damageNumbersGroup = Globals.game.add.group();
        Globals.game.anims.create({
            'key': 'explosion',
            'frames': Globals.game.anims.generateFrameNumbers('explosion', { 'start': 0, 'end': 4 }),
            'frameRate': 10,
        });
    };
    ParticleEffect.play = function (particleEffectKey, x, y) {
        if (x === void 0) { x = (Globals.currentPlayerObj.markerTileX * Utils.tileSize) + (Utils.tileSize / 2); }
        if (y === void 0) { y = (Globals.currentPlayerObj.markerTileY * Utils.tileSize) + (Utils.tileSize / 2); }
        if (particleEffectKey in ParticleEffect.effectsArr) {
            var lifespanMillis = ParticleEffect.effectsArr[particleEffectKey]['lifespanMillis'];
            if (!(particleEffectKey in ParticleEffect.allEmitterObjsArr)) {
                var particles = Globals.game.add.particles(Object.keys(ParticleEffect.effectsArr[particleEffectKey]['spritesArr'])[0]);
                var widthHeight = ParticleEffect.effectsArr[particleEffectKey]['emitterWidthHeightPx'];
                var emitterConfig = {
                    'alpha': { 'start': 1, 'end': 0 },
                    'rotate': { 'min': ParticleEffect.effectsArr[particleEffectKey]['rotationMin'], 'max': ParticleEffect.effectsArr[particleEffectKey]['rotationMax'] },
                    'speed': 0,
                    'lifespan': lifespanMillis,
                    'on': false,
                    'quantity': ParticleEffect.effectsArr[particleEffectKey]['numParticles'],
                    'emitZone': { 'type': 'random', 'source': new Phaser.Geom.Rectangle(-widthHeight / 2, -widthHeight / 2, widthHeight, widthHeight) },
                };
                if (ParticleEffect.effectsArr[particleEffectKey]['is2px']) {
                    emitterConfig['scale'] = { 'start': 8, 'end': 0 };
                    if (ParticleEffect.effectsArr[particleEffectKey]['tint']) {
                        emitterConfig['tint'] = ParticleEffect.effectsArr[particleEffectKey]['tint'];
                    }
                }
                else {
                    emitterConfig['scale'] = { 'start': 1, 'end': 0 };
                }
                ParticleEffect.allEmitterObjsArr[particleEffectKey] = particles.createEmitter(emitterConfig);
                if (particleEffectKey == 'boatTrail' || particleEffectKey == 'boatPaddle') {
                    particles.depth = UtilsClient.depthsArr['boatTrail'];
                }
                else {
                    particles.depth = UtilsClient.depthsArr['particles'];
                }
            }
            // Particles fly away from player.
            // const point = game.physics.arcade.velocityFromAngle(Utils.toDegrees(game.physics.arcade.angleToPointer(Globals.currentPlayerObj.sprite)), -50);
            // if (point.x > 0) {
            //     ParticleEffect.allEmitterObjsArr[particleEffectKey].setXSpeed(0, point.x);
            // } else {
            //     ParticleEffect.allEmitterObjsArr[particleEffectKey].setXSpeed(point.x, 0);
            // }
            // if (point.y > 0) {
            //     ParticleEffect.allEmitterObjsArr[particleEffectKey].setYSpeed(0, point.y);
            // } else {
            //     ParticleEffect.allEmitterObjsArr[particleEffectKey].setYSpeed(point.y, 0);
            // }
            ParticleEffect.allEmitterObjsArr[particleEffectKey].emitParticleAt(x, y);
        }
        else {
            console.error("ParticleEffect key \"" + particleEffectKey + "\" is invalid");
        }
    };
    ParticleEffect.showDamageNumber = function (damageNumber, x, y) {
        var damageNumberText = ParticleEffect.damageNumbersGroup.getFirstDead();
        if (damageNumberText) {
            damageNumberText.setActive(true).setVisible(true);
            damageNumberText.text = damageNumber.toString();
            damageNumberText.x = x;
            damageNumberText.y = y;
        }
        else {
            damageNumberText = Globals.game.add.text(x, y, damageNumber.toString(), { font: '30px Germania One', fill: '#f44336' });
            ParticleEffect.damageNumbersGroup.add(damageNumberText);
            damageNumberText.setOrigin(0.5, 0.5);
            damageNumberText.depth = UtilsClient.depthsArr['particles'];
        }
        damageNumberText.x += Utils.randBetween(-10, 10);
        damageNumberText.y += Utils.randBetween(-10, 10);
        Globals.game.add.tween({
            'targets': damageNumberText,
            'scaleX': 0,
            'scaleY': 0,
            'alpha': 0,
            'x': (Utils.randBool() ? '-=' : '+=') + Utils.randBetween(0, 10),
            'y': '+=10',
            'duration': 300,
            'ease': 'Linear',
            'onComplete': function () {
                damageNumberText.setActive(false).setVisible(false);
                damageNumberText.scaleX = 1;
                damageNumberText.scaleY = 1;
                damageNumberText.alpha = 1;
            },
        });
    };
    ParticleEffect.playExplosion = function (explosionX, explosionY, explosionPower) {
        var explosion = ParticleEffect.explosionsGroup.getFirstDead();
        if (explosion) {
            explosion.setActive(true).setVisible(true);
            explosion.x = explosionX;
            explosion.y = explosionY;
        }
        else {
            explosion = Globals.game.add.sprite(explosionX, explosionY, 'explosion');
            ParticleEffect.explosionsGroup.add(explosion);
            explosion.setOrigin(0.5, 0.5);
            explosion.tint = 0x999999;
            explosion.depth = UtilsClient.depthsArr['particles'];
        }
        explosion.anims.play('explosion');
        explosion.on('animationcomplete', function () {
            explosion.setActive(false).setVisible(false);
        });
        explosion.displayWidth = 126 * explosionPower / 2;
        explosion.displayHeight = 126 * explosionPower / 2;
    };
    ParticleEffect.init = function () {
        for (var itemId in Items.itemsArr) {
            var hexCode = Items.itemsArr[itemId]['hexCode'];
            if (hexCode
                &&
                    Items.itemsArr[itemId]['hasParticleEmitter']
                &&
                    !(hexCode in ParticleEffect.effectsArr)) {
                var tint = (Items.itemsArr[itemId]['isDarkeningParticles']) ? 0xcccccc : null;
                ParticleEffect.effectsArr[hexCode] = { 'numParticles': 5, 'numParticlesToCreate': 15, 'lifespanMillis': 500, 'rotationMin': -ParticleEffect.rotationMinMax, 'rotationMax': ParticleEffect.rotationMinMax, 'emitterWidthHeightPx': Utils.tileSize, 'is2px': true, 'tint': tint, 'spritesArr': {} };
                ParticleEffect.effectsArr[hexCode]['spritesArr'][hexCode] = '/public/assets/particles/2px/' + hexCode + '.png';
            }
        }
    };
    ParticleEffect.rotationMinMax = 250;
    ParticleEffect.effectsArr = {
        // @formatter:off
        'hearts': { 'numParticles': 10, 'numParticlesToCreate': 10, 'lifespanMillis': 500, 'rotationMin': -ParticleEffect.rotationMinMax, 'rotationMax': ParticleEffect.rotationMinMax, 'emitterWidthHeightPx': Utils.tileSize, 'is2px': false, 'tint': null, 'spritesArr': { 'heart': '/public/assets/particles/emote_heart.png', 'hearts': '/public/assets/particles/emote_hearts.png' } },
        'fireballTrail': { 'numParticles': 1, 'numParticlesToCreate': 20, 'lifespanMillis': 250, 'rotationMin': 0, 'rotationMax': 0, 'emitterWidthHeightPx': 10, 'is2px': false, 'tint': null, 'spritesArr': { 'fireballTrail': '/public/assets/particles/fireballTrail.png' } },
        'snowballTrail': { 'numParticles': 1, 'numParticlesToCreate': 20, 'lifespanMillis': 150, 'rotationMin': 0, 'rotationMax': 0, 'emitterWidthHeightPx': Utils.tileSize / 2, 'is2px': true, 'tint': 0xeeeeee, 'spritesArr': { 'snowballTrail': '/public/assets/particles/2px/ffffff.png' } },
        'snowballHit': { 'numParticles': 5, 'numParticlesToCreate': 15, 'lifespanMillis': 500, 'rotationMin': -ParticleEffect.rotationMinMax, 'rotationMax': ParticleEffect.rotationMinMax, 'emitterWidthHeightPx': Utils.tileSize, 'is2px': true, 'tint': 0xeeeeee, 'spritesArr': { 'snowballHit': '/public/assets/particles/2px/ffffff.png' } },
        'boatTrail': { 'numParticles': 1, 'numParticlesToCreate': 15, 'lifespanMillis': 1000, 'rotationMin': 0, 'rotationMax': 0, 'emitterWidthHeightPx': Utils.tileSize, 'is2px': true, 'tint': 0xe7f9ff, 'spritesArr': { 'boatTrail': '/public/assets/particles/2px/ffffff.png' } },
        'boatPaddle': { 'numParticles': 1, 'numParticlesToCreate': 15, 'lifespanMillis': 1000, 'rotationMin': 0, 'rotationMax': 0, 'emitterWidthHeightPx': Utils.tileSize * 1.5, 'is2px': true, 'tint': 0xe7f9ff, 'spritesArr': { 'boatPaddle': '/public/assets/particles/2px/ffffff.png' } },
        'eggHit': { 'numParticles': 5, 'numParticlesToCreate': 15, 'lifespanMillis': 500, 'rotationMin': -ParticleEffect.rotationMinMax, 'rotationMax': ParticleEffect.rotationMinMax, 'emitterWidthHeightPx': Utils.tileSize, 'is2px': true, 'tint': null, 'spritesArr': { 'eggHit': '/public/assets/particles/2px/dfce9b.png' } },
        'swirl_red': { 'numParticles': 5, 'numParticlesToCreate': 15, 'lifespanMillis': 750, 'rotationMin': ParticleEffect.rotationMinMax, 'rotationMax': ParticleEffect.rotationMinMax, 'emitterWidthHeightPx': Utils.tileSize, 'is2px': false, 'tint': 0xdddddd, 'spritesArr': { 'swirl_red': '/public/assets/particles/swirl_red.png', 'swirl_orange': '/public/assets/particles/swirl_orange.png' } },
    };
    ParticleEffect.allEmitterObjsArr = {};
    return ParticleEffect;
}());
ParticleEffect.init();
//# sourceMappingURL=ParticleEffect.js.map