/// <reference path="PlayerClient.ts" />
var Leaderboard = /** @class */ (function () {
    function Leaderboard() {
    }
    Leaderboard.init = function () {
        Leaderboard.$leaderboardContainer = $("<div id=\"leaderboardContainer\"></div>").appendTo('body');
        Leaderboard.$leaderboardTable = $("<table id=\"leaderboardTable\"></table>").appendTo(Leaderboard.$leaderboardContainer);
    };
    Leaderboard.updatePlayerScoresArr = function (updatePlayerScoresArr) {
        for (var playerId in updatePlayerScoresArr) {
            if (playerId in PlayerClient.allPlayerObjs) {
                PlayerClient.allPlayerObjs[playerId].playerScore = updatePlayerScoresArr[playerId];
            }
        }
        Leaderboard.update();
    };
    Leaderboard.update = function () {
        var leaderboardHtml = '';
        if (PlayerClient.allPlayerObjs) {
            // Sort the connected players by score.
            var sortedPlayersArr = [];
            for (var playerId in PlayerClient.allPlayerObjs) {
                sortedPlayersArr.push({ 'playerId': playerId, 'playerScore': PlayerClient.allPlayerObjs[playerId].playerScore });
            }
            sortedPlayersArr.sort(function (a, b) {
                if (a['playerScore'] < b['playerScore']) {
                    return 1;
                }
                else if (a['playerScore'] > b['playerScore']) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
            var i = 1;
            for (var index in sortedPlayersArr) {
                leaderboardHtml += "<tr><td>" + i + ".</td><td>" + PlayerClient.getNameTag(sortedPlayersArr[index]['playerId'], true) + "</td><td>" + Leaderboard.kFormatter(sortedPlayersArr[index]['playerScore']) + "</td></tr>";
                i++;
            }
        }
        Leaderboard.$leaderboardContainer.css('display', ((leaderboardHtml.length > 0) ? 'block' : 'none'));
        Leaderboard.$leaderboardTable.html(leaderboardHtml);
    };
    Leaderboard.kFormatter = function (num) {
        if (num > 999) {
            return (num / 1000).toFixed(1) + 'k';
        }
        else {
            return num.toString();
        }
    };
    return Leaderboard;
}());
//# sourceMappingURL=Leaderboard.js.map