/// <reference path="generated/Items.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="HotbarClient.ts" />
/// <reference path="NotificationMsg.ts" />
/// <reference path="SoundEffect.ts" />
/// <reference path="ParticleEffect.ts" />
/// <reference path="Leaderboard.ts" />
/// <reference path="InventoryModal.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="EntityClient.ts" />
/// <reference path="Modals.ts" />
/// <reference path="../Utils.ts" />
var UtilsClient = /** @class */ (function () {
    function UtilsClient() {
    }
    UtilsClient.byId = function (id) {
        return document.getElementById(id);
    };
    /**
     * See also `generateItems.php` `getTileIndexFromCoord`.
     */
    UtilsClient.getTileIndexFromCoord = function (tileX, tileY) {
        return (UtilsClient.xTiles * (tileY - 1)) + (tileX - 1);
    };
    UtilsClient.updateHotbarAndAccessories = function (responseData) {
        if (Globals.currentPlayerObj && (typeof responseData['accessoryItemsArr'] !== 'undefined' || typeof responseData['hotbarItemsArr'] !== 'undefined')) {
            if (typeof responseData['hotbarItemsArr'] !== 'undefined') {
                Globals.currentPlayerObj.hotbarItemsArr = responseData['hotbarItemsArr'];
            }
            if (typeof responseData['accessoryItemsArr'] !== 'undefined') {
                Globals.currentPlayerObj.accessoryItemsArr = responseData['accessoryItemsArr'];
                Globals.currentPlayerObj.updateLightCircle();
            }
            HotbarClient.update();
        }
    };
    UtilsClient.handleSocketEmitResponse = function (responseData, successFunc, errorFunc) {
        if (typeof responseData['status'] === 'undefined' || responseData['status'] === 'error') {
            // console.log("Response: " + JSON.stringify(responseData));
            NotificationMsg.show(responseData['errorMessage']);
            if (errorFunc) {
                errorFunc(responseData);
            }
        }
        else {
            if (successFunc) {
                successFunc(responseData);
            }
        }
        UtilsClient.updateHotbarAndAccessories(responseData);
        var $containerItemRow = $('.itemRow[data-item-row-type="container"]');
        if (Globals.currentPlayerObj && typeof responseData['containerArr'] !== 'undefined' && $containerItemRow.length && $containerItemRow.is(':visible')) {
            InventoryModal.containerArr = responseData['containerArr'];
            var containerItemRowHtml = '';
            if (Globals.currentPlayerObj.markerTileId == 'enchanting_table') {
                $containerItemRow.attr('data-is-enchanting-table', '1');
                containerItemRowHtml += "<div class=\"enchantingTableButtonSlot\">";
                containerItemRowHtml += " <div class=\"item btn-flat enchantingTableButton\" title=\"Enchant\" onclick=\"Globals.currentPlayerObj.enchantingTableButtonClick();\"></div>";
                containerItemRowHtml += "</div>";
            }
            else {
                $containerItemRow.removeAttr('data-is-enchanting-table');
            }
            containerItemRowHtml += HotbarClient.getItemRowHtml(responseData['containerArr'], false, !Items.itemsArr[Globals.currentPlayerObj.markerTileId]['canRemoveItems'], (Globals.currentPlayerObj.markerTileId == 'hearth' || Globals.currentPlayerObj.markerTileId == 'hearth_lit'), (Globals.currentPlayerObj.markerTileId == 'grindstone'));
            $containerItemRow.html(containerItemRowHtml);
            HotbarClient.updateDraggable('inventoryModal');
        }
        if (Globals.currentPlayerObj && typeof responseData['statBarsDataArr'] !== 'undefined') {
            Globals.currentPlayerObj.updateStatBars(responseData['statBarsDataArr']);
        }
        if (typeof responseData['blockUpdateDataArr'] !== 'undefined') {
            WorldClient.blockUpdate(responseData['blockUpdateDataArr']);
        }
        if (typeof responseData['blockUpdatesDataArr'] !== 'undefined') {
            for (var i in responseData['blockUpdatesDataArr']) {
                WorldClient.blockUpdate(responseData['blockUpdatesDataArr'][i]);
            }
        }
        if (typeof responseData['floorUpdateDataArr'] !== 'undefined') {
            WorldClient.floorUpdate(responseData['floorUpdateDataArr']);
        }
        if (typeof responseData['floorUpdatesDataArr'] !== 'undefined') {
            for (var i in responseData['floorUpdatesDataArr']) {
                WorldClient.floorUpdate(responseData['floorUpdatesDataArr'][i]);
            }
        }
        if (typeof responseData['updatePlayerScoresArr'] !== 'undefined') {
            Leaderboard.updatePlayerScoresArr(responseData['updatePlayerScoresArr']);
        }
        if (Globals.currentPlayerObj && typeof responseData['ridingEntityId'] !== 'undefined') {
            if (responseData['ridingEntityId']) {
                Globals.currentPlayerObj.playerRideEntity(responseData['ridingEntityId']);
            }
            else {
                Globals.currentPlayerObj.playerDismountEntity();
            }
        }
        if (Globals.currentPlayerObj && typeof responseData['playerScore'] !== 'undefined') {
            Globals.currentPlayerObj.playerScore = responseData['playerScore'];
            Leaderboard.update();
        }
        if (typeof responseData['isPaused'] !== 'undefined') {
            if (Globals.currentPlayerObj) {
                Globals.currentPlayerObj.isPaused = responseData['isPaused'];
            }
            if (responseData['isPaused']) {
                Modals.closeAll();
                Modals.allModalObjs['gamePaused'].open();
            }
            else {
                Modals.allModalObjs['gamePaused'].close();
            }
        }
        if (typeof responseData['isGhost'] !== 'undefined') {
            if (Globals.currentPlayerObj) {
                Globals.currentPlayerObj.updateIsGhost(responseData['isGhost']);
            }
        }
        if (typeof responseData['allSoundEffectKeysArr'] !== 'undefined') {
            for (var i in responseData['allSoundEffectKeysArr']) {
                SoundEffect.play(responseData['allSoundEffectKeysArr'][i]);
            }
        }
        if (typeof responseData['allParticleEffectKeysArr'] !== 'undefined') {
            for (var i in responseData['allParticleEffectKeysArr']) {
                ParticleEffect.play(responseData['allParticleEffectKeysArr'][i]);
            }
        }
        UtilsClient.handleExplosionResponse(responseData);
        if (Globals.currentPlayerObj && typeof responseData['attackCooldownMillis'] !== 'undefined') {
            UtilsClient.showAttackCooldown(responseData['attackCooldownMillis']);
        }
    };
    UtilsClient.handleExplosionResponse = function (responseData) {
        if (typeof responseData['explosionCoordArr'] !== 'undefined'
            &&
                responseData['explosionCoordArr']['layerNum'] == WorldClient.currentLayerNum) {
            var explosionX = (responseData['explosionCoordArr']['x'] * Utils.tileSize) + (Utils.tileSize / 2);
            var explosionY = (responseData['explosionCoordArr']['y'] * Utils.tileSize) + (Utils.tileSize / 2);
            if (Utils.distance(Globals.currentPlayerObj.sprite, { 'x': explosionX, 'y': explosionY }) <= 15 * Utils.tileSize) {
                ParticleEffect.playExplosion(explosionX, explosionY, responseData['explosionPower']);
                for (var i = 0; i < Math.round(2.5 * responseData['explosionPower']); i++) {
                    ParticleEffect.playExplosion(explosionX + Utils.randBetween(-Utils.tileSize * responseData['explosionPower'], Utils.tileSize * responseData['explosionPower']), explosionY + Utils.randBetween(-Utils.tileSize * responseData['explosionPower'], Utils.tileSize * responseData['explosionPower']), responseData['explosionPower'] * 0.5);
                }
                Globals.game.cameras.main.shake(100, 0.005 * responseData['explosionPower']);
                SoundEffect.play('explode');
            }
        }
    };
    UtilsClient.showAttackCooldown = function (attackCooldownMillis) {
        if (Globals.currentPlayerObj.attackCooldownTween) {
            Globals.game.tweens.remove(Globals.currentPlayerObj.attackCooldownTween);
        }
        Globals.currentPlayerObj.attackCooldownRectangle.width = PlayerClient.attackCooldownRectangleWidthPx;
        Globals.currentPlayerObj.attackCooldownRectangle.setOrigin(0.5, -12);
        Globals.currentPlayerObj.attackCooldownRectangle.setVisible(true);
        Globals.currentPlayerObj.attackCooldownTween = Globals.game.add.tween({
            'targets': Globals.currentPlayerObj.attackCooldownRectangle,
            'width': 0,
            'duration': attackCooldownMillis,
            'onUpdate': function () {
                Globals.currentPlayerObj.attackCooldownRectangle.setOrigin(0.5, -12);
            },
            'onComplete': function () {
                Globals.currentPlayerObj.attackCooldownRectangle.setVisible(false);
            },
        });
    };
    /**
     * http://www.html5gamedevs.com/topic/7162-tweening-a-tint/?tab=comments#comment-42712
     */
    UtilsClient.healthTweenTint = function (sprite, isRegen) {
        if (isRegen === void 0) { isRegen = false; }
        var startColor = Phaser.Display.Color.HexStringToColor('#ff8a80'); // red accent-1
        var endColor = Phaser.Display.Color.HexStringToColor('#ffffff');
        var timeMillis = 750;
        if (isRegen) {
            startColor = Phaser.Display.Color.HexStringToColor('#ccff90'); // light-green accent-1
            endColor = Phaser.Display.Color.HexStringToColor('#ffffff');
            timeMillis = 750;
        }
        var colorBlend = { 'step': 0 };
        Globals.game.add.tween({
            'targets': colorBlend,
            'step': 100,
            'duration': timeMillis,
            'onUpdate': function () {
                var tweenColor = Phaser.Display.Color.Interpolate.ColorWithColor(startColor, endColor, 100, colorBlend['step']);
                sprite.setTint(Phaser.Display.Color.GetColor(tweenColor.r, tweenColor.g, tweenColor.b));
            },
        });
        sprite.setTint(Phaser.Display.Color.GetColor(startColor.red, startColor.green, startColor.blue));
    };
    // https://github.com/photonstorm/phaser/issues/2494
    UtilsClient.getShortestAngle = function (angle1, angle2) {
        var difference = angle2 - angle1;
        var times = Math.floor((difference - (-180)) / 360);
        return (difference - (times * 360)) * -1;
    };
    UtilsClient.playerUseTimeMillis = 500;
    UtilsClient.playerPlaceTimeMillis = 50;
    UtilsClient.xTiles = 20;
    UtilsClient.yTiles = 16;
    UtilsClient.tileSpacingPx = 3;
    UtilsClient.transparentTileIndex = UtilsClient.getTileIndexFromCoord(UtilsClient.xTiles, UtilsClient.yTiles);
    UtilsClient.depthsArr = {
        'floor': 0,
        'world': 1,
        'boatTrail': 1.5,
        'playerRidingEntity': 1.6,
        'playerCooldownBar': 1.7,
        'playerNameTag': 1.8,
        'player': 2,
        'entities': 3,
        'leaves': 4,
        'marker': 5,
        'particles': 6,
        'newItem': 7,
        'light': 8,
    };
    return UtilsClient;
}());
//# sourceMappingURL=UtilsClient.js.map