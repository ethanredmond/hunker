/// <reference path="../Utils.ts" />
/// <reference path="WorldClient.ts" />
/// <reference path="ChatClient.ts" />
/// <reference path="DayNightCycleClient.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="Globals.ts" />
/// <reference path="../../node_modules/phaser/types/phaser.d.ts" />
// https://gamemechanicexplorer.com/#lighting-3
var LightCircle = /** @class */ (function () {
    function LightCircle(layerNum, x, y, lightCircleRadius, lightCircleTint, id) {
        if (id === void 0) { id = layerNum + ':' + x + ',' + y; }
        this.id = id;
        this.x = (x * Utils.tileSize) + (Utils.tileSize / 2);
        this.y = (y * Utils.tileSize) + (Utils.tileSize / 2);
        this.layerNum = layerNum;
        this.lightCircleRadius = lightCircleRadius;
        this.lightCircleTint = lightCircleTint;
        this.isShowing = true;
        LightCircle.allObjsArr[this.id] = this;
    }
    LightCircle.init = function () {
        if (localStorage.getItem('isBrightening') == '1') {
            ChatClient.brighten();
        }
        LightCircle.shadowTexture = Globals.game.textures.createCanvas('shadowTexture', window.innerWidth, window.innerHeight);
        LightCircle.shadowSprite = Globals.game.add.image(0, 0, 'shadowTexture');
        LightCircle.shadowSprite.depth = UtilsClient.depthsArr['light'];
        LightCircle.shadowSprite.setOrigin(0);
        LightCircle.shadowSprite.setScrollFactor(0); // Fixes to camera
        LightCircle.shadowSprite.setBlendMode(Phaser.BlendModes.MULTIPLY);
    };
    LightCircle.updateShadowTexture = function () {
        LightCircle.currentUpdateTickNum++;
        if (LightCircle.currentUpdateTickNum >= LightCircle.nextUpdateTickNum) {
            LightCircle.shadowTexture.context.fillStyle = LightCircle.shadowRgb;
            LightCircle.shadowTexture.context.fillRect(0, 0, window.innerWidth, window.innerHeight);
            if (parseInt(LightCircle.shadowRgb.slice(4, LightCircle.shadowRgb.indexOf(','))) <= 25) {
                for (var coordStr in LightCircle.allObjsArr) {
                    var lightCircleObj = LightCircle.allObjsArr[coordStr];
                    if (lightCircleObj.isShowing
                        &&
                            lightCircleObj.layerNum == WorldClient.currentLayerNum
                        &&
                            lightCircleObj.lightCircleRadius
                        &&
                            lightCircleObj.x >= Globals.game.cameras.main.scrollX - lightCircleObj.lightCircleRadius
                        &&
                            lightCircleObj.y >= Globals.game.cameras.main.scrollY - lightCircleObj.lightCircleRadius
                        &&
                            lightCircleObj.x <= Globals.game.cameras.main.scrollX + window.innerWidth + lightCircleObj.lightCircleRadius
                        &&
                            lightCircleObj.y <= Globals.game.cameras.main.scrollY + window.innerHeight + lightCircleObj.lightCircleRadius) {
                        var lightCircleRadius = lightCircleObj.lightCircleRadius;
                        if (lightCircleObj.lightCircleTint != 'blue') {
                            lightCircleRadius += Utils.randBetween(1, 20);
                        }
                        var lightCircleObjRelX = lightCircleObj.x - Globals.game.cameras.main.scrollX;
                        var lightCircleObjRelY = lightCircleObj.y - Globals.game.cameras.main.scrollY;
                        var gradient = LightCircle.shadowTexture.context.createRadialGradient(lightCircleObjRelX, lightCircleObjRelY, 0, lightCircleObjRelX, lightCircleObjRelY, lightCircleRadius);
                        if (lightCircleObj.lightCircleTint == 'red') {
                            gradient.addColorStop(0.0, 'rgba(150, 50, 50, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 50, 15, 15, 0.0)');
                        }
                        else if (lightCircleObj.lightCircleTint == 'orange') {
                            gradient.addColorStop(0.0, 'rgba(150, 75,  0, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 50, 15, 15, 0.0)');
                        }
                        else if (lightCircleObj.lightCircleTint == 'cyan') {
                            gradient.addColorStop(0.0, 'rgba(100, 255, 255, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 15,  50,  50, 0.0)');
                        }
                        else if (lightCircleObj.lightCircleTint == 'blue') {
                            gradient.addColorStop(0.0, 'rgba( 50, 100, 255, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 15,  50,  50, 0.0)');
                        }
                        else {
                            gradient.addColorStop(0.0, 'rgba(150, 150, 150, 1.0)');
                            gradient.addColorStop(1.0, 'rgba( 50,  50,  50, 0.0)');
                        }
                        LightCircle.shadowTexture.context.beginPath();
                        LightCircle.shadowTexture.context.fillStyle = gradient;
                        LightCircle.shadowTexture.context.arc(lightCircleObjRelX, lightCircleObjRelY, lightCircleRadius, 0, Math.PI * 2);
                        LightCircle.shadowTexture.context.fill();
                    }
                }
            }
            LightCircle.shadowTexture.refresh();
            LightCircle.nextUpdateTickNum += LightCircle.lightTick;
        }
    };
    LightCircle.updateShadowRgb = function () {
        if (LightCircle.isBrightening) {
            LightCircle.shadowRgb = 'rgb(255, 255, 255)';
        }
        else {
            if (WorldClient.currentLayerNum == 0) { // Overworld.
                var startNum_1 = parseInt(LightCircle.shadowRgb.slice(4, LightCircle.shadowRgb.indexOf(',')));
                var endNum_1 = 25; // Nighttime.
                if (DayNightCycleClient.stage == 'day') { // Daytime.
                    if (Globals.currentPlayerObj && Globals.currentPlayerObj.isGhost) {
                        endNum_1 = 25;
                    }
                    else {
                        endNum_1 = 255;
                    }
                }
                if (startNum_1 != endNum_1) {
                    var shadowBlend_1 = { 'step': 0 };
                    Globals.game.add.tween({
                        'targets': shadowBlend_1,
                        'step': 100,
                        'duration': LightCircle.updateShadowMillis,
                        'onUpdate': function () {
                            var newColorObj = Phaser.Display.Color.Interpolate.RGBWithRGB(startNum_1, startNum_1, startNum_1, endNum_1, endNum_1, endNum_1, 100, shadowBlend_1['step']);
                            LightCircle.shadowRgb = 'rgb(' + newColorObj.r + ', ' + newColorObj.g + ', ' + newColorObj.b + ')';
                        }
                    });
                    setTimeout(function () {
                        LightCircle.shadowRgb = 'rgb(' + endNum_1 + ', ' + endNum_1 + ', ' + endNum_1 + ')';
                    }, LightCircle.updateShadowMillis);
                }
            }
            else { // Caves.
                LightCircle.shadowRgb = 'rgb(0, 0, 0)';
            }
        }
    };
    LightCircle.prototype.destroy = function () {
        delete LightCircle.allObjsArr[this.id];
    };
    LightCircle.allObjsArr = {};
    LightCircle.currentUpdateTickNum = 0;
    LightCircle.nextUpdateTickNum = 0;
    LightCircle.shadowRgb = 'rgb(0, 0, 0)';
    LightCircle.defaultLightTick = 3;
    LightCircle.lightTick = LightCircle.defaultLightTick;
    LightCircle.updateShadowMillis = 2000;
    return LightCircle;
}());
//# sourceMappingURL=LightCircle.js.map