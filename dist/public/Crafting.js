/// <reference path="./generated/Items.ts" />
/// <reference path="../Utils.ts" />
/// <reference path="UtilsClient.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="HotbarClient.ts" />
/// <reference path="Modals.ts" />
var Crafting = /** @class */ (function () {
    function Crafting() {
    }
    Crafting.initSockets = function () {
        Globals.socket.on('finishCraftingItem', function (response) {
            UtilsClient.handleSocketEmitResponse(response, function () {
                Globals.currentPlayerObj.isCrafting = false;
                Globals.currentPlayerObj.craftingItemId = null;
                Globals.currentPlayerObj.updateIndicatorSprite();
                $('.rotate').removeClass('rotate'); // Stop rotating everything.
                $('#hotbarItems .item.ui-draggable-handle').draggable('enable');
            });
        });
    };
    Crafting.startCraftingItem = function ($item) {
        if (!Globals.currentPlayerObj.isCrafting) { // Not crafting.
            Globals.socket.emit('startCraftingItem', $item.data('item-id'), function (response) {
                UtilsClient.handleSocketEmitResponse(response, function () {
                    Globals.currentPlayerObj.isCrafting = true;
                    Globals.currentPlayerObj.craftingItemId = $item.data('item-id');
                    Globals.currentPlayerObj.updateIndicatorSprite();
                    $item.find('.itemImg').addClass('rotate'); // Rotate the currently crafting item's image.
                    var $craftingButtonProgressIndicator = $("<div class=\"craftingProgressIndicator\"></div>"); // Add crafting progress indicator to the crafting button.
                    var $craftingProgressIndicator = $("<div class=\"craftingProgressIndicator\"></div>"); // Add crafting progress indicator to the currently crafting item.
                    $item.append($craftingProgressIndicator);
                    $craftingButtonProgressIndicator.animate({ height: '100%' }, response['itemCraftingTimeMillis'], 'linear', function () {
                        $(this).remove(); // Remove indicator when finished.
                    });
                    $craftingProgressIndicator.animate({ height: '100%' }, response['itemCraftingTimeMillis'], 'linear', function () {
                        $(this).remove(); // Remove indicator when finished.
                    });
                    $('#hotbarItems .item.ui-draggable-handle').draggable('disable');
                });
            });
        }
    };
    Crafting.getItemTooltip = function (itemId, itemDurability, itemEnchantmentsArr) {
        var itemTooltip = '';
        if (itemId && itemId in Items.itemsArr) {
            itemTooltip += "<b>" + Items.itemsArr[itemId]['label'] + "</b> <i>" + itemId + "</i><br>";
            if (Items.itemsArr[itemId]['itemDesc'] != null) {
                itemTooltip += Items.itemsArr[itemId]['itemDesc'] + "<br>";
            }
            if (itemEnchantmentsArr) {
                for (var enchantmentType in itemEnchantmentsArr) {
                    itemTooltip += "<b>" + Utils.allEnchantmentsArr[enchantmentType]['label'] + ' ' + Utils.toRomanNumerals(itemEnchantmentsArr[enchantmentType]) + "</b><br>";
                }
            }
            if (Items.itemsArr[itemId]['itemDurability'] != null) {
                if (itemDurability) {
                    itemTooltip += "<b>" + itemDurability + " / " + Items.itemsArr[itemId]['itemDurability'] + "</b> uses left<br>";
                }
                else {
                    itemTooltip += "<b>" + Items.itemsArr[itemId]['itemDurability'] + "</b> durability<br>";
                }
            }
            if (Items.itemsArr[itemId]['hungerValue'] != null) {
                itemTooltip += "<b>" + Items.itemsArr[itemId]['hungerValue'] + "</b> hunger<br>";
            }
            if (Items.itemsArr[itemId]['warmthValue'] != null) {
                itemTooltip += "<b>" + Items.itemsArr[itemId]['warmthValue'] + "</b> warmth<br>";
            }
            if (Items.itemsArr[itemId]['healthValue'] != null) {
                itemTooltip += "<b>" + Items.itemsArr[itemId]['healthValue'] + "</b> health<br>";
            }
            if (Items.itemsArr[itemId]['entityDamageValue'] != null) {
                itemTooltip += "<b>" + Items.itemsArr[itemId]['entityDamageValue'] + "</b> damage<br>";
            }
            if (Items.itemsArr[itemId]['attackCooldownMillis'] != null) {
                itemTooltip += "<b>" + (Items.itemsArr[itemId]['attackCooldownMillis'] / 1000) + "</b>s cooldown<br>";
            }
            if (Items.itemsArr[itemId]['attackRangeDistance'] != null) {
                itemTooltip += "<b>" + Items.itemsArr[itemId]['attackRangeDistance'] + "</b>x distance<br>";
            }
            if (Items.itemsArr[itemId]['attackKnockbackMult'] != null) {
                itemTooltip += "<b>" + Items.itemsArr[itemId]['attackKnockbackMult'] + "</b>x knockback<br>";
            }
            if (Items.itemsArr[itemId]['heatLossReductionPcnt'] != null) {
                itemTooltip += "<b>" + Math.round(Items.itemsArr[itemId]['heatLossReductionPcnt'] * 100) + "%</b> heat loss reduction<br>";
            }
            if (Items.itemsArr[itemId]['damageReductionPcnt'] != null) {
                itemTooltip += "<b>" + (Math.round(Items.itemsArr[itemId]['damageReductionPcnt'] * 100 * 10) / 10) + "%</b> damage reduction<br>";
            }
            var requiredItemsForCraftingArr = Items.itemsArr[itemId]['requiredItemsForCraftingArr'];
            if (requiredItemsForCraftingArr != null) {
                itemTooltip += "Crafting: ";
                for (var loopItemId in requiredItemsForCraftingArr) {
                    if (loopItemId in Items.itemsArr) {
                        itemTooltip += "<img class='itemTooltipImg' src='/public" + Items.itemsArr[loopItemId]['itemSrc'] + "' alt='" + Items.itemsArr[loopItemId]['label'] + "'>";
                        itemTooltip += " x " + requiredItemsForCraftingArr[loopItemId] + " ";
                    }
                    else {
                        console.log("\"" + loopItemId + "\" not found");
                    }
                }
                var requiredBlockForCraftingItemId = Items.itemsArr[itemId]['requiredBlockForCraftingItemId'];
                if (requiredBlockForCraftingItemId != null) {
                    if (requiredBlockForCraftingItemId == 'heater') {
                        requiredBlockForCraftingItemId = 'fire';
                    }
                    itemTooltip += "+ <img class='itemTooltipImg' src='/public" + Items.itemsArr[requiredBlockForCraftingItemId]['itemSrc'] + "' alt='" + Items.itemsArr[requiredBlockForCraftingItemId]['label'] + "'>";
                }
                itemTooltip += "<br>";
                var itemCraftingTimeMillis = Utils.itemCraftingTimeMillis(itemId);
                if (itemCraftingTimeMillis > 0) {
                    itemTooltip += Math.round(itemCraftingTimeMillis / 1000 * 10) / 10 + " secs to " + (Items.itemsArr[itemId]['requiredBlockForCraftingItemId'] == 'hearth' ? 'smelt' : 'craft');
                }
            }
        }
        return itemTooltip;
    };
    return Crafting;
}());
//# sourceMappingURL=Crafting.js.map