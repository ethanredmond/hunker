/// <reference path="UtilsClient.ts" />
/// <reference path="InventoryModal.ts" />
/// <reference path="Crafting.ts" />
var Modals = /** @class */ (function () {
    function Modals() {
    }
    Modals.init = function () {
        Modals.initJoinGameModal();
        Modals.initPlayerDiedModal();
        Modals.initHowToPlayModal();
        Modals.initGamePausedModal();
        Modals.initChangePlayerImageModal();
        Modals.initChangeSettingModal('difficulty');
        Modals.initChangeSettingModal('gameMode');
        Modals.initDisconnectedModal();
        InventoryModal.initInventoryModal();
    };
    Modals.initJoinGameModal = function () {
        Modals.allModalElements['joinGame'] = UtilsClient.byId('joinGameModal');
        M.Modal.init(Modals.allModalElements['joinGame'], {
            'dismissible': false,
            'inDuration': 0,
            'outDuration': 0,
            'onOpenStart': function () {
                Globals.disableInput();
            },
            'onCloseStart': function () {
                Globals.enableInput();
            },
        });
        Modals.allModalObjs['joinGame'] = M.Modal.getInstance(Modals.allModalElements['joinGame']);
        Modals.allModalObjs['joinGame'].open();
    };
    Modals.initPlayerDiedModal = function () {
        Modals.allModalElements['playerDiedGame'] = UtilsClient.byId('playerDiedGameModal');
        M.Modal.init(Modals.allModalElements['playerDiedGame'], {
            'dismissible': false,
            'inDuration': 0,
            'outDuration': 0,
        });
        Modals.allModalObjs['playerDied'] = M.Modal.getInstance(Modals.allModalElements['playerDiedGame']);
    };
    Modals.initHowToPlayModal = function () {
        Modals.allModalElements['howToPlay'] = UtilsClient.byId('howToPlayModal');
        M.Modal.init(Modals.allModalElements['howToPlay'], {
            'inDuration': 0,
            'outDuration': 0,
        });
        Modals.allModalObjs['howToPlay'] = M.Modal.getInstance(Modals.allModalElements['howToPlay']);
    };
    Modals.initGamePausedModal = function () {
        Modals.allModalElements['gamePaused'] = UtilsClient.byId('gamePausedModal');
        M.Modal.init(Modals.allModalElements['gamePaused'], {
            'dismissible': false,
            'inDuration': 0,
            'outDuration': 0,
            'onOpenStart': function () {
                Globals.disableInput();
            },
            'onCloseStart': function () {
                Globals.enableInput();
            },
        });
        Modals.allModalObjs['gamePaused'] = M.Modal.getInstance(Modals.allModalElements['gamePaused']);
    };
    Modals.initChangePlayerImageModal = function () {
        Modals.allModalElements['changePlayerImage'] = UtilsClient.byId('changePlayerImageModal');
        M.Modal.init(Modals.allModalElements['changePlayerImage'], {
            'inDuration': 0,
            'outDuration': 0,
        });
        Modals.allModalObjs['changePlayerImage'] = M.Modal.getInstance(Modals.allModalElements['changePlayerImage']);
    };
    Modals.initChangeSettingModal = function (settingName) {
        var settingValue = localStorage.getItem(settingName);
        var settingsArr;
        if (settingName == 'gameMode') {
            settingsArr = Utils.playerGameModesArr;
            if (!settingValue || !(settingValue in settingsArr)) {
                settingValue = 'survival';
            }
            PlayerClient.gameModeStr = settingValue;
        }
        else {
            settingsArr = Utils.playerDifficultiesArr;
            if (!settingValue || !(settingValue in settingsArr)) {
                settingValue = 'normal';
            }
            PlayerClient.difficultyStr = settingValue;
        }
        Modals.allModalElements["change" + Utils.ucfirst(settingName)] = UtilsClient.byId("change" + Utils.ucfirst(settingName) + "Modal");
        var changeSettingModalHtml = '';
        changeSettingModalHtml += "<ul class=\"collection\">";
        for (var settingKey in settingsArr) {
            changeSettingModalHtml += "\n            <li data-" + settingName + "=\"" + settingKey + "\" onclick=\"Modals.changeSetting('" + settingName + "', '" + settingKey + "');\" class=\"collection-item" + (settingKey == settingValue ? ' active' : '') + "\">\n              <span class=\"title\">" + settingsArr[settingKey]['label'] + "</span>\n              <div class=\"smallText\">" + settingsArr[settingKey]['description'] + "</div>\n            </li>\n";
        }
        changeSettingModalHtml += "</ul>";
        Modals.allModalElements["change" + Utils.ucfirst(settingName)].children[0].innerHTML = changeSettingModalHtml;
        $(".player" + Utils.ucfirst(settingName) + "Text").text(settingsArr[settingValue]['label']);
        $(".player" + Utils.ucfirst(settingName) + "Link").on('click', function () {
            Modals.allModalObjs["change" + Utils.ucfirst(settingName)].open();
        });
        M.Modal.init(Modals.allModalElements["change" + Utils.ucfirst(settingName)], {
            'inDuration': 0,
            'outDuration': 0,
        });
        Modals.allModalObjs["change" + Utils.ucfirst(settingName)] = M.Modal.getInstance(Modals.allModalElements["change" + Utils.ucfirst(settingName)]);
    };
    Modals.changeSetting = function (settingName, settingKey) {
        if (Globals.currentPlayerObj) {
            Globals.socket.emit('changeSetting', settingName, settingKey, function (response) {
                UtilsClient.handleSocketEmitResponse(response, function () {
                    Modals.changeSettingResponse(settingName, settingKey);
                });
            });
        }
        else {
            Modals.changeSettingResponse(settingName, settingKey);
        }
    };
    Modals.changeSettingResponse = function (settingName, settingKey) {
        localStorage.setItem(settingName, settingKey);
        var settingsArr;
        if (settingName == 'gameMode') {
            settingsArr = Utils.playerGameModesArr;
            if (Globals.currentPlayerObj) {
                Globals.currentPlayerObj.healthBar.updateIsShowing(settingsArr[settingKey]['hasHealth']);
                Globals.currentPlayerObj.warmthBar.updateIsShowing(settingsArr[settingKey]['hasWarmth']);
                Globals.currentPlayerObj.hungerBar.updateIsShowing(settingsArr[settingKey]['hasHunger']);
            }
            PlayerClient.gameModeStr = settingKey;
        }
        else {
            settingsArr = Utils.playerDifficultiesArr;
            PlayerClient.difficultyStr = settingKey;
        }
        $(".player" + Utils.ucfirst(settingName) + "Text").text(settingsArr[settingKey]['label']);
        $("#change" + Utils.ucfirst(settingName) + "Modal .collection-item.active").removeClass('active');
        $("#change" + Utils.ucfirst(settingName) + "Modal .collection-item[data-" + settingName + "=" + settingKey + "]").addClass('active');
        Modals.allModalObjs["change" + Utils.ucfirst(settingName)].close();
    };
    Modals.initDisconnectedModal = function () {
        Modals.allModalElements['disconnected'] = UtilsClient.byId('disconnectedModal');
        M.Modal.init(Modals.allModalElements['disconnected'], {
            'dismissible': false,
            'inDuration': 0,
            'outDuration': 0,
            'onOpenStart': function () {
                Globals.disableInput();
            },
            'onCloseStart': function () {
                Globals.enableInput();
            },
        });
        Modals.allModalObjs['disconnected'] = M.Modal.getInstance(Modals.allModalElements['disconnected']);
    };
    Modals.closeAll = function () {
        for (var modalId in Modals.allModalObjs) {
            if (modalId != 'gamePaused') {
                Modals.allModalObjs[modalId].close();
            }
        }
        $('.ui-tooltip').hide();
    };
    Modals.allModalElements = {};
    Modals.allModalObjs = {};
    return Modals;
}());
//# sourceMappingURL=Modals.js.map