/// <reference path="PlayerClient.ts" />
var Globals = /** @class */ (function () {
    function Globals() {
    }
    Globals.disableInput = function () {
        Globals.isInputEnabled = false;
        if (Globals.game) {
            Globals.game.input.keyboard.clearCaptures();
        }
    };
    Globals.enableInput = function () {
        Globals.isInputEnabled = true;
    };
    Globals.isGameCreated = false;
    return Globals;
}());
//# sourceMappingURL=Globals.js.map