var NotificationMsg = /** @class */ (function () {
    function NotificationMsg() {
    }
    NotificationMsg.init = function () {
        NotificationMsg.$notificationMsg = $("<div id=\"notificationMsg\"></div>").appendTo('body');
    };
    NotificationMsg.show = function (notificationText) {
        if (NotificationMsg.$notificationMsg) {
            if (notificationText) {
                NotificationMsg.$notificationMsg.text(notificationText).fadeIn(200);
                clearTimeout(NotificationMsg.hideMsgTimeout);
                NotificationMsg.hideMsgTimeout = setTimeout(function () {
                    NotificationMsg.$notificationMsg.fadeOut(1000);
                }, 1000);
            }
            else {
                console.trace();
                console.error("notificationText not set");
            }
        }
    };
    return NotificationMsg;
}());
//# sourceMappingURL=NotificationMsg.js.map