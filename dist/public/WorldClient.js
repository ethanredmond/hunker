/// <reference path="generated/Items.ts" />
/// <reference path="UtilsClient.ts" />
/// <reference path="LightCircle.ts" />
/// <reference path="PlayerClient.ts" />
/// <reference path="Minimap.ts" />
/// <reference path="InventoryModal.ts" />
/// <reference path="EntityClient.ts" />
/// <reference path="Modals.ts" />
var WorldClient = /** @class */ (function () {
    function WorldClient() {
    }
    WorldClient.initFloor = function () {
        if (WorldClient.floorTilemap) {
            WorldClient.floorTilemap.destroy();
        }
        WorldClient.floorTilemap = Globals.game.make.tilemap({ 'data': WorldClient.getTilemapDataCsv(WorldClient.floorArr[WorldClient.currentLayerNum]), 'tileWidth': Utils.tileSize, 'tileHeight': Utils.tileSize }); // , 'tileSpacing': UtilsClient.tileSpacingPx
        var floorTileset = WorldClient.floorTilemap.addTilesetImage('spritesheet_tiles', 'spritesheet_tiles', Utils.tileSize, Utils.tileSize, 0, UtilsClient.tileSpacingPx);
        WorldClient.floorTilemap.setCollisionBetween(0, UtilsClient.transparentTileIndex - 1, true);
        WorldClient.floorTilemap.setCollision(Items.itemsArr['water']['tileIndexesArr'].concat(Items.itemsArr['bridge']['tileIndexesArr']).concat(Items.itemsArr['bridge']['altTileIndexesArr']), false, true);
        if (WorldClient.layerFloor) {
            WorldClient.layerFloor.destroy();
        }
        WorldClient.layerFloor = WorldClient.floorTilemap.createDynamicLayer(0, floorTileset, 0, 0);
        WorldClient.layerFloor.depth = UtilsClient.depthsArr['floor'];
    };
    WorldClient.initWorld = function () {
        if (WorldClient.worldTilemap) {
            WorldClient.worldTilemap.destroy();
        }
        WorldClient.worldTilemap = Globals.game.make.tilemap({ 'data': WorldClient.getTilemapDataCsv(WorldClient.worldArr[WorldClient.currentLayerNum]), 'tileWidth': Utils.tileSize, 'tileHeight': Utils.tileSize }); // , 'tileSpacing': UtilsClient.tileSpacingPx
        var worldTileset = WorldClient.worldTilemap.addTilesetImage('spritesheet_tiles', 'spritesheet_tiles', Utils.tileSize, Utils.tileSize, 0, UtilsClient.tileSpacingPx);
        WorldClient.worldTilemap.setCollisionBetween(0, UtilsClient.transparentTileIndex - 1, true);
        // Disable collision on some tiles. https://phaser.io/docs/2.4.4/Phaser.Tilemap.html#setCollision
        var disablingCollisionTileIndexesArr = [];
        for (var itemId in Items.itemsArr) {
            if (Items.itemsArr[itemId]['isDisablingCollision'] && Items.itemsArr[itemId]['tileIndexesArr']) {
                for (var i in Items.itemsArr[itemId]['tileIndexesArr']) {
                    disablingCollisionTileIndexesArr.push(Items.itemsArr[itemId]['tileIndexesArr'][i]);
                }
            }
        }
        WorldClient.worldTilemap.setCollision(disablingCollisionTileIndexesArr, false, true);
        if (WorldClient.layerCollide) {
            WorldClient.layerCollide.destroy();
        }
        WorldClient.layerCollide = WorldClient.worldTilemap.createDynamicLayer(0, worldTileset, 0, 0);
        WorldClient.layerCollide.depth = UtilsClient.depthsArr['world'];
        if (Globals.currentPlayerObj) {
            if (Globals.currentPlayerObj.spriteCollider) {
                Globals.currentPlayerObj.spriteCollider.destroy();
            }
            Globals.currentPlayerObj.spriteCollider = Globals.game.physics.add.collider(Globals.currentPlayerObj.sprite, WorldClient.layerCollide);
        }
        for (var y = 0; y < Utils.worldHeight; y++) {
            for (var x = 0; x < Utils.worldWidth; x++) {
                if (WorldClient.worldArr[WorldClient.currentLayerNum][y][x] != null && WorldClient.worldArr[WorldClient.currentLayerNum][y][x][0] in Utils.tileIndexToItemIdArr) {
                    var tileId = Utils.tileIndexToItemIdArr[WorldClient.worldArr[WorldClient.currentLayerNum][y][x][0]];
                    var blockCoordStr = WorldClient.currentLayerNum + ':' + x + ',' + y;
                    var blockCoordArr = {
                        'layerNum': WorldClient.currentLayerNum,
                        'x': x,
                        'y': y,
                    };
                    if (tileId in Items.itemsArr
                        &&
                            (tileId == 'tree'
                                ||
                                    tileId == 'tree_birch'
                                ||
                                    tileId == 'tree_acacia'
                                ||
                                    tileId == 'windmill')
                        &&
                            !(blockCoordStr in WorldClient.allLeavesOverlaySpritesArr)) {
                        WorldClient.createLeavesOverlaySprite(blockCoordArr, blockCoordStr, false, tileId);
                    }
                    if (tileId in Items.itemsArr && Items.itemsArr[tileId]['lightCircleRadius'] && !(blockCoordStr in LightCircle.allObjsArr)) {
                        new LightCircle(blockCoordArr['layerNum'], blockCoordArr['x'], blockCoordArr['y'], Items.itemsArr[tileId]['lightCircleRadius'], Items.itemsArr[tileId]['lightCircleTint']);
                    }
                    if (tileId == 'spawn') {
                        WorldClient.spawnCoord = blockCoordArr;
                    }
                    else if (tileId == 'marker') {
                        WorldClient.allMarkerCoords[blockCoordStr] = blockCoordArr;
                    }
                }
            }
        }
        WorldClient.setTreesVisibility();
        LightCircle.updateShadowRgb();
    };
    WorldClient.getTilemapDataCsv = function (layerArr) {
        var tilemapDataArr = [];
        for (var y = 0; y < layerArr.length; y++) {
            var tilemapDataRow = [];
            for (var x = 0; x < layerArr[y].length; x++) {
                if (layerArr[y][x] != null) {
                    tilemapDataRow.push(layerArr[y][x][0]);
                }
                else {
                    tilemapDataRow.push(UtilsClient.transparentTileIndex);
                }
            }
            tilemapDataArr.push(tilemapDataRow);
        }
        return tilemapDataArr;
    };
    WorldClient.getDestroyStageSprite = function (currentHealth, maxHealth) {
        // console.log('---------');
        // console.log(currentHealth);
        // console.log(maxHealth);
        // console.log((currentHealth / maxHealth) * 10);
        // console.log(10 - Math.floor((currentHealth / maxHealth) * 10));
        // console.log(Math.min(Math.max(10 - Math.floor((currentHealth / maxHealth) * 10), 0), 10));
        // console.log('destroyStage' + Math.min(Math.max(10 - Math.floor((currentHealth / maxHealth) * 10), 0), 10));
        return 'destroyStage' + Math.min(Math.max(10 - Math.floor((currentHealth / maxHealth) * 10), 0), 10);
    };
    WorldClient.blockUpdate = function (blockUpdateDataArr) {
        var blockCoordStr = blockUpdateDataArr['blockCoordArr']['layerNum'] + ':' + blockUpdateDataArr['blockCoordArr']['x'] + ',' + blockUpdateDataArr['blockCoordArr']['y'];
        if ('blockDataArr' in blockUpdateDataArr) {
            // Block removed.
            if (blockUpdateDataArr['blockDataArr'] == null || blockUpdateDataArr['blockDataArr'][0] == null) {
                if (blockUpdateDataArr['blockCoordArr']['layerNum'] == WorldClient.currentLayerNum) {
                    WorldClient.worldTilemap.putTileAt(UtilsClient.transparentTileIndex, blockUpdateDataArr['blockCoordArr']['x'], blockUpdateDataArr['blockCoordArr']['y']);
                }
                delete WorldClient.allMarkerCoords[blockCoordStr];
                if (blockCoordStr in WorldClient.allLeavesOverlaySpritesArr) {
                    WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].destroy();
                }
                delete WorldClient.allLeavesOverlaySpritesArr[blockCoordStr];
                delete WorldClient.allWindmillOverlayTweensArr[blockCoordStr];
                if (blockCoordStr in LightCircle.allObjsArr) {
                    LightCircle.allObjsArr[blockCoordStr].destroy();
                }
                delete LightCircle.allObjsArr[blockCoordStr];
                // Block added.
            }
            else if (blockUpdateDataArr['blockDataArr'][0] != null
                &&
                    (WorldClient.worldArr[blockUpdateDataArr['blockCoordArr']['layerNum']][blockUpdateDataArr['blockCoordArr']['y']][blockUpdateDataArr['blockCoordArr']['x']] == null
                        ||
                            blockUpdateDataArr['blockDataArr'][0] != WorldClient.worldArr[blockUpdateDataArr['blockCoordArr']['layerNum']][blockUpdateDataArr['blockCoordArr']['y']][blockUpdateDataArr['blockCoordArr']['x']][0])) {
                if (blockUpdateDataArr['blockCoordArr']['layerNum'] == WorldClient.currentLayerNum) {
                    WorldClient.worldTilemap.putTileAt(blockUpdateDataArr['blockDataArr'][0], blockUpdateDataArr['blockCoordArr']['x'], blockUpdateDataArr['blockCoordArr']['y']);
                }
                if ('blockId' in blockUpdateDataArr && blockUpdateDataArr['blockId'] in Items.itemsArr) {
                    if (blockUpdateDataArr['blockId'] == 'spawn') {
                        WorldClient.spawnCoord = blockUpdateDataArr['blockCoordArr'];
                    }
                    else if (blockUpdateDataArr['blockId'] == 'marker') {
                        WorldClient.allMarkerCoords[blockCoordStr] = blockUpdateDataArr['blockCoordArr'];
                    }
                    else if (blockUpdateDataArr['blockId'] == 'tree' || blockUpdateDataArr['blockId'] == 'tree_birch' || blockUpdateDataArr['blockId'] == 'tree_acacia' || blockUpdateDataArr['blockId'] == 'windmill') {
                        WorldClient.createLeavesOverlaySprite(blockUpdateDataArr['blockCoordArr'], blockCoordStr, true, blockUpdateDataArr['blockId']);
                    }
                    if (Items.itemsArr[blockUpdateDataArr['blockId']]['lightCircleRadius']) {
                        new LightCircle(blockUpdateDataArr['blockCoordArr']['layerNum'], blockUpdateDataArr['blockCoordArr']['x'], blockUpdateDataArr['blockCoordArr']['y'], Items.itemsArr[blockUpdateDataArr['blockId']]['lightCircleRadius'], Items.itemsArr[blockUpdateDataArr['blockId']]['lightCircleTint']);
                    }
                }
            }
            WorldClient.worldArr[blockUpdateDataArr['blockCoordArr']['layerNum']][blockUpdateDataArr['blockCoordArr']['y']][blockUpdateDataArr['blockCoordArr']['x']] = blockUpdateDataArr['blockDataArr'];
        }
        // Update marker crack sprite if hovered over block.
        if (Globals.currentPlayerObj
            &&
                blockUpdateDataArr['blockCoordArr']['layerNum'] == Globals.currentPlayerObj.currentLayerNum
            &&
                blockUpdateDataArr['blockCoordArr']['x'] == Globals.currentPlayerObj.markerTileX
            &&
                blockUpdateDataArr['blockCoordArr']['y'] == Globals.currentPlayerObj.markerTileY) {
            Globals.currentPlayerObj.updateMarkerCrackSprite();
            if (Modals.allModalElements['inventory'].style.display == 'block') {
                if (('blockDataArr' in blockUpdateDataArr) && (blockUpdateDataArr['blockDataArr'] == null || blockUpdateDataArr['blockDataArr'][0] == null)) {
                    InventoryModal.open();
                }
                else {
                    InventoryModal.updateInventoryContainer();
                }
            }
        }
    };
    WorldClient.floorUpdate = function (floorUpdateDataArr) {
        WorldClient.floorArr[floorUpdateDataArr['floorCoordArr']['layerNum']][floorUpdateDataArr['floorCoordArr']['y']][floorUpdateDataArr['floorCoordArr']['x']] = floorUpdateDataArr['floorDataArr'];
        if (floorUpdateDataArr['floorCoordArr']['layerNum'] == WorldClient.currentLayerNum) {
            WorldClient.floorTilemap.putTileAt(floorUpdateDataArr['floorDataArr'][0], floorUpdateDataArr['floorCoordArr']['x'], floorUpdateDataArr['floorCoordArr']['y']);
        }
        // Update marker crack sprite if hovered over floor.
        if (Globals.currentPlayerObj && floorUpdateDataArr['floorCoordArr']['layerNum'] == Globals.currentPlayerObj.currentLayerNum && floorUpdateDataArr['floorCoordArr']['x'] == Globals.currentPlayerObj.markerTileX && floorUpdateDataArr['floorCoordArr']['y'] == Globals.currentPlayerObj.markerTileY) {
            Globals.currentPlayerObj.updateMarkerCrackSprite();
        }
    };
    WorldClient.createLeavesOverlaySprite = function (blockCoordArr, blockCoordStr, isUpdatingOrder, blockItemId) {
        var leavesRange = 1;
        if (WorldClient.biomeCoordsPerLayerArr[blockCoordArr['layerNum']]
            &&
                WorldClient.biomeCoordsPerLayerArr[blockCoordArr['layerNum']][blockCoordArr['y']]
            &&
                WorldClient.biomeCoordsPerLayerArr[blockCoordArr['layerNum']][blockCoordArr['y']][blockCoordArr['x']] == 'christmas'
            &&
                blockItemId == 'tree') {
            WorldClient.allLeavesOverlaySpritesArr[blockCoordStr] = Globals.game.add.tileSprite(blockCoordArr['x'] * Utils.tileSize + (Utils.tileSize / 2), blockCoordArr['y'] * Utils.tileSize + (Utils.tileSize / 2), ((leavesRange * 2) + 1) * Utils.tileSize, ((leavesRange * 2) + 1) * Utils.tileSize, 'leaves_tree_christmas_' + Utils.randBetween(1, 4));
        }
        else {
            var overlaySprite = 'leaves_' + blockItemId;
            if (blockItemId == 'tree_acacia') {
                overlaySprite = 'leaves_tree_birch';
            }
            else if (blockItemId == 'windmill') {
                overlaySprite = 'windmill_top';
            }
            WorldClient.allLeavesOverlaySpritesArr[blockCoordStr] = Globals.game.add.sprite(blockCoordArr['x'] * Utils.tileSize + (Utils.tileSize / 2), blockCoordArr['y'] * Utils.tileSize + (Utils.tileSize / 2), overlaySprite);
            if (blockItemId == 'windmill') {
                WorldClient.allWindmillOverlayTweensArr[blockCoordStr] = Globals.game.add.tween({
                    'targets': WorldClient.allLeavesOverlaySpritesArr[blockCoordStr],
                    'angle': 180,
                    'repeat': -1,
                });
            }
        }
        WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].depth = UtilsClient.depthsArr['leaves'];
        if (blockItemId == 'tree_acacia') {
            WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].tint = 0x77ff00;
        }
        if (blockCoordArr['layerNum'] != WorldClient.currentLayerNum) {
            WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].visible = false;
        }
    };
    WorldClient.setTreesVisibility = function () {
        for (var blockCoordStr in WorldClient.allLeavesOverlaySpritesArr) {
            WorldClient.allLeavesOverlaySpritesArr[blockCoordStr].visible = (parseInt(blockCoordStr) == WorldClient.currentLayerNum);
        }
    };
    WorldClient.updateCurrentLayerNum = function (currentLayerNum) {
        WorldClient.currentLayerNum = currentLayerNum;
        if (Minimap.$minimapCurrentLayerNumDiv) {
            if (WorldClient.currentLayerNum) {
                Minimap.$minimapCurrentLayerNumDiv.html(WorldClient.currentLayerNum.toString());
            }
            else {
                Minimap.$minimapCurrentLayerNumDiv.html('');
            }
        }
    };
    WorldClient.currentLayerNum = 0;
    WorldClient.worldTilemap = null;
    WorldClient.floorTilemap = null;
    WorldClient.layerCollide = null;
    WorldClient.layerFloor = null;
    WorldClient.allLeavesOverlaySpritesArr = {};
    WorldClient.allWindmillOverlayTweensArr = {};
    WorldClient.worldArr = {};
    WorldClient.floorArr = {};
    WorldClient.biomeCoordsPerLayerArr = {};
    WorldClient.allMarkerCoords = {};
    WorldClient.spawnCoord = null;
    return WorldClient;
}());
//# sourceMappingURL=WorldClient.js.map