(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./NoiseMap", "./WorldServer", "./CaveGenerator"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var NoiseMap_1 = require("./NoiseMap");
    var WorldServer_1 = require("./WorldServer");
    var CaveGenerator_1 = require("./CaveGenerator");
    var WorldGenerator = /** @class */ (function () {
        function WorldGenerator() {
        }
        WorldGenerator.generateOverworldLayer = function () {
            var worldArr = [];
            var floorArr = [];
            var heightNoiseMap = NoiseMap_1.NoiseMap.generate(60, 4);
            var temperatureNoiseMap = NoiseMap_1.NoiseMap.generate(100, 4);
            var humidityNoiseMap = NoiseMap_1.NoiseMap.generate(100, 4);
            var altNoiseMap = NoiseMap_1.NoiseMap.generate(100, 4);
            for (var y = 0; y < Utils.worldHeight; y++) {
                WorldGenerator.biomeCoordsArr[y] = [];
                for (var x = 0; x < Utils.worldWidth; x++) {
                    var noiseHeight = heightNoiseMap[y][x];
                    var floorItemId = void 0;
                    var biomeName = void 0;
                    if (noiseHeight <= 0.4) {
                        if (noiseHeight > 0.3) {
                            floorItemId = 'sand';
                        }
                        else {
                            floorItemId = 'water';
                        }
                    }
                    else {
                        var noiseTemperature = temperatureNoiseMap[y][x];
                        var noiseHumidity = humidityNoiseMap[y][x];
                        var noiseAlt = altNoiseMap[y][x];
                        for (var biomeNameLoop in WorldGenerator.biomeDetailsArr) {
                            if (noiseTemperature >= WorldGenerator.biomeDetailsArr[biomeNameLoop]['temperatureMin']
                                &&
                                    noiseTemperature < WorldGenerator.biomeDetailsArr[biomeNameLoop]['temperatureMax']
                                &&
                                    noiseHumidity >= WorldGenerator.biomeDetailsArr[biomeNameLoop]['humidityMin']
                                &&
                                    noiseHumidity < WorldGenerator.biomeDetailsArr[biomeNameLoop]['humidityMax']
                                &&
                                    noiseAlt >= WorldGenerator.biomeDetailsArr[biomeNameLoop]['altMin']
                                &&
                                    noiseAlt < WorldGenerator.biomeDetailsArr[biomeNameLoop]['altMax']) {
                                biomeName = biomeNameLoop;
                                break;
                            }
                        }
                        floorItemId = WorldGenerator.biomeDetailsArr[biomeName]['floorItemId'];
                    }
                    WorldGenerator.biomeCoordsArr[y][x] = biomeName;
                    floorArr[y][x] = [Items.itemsArr[floorItemId]['tileIndexesArr'][0], Items.itemsArr[floorItemId]['structureDurability']];
                }
            }
            for (var y = 0; y < Utils.worldHeight; y++) {
                for (var x = 0; x < Utils.worldWidth; x++) {
                    var biomeName = WorldGenerator.biomeCoordsArr[y][x];
                    if (biomeName && biomeName in WorldGenerator.biomeDetailsArr && WorldGenerator.biomeDetailsArr[biomeName]['blocksToSpawnArr']) {
                        for (var itemId in WorldGenerator.biomeDetailsArr[biomeName]['blocksToSpawnArr']) {
                            var chancePcnt = WorldGenerator.biomeDetailsArr[biomeName]['blocksToSpawnArr'][itemId];
                            if (Math.random() <= chancePcnt && !WorldGenerator.isInvalidTerrainCoord(itemId, layerNum, x, y)) {
                                WorldServer_1.WorldServer.addTerrainAtBlock(itemId, layerNum, x, y);
                                break;
                            }
                        }
                    }
                }
            }
            for (var i = 0; i < Utils.worldBlocksNum * 0.0050; i++) {
                WorldServer_1.WorldServer.addTerrainAtRandomBlock('wheat_stage_4', layerNum);
            }
            return [worldArr, floorArr];
        };
        WorldGenerator.addTerrainAtRandomBlockToLayer = function (layerNum) {
            if (layerNum == 0) {
                // @formatter:off
            }
            else if (layerNum <= -1) {
                var caveGenerator = new CaveGenerator_1.CaveGenerator(0.39, 3, 3, 3, [[Items.itemsArr['stone']['tileIndexesArr'][0], Items.itemsArr['stone']['structureDurability']]], [null], false, layerNum);
                WorldServer_1.WorldServer.worldArr[layerNum] = Utils.dereferenceObj(caveGenerator.generateWorldArr(WorldServer_1.WorldServer.worldArr[layerNum]));
                for (var i = 0; i < Utils.worldBlocksNum * 0.0100; i++) {
                    WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_coal', layerNum);
                }
                if (layerNum == -1) {
                    for (var i = 0; i < Utils.worldBlocksNum * 0.0060; i++) {
                        WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_silver', layerNum);
                    }
                }
                else if (layerNum == -2) {
                    for (var i = 0; i < Utils.worldBlocksNum * 0.0025; i++) {
                        WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_silver', layerNum);
                    }
                    for (var i = 0; i < Utils.worldBlocksNum * 0.0035; i++) {
                        WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_gold', layerNum);
                    }
                }
                else if (layerNum == -3) {
                    for (var i = 0; i < Utils.worldBlocksNum * 0.0015; i++) {
                        WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_silver', layerNum);
                    }
                    for (var i = 0; i < Utils.worldBlocksNum * 0.0025; i++) {
                        WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_gold', layerNum);
                    }
                    for (var i = 0; i < Utils.worldBlocksNum * 0.0020; i++) {
                        WorldServer_1.WorldServer.addTerrainAtRandomBlock('ore_diamond', layerNum);
                    }
                }
                // @formatter:on
            }
        };
        WorldGenerator.addMushroomsToLayer = function (layerNum) {
            // @formatter:off
            for (var i = 0; i < Utils.worldBlocksNum * 0.0025; i++) {
                WorldServer_1.WorldServer.addTerrainAtRandomBlock('mushroom_tan', layerNum);
            }
            for (var i = 0; i < Utils.worldBlocksNum * 0.0025; i++) {
                WorldServer_1.WorldServer.addTerrainAtRandomBlock('mushroom_red', layerNum);
            }
            for (var i = 0; i < Utils.worldBlocksNum * 0.0050; i++) {
                WorldServer_1.WorldServer.addTerrainAtRandomBlock('mushroom_brown', layerNum);
            }
            var numMushrooms = 10;
            if (layerNum == -2) {
                numMushrooms = 25;
            }
            else if (layerNum == -3) {
                numMushrooms = 50;
            }
            for (var i = 0; i < Utils.worldBlocksNum * numMushrooms / 10000; i++) {
                WorldServer_1.WorldServer.addTerrainAtRandomBlock('mushroom_blue', layerNum);
            }
            // @formatter:on
        };
        WorldGenerator.isInvalidTerrainCoord = function (itemId, layerNum, terrainX, terrainY) {
            return ((WorldServer_1.WorldServer.terrainDataArr[itemId]['isOverwritingTerrain']
                &&
                    WorldServer_1.WorldServer.worldArr[layerNum][terrainY][terrainX] == null)
                ||
                    (!WorldServer_1.WorldServer.terrainDataArr[itemId]['isOverwritingTerrain']
                        &&
                            WorldServer_1.WorldServer.worldArr[layerNum][terrainY][terrainX] != null)
                ||
                    (WorldServer_1.WorldServer.terrainDataArr[itemId]['floorIdsArr'] && WorldServer_1.WorldServer.terrainDataArr[itemId]['floorIdsArr'].indexOf(Utils.tileIndexToItemIdArr[WorldServer_1.WorldServer.floorArr[layerNum][terrainY][terrainX][0]]) == -1)
                ||
                    (Items.itemsArr[itemId]['isRequiringWater']
                        &&
                            !WorldServer_1.WorldServer.checkBlockNearWater(layerNum, terrainX, terrainY, (itemId.indexOf('tree_apple') == 0) ? 3 : 1))
                ||
                    (itemId.indexOf('mushroom_') === 0 // Only for mushrooms,
                        &&
                            layerNum < 0
                        &&
                            (terrainY + 1 >= Utils.worldHeight // Mushroom is at edge of world, or,
                                ||
                                    WorldServer_1.WorldServer.worldArr[layerNum][terrainY + 1][terrainX] == null // Block below mushroom is empty, or,
                                ||
                                    (WorldServer_1.WorldServer.worldArr[layerNum][terrainY + 1][terrainX][0] != Items.itemsArr['stone']['tileIndexesArr'][0] // Block below mushroom isn't stone.
                                        &&
                                            WorldServer_1.WorldServer.worldArr[layerNum][terrainY + 1][terrainX][0] != Items.itemsArr['brick_stone']['tileIndexesArr'][0] // Block below mushroom isn't stone brick.
                                        &&
                                            WorldServer_1.WorldServer.worldArr[layerNum][terrainY + 1][terrainX][0] != Items.itemsArr['brick_stone_small']['tileIndexesArr'][0] // Block below mushroom isn't stone brick.
                                        &&
                                            WorldServer_1.WorldServer.worldArr[layerNum][terrainY + 1][terrainX][0] != Items.itemsArr['obsidian']['tileIndexesArr'][0] // Block below mushroom isn't stone brick.
                                    )))
                ||
                    ((itemId == 'tree'
                        ||
                            itemId == 'tree_birch'
                        ||
                            itemId == 'cactus')
                        &&
                            !WorldServer_1.WorldServer.isCoord3x3Empty(layerNum, terrainX, terrainY)));
        };
        WorldGenerator.addTerrainAtRandomBlock = function (itemId, layerNum) {
            var terrainX = null;
            var terrainY = null;
            var numIterations = 0;
            while (numIterations < 50
                &&
                    (terrainX == null
                        ||
                            terrainY == null
                        ||
                            WorldServer_1.WorldServer.isInvalidTerrainCoord(itemId, layerNum, terrainX, terrainY))) {
                terrainX = Utils.randBetween(0, Utils.worldWidth - 1);
                terrainY = Utils.randBetween(0, Utils.worldHeight - 1);
                numIterations++;
            }
            if (numIterations == 50) {
                return;
            }
            WorldServer_1.WorldServer.addTerrainAtBlock(itemId, layerNum, terrainX, terrainY);
        };
        WorldGenerator.addTerrainAtBlock = function (itemId, layerNum, terrainX, terrainY) {
            WorldServer_1.WorldServer.addTerrainBlock(itemId, layerNum, terrainX, terrainY);
            if (WorldServer_1.WorldServer.terrainDataArr[itemId]['maxBlocks'] > 1) {
                var numIterations = 0;
                var numBlocksInTerrain = Utils.randBetween(WorldServer_1.WorldServer.terrainDataArr[itemId]['minBlocks'], WorldServer_1.WorldServer.terrainDataArr[itemId]['maxBlocks']);
                for (var i = 0; i < numBlocksInTerrain - 1; i++) {
                    var newTerrainX = -1;
                    var newTerrainY = -1;
                    numIterations = 0;
                    while (numIterations < 50
                        &&
                            (newTerrainX < 0
                                ||
                                    newTerrainX >= Utils.worldWidth
                                ||
                                    newTerrainY < 0
                                ||
                                    newTerrainY >= Utils.worldHeight
                                ||
                                    (WorldServer_1.WorldServer.terrainDataArr[itemId]['isOverwritingTerrain']
                                        &&
                                            WorldServer_1.WorldServer.worldArr[layerNum][newTerrainY][newTerrainX] == null)
                                ||
                                    (!WorldServer_1.WorldServer.terrainDataArr[itemId]['isOverwritingTerrain']
                                        &&
                                            WorldServer_1.WorldServer.worldArr[layerNum][newTerrainY][newTerrainX] != null)
                                ||
                                    (WorldServer_1.WorldServer.terrainDataArr[itemId]['floorIdsArr'] && WorldServer_1.WorldServer.terrainDataArr[itemId]['floorIdsArr'].indexOf(Utils.tileIndexToItemIdArr[WorldServer_1.WorldServer.floorArr[layerNum][newTerrainY][newTerrainX][0]]) == -1)
                                ||
                                    (Items.itemsArr[itemId]['isRequiringWater']
                                        &&
                                            !WorldServer_1.WorldServer.checkBlockNearWater(layerNum, newTerrainX, newTerrainY, (itemId.indexOf('tree_apple') == 0) ? 3 : 1)))) {
                        var xDiff = 0;
                        var yDiff = 0;
                        if (Utils.randBool()) {
                            xDiff = (Utils.randBool()) ? 1 : -1;
                            yDiff = 0;
                        }
                        else {
                            xDiff = 0;
                            yDiff = (Utils.randBool()) ? 1 : -1;
                        }
                        newTerrainX = terrainX + xDiff;
                        newTerrainY = terrainY + yDiff;
                        numIterations++;
                    }
                    if (numIterations >= 50) {
                        break;
                    }
                    if (!(newTerrainY in WorldServer_1.WorldServer.worldArr[layerNum]) || !(newTerrainX in WorldServer_1.WorldServer.worldArr[layerNum][newTerrainY])) {
                        console.log('newTerrainX', newTerrainX, 'newTerrainY', newTerrainY, 'numIterations', numIterations);
                    }
                    WorldServer_1.WorldServer.addTerrainBlock(itemId, layerNum, newTerrainX, newTerrainY);
                    terrainY = newTerrainY;
                    terrainX = newTerrainX;
                }
            }
        };
        WorldGenerator.addTerrainBlock = function (itemId, layerNum, terrainX, terrainY) {
            var floorBlockId = null;
            if (itemId.indexOf('flower_') == -1 // Not a flower.
                &&
                    itemId in Items.itemsArr
                &&
                    Items.itemsArr[itemId]['cropRequiredFloorIdsArr']
                &&
                    Items.itemsArr[itemId]['cropRequiredFloorIdsArr'].indexOf('dirt') != -1) {
                floorBlockId = 'dirt';
            }
            if (Items.itemsArr[itemId]['isFloor']) {
                WorldServer_1.WorldServer.floorArr[layerNum][terrainY][terrainX] = [Utils.randElFromArr(Items.itemsArr[itemId]['tileIndexesArr']), Items.itemsArr[itemId]['structureDurability']];
            }
            else {
                WorldServer_1.WorldServer.worldArr[layerNum][terrainY][terrainX] = [Utils.randElFromArr(Items.itemsArr[itemId]['tileIndexesArr']), Items.itemsArr[itemId]['structureDurability']];
                if (floorBlockId) {
                    WorldServer_1.WorldServer.floorArr[layerNum][terrainY][terrainX] = [Utils.randElFromArr(Items.itemsArr[floorBlockId]['tileIndexesArr']), Items.itemsArr[floorBlockId]['structureDurability']];
                }
            }
        };
        WorldGenerator.biomeDetailsArr = {
            // @formatter:off
            // Cold & dry.
            'tundra': { 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.01, 'flower_blue': 0.04 } },
            // 'plurple':      {'floorItemId': 'plurple',      'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.66, 'altMax': 1.00, 'blocksToSpawnArr': {'stone': 0.01, 'ore_silver': 0.01, 'flower_purple': 0.04, 'water': 0.01}},
            // Cold & fertile.
            'taiga': { 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.10, 'tree_birch': 0.005, 'tree_apple': 0.05, 'stone': 0.01 } },
            'christmas': { 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.50, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.10, 'tree_birch': 0.005, /*'tree_christmas': 0.10, */ 'tree_apple': 0.05, 'stone': 0.01 } },
            // Cold & wet.
            'ice': { 'floorItemId': 'ice', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.66, 'humidityMax': 1.00, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'stone': 0.03 } },
            'rings': { 'floorItemId': 'ice', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.66, 'humidityMax': 1.00, 'altMin': 0.50, 'altMax': 1.00, 'blocksToSpawnArr': { 'stone': 0.03 } },
            // Temperate & dry.
            'meadow': { 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.01, 'tree_birch': 0.005, 'flower_red': 0.02, 'flower_purple': 0.02, 'flower_white': 0.02, 'flower_yellow': 0.02 } },
            'plains': { 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.50, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.01, 'tree_birch': 0.005, 'flower_red': 0.02, 'water': 0.01 } },
            // Temperate & fertile.
            'forest': { 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.00, 'altMax': 0.66, 'blocksToSpawnArr': { 'tree': 0.10, 'tree_birch': 0.005, 'tree_apple': 0.10, 'stone': 0.01, 'flower_red': 0.02 } },
            'forest_birch': { 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.66, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.005, 'tree_birch': 0.10, 'tree_apple': 0.10, 'stone': 0.01, 'flower_white': 0.02 } },
            // Temperate & wet.
            'swamp': { 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.66, 'humidityMax': 1.00, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.01, 'water': 0.10 } },
            'bog': { 'floorItemId': 'dirt', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.66, 'humidityMax': 1.00, 'altMin': 0.50, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.01, 'water': 0.10 } },
            // Hot & dry.
            'desert': { 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.00, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 0.66, 'blocksToSpawnArr': { 'cactus': 0.10, 'stone': 0.01 } },
            'mesa': { 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.00, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.66, 'altMax': 1.00, 'blocksToSpawnArr': { 'cactus': 0.01, 'stone': 0.20, 'ore_gold': 0.005, 'ore_coal': 0.005 } },
            // Hot & fertile.
            'savanna': { 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.00, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.00, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.05, 'water': 0.02 } },
            // 'scarlet':      {'floorItemId': 'dungeon',      'temperatureMin': 0.66, 'temperatureMax': 1.00, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.66, 'altMax': 1.00, 'blocksToSpawnArr': {'mushroom_red': 0.04, 'stone': 0.01, 'lava': 0.05, 'ore_gold': 0.005}},
            // Hot & wet.
            'jungle': { 'floorItemId': 'grass', 'temperatureMin': 0.66, 'temperatureMax': 1.00, 'humidityMin': 0.66, 'humidityMax': 1.00, 'altMin': 0.00, 'altMax': 1.00, 'blocksToSpawnArr': { 'tree': 0.25, 'tree_apple': 0.05, 'water': 0.02 } },
        };
        WorldGenerator.biomeCoordsArr = [];
        WorldGenerator.terrainDataArr = {
            // @formatter:off
            'tree': { 'floorIdsArr': Items.itemsArr['tree']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'tree_birch': { 'floorIdsArr': Items.itemsArr['tree_birch']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'stone': { 'floorIdsArr': ['snow', 'grass', 'sand', 'ice', 'cave', 'dungeon'], 'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks': 10 },
            'tree_apple': { 'floorIdsArr': Items.itemsArr['tree_apple']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks': 7 },
            'cactus': { 'floorIdsArr': Items.itemsArr['cactus']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'flower_red': { 'floorIdsArr': ['grass'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'flower_blue': { 'floorIdsArr': ['snow'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'flower_purple': { 'floorIdsArr': ['grass'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'flower_white': { 'floorIdsArr': ['grass'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'flower_yellow': { 'floorIdsArr': ['grass'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'wheat_stage_4': { 'floorIdsArr': Utils.playerSpawnFloorIdsArr, 'isOverwritingTerrain': false, 'minBlocks': 3, 'maxBlocks': 4 },
            'ore_coal': { 'floorIdsArr': ['cave', 'dungeon'], 'isOverwritingTerrain': true, 'minBlocks': 4, 'maxBlocks': 9 },
            'ore_silver': { 'floorIdsArr': ['cave', 'dungeon'], 'isOverwritingTerrain': true, 'minBlocks': 4, 'maxBlocks': 6 },
            'ore_gold': { 'floorIdsArr': ['cave', 'dungeon'], 'isOverwritingTerrain': true, 'minBlocks': 4, 'maxBlocks': 6 },
            'ore_diamond': { 'floorIdsArr': ['cave', 'dungeon'], 'isOverwritingTerrain': true, 'minBlocks': 4, 'maxBlocks': 6 },
            'mushroom_tan': { 'floorIdsArr': Items.itemsArr['mushroom_tan']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'mushroom_red': { 'floorIdsArr': Items.itemsArr['mushroom_red']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'mushroom_brown': { 'floorIdsArr': Items.itemsArr['mushroom_brown']['cropRequiredFloorIdsArr'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'mushroom_blue': { 'floorIdsArr': ['cave'], 'isOverwritingTerrain': false, 'minBlocks': 1, 'maxBlocks': 1 },
            'water': { 'floorIdsArr': null, 'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks': 5 },
            'lava': { 'floorIdsArr': null, 'isOverwritingTerrain': false, 'minBlocks': 5, 'maxBlocks': 5 },
        };
        return WorldGenerator;
    }());
    exports.WorldGenerator = WorldGenerator;
});
//# sourceMappingURL=WorldGenerator.js.map