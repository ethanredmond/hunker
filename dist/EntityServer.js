"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = require('./Utils');
var Items = require('./public/generated/Items');
var UtilsServer_1 = require("./UtilsServer");
var DayNightCycleServer_1 = require("./DayNightCycleServer");
var PlayerServer_1 = require("./PlayerServer");
var WorldServer_1 = require("./WorldServer");
var HotbarServer_1 = require("./HotbarServer");
var EntityServer = /** @class */ (function () {
    function EntityServer(entityDetailsArr) {
        this.isPanicking = false;
        this.id = entityDetailsArr['id'] || Utils.randId(8);
        this.type = entityDetailsArr['type'];
        this.layerNum = entityDetailsArr['layerNum'];
        this.x = entityDetailsArr['x'];
        this.y = entityDetailsArr['y'];
        this.angle = entityDetailsArr['angle'] || Utils.randBetween(0, 365);
        this.sizeMult = entityDetailsArr['sizeMult'] || Utils.randBetween(Utils.allEntityTypesArr[this.type]['minSizeMult'] * 100, Utils.allEntityTypesArr[this.type]['maxSizeMult'] * 100) / 100;
        this.isBaby = entityDetailsArr['isBaby'];
        this.health = entityDetailsArr['health'] || (Utils.allEntityTypesArr[this.type]['health'] * this.sizeMult);
        this.boundsArr = entityDetailsArr['boundsArr'] || null;
        this.isColliding = Utils.allEntityTypesArr[this.type]['isColliding'];
        this.nextAttackTickNum = 0;
        this.isFed = false;
        this.hasWool = true;
        this.nextFeedTime = 0;
        if (this.isBaby) {
            this.health /= 2;
            if (entityDetailsArr['growUpTime']) {
                this.growUpTime = entityDetailsArr['growUpTime'];
            }
        }
        if (Utils.allEntityTypesArr[this.type]['isProjectile']) {
            this.isProjectile = true;
            if (entityDetailsArr['fromPlayerId']) {
                this.fromPlayerId = entityDetailsArr['fromPlayerId'];
            }
            if (entityDetailsArr['fromEntityId']) {
                this.fromEntityId = entityDetailsArr['fromEntityId'];
            }
            this.projectileChargeMult = entityDetailsArr['projectileChargeMult'];
            this.projectileMaxDistance = Utils.allEntityTypesArr[this.type]['projectileMaxDistance'] * this.projectileChargeMult * this.projectileChargeMult * this.projectileChargeMult;
            this.originalPosArr = {
                'x': entityDetailsArr['x'],
                'y': entityDetailsArr['y'],
            };
        }
        if (Utils.allEntityTypesArr[this.type]['isExplosive']) {
            this.isExplosive = true;
            this.fuseLengthNum = Utils.allEntityTypesArr[this.type]['fuseLengthNum'];
        }
        if (this.type == 'axehead') {
            this.axeheadNextChargeTickNum = 0;
            this.axeheadStopChargeTickNum = 0;
            this.axeheadChargeCountdownNum = 5;
            this.axeheadChargeTargetArr = null;
        }
        if (this.type == 'archer') {
            this.archerProjectileCharge = 0;
            this.archerProjectileMaxCharge = Math.round(12 * this.sizeMult);
        }
        EntityServer.allEntityObjs[this.id] = this;
        if (this.isSavingInRedis()) {
            UtilsServer_1.UtilsServer.redisClient.sadd('entityIds', this.id.toString());
            var entityDataArr = {
                'type': this.type,
                'health': this.health,
                'layerNum': this.layerNum,
                'x': this.x,
                'y': this.y,
                'angle': this.angle,
                'isBaby': (this.isBaby ? '1' : '0'),
                'sizeMult': this.sizeMult,
            };
            if (this.isBaby && this.growUpTime) {
                entityDataArr['growUpTime'] = this.growUpTime;
            }
            if (this.boundsArr) {
                entityDataArr['boundsArr'] = JSON.stringify(this.boundsArr);
            }
            UtilsServer_1.UtilsServer.redisClient.hmset('entity:' + this.id, entityDataArr);
        }
    }
    EntityServer.prototype.updateHealth = function (healthUpdate) {
        if (healthUpdate < 0) {
            if (Utils.allEntityTypesArr[this.type]['hasPanicMode']) {
                this.isPanicking = true;
            }
            this.nextIdleStartTime = Utils.time();
        }
        this.health += healthUpdate;
        this.health = Math.round(this.health * 10) / 10;
        if (this.health <= 0) {
            this.kill();
            return true;
        }
        else {
            if (this.isSavingInRedis()) {
                UtilsServer_1.UtilsServer.redisClient.hset('entity:' + this.id, 'health', this.health.toString());
            }
            return false;
        }
    };
    EntityServer.prototype.update = function (tickNum) {
        var _a;
        if (this.isProjectile && !this.projectileChargeMult) { // Do nothing if projectile not moving.
            this.checkPlayerCanPickupProjectile();
            return;
        }
        var nowTime = Utils.time();
        var currentXRounded = Math.floor(this.x / Utils.tileSize);
        var currentYRounded = Math.floor(this.y / Utils.tileSize);
        var collidingWithBlockId;
        if (this.isColliding && !this.isProjectile) {
            var isColliding = void 0;
            _a = UtilsServer_1.UtilsServer.isCoordCollidingWithWorld(WorldServer_1.WorldServer.worldArr[this.layerNum], currentXRounded, currentYRounded), isColliding = _a[0], collidingWithBlockId = _a[1];
            if (isColliding) {
                var newCoordArr = WorldServer_1.WorldServer.findEmptySquareInRadius(currentXRounded, currentYRounded, this.layerNum);
                if (newCoordArr) {
                    this.x = newCoordArr['x'];
                    this.y = newCoordArr['y'];
                }
            }
        }
        // Burn monsters on overworld in daytime.
        if (this.layerNum == 0
            &&
                Utils.randBetween(1, 4) == 1
            &&
                Utils.allEntityTypesArr[this.type]['isMonster']
            &&
                DayNightCycleServer_1.DayNightCycleServer.stage == 'day'
            &&
                this.updateHealth(Utils.randBetween(-13, -7))) {
            return;
        }
        var oldCoordArr = {
            'x': this.x,
            'y': this.y,
        };
        var floorItemId = null;
        var targetObj = null;
        var targetCoordArr = null;
        var targetDistance = null;
        var isRunningAwayFromTarget = false;
        if (this.isProjectile) {
            if (Utils.distance(this.originalPosArr, this) >= this.projectileMaxDistance) { // Moved too far, despawn projectile.
                this.despawnProjectile();
                return;
            }
            targetCoordArr = {
                'x': this.x + (500 * Math.cos(Utils.toRadians(this.angle))),
                'y': this.y + (500 * Math.sin(Utils.toRadians(this.angle))),
            };
            targetDistance = Utils.distance(this, targetCoordArr);
        }
        else {
            if (collidingWithBlockId == 'trap' && this.type != 'axehead') {
                return;
            }
            // Take damage if in lava.
            floorItemId = Utils.tileIndexToItemIdArr[WorldServer_1.WorldServer.floorArr[this.layerNum][currentYRounded][currentXRounded][0]];
            if (floorItemId == 'lava' && this.updateHealth(-5)) {
                return; // Died.
            }
            // Grow up, baby.
            if (this.isBaby && this.growUpTime && this.growUpTime <= nowTime) {
                this.health *= 2;
                this.isBaby = false;
                delete this.growUpTime;
            }
            // Regrow wool for sheep.
            if (this.type == 'sheep' && !this.hasWool && this.regrowWoolTime <= nowTime) {
                this.hasWool = true;
                delete this.regrowWoolTime;
            }
            if (this.angryAtEntityId) {
                if (this.angryAtEntityId in EntityServer.allEntityObjs) {
                    targetObj = EntityServer.allEntityObjs[this.angryAtEntityId];
                    targetCoordArr = {
                        'x': targetObj.x,
                        'y': targetObj.y,
                    };
                    targetDistance = Utils.distance(this, targetCoordArr);
                }
                else {
                    this.angryAtEntityId = null;
                }
            }
            if (!targetObj) {
                var targetDetailsArr = this.findTargetPlayer();
                if (targetDetailsArr) {
                    targetObj = targetDetailsArr['targetObj'];
                    targetCoordArr = targetDetailsArr['targetCoordArr'];
                    targetDistance = targetDetailsArr['targetDistance'];
                    isRunningAwayFromTarget = targetDetailsArr['isRunningAwayFromTarget'];
                }
            }
            if (!targetObj) {
                this.archerProjectileCharge = 0;
            }
            if (!targetCoordArr && this.isBaby) {
                var closestParentCoordArr = null;
                var closestParentDistance = 100000;
                for (var entityId in EntityServer.allEntityObjs) {
                    var entityObj = EntityServer.allEntityObjs[entityId];
                    if (entityObj.type == this.type
                        &&
                            entityObj.layerNum == this.layerNum
                        &&
                            !entityObj.isBaby) {
                        var entityDistance = Utils.distance(this, entityObj);
                        if (!closestParentDistance
                            ||
                                entityDistance < closestParentDistance) {
                            closestParentCoordArr = EntityServer.allEntityObjs[entityId];
                            closestParentDistance = entityDistance;
                        }
                    }
                }
                if (closestParentDistance > 3 * Utils.tileSize) {
                    targetCoordArr = closestParentCoordArr;
                    targetDistance = closestParentDistance;
                    this.nextIdleStartTime = Utils.time();
                }
            }
            if (this.axeheadChargeTargetArr) {
                targetCoordArr = this.axeheadChargeTargetArr;
                targetDistance = Utils.distance(this, targetCoordArr);
            }
            if (this.isPanicking && !this.lastPanicTickNum) {
                this.lastPanicTickNum = tickNum;
            }
            if (this.isPanicking && this.lastPanicTickNum && tickNum < this.lastPanicTickNum + EntityServer.panicTicksNum) {
                if (!this.panicRotation || (tickNum - this.lastPanicTickNum) % 3 == 0) {
                    this.panicRotation = Utils.randBetween(-Math.PI, Math.PI);
                }
                this.panicRotation += (Utils.randBool()) ? 1 : -1;
                targetCoordArr = Utils.dereferenceObj({
                    'x': this.x - (500 * Math.cos(this.panicRotation)),
                    'y': this.y - (500 * Math.sin(this.panicRotation)),
                });
                targetDistance = Utils.distance(this, targetCoordArr);
            }
            else {
                this.isPanicking = false;
                this.lastPanicTickNum = null;
            }
            if (this.knockbackCoordArr) {
                targetCoordArr = Utils.dereferenceObj(this.knockbackCoordArr);
                targetDistance = Utils.distance(this, targetCoordArr);
                this.knockbackCoordArr = null;
                this.isKnockbackTick = true;
            }
            else {
                this.isKnockbackTick = false;
                this.knockbackMult = null;
            }
        }
        if (!( // Don't move if exploding.
        this.isExplosive
            &&
                !this.isProjectile
            &&
                this.fuseLengthNum <= Utils.allEntityTypesArr[this.type]['fuseAutoDecrementLengthNum'])
            &&
                !Utils.allEntityTypesArr[this.type]['isRideable']
            &&
                (this.type.indexOf('slime') != 0
                    ||
                        tickNum % 3 == 0)) {
            if (targetCoordArr) {
                this.isIdling = false;
            }
            else {
                // Idle.
                if (!this.nextIdleStartTime) {
                    this.nextIdleStartTime = nowTime + (Utils.randBetween(0, 8) * 1000);
                }
                if (this.nextIdleStartTime <= nowTime) {
                    this.idleRotation = Utils.randBetween(-Math.PI, Math.PI);
                    this.idleEndTime = nowTime + (Utils.randBetween(2, 3) * 1000);
                    this.nextIdleStartTime = this.idleEndTime + (Utils.randBetween(0, 8) * 1000);
                }
                if (this.idleEndTime >= nowTime) {
                    targetCoordArr = {
                        'x': this.x - (500 * Math.cos(this.idleRotation)),
                        'y': this.y - (500 * Math.sin(this.idleRotation)),
                    };
                    targetDistance = Utils.distance(this, targetCoordArr);
                    this.isIdling = true;
                }
                else {
                    return;
                }
            }
            if (targetCoordArr && (targetCoordArr['x'] != oldCoordArr['x'] || targetCoordArr['y'] != oldCoordArr['y'] || this.type == 'archer')) {
                // https://gamedev.stackexchange.com/questions/50978/moving-a-sprite-towards-an-x-and-y-coordinate
                // Move towards the target.
                var entitySpeed = Utils.allEntityTypesArr[this.type]['speed'] * (Utils.allEntityTypesArr[this.type]['tickIntervalMillis'] * (0.5 / 500)); // (0.5 is just to turn a phaser speed to a server speed [approximately]).
                if (this.isProjectile) {
                    entitySpeed *= this.projectileChargeMult;
                    entitySpeed *= this.projectileChargeMult;
                }
                else {
                    if (this.type == 'axehead') {
                        if (targetObj && this.axeheadStopChargeTickNum <= tickNum && this.axeheadNextChargeTickNum < tickNum) {
                            this.axeheadChargeCountdownNum--;
                            if (this.axeheadChargeCountdownNum < 0) {
                                this.axeheadChargeCountdownNum = 5;
                                this.axeheadNextChargeTickNum = tickNum + 15;
                                this.axeheadStopChargeTickNum = tickNum + 5;
                                this.axeheadChargeTargetArr = { 'x': targetObj.x, 'y': targetObj.y };
                            }
                        }
                        if (this.axeheadStopChargeTickNum <= tickNum) {
                            this.axeheadNextChargeTickNum = null;
                            this.axeheadStopChargeTickNum = null;
                            this.axeheadChargeTargetArr = null;
                        }
                    }
                    if (this.isIdling) { // Entities move slower when idling.
                        entitySpeed *= 0.75;
                    }
                    if (this.isPanicking) { // Entities move faster when panicking.
                        entitySpeed *= 1.5;
                    }
                    if (this.isBaby) { // Baby entities move faster.
                        entitySpeed *= 1.25;
                    }
                    // Slow down if in lava or water.
                    if (floorItemId && Items.itemsArr[floorItemId]['speedMult']) {
                        entitySpeed *= Items.itemsArr[floorItemId]['speedMult'];
                    }
                    entitySpeed /= this.sizeMult;
                    if (targetObj && Utils.allEntityTypesArr[this.type]['isPassive']) {
                        if (targetObj instanceof PlayerServer_1.PlayerServer && targetObj.selectedItemId == Utils.allEntityTypesArr[this.type]['feedingItemId']) {
                            // Follow players with feed item selected and slow down.
                            entitySpeed *= 0.75;
                        }
                        else {
                            isRunningAwayFromTarget = true;
                        }
                    }
                    if (this.type == 'axehead') {
                        if (!this.axeheadChargeTargetArr) {
                            entitySpeed *= 0.25;
                        }
                    }
                    if (this.isKnockbackTick) {
                        if (!isRunningAwayFromTarget) {
                            entitySpeed *= -1;
                        }
                        entitySpeed *= this.knockbackMult;
                    }
                    else if (this.type == 'archer') {
                        if (this.archerProjectileCharge >= this.archerProjectileMaxCharge) {
                            entitySpeed = 0;
                        }
                        else if (targetObj) {
                            if (targetDistance <= 4 * Utils.tileSize) {
                                entitySpeed *= -1;
                            }
                            else if (targetDistance <= 5 * Utils.tileSize) {
                                entitySpeed = 0;
                            }
                        }
                        if (this.archerProjectileCharge) {
                            entitySpeed *= 0.5;
                        }
                    }
                }
                // Calculate direction towards target.
                var dx = targetCoordArr['x'] - this.x;
                var dy = targetCoordArr['y'] - this.y;
                // Normalize.
                var toTargetX = 0;
                var toTargetY = 0;
                if (targetDistance >= Utils.allEntityTypesArr[this.type]['hitboxMult'] * Utils.tileSize || this.type == 'archer' || isRunningAwayFromTarget) {
                    toTargetX = dx / targetDistance;
                    toTargetY = dy / targetDistance;
                }
                else {
                    if (this.type == 'axehead') {
                        this.axeheadStopChargeTickNum = null;
                    }
                }
                var diffX = ((toTargetX * entitySpeed) * (isRunningAwayFromTarget ? -1 : 1));
                var diffY = ((toTargetY * entitySpeed) * (isRunningAwayFromTarget ? -1 : 1));
                var newX = this.x + diffX;
                var newY = this.y + diffY;
                newX = Math.round(newX * 10) / 10;
                newY = Math.round(newY * 10) / 10;
                var boundsArr = this.boundsArr || { 'x1': 0, 'y1': 0, 'x2': Utils.worldWidth, 'y2': Utils.worldHeight };
                if (newX < boundsArr['x1'] * Utils.tileSize
                    ||
                        newX >= boundsArr['x2'] * Utils.tileSize) {
                    newX = oldCoordArr['x'];
                }
                if (newY < boundsArr['y1'] * Utils.tileSize
                    ||
                        newY >= boundsArr['y2'] * Utils.tileSize) {
                    newY = oldCoordArr['y'];
                }
                var newXRounded = Math.floor(newX / Utils.tileSize);
                var newYRounded = Math.floor(newY / Utils.tileSize);
                if (this.isColliding) {
                    var checkedCoordsArr = [];
                    // Check collisions of new position.
                    checkedCoordsArr.push(newXRounded + ',' + newYRounded);
                    if (UtilsServer_1.UtilsServer.isCoordCollidingWithWorld(WorldServer_1.WorldServer.worldArr[this.layerNum], newXRounded, newYRounded)[0]) {
                        if (this.isProjectile) {
                            this.despawnProjectile();
                        }
                        this.panicRotation = null; // Panic somewhere else.
                        if (this.type == 'archer') {
                            newX = this.x;
                            newY = this.y;
                        }
                        else {
                            return;
                        }
                    }
                    // Check intermediate collisions from current pos to new pos.
                    var checksNum = Math.ceil(Math.max(Math.abs(diffX), Math.abs(diffY)) / Utils.tileSize);
                    // if (this.isBaby && targetObj) {
                    //     console.log(this.type, diffX, diffY, entitySpeed, checksNum);
                    // }
                    for (var i = 1; i < checksNum; i++) {
                        var intermediateX = this.x + (diffX * (i / checksNum));
                        var intermediateY = this.y + (diffY * (i / checksNum));
                        var intermediateXRounded = Math.floor(intermediateX / Utils.tileSize);
                        var intermediateYRounded = Math.floor(intermediateY / Utils.tileSize);
                        var intermediateCoordStr = intermediateXRounded + ',' + intermediateYRounded;
                        if (checkedCoordsArr.indexOf(intermediateCoordStr) == -1) {
                            checkedCoordsArr.push(intermediateCoordStr);
                            // if (intermediateXRounded != newXRounded && intermediateYRounded != newYRounded) {
                            //     console.log(intermediateXRounded);
                            //     console.log(intermediateYRounded);
                            //     console.log(WorldServer.isCoordCollidingWithWorld(this.layerNum, intermediateXRounded, intermediateYRounded)[0]);
                            // }
                            if (UtilsServer_1.UtilsServer.isCoordCollidingWithWorld(WorldServer_1.WorldServer.worldArr[this.layerNum], intermediateXRounded, intermediateYRounded)[0]) {
                                if (this.isProjectile) {
                                    this.despawnProjectile();
                                }
                                this.panicRotation = null; // Panic somewhere else.
                                if (this.type == 'archer') {
                                    newX = this.x;
                                    newY = this.y;
                                    break;
                                }
                                else {
                                    return;
                                }
                            }
                        }
                    }
                }
                if (!this.isProjectile) {
                    var newFloorItemId = Utils.tileIndexToItemIdArr[WorldServer_1.WorldServer.floorArr[this.layerNum][newYRounded][newXRounded][0]];
                    var floorIdsArr = Utils.allEntityTypesArr[this.type]['idleFloorIdsArr'];
                    if (this.isPanicking
                        ||
                            targetObj // && Utils.allEntityTypesArr[this.type]['isPassive'] && targetObj.selectedItemId == Utils.allEntityTypesArr[this.type]['feedingItemId']
                    ) {
                        floorIdsArr = Utils.allEntityTypesArr[this.type]['moveFloorIdsArr'];
                    }
                    if (newFloorItemId
                        &&
                            floorIdsArr
                        &&
                            floorIdsArr.indexOf(newFloorItemId) == -1) {
                        this.panicRotation = null; // Panic somewhere else.
                        if (this.type == 'archer') {
                            newX = this.x;
                            newY = this.y;
                        }
                        else {
                            return;
                        }
                    }
                }
                if (!isNaN(newX) && !isNaN(newY)) {
                    this.x = newX;
                    this.y = newY;
                    if (this.isProjectile) {
                        this.projectileDamageEntities();
                        if (!this.fromPlayerId || this.type == 'snowballEntity') {
                            this.projectileDamagePlayers();
                        }
                    }
                    else {
                        if (!this.archerProjectileCharge || this.archerProjectileCharge < this.archerProjectileMaxCharge) {
                            // Rotate to face direction moving in.
                            this.angle = Utils.toDegrees(Utils.rotation(targetCoordArr, oldCoordArr));
                            if (isRunningAwayFromTarget) {
                                this.angle += 180;
                            }
                            this.angle = Math.round(this.angle * 10) / 10;
                        }
                        if (targetObj
                            &&
                                !this.isBaby) {
                            if (Utils.allEntityTypesArr[this.type]['damage']) {
                                if (targetDistance <= Utils.tileSize * 0.9 + (Utils.allEntityTypesArr[this.type]['damageRadiusMult'] - 1) * Utils.tileSize // Player still in range.
                                    &&
                                        this.thisAttackTickNum == tickNum) {
                                    var healthUpdate = -Utils.allEntityTypesArr[this.type]['damage'] * this.sizeMult;
                                    if (targetObj instanceof PlayerServer_1.PlayerServer) {
                                        if (targetObj.updateHealth(healthUpdate, true, this.type)) {
                                            if (this.type == 'axehead') {
                                                var _this_1 = this;
                                                setTimeout(function () {
                                                    // Emit to all players.
                                                    UtilsServer_1.UtilsServer.io.emit('handleMiscEmit', {
                                                        'status': 'success',
                                                        'allSoundEffectKeysArr': [_this_1.type + '_celebrate'],
                                                    });
                                                }, 1000);
                                            }
                                        }
                                    }
                                    else {
                                        targetObj.updateHealth(healthUpdate);
                                        if (Utils.allEntityTypesArr[targetObj.type]['isMonster']) {
                                            targetObj.angryAtEntityId = this.id;
                                        }
                                    }
                                    if (targetObj instanceof PlayerServer_1.PlayerServer && targetObj.hasRingEffect('ring_thorns')) {
                                        this.damageEntityFromPlayer(healthUpdate / 2, targetObj, Items.itemsArr['ring_thorns']['attackKnockbackMult']);
                                    }
                                }
                                else if (this.thisAttackTickNum == tickNum + 1) {
                                    if (this.isColliding) {
                                        this.angle -= 60;
                                    }
                                }
                                else if (targetDistance <= Utils.tileSize * 0.9 + (Utils.allEntityTypesArr[this.type]['damageRadiusMult'] - 1) * Utils.tileSize // Player in range.
                                    &&
                                        tickNum >= this.nextAttackTickNum) {
                                    this.nextAttackTickNum = tickNum + 2 + Utils.allEntityTypesArr[this.type]['attackCooldownTicksNum'];
                                    this.thisAttackTickNum = tickNum + 2;
                                    if (this.isColliding) {
                                        this.angle += 60;
                                    }
                                }
                            }
                            else {
                                if (this.type == 'archer') {
                                    if (this.archerProjectileCharge >= this.archerProjectileMaxCharge) {
                                        if (!this.nextAttackTickNum) {
                                            this.nextAttackTickNum = tickNum + 1;
                                        }
                                        if (tickNum >= this.nextAttackTickNum) {
                                            EntityServer.createProjectile({
                                                'x': this.x,
                                                'y': this.y,
                                                'layerNum': this.layerNum,
                                            }, this.angle, 'bow', this.archerProjectileCharge, null, this.id, this.sizeMult);
                                            this.archerProjectileCharge = 0;
                                            this.nextAttackTickNum = null;
                                        }
                                    }
                                    else if (targetDistance <= 5 * Utils.tileSize) {
                                        this.archerProjectileCharge += 2;
                                        this.archerProjectileCharge = Math.min(this.archerProjectileCharge, this.archerProjectileMaxCharge);
                                        this.angle += (this.archerProjectileCharge / this.archerProjectileMaxCharge) * 90;
                                    }
                                    else {
                                        this.archerProjectileCharge = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    console.log('-----------');
                    console.log('newX', newX);
                    console.log('newY', newY);
                    console.log('targetCoordArr', targetCoordArr);
                    console.log('targetDistance', targetDistance);
                }
            }
            if (this.type == 'chicken' && !this.isBaby && tickNum % 10 == 0 && Utils.randBetween(1, 10) == 10) {
                new EntityServer({
                    'type': 'eggEntity',
                    'layerNum': this.layerNum,
                    'x': this.x,
                    'y': this.y,
                    'isBaby': false,
                });
            }
        }
        if (!this.isBaby && this.isExplosive) {
            if ((targetObj && targetDistance <= Utils.tileSize * 1.5) || this.fuseLengthNum <= Utils.allEntityTypesArr[this.type]['fuseAutoDecrementLengthNum']) {
                this.fuseLengthNum--;
                if (this.fuseLengthNum <= 0) {
                    this.kill();
                    this.explode();
                }
            }
            else {
                this.fuseLengthNum = Utils.allEntityTypesArr[this.type]['fuseLengthNum'];
            }
        }
    };
    EntityServer.createProjectile = function (coordArr, projectileAngle, projectileItemId, projectileCharge, fromPlayerId, fromEntityId, sizeMult) {
        if (fromEntityId === void 0) { fromEntityId = null; }
        if (sizeMult === void 0) { sizeMult = null; }
        var projectileAmmoId = Items.itemsArr[projectileItemId]['projectileAmmoId'];
        var projectileMaxCharge = Items.itemsArr[projectileItemId]['projectileMaxCharge'];
        projectileCharge = Math.min(projectileCharge, projectileMaxCharge);
        projectileCharge = Math.max(projectileCharge, 0);
        var projectileChargeMult = Math.min(projectileCharge / projectileMaxCharge, 1);
        var projectileAccuracy = Items.itemsArr[projectileAmmoId]['projectileAccuracy'] * (2 - projectileChargeMult) * (2 - projectileChargeMult); // More accuracy with more charge.
        var projectileRandomAccuracy = Utils.randBetween(-projectileAccuracy * 100, projectileAccuracy * 100) / 100;
        projectileAngle += projectileRandomAccuracy;
        if (fromPlayerId) {
            projectileAngle += -90 * projectileChargeMult;
        }
        else {
            projectileAngle += -90;
        }
        new EntityServer({
            'type': (projectileAmmoId + 'Entity'),
            'layerNum': coordArr['layerNum'],
            'x': coordArr['x'] + (Utils.tileSize * 0.5 * Math.cos(Utils.toRadians(projectileAngle))),
            'y': coordArr['y'] + (Utils.tileSize * 0.5 * Math.sin(Utils.toRadians(projectileAngle))),
            'angle': projectileAngle,
            'isBaby': false,
            'fromPlayerId': fromPlayerId,
            'fromEntityId': fromEntityId,
            'projectileChargeMult': projectileChargeMult,
            'sizeMult': sizeMult,
        });
    };
    EntityServer.prototype.checkPlayerCanPickupProjectile = function () {
        if (Utils.allEntityTypesArr[this.type]['yieldItemsArr'] && (this.fromPlayerId || this.type == 'eggEntity')) {
            for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
                var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[playerId];
                if (this.layerNum == playerObj.currentLayerNum) {
                    if (Utils.distance(this, playerObj) <= Utils.tileSize * 0.5) {
                        var newItemIndex = Utils.getNewItemIndex(PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr, Utils.allEntityTypesArr[this.type]['yieldItemsArr'][0]['itemId']);
                        if (newItemIndex) {
                            var prevHotbarArr = Utils.dereferenceObj(PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr);
                            PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr = HotbarServer_1.HotbarServer.addItemsFromArr(PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr, Utils.allEntityTypesArr[this.type]['yieldItemsArr']);
                            var responseData = {
                                'status': 'success',
                                'hotbarItemsArr': PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr,
                            };
                            UtilsServer_1.UtilsServer.io.to(playerId).emit('handleMiscEmit', responseData);
                            PlayerServer_1.PlayerServer.allPlayerObjs[playerId].updateHotbarRedis(prevHotbarArr);
                            this.kill(true);
                            break;
                        }
                    }
                }
            }
        }
    };
    EntityServer.prototype.findTargetPlayer = function () {
        var targetPlayerObj = null;
        var targetCoordArr = null;
        var targetDistance = null;
        var isRunningAwayFromTarget = false;
        for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
            var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[playerId];
            var distanceToPlayer = EntityServer.allDistancesArr[this.id][playerId];
            if (playerObj
                &&
                    !playerObj.isPaused
                &&
                    !playerObj.isGhost
                &&
                    (!Utils.allEntityTypesArr[this.type]['isMonster'] // Not a monster, or,
                        ||
                            Utils.playerGameModesArr[playerObj.gameModeStr]['isSeenByMonsters'] // Is a monster and mode is seen by monsters.
                    )
                &&
                    playerObj.currentLayerNum == this.layerNum
                &&
                    (targetDistance == null // There is no last player, or,
                        ||
                            distanceToPlayer < targetDistance // This player is closer than the last player.
                    )
                &&
                    (
                    // (
                    //     this.type == 'polar_bear'
                    //     &&
                    //     (
                    //         (
                    //             DayNightCycleServer.stage == 'night' // At night,
                    //             &&
                    //             distanceToPlayer <= 10 * Utils.tileSize // Attack players closer than 10 blocks.
                    //         )
                    //         ||
                    //         (
                    //             DayNightCycleServer.stage == 'day' // During the day,
                    //             &&
                    //             distanceToPlayer <= 5 * Utils.tileSize // Attack players closer than 4 blocks.
                    //         )
                    //     )
                    // )
                    // ||
                    (this.type == 'wolf'
                        &&
                            !this.isBaby
                        &&
                            distanceToPlayer <= 1.5 * Utils.tileSize // Attack players closer than 1.5 blocks.
                    )
                        ||
                            (this.type == 'wolf'
                                &&
                                    this.isBaby
                                &&
                                    distanceToPlayer <= 5 * Utils.tileSize // Run to players closer than 5 blocks.
                            )
                        ||
                            (this.type == 'zombie'
                                &&
                                    distanceToPlayer <= Math.max(Math.min(playerObj.nightsSurvivedNum, (this.layerNum == 0) ? 5 : 4), 1) * Utils.tileSize // Attack players based on num cycles survived maxed at 3 blocks.
                            )
                        ||
                            (this.type.indexOf('slime') == 0
                                &&
                                    distanceToPlayer <= Math.max(Math.min(playerObj.nightsSurvivedNum, (this.layerNum == 0) ? 5 : 4), 1) * Utils.tileSize // Attack players based on num cycles survived maxed at 4 blocks.
                            )
                        ||
                            (this.type == 'archer'
                                &&
                                    distanceToPlayer <= Math.max(Math.min(playerObj.nightsSurvivedNum, 7), 1) * Utils.tileSize // Attack players based on num cycles survived maxed at 4 blocks.
                            )
                        // ||
                        // (
                        //     this.type == 'ghost'
                        //     &&
                        //     distanceToPlayer <= 7 * Utils.tileSize // Attack players closer than 5 blocks.
                        // )
                        ||
                            (Utils.allEntityTypesArr[this.type]['feedingItemId']
                                &&
                                    !Utils.allEntityTypesArr[this.type]['isMonster']
                                &&
                                    distanceToPlayer <= 1.75 * Utils.tileSize // Follow players closer than 2 blocks.
                                &&
                                    playerObj.selectedItemId == Utils.allEntityTypesArr[this.type]['feedingItemId'])
                        ||
                            this.type == 'polar_bear'
                        ||
                            this.type == 'axehead')) {
                if (this.type == 'polar_bear') {
                    var filterOutputArr = this.playerFilter_polarBear(playerObj, distanceToPlayer);
                    if (!filterOutputArr['isReactingToPlayer']) {
                        continue;
                    }
                    isRunningAwayFromTarget = filterOutputArr['isRunningAwayFromTarget'];
                }
                else if (this.type == 'axehead') {
                    var filterOutputArr = this.playerFilter_axehead(playerObj, distanceToPlayer);
                    if (!filterOutputArr['isReactingToPlayer']) {
                        continue;
                    }
                    isRunningAwayFromTarget = filterOutputArr['isRunningAwayFromTarget'];
                }
                targetPlayerObj = playerObj;
                targetCoordArr = {
                    'x': playerObj.x,
                    'y': playerObj.y,
                };
                targetDistance = distanceToPlayer;
                this.nextIdleStartTime = Utils.time();
            }
        }
        if (targetDistance) {
            return {
                'targetObj': targetPlayerObj,
                'targetCoordArr': targetCoordArr,
                'targetDistance': targetDistance,
                'isRunningAwayFromTarget': isRunningAwayFromTarget,
            };
        }
        else {
            return null;
        }
    };
    EntityServer.prototype.playerFilter_polarBear = function (targetPlayerObj, distanceToPlayer) {
        var xRounded = Math.floor(targetPlayerObj.x / Utils.tileSize);
        var yRounded = Math.floor(targetPlayerObj.y / Utils.tileSize);
        var playerFloorItemId = Utils.tileIndexToItemIdArr[WorldServer_1.WorldServer.floorArr[targetPlayerObj.currentLayerNum][yRounded][xRounded][0]];
        if (Utils.allEntityTypesArr['polar_bear']['moveFloorIdsArr'].indexOf(playerFloorItemId) != -1) {
            return {
                'isReactingToPlayer': distanceToPlayer <= 1.5 * Utils.tileSize,
                'isRunningAwayFromTarget': false,
            };
        }
        else {
            return {
                'isReactingToPlayer': distanceToPlayer <= 2 * Utils.tileSize,
                'isRunningAwayFromTarget': true,
            };
        }
    };
    EntityServer.prototype.playerFilter_axehead = function (targetPlayerObj, distanceToPlayer) {
        if (this.boundsArr
            &&
                (targetPlayerObj.x < (this.boundsArr['x1'] - 1) * Utils.tileSize
                    ||
                        targetPlayerObj.x >= (this.boundsArr['x2'] + 1) * Utils.tileSize
                    ||
                        targetPlayerObj.y < (this.boundsArr['y1'] - 1) * Utils.tileSize
                    ||
                        targetPlayerObj.y >= (this.boundsArr['y2'] + 1) * Utils.tileSize)) {
            return {
                'isReactingToPlayer': distanceToPlayer <= 4 * Utils.tileSize,
                'isRunningAwayFromTarget': true,
            };
        }
        else {
            return {
                'isReactingToPlayer': distanceToPlayer <= 20 * Utils.tileSize,
                'isRunningAwayFromTarget': false,
            };
        }
    };
    EntityServer.prototype.projectileDamageEntities = function () {
        for (var entityId in EntityServer.allEntityObjs) {
            var entityObj = EntityServer.allEntityObjs[entityId];
            if (this.layerNum == entityObj.layerNum && !entityObj.isProjectile) {
                if (Utils.distance(this, entityObj) <= (0.5 + (Utils.allEntityTypesArr[entityObj.type]['hitboxMult'] - 1)) * Utils.tileSize) {
                    if (this.fromPlayerId && this.fromPlayerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
                        entityObj.damageEntityFromPlayer(-Utils.allEntityTypesArr[this.type]['damage'] * this.projectileChargeMult, PlayerServer_1.PlayerServer.allPlayerObjs[this.fromPlayerId], Utils.allEntityTypesArr[this.type]['knockbackMult']);
                    }
                    else {
                        entityObj.addKnockback(Utils.rotation(entityObj, this), 0.5);
                        var isDead = entityObj.updateHealth(-Utils.allEntityTypesArr[this.type]['damage'] * this.projectileChargeMult);
                        if (isDead
                            &&
                                Utils.allEntityTypesArr[entityObj.type]['isRideable']
                            &&
                                entityObj.isRiding
                            &&
                                entityObj.ridingPlayerId
                            &&
                                entityObj.ridingPlayerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
                            PlayerServer_1.PlayerServer.allPlayerObjs[entityObj.ridingPlayerId].addItemsAndEmit(Utils.allEntityTypesArr[entityObj.type]['yieldItemsArr']);
                        }
                        if (Utils.allEntityTypesArr[entityObj.type]['isMonster']) {
                            entityObj.angryAtEntityId = this.fromEntityId;
                        }
                    }
                    if (this.isExplosive) {
                        this.explode();
                    }
                    this.kill();
                    break;
                }
            }
        }
    };
    EntityServer.prototype.addKnockback = function (rotation, knockbackMult) {
        if (rotation) {
            if (knockbackMult) { // Don't do anything if no knockback.
                this.knockbackCoordArr = {
                    'x': this.x - (500 * Math.cos(rotation)),
                    'y': this.y - (500 * Math.sin(rotation)),
                };
                this.knockbackMult = knockbackMult;
            }
        }
        else {
            console.log('rotation', rotation);
            console.log('knockbackMult', knockbackMult);
        }
    };
    EntityServer.prototype.damageEntityFromPlayer = function (healthUpdate, playerObj, knockbackMult) {
        var prevHotbarArr = Utils.dereferenceObj(playerObj.hotbarItemsArr);
        playerObj.playerDamageEntity(this, healthUpdate, Utils.rotation(this, playerObj), knockbackMult);
        var responseData = {
            'status': 'success',
            'hotbarItemsArr': playerObj.hotbarItemsArr,
        };
        UtilsServer_1.UtilsServer.io.to(playerObj.id).emit('handleMiscEmit', responseData);
        playerObj.updateHotbarRedis(prevHotbarArr);
    };
    EntityServer.prototype.projectileDamagePlayers = function () {
        for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
            var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[playerId];
            if (this.layerNum == playerObj.currentLayerNum) {
                if (Utils.distance(this, playerObj) <= Utils.tileSize * 0.5) {
                    this.kill();
                    if (this.type == 'snowballEntity') {
                        var prevHotbarArr = Utils.dereferenceObj(PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr);
                        PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr = HotbarServer_1.HotbarServer.addItemsFromArr(PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr, Utils.allEntityTypesArr[this.type]['yieldItemsArr']);
                        var responseData = {
                            'status': 'success',
                            'hotbarItemsArr': PlayerServer_1.PlayerServer.allPlayerObjs[playerId].hotbarItemsArr,
                        };
                        UtilsServer_1.UtilsServer.io.to(playerId).emit('handleMiscEmit', responseData);
                        PlayerServer_1.PlayerServer.allPlayerObjs[playerId].updateHotbarRedis(prevHotbarArr);
                    }
                    playerObj.updateHealth(-Utils.allEntityTypesArr[this.type]['damage'] * this.projectileChargeMult, true, 'arrow');
                    break;
                }
            }
        }
    };
    EntityServer.prototype.kill = function (isPickupProjectile) {
        if (isPickupProjectile === void 0) { isPickupProjectile = false; }
        if (this.isSavingInRedis()) {
            UtilsServer_1.UtilsServer.redisClient.srem('entityIds', this.id.toString());
            UtilsServer_1.UtilsServer.redisClient.del('entity:' + this.id);
        }
        delete EntityServer.allEntityObjs[this.id];
        if (this.type == 'eggEntity' && !isPickupProjectile && Utils.randBetween(1, 8) == 8) {
            new EntityServer({
                'type': 'chicken',
                'layerNum': this.layerNum,
                'x': this.x,
                'y': this.y,
                'isBaby': true,
                'growUpTime': Utils.time() + (Utils.cycleLengthMillis * Utils.allEntityTypesArr['chicken']['cyclesUntilAdultNum']),
            });
        }
    };
    EntityServer.prototype.explode = function () {
        WorldServer_1.WorldServer.createExplosion({
            'layerNum': this.layerNum,
            'x': Math.floor(this.x / Utils.tileSize),
            'y': Math.floor(this.y / Utils.tileSize),
        }, Utils.allEntityTypesArr[this.type]['explosionPower'] * (this.isProjectile ? this.projectileChargeMult : this.sizeMult), Utils.allEntityTypesArr[this.type]['explosionFloorPcnt'], Utils.allEntityTypesArr[this.type]['explosionFloorId']);
    };
    EntityServer.prototype.despawnProjectile = function () {
        var projectileDespawnMillis = (this.fromPlayerId) ? Utils.allEntityTypesArr[this.type]['projectileDespawnMillis'] : 500;
        if (projectileDespawnMillis) {
            var _this_2 = this;
            setTimeout(function () {
                if (_this_2) {
                    if (_this_2.isExplosive) {
                        _this_2.explode();
                    }
                    _this_2.kill();
                }
            }, projectileDespawnMillis);
        }
        else {
            if (this.isExplosive) {
                this.explode();
            }
            this.kill();
        }
        this.projectileChargeMult = 0;
    };
    EntityServer.startEntityUpdateInterval = function () {
        var intervalNum = 0;
        var tickNum = 0;
        var nextTickSaveNum = 0;
        setInterval(function () {
            intervalNum += 10;
            if (Object.keys(PlayerServer_1.PlayerServer.allPlayerObjs).length) { // Don't do anything if no players joined.
                var isEntityUpdateInterval = (intervalNum % 250 == 0);
                var entityIdsToUpdateArr = []; // Unique array of entity ids to be updated this tick.
                var entityIdsToUpdatePerPlayerArr = {}; // Each entity to be updated as the key, an array of player ids to be updated as the value.
                for (var entityId in EntityServer.allEntityObjs) {
                    if (isEntityUpdateInterval) {
                        EntityServer.allDistancesArr[entityId] = {};
                    }
                    for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
                        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[playerId];
                        if (playerObj.currentLayerNum == EntityServer.allEntityObjs[entityId].layerNum) {
                            if (isEntityUpdateInterval || EntityServer.allEntityObjs[entityId].isProjectile) {
                                if (!EntityServer.allEntityObjs[entityId].isProjectile) {
                                    EntityServer.allDistancesArr[entityId][playerId] = Utils.distance(playerObj, EntityServer.allEntityObjs[entityId]); // Get distance between this player and the entity, and save it to an array.
                                }
                                if (EntityServer.allEntityObjs[entityId].isProjectile
                                    ||
                                        EntityServer.allDistancesArr[entityId][playerId] <= 16 * Utils.tileSize) { // Update entities if closer than 16 blocks to a player.
                                    if (!(entityId in entityIdsToUpdateArr)) { // Add entity id into array of entities to update if not already there.
                                        entityIdsToUpdateArr.push(entityId);
                                    }
                                    if (!(entityId in entityIdsToUpdatePerPlayerArr)) {
                                        entityIdsToUpdatePerPlayerArr[entityId] = [];
                                    }
                                    entityIdsToUpdatePerPlayerArr[entityId].push(playerId);
                                }
                            }
                        }
                    }
                }
                var entityDataToEmitArr = {}; // {'PLAYER_ID_1': {'ENTITY_ID_1': {'x': X, 'y': Y, ... }}}
                for (var entityId in entityIdsToUpdatePerPlayerArr) {
                    if (entityId in EntityServer.allEntityObjs && EntityServer.allEntityObjs[entityId]) {
                        EntityServer.allEntityObjs[entityId].update(tickNum); // Update the entity.
                        if (entityId in EntityServer.allEntityObjs) {
                            var entityObj = EntityServer.allEntityObjs[entityId];
                            var entityUpdateArr = {
                                'type': entityObj.type,
                                'x': entityObj.x,
                                'y': entityObj.y,
                                'angle': entityObj.angle,
                                'isBaby': entityObj.isBaby,
                                'sizeMult': entityObj.sizeMult,
                                'health': entityObj.health,
                            };
                            if (entityObj.isExplosive) {
                                entityUpdateArr['fuseLengthNum'] = entityObj.fuseLengthNum;
                            }
                            if (entityObj.type == 'archer') {
                                entityUpdateArr['archerProjectileCharge'] = entityObj.archerProjectileCharge;
                            }
                            else if (entityObj.type == 'sheep') {
                                entityUpdateArr['hasWool'] = entityObj.hasWool;
                            }
                            if (entityObj.isKnockbackTick) {
                                entityUpdateArr['isKnockbackTick'] = entityObj.isKnockbackTick;
                            }
                            if (entityObj.isRiding) {
                                entityUpdateArr['isRiding'] = entityObj.isRiding;
                            }
                            for (var i in entityIdsToUpdatePerPlayerArr[entityId]) {
                                var playerId = entityIdsToUpdatePerPlayerArr[entityId][i];
                                if (!(playerId in entityDataToEmitArr)) {
                                    entityDataToEmitArr[playerId] = {};
                                }
                                entityDataToEmitArr[playerId][entityId] = entityUpdateArr;
                            }
                        }
                    }
                }
                // Emit the entities to update per client.
                for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
                    if (playerId in entityDataToEmitArr) {
                        UtilsServer_1.UtilsServer.io.to(playerId).emit('allEntitiesUpdate', {
                            'isEntityUpdateInterval': isEntityUpdateInterval,
                            'entityDataToEmitArr': entityDataToEmitArr[playerId],
                        });
                    }
                    else {
                        UtilsServer_1.UtilsServer.io.to(playerId).emit('allEntitiesUpdate', {
                            'isEntityUpdateInterval': isEntityUpdateInterval,
                            'entityDataToEmitArr': {},
                        });
                    }
                }
                if (isEntityUpdateInterval) {
                    tickNum++;
                    if (tickNum >= nextTickSaveNum) {
                        // Save passive entities to Redis.
                        for (var entityId in entityIdsToUpdatePerPlayerArr) {
                            if (entityId in EntityServer.allEntityObjs && EntityServer.allEntityObjs[entityId].isSavingInRedis()) {
                                UtilsServer_1.UtilsServer.redisClient.hmset('entity:' + entityId, {
                                    'type': EntityServer.allEntityObjs[entityId].type,
                                    'health': EntityServer.allEntityObjs[entityId].health,
                                    'layerNum': EntityServer.allEntityObjs[entityId].layerNum,
                                    'x': EntityServer.allEntityObjs[entityId].x,
                                    'y': EntityServer.allEntityObjs[entityId].y,
                                    'angle': EntityServer.allEntityObjs[entityId].angle,
                                    'isBaby': (EntityServer.allEntityObjs[entityId].isBaby ? '1' : '0'),
                                    'sizeMult': EntityServer.allEntityObjs[entityId].sizeMult,
                                });
                            }
                        }
                        nextTickSaveNum += 10;
                    }
                }
            }
        }, 10);
    };
    EntityServer.manageEntities = function () {
        var _a;
        var isDebuggingManageEntities = false;
        // Count entities.
        var numEntityTypeArr = {};
        for (var entityType in Utils.allEntityTypesArr) {
            if (Utils.allEntityTypesArr[entityType]['minInWorld']) {
                numEntityTypeArr[entityType] = 0;
            }
        }
        for (var entityId in EntityServer.allEntityObjs) {
            var entityObj = EntityServer.allEntityObjs[entityId];
            if (Utils.allEntityTypesArr[entityObj.type]['minInWorld']) {
                numEntityTypeArr[entityObj.type]++;
            }
        }
        // Spawn entities.
        for (var entityType in numEntityTypeArr) {
            var numEntity = numEntityTypeArr[entityType];
            var minEntities = Utils.allEntityTypesArr[entityType]['minInWorld'];
            if (minEntities && numEntity < minEntities) {
                for (var i = 0; i < minEntities - numEntity; i++) {
                    EntityServer.spawnEntity(entityType);
                }
            }
        }
        // Loop through all monsters, kill if more than `entityMaxDistanceFromPlayer` blocks from a player.
        var entityMaxDistanceFromPlayer = 20;
        for (var entityId in EntityServer.allEntityObjs) {
            var entityObj = EntityServer.allEntityObjs[entityId];
            if (Utils.allEntityTypesArr[entityObj.type]['isMonster']) {
                var didFindClosePlayer = false;
                for (var playerId in EntityServer.allDistancesArr[entityId]) {
                    if (EntityServer.allDistancesArr[entityId][playerId] <= entityMaxDistanceFromPlayer * Utils.tileSize
                        &&
                            (!(playerId in PlayerServer_1.PlayerServer.allPlayerObjs)
                                ||
                                    Utils.playerGameModesArr[PlayerServer_1.PlayerServer.allPlayerObjs[playerId].gameModeStr]['isSpawningMonsters'])) {
                        didFindClosePlayer = true;
                        break;
                    }
                }
                if (!didFindClosePlayer) {
                    if (isDebuggingManageEntities)
                        console.log("Killing entity because wasn't close enough to player", entityId);
                    entityObj.kill();
                }
            }
        }
        // Loop through all players, check that there's at least X monsters within 20 blocks. If not, create monsters.
        for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
            var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[playerId];
            if (Utils.playerGameModesArr[playerObj.gameModeStr]['isSpawningMonsters'] && playerObj.currentLayerNum in EntityServer.monsterDistributionPerLayerArr) {
                if (playerObj.currentLayerNum == 0 && DayNightCycleServer_1.DayNightCycleServer.stage == 'day') {
                    continue;
                }
                var numMonstersPerPlayer = EntityServer.monsterDistributionPerLayerArr[playerObj.currentLayerNum]['monstersPerPlayerNum'];
                if (playerObj.currentLayerNum == 0) {
                    var monstersMult = Math.min(Math.max(playerObj.nightsSurvivedNum, 1) / 6, 2);
                    numMonstersPerPlayer = Math.round(numMonstersPerPlayer * monstersMult);
                }
                var numMonstersInRange = 0;
                for (var entityId in EntityServer.allEntityObjs) {
                    if (Utils.allEntityTypesArr[EntityServer.allEntityObjs[entityId].type]['isMonster']) {
                        if (EntityServer.allDistancesArr[entityId][playerId] < entityMaxDistanceFromPlayer * Utils.tileSize) {
                            numMonstersInRange++;
                            if (numMonstersInRange >= numMonstersPerPlayer) {
                                break;
                            }
                        }
                    }
                }
                if (isDebuggingManageEntities)
                    console.log('numMonstersInRange', numMonstersInRange);
                if (isDebuggingManageEntities)
                    console.log('numMonstersPerPlayer', numMonstersPerPlayer);
                var numMonstersToSpawn = numMonstersPerPlayer - numMonstersInRange;
                // if (numMonstersToSpawn > 1 && playerObj.currentLayerNum == 0) { // Spawn one at a time on overworld.
                //     numMonstersToSpawn = 1;
                // }
                for (var i = 0; i < numMonstersToSpawn; i++) {
                    var entityX = void 0;
                    var entityY = void 0;
                    var numIterations = void 0;
                    _a = WorldServer_1.WorldServer.findEmptyMonsterSpawnPosSpot(Math.floor(playerObj.x / Utils.tileSize), Math.floor(playerObj.y / Utils.tileSize), playerObj.currentLayerNum, Utils.allEntityTypesArr['zombie']['spawnFloorIdsArr']), entityX = _a[0], entityY = _a[1], numIterations = _a[2];
                    if (numIterations < 100) {
                        var monsterTypesArr = Utils.dereferenceObj(EntityServer.monsterDistributionPerLayerArr[playerObj.currentLayerNum]['monsterTypesArr']);
                        if (Utils.tileIndexToItemIdArr[WorldServer_1.WorldServer.floorArr[playerObj.currentLayerNum][entityY][entityX][0]] == 'dungeon') {
                            for (var i_1 = 0; i_1 < 20; i_1++) {
                                monsterTypesArr.push('archer');
                            }
                        }
                        var entityType = Utils.randElFromArr(monsterTypesArr);
                        if (isDebuggingManageEntities)
                            console.log('Spawning ' + entityType + ' at ' + playerObj.currentLayerNum + ':' + entityX + ',' + entityY + ' numIterations: ' + numIterations);
                        var newEntityObj = new EntityServer({
                            'type': entityType,
                            'layerNum': playerObj.currentLayerNum,
                            'x': (entityX * Utils.tileSize) + (Utils.tileSize / 2),
                            'y': (entityY * Utils.tileSize) + (Utils.tileSize / 2),
                            'isBaby': false,
                        });
                        EntityServer.allDistancesArr[newEntityObj.id] = {};
                        for (var playerId_1 in PlayerServer_1.PlayerServer.allPlayerObjs) {
                            if (PlayerServer_1.PlayerServer.allPlayerObjs[playerId_1].currentLayerNum == playerObj.currentLayerNum) {
                                EntityServer.allDistancesArr[newEntityObj.id][playerId_1] = Utils.distance(PlayerServer_1.PlayerServer.allPlayerObjs[playerId_1], newEntityObj);
                            }
                        }
                    }
                    else {
                        if (isDebuggingManageEntities)
                            console.log('Spawn failed');
                    }
                }
            }
        }
    };
    EntityServer.prototype.isSavingInRedis = function () {
        return (Utils.allEntityTypesArr[this.type]['isSavingInRedis'] || this.isBaby);
    };
    EntityServer.spawnEntity = function (entityType) {
        var _a;
        var entityX;
        var entityY;
        var numIterations;
        _a = WorldServer_1.WorldServer.findEmptySpawnPosSpot(Utils.allEntityTypesArr[entityType]['spawnLayerNum'], Utils.allEntityTypesArr[entityType]['spawnFloorIdsArr']), entityX = _a[0], entityY = _a[1], numIterations = _a[2];
        if (numIterations < 50) {
            return new EntityServer({
                'type': entityType,
                'layerNum': Utils.allEntityTypesArr[entityType]['spawnLayerNum'],
                'x': (entityX * Utils.tileSize) + (Utils.tileSize / 2),
                'y': (entityY * Utils.tileSize) + (Utils.tileSize / 2),
                'angle': 0,
                'isBaby': false,
            });
        }
        return false;
    };
    EntityServer.init = function () {
        UtilsServer_1.UtilsServer.redisClient.smembers('entityIds', function (error, entityIdsArr) {
            if (error) {
                throw error;
            }
            else if (!entityIdsArr || !entityIdsArr.length) {
                for (var entityType in Utils.allEntityTypesArr) {
                    EntityServer.initSpawnNewEntities(entityType);
                }
            }
            else {
                var _loop_1 = function (i) {
                    var entityId = entityIdsArr[i];
                    UtilsServer_1.UtilsServer.redisClient.hgetall('entity:' + entityId, function (error, entityObj) {
                        if (error) {
                            throw error;
                        }
                        else if (entityObj) {
                            var entityDataArr = {
                                'id': entityId,
                                'type': entityObj.type,
                                'health': parseFloat(entityObj.health),
                                'layerNum': parseInt(entityObj.layerNum),
                                'x': parseFloat(entityObj.x),
                                'y': parseFloat(entityObj.y),
                                'angle': parseFloat(entityObj.angle),
                                'isBaby': (entityObj.isBaby == '1'),
                                'growUpTime': parseInt(entityObj.growUpTime) || null,
                                'sizeMult': parseFloat(entityObj.sizeMult),
                            };
                            if (entityObj.boundsArr && JSON.parse(entityObj.boundsArr)) {
                                entityDataArr['boundsArr'] = JSON.parse(entityObj.boundsArr);
                            }
                            new EntityServer(entityDataArr);
                        }
                    });
                };
                for (var i in entityIdsArr) {
                    _loop_1(i);
                }
                for (var entityType in Utils.allEntityTypesArr) {
                    if (!Utils.allEntityTypesArr[entityType]['isSavingInRedis']) {
                        EntityServer.initSpawnNewEntities(entityType);
                    }
                }
            }
        });
    };
    EntityServer.initSpawnNewEntities = function (entityType) {
        var numIterations = 0;
        var numEntitiesSpawned = 0;
        var initNum = Utils.allEntityTypesArr[entityType]['initNum'];
        while (numIterations < initNum * 2 && numEntitiesSpawned < initNum) {
            if (EntityServer.spawnEntity(entityType)) {
                numEntitiesSpawned++;
            }
            numIterations++;
        }
    };
    EntityServer.allEntityObjs = {};
    EntityServer.allDistancesArr = {};
    EntityServer.allFedEntityIdsByTypeArr = {};
    EntityServer.monsterDistributionPerLayerArr = {
        '0': {
            'monstersPerPlayerNum': 10,
            'monsterTypesArr': ['zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'archer', 'slime'],
        },
        '-1': {
            'monstersPerPlayerNum': 15,
            'monsterTypesArr': ['zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'archer', 'slime'],
        },
        '-2': {
            'monstersPerPlayerNum': 25,
            'monsterTypesArr': ['zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'archer', 'slime', 'slime'],
        },
        '-3': {
            'monstersPerPlayerNum': 35,
            'monsterTypesArr': ['zombie', 'zombie', 'zombie', 'zombie', 'zombie', 'archer', 'archer', 'slime', 'slime', 'slime'],
        },
    };
    EntityServer.panicTicksNum = 15;
    return EntityServer;
}());
exports.EntityServer = EntityServer;
//# sourceMappingURL=EntityServer.js.map