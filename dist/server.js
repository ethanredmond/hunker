"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// node ~/mnt/ebs1/git/hunker/server.js
var express = require("express");
var SocketIO = require("socket.io");
var redis = require("redis");
var UtilsServer_1 = require("./UtilsServer");
var app = express();
var server = require('http').Server(app);
UtilsServer_1.UtilsServer.io = SocketIO.listen(server);
if (process.env.REDIS_URL) {
    UtilsServer_1.UtilsServer.redisClient = redis.createClient(process.env.REDIS_URL);
}
else {
    UtilsServer_1.UtilsServer.redisClient = redis.createClient(6379, '127.0.0.1');
}
var session = require('express-session'); // https://www.npmjs.com/package/express-session
var RedisStore = require('connect-redis')(session); // https://www.npmjs.com/package/connect-redis
var bodyParser = require('body-parser');
require('../public/ext/js/seedrandom.min');
var Utils = require('./Utils');
var WorldServer_1 = require("./WorldServer");
var DayNightCycleServer_1 = require("./DayNightCycleServer");
var PlayerServer_1 = require("./PlayerServer");
var EntityServer_1 = require("./EntityServer");
var Timer_1 = require("./Timer");
var ChatServer_1 = require("./ChatServer");
var LayerGenerator_1 = require("./LayerGenerator");
WorldServer_1.WorldServer.init(function () {
    WorldServer_1.WorldServer.initSpawn();
    EntityServer_1.EntityServer.init();
    if (WorldServer_1.WorldServer.allDungeonObjsArr.length) {
        setTimeout(function () {
            // Summon guardians.
            for (var i in WorldServer_1.WorldServer.allDungeonObjsArr) {
                var dungeonArr = WorldServer_1.WorldServer.allDungeonObjsArr[i];
                var entityX = (dungeonArr['dungeonBoundsArr']['x1'] + Math.floor(dungeonArr['dungeonObj'].roomsArr[0]['x1'] + (dungeonArr['dungeonObj'].roomsArr[0]['x2'] - dungeonArr['dungeonObj'].roomsArr[0]['x1']) / 2)) * Utils.tileSize;
                var entityY = (dungeonArr['dungeonBoundsArr']['y1'] + Math.floor(dungeonArr['dungeonObj'].roomsArr[0]['y1'] + (dungeonArr['dungeonObj'].roomsArr[0]['y2'] - dungeonArr['dungeonObj'].roomsArr[0]['y1']) / 2)) * Utils.tileSize;
                new EntityServer_1.EntityServer({
                    'type': 'axehead',
                    'layerNum': dungeonArr['dungeonObj'].layerNum,
                    'x': entityX,
                    'y': entityY,
                    'isBaby': false,
                    'boundsArr': {
                        'x1': dungeonArr['dungeonBoundsArr']['x1'] + dungeonArr['dungeonObj'].roomsArr[0]['x1'],
                        'y1': dungeonArr['dungeonBoundsArr']['y1'] + dungeonArr['dungeonObj'].roomsArr[0]['y1'],
                        'x2': dungeonArr['dungeonBoundsArr']['x1'] + dungeonArr['dungeonObj'].roomsArr[0]['x2'],
                        'y2': dungeonArr['dungeonBoundsArr']['y1'] + dungeonArr['dungeonObj'].roomsArr[0]['y2'],
                    },
                });
            }
            // Count entities.
            // const entityTypeCountArr = {};
            // for (let entityId in EntityServer.allEntityObjs) {
            //     if (!(EntityServer.allEntityObjs[entityId].type in entityTypeCountArr)) {
            //         entityTypeCountArr[EntityServer.allEntityObjs[entityId].type] = 0;
            //     }
            //     entityTypeCountArr[EntityServer.allEntityObjs[entityId].type]++;
            // }
            // console.log(entityTypeCountArr);
        }, 1000);
    }
    console.log("Loaded world");
});
Timer_1.Timer.init();
var socketIdToUserNameArr = {};
var sessionMiddleware = session({
    'secret': '7d90e04e426a79af11ca509383f1526f',
    'store': new RedisStore({ 'host': '127.0.0.1', 'port': 6379, 'client': UtilsServer_1.UtilsServer.redisClient }),
    'saveUninitialized': false,
    'resave': false,
});
UtilsServer_1.UtilsServer.io.use(function (socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
});
app.use(sessionMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var dirName = '/mnt/ebs1/git/hunker/';
if (process.env.PORT != '80') {
    dirName = require('os').homedir() + "/mnt/ebs1/git/hunker/";
}
app.use(express.static(dirName));
app.get('/', function (req, res) {
    res.sendFile(dirName + 'public/index.html');
});
app.post('/logout', function (requestObj, resultObj) {
    if (requestObj) {
        requestObj.session.destroy(function () {
        });
        socketIdToUserNameArr[requestObj.body['socketId']] = null;
        resultObj.json({
            'status': 'success',
        });
    }
});
app.post('/deleteAccount', function (requestObj, resultObj) {
    UtilsServer_1.UtilsServer.redisClient.del('user:' + requestObj.session.userName, null, function (error) {
        if (error) {
            throw error;
        }
        else {
            UtilsServer_1.UtilsServer.redisClient.del('player:' + requestObj.session.userName, null);
            requestObj.session.destroy(function () {
            });
            socketIdToUserNameArr[requestObj.body['socketId']] = null;
            resultObj.json({
                'status': 'success',
            });
        }
    });
});
app.post('/login', function (requestObj, resultObj) {
    if (!('userName' in requestObj.body) || !requestObj.body['userName']) {
        resultObj.json({
            'status': 'error',
            'errorMessage': "Username is empty",
            'errorMessageInputId': 'userName',
        });
    }
    else if (!('passwordCode' in requestObj.body) || !requestObj.body['passwordCode']) {
        resultObj.json({
            'status': 'error',
            'errorMessage': "Password is empty",
            'errorMessageInputId': 'passwordCode',
        });
    }
    else {
        UtilsServer_1.UtilsServer.redisClient.hgetall('user:' + requestObj.body['userName'], function (error, userObj) {
            if (error) {
                throw error;
            }
            else if (!userObj) {
                resultObj.json({
                    'status': 'error',
                    'errorMessage': 'No user with this user name',
                    'errorMessageInputId': 'userName',
                });
            }
            else {
                var checkPasswordHash = UtilsServer_1.UtilsServer.getPasswordHash(requestObj.body['passwordCode'], userObj['passwordSalt']);
                if (checkPasswordHash == userObj['passwordHash']) {
                    requestObj.session.userName = requestObj.body['userName'];
                    socketIdToUserNameArr[requestObj.body['socketId']] = requestObj.body['userName'];
                    resultObj.json({
                        'status': 'success',
                        'userName': userObj['userName'],
                        'userScore': userObj['userScore'],
                    });
                }
                else {
                    resultObj.json({
                        'status': 'error',
                        'errorMessage': "Password incorrect",
                        'errorMessageInputId': 'passwordCode',
                    });
                }
            }
        });
    }
});
app.post('/register', function (requestObj, resultObj) {
    if (!('userName' in requestObj.body) || !requestObj.body['userName']) {
        resultObj.json({
            'status': 'error',
            'errorMessage': "Username is empty",
            'errorMessageInputId': 'userName',
        });
    }
    else if (!('passwordCode' in requestObj.body) || !requestObj.body['passwordCode']) {
        resultObj.json({
            'status': 'error',
            'errorMessage': "Password is empty",
            'errorMessageInputId': 'passwordCode',
        });
    }
    else {
        UtilsServer_1.UtilsServer.redisClient.hgetall('user:' + requestObj.body['userName'], function (error, userObj) {
            if (error) {
                throw error;
            }
            else if (!userObj) {
                var passwordSalt = UtilsServer_1.UtilsServer.generateSalt(32);
                var passwordHash = UtilsServer_1.UtilsServer.getPasswordHash(requestObj.body['passwordCode'], passwordSalt);
                UtilsServer_1.UtilsServer.redisClient.hmset('user:' + requestObj.body['userName'], {
                    'userName': requestObj.body['userName'],
                    'passwordSalt': passwordSalt,
                    'passwordHash': passwordHash,
                    'userScore': 0,
                });
                requestObj.session.userName = requestObj.body['userName'];
                socketIdToUserNameArr[requestObj.body['socketId']] = requestObj.body['userName'];
                resultObj.json({
                    'status': 'success',
                    'userName': requestObj.body['userName'],
                    'userScore': 0,
                });
            }
            else {
                resultObj.json({
                    'status': 'error',
                    'errorMessage': 'Account already exists',
                    'errorMessageInputId': 'userName',
                });
            }
        });
    }
});
UtilsServer_1.UtilsServer.redisClient.on('connect', function () {
    console.log('Redis client connected');
});
UtilsServer_1.UtilsServer.redisClient.on('error', function (err) {
    console.log(err);
});
UtilsServer_1.UtilsServer.io.on('connection', function (socket) {
    console.log("Socket " + socket.id + " connected.");
    var userStateArr = {
        'isLoggedIn': false,
    };
    if (socket.request.session && 'userName' in socket.request.session && socket.request.session.userName) {
        userStateArr['isLoggedIn'] = true;
        userStateArr['userName'] = socket.request.session.userName;
        socketIdToUserNameArr[socket.id] = socket.request.session.userName;
        UtilsServer_1.UtilsServer.redisClient.hmget('user:' + socket.request.session.userName, 'userScore', function (error, userScore) {
            if (error) {
                throw error;
            }
            else if (!userScore || !userScore[0] || !parseInt(userScore[0])) {
                userStateArr['userScore'] = 0;
                socket.emit('userState', userStateArr);
            }
            else {
                userStateArr['userScore'] = parseInt(userScore[0]);
                socket.emit('userState', userStateArr);
            }
        });
    }
    else {
        socket.emit('userState', userStateArr);
    }
    socket.on('joinGame', function (playerJoinDetailsArr, responseFn) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            new PlayerServer_1.PlayerServer(socket, playerJoinDetailsArr, socketIdToUserNameArr[socket.id], responseFn);
        }
    });
    socket.on('disconnect', function () {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].kill('disconnected');
        }
    });
    socket.on('playerMovement', function (movementData) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].movement(socket, movementData);
        }
    });
    socket.on('startSwing', function (startSwingCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].startSwing(socket, startSwingCoordArr, responseFn);
        }
    });
    socket.on('feedEntities', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].feedEntities(socket, responseFn);
        }
    });
    socket.on('rideEntities', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].rideEntities(socket, responseFn);
        }
    });
    socket.on('dismountEntity', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].dismountEntity(socket, responseFn);
        }
    });
    socket.on('toggleBlock', function (doorCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].toggleBlock(socket, doorCoordArr, responseFn);
        }
    });
    socket.on('triggerExplosive', function (doorCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].triggerExplosive(doorCoordArr, responseFn);
        }
    });
    socket.on('clickRail', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].clickRail(responseFn);
        }
    });
    socket.on('cancelClickRail', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].cancelClickRail(responseFn);
        }
    });
    socket.on('fillBucket', function (coordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].fillBucket(socket, coordArr, responseFn);
        }
    });
    socket.on('placeEntity', function (placeEntityCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].placeEntity(socket, placeEntityCoordArr, responseFn);
        }
    });
    socket.on('placeBlock', function (placeBlockCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].placeBlock(socket, placeBlockCoordArr, responseFn);
        }
    });
    socket.on('useItem', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].useItem(socket, responseFn);
        }
    });
    socket.on('fireProjectile', function (projectileData, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].fireProjectile(socket, projectileData, responseFn);
        }
    });
    socket.on('openContainer', function (containerCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].openContainer(containerCoordArr, responseFn);
        }
    });
    socket.on('enchantItem', function (tableCoordArr, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].enchantItem(tableCoordArr, responseFn);
        }
    });
    socket.on('selectItem', function (itemToSelectIndex, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].selectItem(socket, itemToSelectIndex, responseFn);
        }
    });
    socket.on('swapItem', function (swapItemData, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].swapItem(socket, swapItemData, responseFn);
        }
    });
    socket.on('dropItem', function (dropItemData, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].dropItem(socket, dropItemData, responseFn);
        }
    });
    socket.on('takeItem', function (takeItemData, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].takeItem(socket, takeItemData, responseFn);
        }
    });
    socket.on('startCraftingItem', function (itemId, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].startCraftingItem(socket, itemId, responseFn);
        }
    });
    socket.on('playGame', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].playGame(socket, responseFn);
        }
    });
    socket.on('pauseGame', function (responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].pauseGame(socket, responseFn);
        }
    });
    socket.on('changeSetting', function (settingName, settingKey, responseFn) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[socket.id].changeSetting(settingName, settingKey, responseFn);
        }
    });
    socket.on('playerMessage', function (messageText) {
        if (socket.id in PlayerServer_1.PlayerServer.allPlayerObjs) {
            if (messageText.indexOf('/') == 0) { // Is command.
                ChatServer_1.ChatServer.handleCommandServer(messageText, socket);
            }
            else {
                // Emit message to all players.
                UtilsServer_1.UtilsServer.io.emit('playerMessage', {
                    'playerId': socket.id,
                    'text': messageText.substring(0, 100),
                    'soundEffectKey': 'notification',
                });
            }
        }
    });
    if (Utils.isDev) {
        socket.on('generateLayer', function (layerNum, responseFn) {
            Utils.isWorldPage = true;
            var layerObj = new LayerGenerator_1.LayerGenerator(layerNum, WorldServer_1.WorldServer.worldSeed || Utils.randId(8));
            responseFn({
                'worldLayerArr': layerObj.worldLayerArr,
                'floorLayerArr': layerObj.floorLayerArr,
                'seed': layerObj.seed,
            });
            Utils.isWorldPage = false;
        });
    }
});
server.listen(process.env.PORT || 7777, function () {
    console.log("Server open at http://localhost:" + server.address().port + "/ and http://192.168.33.77:" + server.address().port + "/"); // Get local ip address with `hostname -I`.
});
EntityServer_1.EntityServer.startEntityUpdateInterval();
var tickIntervalMillis = 1000;
var tickNum = 0;
setInterval(gameTick, tickIntervalMillis);
function gameTick() {
    var is5thTick = (tickNum % 5 === 0);
    tickNum++;
    if (Object.keys(PlayerServer_1.PlayerServer.allPlayerObjs).length) { // Don't do anything if no players joined.
        for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
            PlayerServer_1.PlayerServer.allPlayerObjs[playerId].updateStatBars(is5thTick);
        }
        if (is5thTick) {
            // Update all player scores every 5 seconds.
            var updatePlayerScoresArr = {};
            for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
                updatePlayerScoresArr[playerId] = PlayerServer_1.PlayerServer.allPlayerObjs[playerId].playerScore;
            }
            UtilsServer_1.UtilsServer.io.emit('updatePlayerScoresArr', updatePlayerScoresArr); // Emit to all players.
            WorldServer_1.WorldServer.growCrops();
        }
        WorldServer_1.WorldServer.updateAllHearths();
        WorldServer_1.WorldServer.updateAllGrindstones();
    }
    if (tickNum % 2 === 0) {
        EntityServer_1.EntityServer.manageEntities();
    }
}
DayNightCycleServer_1.DayNightCycleServer.startDay();
DayNightCycleServer_1.DayNightCycleServer.gameStartMillis = Utils.time();
//# sourceMappingURL=server.js.map