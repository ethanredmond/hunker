"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = require('./Utils');
var UtilsServer_1 = require("./UtilsServer");
var WorldServer_1 = require("./WorldServer");
var Timer = /** @class */ (function () {
    function Timer() {
    }
    Timer.init = function () {
        UtilsServer_1.UtilsServer.redisClient.hgetall('timeToLiveTimer', function (error, timeToLiveTimersArr) {
            if (error) {
                throw error;
            }
            else if (timeToLiveTimersArr != null) {
                for (var blockCoordStr in timeToLiveTimersArr) {
                    Timer.addTimer('timeToLive', blockCoordStr, parseInt(timeToLiveTimersArr[blockCoordStr]), true);
                }
            }
        });
    };
    Timer.addTimer = function (timerType, blockCoordStr, activateTimerTime, isInit) {
        if (isInit === void 0) { isInit = false; }
        if (Timer.allTimerTypesArr.indexOf(timerType) == -1) {
            throw new Error("Timer type of \"" + timerType + "\" is invalid");
        }
        if (activateTimerTime <= Utils.time()) {
            if (timerType == 'timeToLive') {
                Timer.timeToLiveTimerEnded(blockCoordStr);
            }
        }
        else {
            if (!(timerType in Timer.allTimeoutsArr)) {
                Timer.allTimeoutsArr[timerType] = {};
            }
            Timer.allTimeoutsArr[timerType][blockCoordStr] = setTimeout(function () {
                if (timerType == 'timeToLive') {
                    Timer.timeToLiveTimerEnded(blockCoordStr);
                }
            }, activateTimerTime - Utils.time());
            if (!isInit) {
                UtilsServer_1.UtilsServer.redisClient.hset('timeToLiveTimer', blockCoordStr, activateTimerTime.toString());
            }
        }
    };
    Timer.removeTimersForBlock = function (blockCoordStr) {
        for (var i in Timer.allTimerTypesArr) {
            var timerType = Timer.allTimerTypesArr[i];
            if (timerType in Timer.allTimeoutsArr && blockCoordStr in Timer.allTimeoutsArr[timerType]) {
                clearTimeout(Timer.allTimeoutsArr[timerType][blockCoordStr]);
                delete Timer.allTimeoutsArr[timerType][blockCoordStr];
            }
            UtilsServer_1.UtilsServer.redisClient.hdel(timerType + 'Timer', blockCoordStr);
        }
    };
    // noinspection JSUnusedGlobalSymbols
    Timer.timeToLiveTimerEnded = function (blockCoordStr) {
        var blockLayerNum = blockCoordStr.split(':')[0];
        var splitBlockCoordStrArr = blockCoordStr.split(':')[1].split(',');
        var blockCoordArr = {
            'layerNum': parseInt(blockLayerNum),
            'x': parseInt(splitBlockCoordStrArr[0]),
            'y': parseInt(splitBlockCoordStrArr[1]),
        };
        WorldServer_1.WorldServer.destroyBlock(blockCoordArr, blockCoordStr);
        // Emit to all players.
        UtilsServer_1.UtilsServer.io.emit('blockUpdate', {
            'blockCoordArr': blockCoordArr,
            'blockDataArr': null,
        });
    };
    Timer.allTimeoutsArr = {};
    Timer.allTimerTypesArr = ['timeToLive'];
    return Timer;
}());
exports.Timer = Timer;
//# sourceMappingURL=Timer.js.map