"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = require('./Utils');
var Items = require('./public/generated/Items');
var Chat = require('./Chat');
var UtilsServer_1 = require("./UtilsServer");
var PlayerServer_1 = require("./PlayerServer");
var EntityServer_1 = require("./EntityServer");
var WorldServer_1 = require("./WorldServer");
var HotbarServer_1 = require("./HotbarServer");
var DayNightCycleServer_1 = require("./DayNightCycleServer");
var ChatServer = /** @class */ (function () {
    function ChatServer() {
    }
    ChatServer.handleCommandServer = function (messageText, socket) {
        for (var commandKey in Chat.allCommandsArr) {
            if (messageText.indexOf(commandKey) == 0 && !Chat.allCommandsArr[commandKey]['isLocal']) {
                // @ts-ignore
                ChatServer[Chat.allCommandsArr[commandKey]['functionName']](messageText, socket);
                break;
            }
        }
    };
    ChatServer.givePlayerItems = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        var commandArgsArr = messageText.substr('/give '.length).split(' ');
        var itemToGiveId = commandArgsArr[0];
        var prevSelectedItemId = playerObj.selectedItemId;
        var prevHotbarArr = Utils.dereferenceObj(playerObj.hotbarItemsArr);
        var playerMessageText;
        if (itemToGiveId == 'gear_diamond' || itemToGiveId == 'gear_gold' || itemToGiveId == 'gear_silver' || itemToGiveId == 'gear_crude' || itemToGiveId == 'gear_stone' || itemToGiveId == 'gear_wood') {
            var itemsToGiveArr = void 0;
            if (itemToGiveId == 'gear_diamond') {
                itemsToGiveArr = [
                    { 'itemId': 'sword_diamond', 'itemCount': 1 },
                    { 'itemId': 'pick_diamond', 'itemCount': 1 },
                    { 'itemId': 'hoe_diamond', 'itemCount': 1 },
                    { 'itemId': 'axe_diamond', 'itemCount': 1 },
                    { 'itemId': 'shovel_diamond', 'itemCount': 1 },
                    { 'itemId': 'planks', 'itemCount': 64 },
                    { 'itemId': 'stone', 'itemCount': 64 },
                    { 'itemId': 'stew', 'itemCount': 64 },
                    { 'itemId': 'torch', 'itemCount': 64 },
                    { 'itemId': 'helmet_diamond', 'itemCount': 1 },
                    { 'itemId': 'chestplate_diamond', 'itemCount': 1 },
                    { 'itemId': 'leggings_diamond', 'itemCount': 1 },
                    { 'itemId': 'boots_diamond', 'itemCount': 1 },
                    { 'itemId': 'bag', 'itemCount': 1 },
                    { 'itemId': 'backpack', 'itemCount': 1 },
                ];
            }
            else if (itemToGiveId == 'gear_gold') {
                itemsToGiveArr = [
                    { 'itemId': 'sword_gold', 'itemCount': 1 },
                    { 'itemId': 'pick_gold', 'itemCount': 1 },
                    { 'itemId': 'hoe_gold', 'itemCount': 1 },
                    { 'itemId': 'axe_gold', 'itemCount': 1 },
                    { 'itemId': 'shovel_gold', 'itemCount': 1 },
                    { 'itemId': 'planks', 'itemCount': 64 },
                    { 'itemId': 'stone', 'itemCount': 64 },
                    { 'itemId': 'stew', 'itemCount': 64 },
                    { 'itemId': 'torch', 'itemCount': 64 },
                    { 'itemId': 'helmet_gold', 'itemCount': 1 },
                    { 'itemId': 'chestplate_gold', 'itemCount': 1 },
                    { 'itemId': 'leggings_gold', 'itemCount': 1 },
                    { 'itemId': 'boots_gold', 'itemCount': 1 },
                    { 'itemId': 'bag', 'itemCount': 1 },
                    { 'itemId': 'backpack', 'itemCount': 1 },
                ];
            }
            else if (itemToGiveId == 'gear_silver') {
                itemsToGiveArr = [
                    { 'itemId': 'sword_silver', 'itemCount': 1 },
                    { 'itemId': 'pick_silver', 'itemCount': 1 },
                    { 'itemId': 'hoe_silver', 'itemCount': 1 },
                    { 'itemId': 'axe_silver', 'itemCount': 1 },
                    { 'itemId': 'shovel_silver', 'itemCount': 1 },
                    { 'itemId': 'planks', 'itemCount': 64 },
                    { 'itemId': 'stone', 'itemCount': 64 },
                    { 'itemId': 'stew', 'itemCount': 64 },
                    { 'itemId': 'torch', 'itemCount': 64 },
                    { 'itemId': 'helmet_silver', 'itemCount': 1 },
                    { 'itemId': 'chestplate_silver', 'itemCount': 1 },
                    { 'itemId': 'leggings_silver', 'itemCount': 1 },
                    { 'itemId': 'boots_silver', 'itemCount': 1 },
                    { 'itemId': 'bag', 'itemCount': 1 },
                    { 'itemId': 'backpack', 'itemCount': 1 },
                ];
            }
            else if (itemToGiveId == 'gear_crude') {
                itemsToGiveArr = [
                    { 'itemId': 'sword_stone', 'itemCount': 1 },
                    { 'itemId': 'pick_stone', 'itemCount': 1 },
                    { 'itemId': 'hoe_stone', 'itemCount': 1 },
                    { 'itemId': 'axe_stone', 'itemCount': 1 },
                    { 'itemId': 'shovel_stone', 'itemCount': 1 },
                    { 'itemId': 'planks', 'itemCount': 64 },
                    { 'itemId': 'stone', 'itemCount': 64 },
                    { 'itemId': 'stew', 'itemCount': 64 },
                    { 'itemId': 'torch', 'itemCount': 64 },
                    { 'itemId': 'helmet_crude', 'itemCount': 1 },
                    { 'itemId': 'chestplate_crude', 'itemCount': 1 },
                    { 'itemId': 'leggings_crude', 'itemCount': 1 },
                    { 'itemId': 'boots_crude', 'itemCount': 1 },
                    { 'itemId': 'bag', 'itemCount': 1 },
                    { 'itemId': 'backpack', 'itemCount': 1 },
                ];
            }
            else if (itemToGiveId == 'gear_stone') {
                itemsToGiveArr = [
                    { 'itemId': 'pick_stone', 'itemCount': 1 },
                    { 'itemId': 'hoe_wood', 'itemCount': 1 },
                    { 'itemId': 'axe_stone', 'itemCount': 1 },
                    { 'itemId': 'apple', 'itemCount': 4 },
                    { 'itemId': 'torch', 'itemCount': 1 },
                ];
            }
            else if (itemToGiveId == 'gear_wood') {
                itemsToGiveArr = [
                    { 'itemId': 'pick_wood', 'itemCount': 1 },
                    { 'itemId': 'axe_wood', 'itemCount': 1 },
                    { 'itemId': 'torch', 'itemCount': 1 },
                ];
            }
            playerObj.hotbarItemsArr = HotbarServer_1.HotbarServer.addItemsFromArr(playerObj.hotbarItemsArr, itemsToGiveArr);
            playerMessageText = "Gave " + itemToGiveId;
        }
        else {
            if (!itemToGiveId || !(itemToGiveId in Items.itemsArr) || !Items.itemsArr[itemToGiveId]['isObtainable']) {
                UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text': "\"" + itemToGiveId + "\" not found, try <code>/give wood 5</code>",
                });
                return;
            }
            var numItemsToGive = 1;
            if (1 in commandArgsArr) {
                numItemsToGive = parseInt(commandArgsArr[1]);
            }
            if (!numItemsToGive) {
                UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text': "Invalid items num, try <code>/give " + itemToGiveId + " 5</code>",
                });
                return;
            }
            numItemsToGive = Math.min(numItemsToGive, 64);
            numItemsToGive = Math.max(numItemsToGive, 1);
            var newItemIndex = Utils.getNewItemIndex(playerObj.hotbarItemsArr, itemToGiveId);
            if (newItemIndex == null) {
                UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text': "Hotbar full",
                });
                return;
            }
            // Add items to hotbar.
            playerObj.hotbarItemsArr = HotbarServer_1.HotbarServer.addItemsAtIndex(playerObj.hotbarItemsArr, { 'itemId': itemToGiveId, 'itemCount': numItemsToGive }, newItemIndex);
            playerMessageText = "Gave " + numItemsToGive + "x " + Utils.lcfirst(Items.itemsArr[itemToGiveId]['label']);
        }
        playerObj.selectedItemId = playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemId'];
        playerObj.updateHotbarRedis(prevHotbarArr);
        if (prevSelectedItemId != playerObj.selectedItemId) {
            socket.broadcast.emit('selectItem', playerObj.id, playerObj.selectedItemId); // Emit to all other players.
        }
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': playerMessageText,
            'hotbarItemsArr': playerObj.hotbarItemsArr,
        });
    };
    ChatServer.enchantItem = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        if (!playerObj.selectedItemId) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Nothing selected",
            });
            return;
        }
        var commandArgsArr = messageText.substr('/enchant '.length).split(' ');
        var enchantmentType = commandArgsArr[0];
        var prevHotbarArr = Utils.dereferenceObj(playerObj.hotbarItemsArr);
        var playerMessageText;
        if (!enchantmentType || !(enchantmentType in Utils.allEnchantmentsArr)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "\"" + enchantmentType + "\" not found, try <code>/enchant durability</code>",
            });
            return;
        }
        var tierNum = 1;
        if (1 in commandArgsArr) {
            tierNum = parseInt(commandArgsArr[1]);
        }
        if (!tierNum) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Invalid tier num, try <code>/enchant " + enchantmentType + " 2</code>",
            });
            return;
        }
        if (!Utils.playerGameModesArr[playerObj.gameModeStr]['hasInfiniteBlocks']) {
            tierNum = Math.min(tierNum, Utils.allEnchantmentsArr[enchantmentType]['tierMax']);
        }
        tierNum = Math.max(tierNum, 1);
        if (!('itemEnchantmentsArr' in playerObj.hotbarItemsArr[playerObj.selectedItemIndex])) {
            playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemEnchantmentsArr'] = {};
        }
        playerObj.hotbarItemsArr[playerObj.selectedItemIndex]['itemEnchantmentsArr'][enchantmentType] = tierNum;
        playerMessageText = "Enchanted with " + Utils.allEnchantmentsArr[enchantmentType]['label'] + ' ' + Utils.toRomanNumerals(tierNum);
        console.log('enchantmentType', enchantmentType);
        playerObj.updateHotbarRedis(prevHotbarArr);
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': playerMessageText,
            'hotbarItemsArr': playerObj.hotbarItemsArr,
        });
    };
    ChatServer.setPlayerLayer = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        var layerNum = parseInt(messageText.substr('/layer '.length));
        playerObj.setCurrentLayerNum(layerNum, function (response) {
            if (response['status'] == 'error') {
                UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'error',
                    'text': response['errorMessage'],
                });
            }
            else {
                UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                    'status': 'success',
                    'text': "Set layer to " + layerNum,
                });
            }
        });
    };
    ChatServer.killPlayer = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        playerObj.kill('command');
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Killed",
        });
    };
    ChatServer.revivePlayer = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        playerObj.revive();
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Revived",
        });
    };
    ChatServer.startDay = function (messageText, socket) {
        DayNightCycleServer_1.DayNightCycleServer.startDay();
        clearTimeout(DayNightCycleServer_1.DayNightCycleServer.nextTimeout);
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Started day",
        });
    };
    ChatServer.startNight = function (messageText, socket) {
        DayNightCycleServer_1.DayNightCycleServer.startNight();
        clearTimeout(DayNightCycleServer_1.DayNightCycleServer.nextTimeout);
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Started night",
        });
    };
    ChatServer.setPlayerStatBar = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        var whichStatBar = messageText.substr(1, 6);
        var newStatBarValue = parseInt(messageText.substr(8));
        if (!newStatBarValue && newStatBarValue !== 0) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': messageText.substr(8) + " is invalid",
            });
            return;
        }
        if (whichStatBar != 'health' && whichStatBar != 'warmth' && whichStatBar != 'hunger') {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': messageText.substr(8) + " is invalid",
            });
            return;
        }
        newStatBarValue = Math.max(newStatBarValue, 0);
        newStatBarValue = Math.min(newStatBarValue, 20);
        playerObj[whichStatBar] = newStatBarValue;
        if (playerObj.userName) {
            UtilsServer_1.UtilsServer.redisClient.hset('player:' + playerObj.userName, whichStatBar, playerObj[whichStatBar].toString());
        }
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Set your " + whichStatBar + " to " + playerObj[whichStatBar],
        });
        var statBarsDataArr = {};
        statBarsDataArr[whichStatBar] = playerObj[whichStatBar];
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('updateStatBars', { 'status': 'success', 'statBarsDataArr': statBarsDataArr });
    };
    ChatServer.summonEntity = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        var entityType = messageText.substr('/summon '.length);
        var summonNum = 1;
        if (entityType.indexOf(' ') != -1) {
            summonNum = parseInt(entityType.split(' ')[1]);
            entityType = entityType.split(' ')[0];
        }
        if (!entityType || !(entityType in Utils.allEntityTypesArr)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': entityType + " is invalid",
            });
            return;
        }
        if (!summonNum || isNaN(summonNum)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': summonNum + " is invalid",
            });
            return;
        }
        for (var i = 0; i < summonNum; i++) {
            new EntityServer_1.EntityServer({
                'type': entityType,
                'layerNum': playerObj.currentLayerNum,
                'x': playerObj.x,
                'y': playerObj.y,
                'isBaby': false,
            });
        }
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Summoned " + entityType,
        });
    };
    ChatServer.setNightsSurvivedNum = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        var nightsSurvivedNum = parseInt(messageText.slice('/nights '.length));
        if (!nightsSurvivedNum && nightsSurvivedNum !== 0) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': messageText.substr(8) + " is invalid",
            });
            return;
        }
        nightsSurvivedNum = Math.max(nightsSurvivedNum, 0);
        playerObj.nightsSurvivedNum = nightsSurvivedNum;
        if (playerObj.userName) {
            UtilsServer_1.UtilsServer.redisClient.hset('player:' + playerObj.userName, 'nightsSurvivedNum', nightsSurvivedNum.toString());
        }
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Set your nights survived to " + nightsSurvivedNum,
        });
    };
    // static setDifficulty(messageText, socket) {
    //     if (!(socket.id in PlayerServer.allPlayerObjs)) {
    //         UtilsServer.io.to(socket.id).emit('playerMessage', {
    //             'status': 'error',
    //             'text':   "Player not found",
    //         });
    //         return;
    //     }
    //     const playerObj      = PlayerServer.allPlayerObjs[socket.id];
    //     let difficultyLetter = messageText.slice('/difficulty '.length);
    //     if (!(difficultyLetter in Utils.playerDifficultyLettersArr)) {
    //         UtilsServer.io.to(socket.id).emit('playerMessage', {
    //             'status': 'error',
    //             'text':   messageText.substr(8) + " is invalid",
    //         });
    //         return;
    //     }
    //     playerObj.difficultyLetter = difficultyLetter;
    //     if (playerObj.userName) {
    //         UtilsServer.redisClient.hset('player:' + playerObj.userName, 'difficultyLetter', difficultyLetter);
    //     }
    //     // Emit to specific player.
    //     UtilsServer.io.to(socket.id).emit('playerMessage', {
    //         'status': 'success',
    //         'text':   "Set your difficulty to \"" + difficultyLetter + "\"",
    //     });
    // }
    ChatServer.setSpawn = function (messageText, socket) {
        if (!(socket.id in PlayerServer_1.PlayerServer.allPlayerObjs)) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Player not found",
            });
            return;
        }
        var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[socket.id];
        if (playerObj.currentLayerNum != 0) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Can only set on overworld",
            });
            return;
        }
        var playerXRounded = Math.floor(playerObj.x / Utils.tileSize);
        var playerYRounded = Math.floor(playerObj.y / Utils.tileSize);
        if (WorldServer_1.WorldServer.worldArr[0][playerYRounded][playerXRounded]) {
            UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
                'status': 'error',
                'text': "Block at spawn",
            });
            return;
        }
        WorldServer_1.WorldServer.worldArr[WorldServer_1.WorldServer.spawnLayerNum][WorldServer_1.WorldServer.spawnY][WorldServer_1.WorldServer.spawnX] = null;
        WorldServer_1.WorldServer.updateSquareRedis({
            'layerNum': WorldServer_1.WorldServer.spawnLayerNum,
            'x': WorldServer_1.WorldServer.spawnX,
            'y': WorldServer_1.WorldServer.spawnY,
        });
        UtilsServer_1.UtilsServer.io.emit('blockUpdate', {
            'blockId': null,
            'blockCoordArr': {
                'layerNum': WorldServer_1.WorldServer.spawnLayerNum,
                'x': WorldServer_1.WorldServer.spawnX,
                'y': WorldServer_1.WorldServer.spawnY,
            },
            'blockDataArr': WorldServer_1.WorldServer.worldArr[WorldServer_1.WorldServer.spawnLayerNum][WorldServer_1.WorldServer.spawnY][WorldServer_1.WorldServer.spawnX],
        }); // Emit to all players.
        WorldServer_1.WorldServer.spawnLayerNum = playerObj.currentLayerNum;
        WorldServer_1.WorldServer.spawnX = playerXRounded;
        WorldServer_1.WorldServer.spawnY = playerYRounded;
        var spawnCoordStr = playerObj.currentLayerNum + ':' + playerXRounded + ',' + playerYRounded;
        console.log(spawnCoordStr);
        UtilsServer_1.UtilsServer.redisClient.set('spawn', spawnCoordStr);
        WorldServer_1.WorldServer.addSpawnBlock();
        UtilsServer_1.UtilsServer.io.emit('blockUpdate', {
            'blockId': 'spawn',
            'blockCoordArr': {
                'layerNum': WorldServer_1.WorldServer.spawnLayerNum,
                'x': WorldServer_1.WorldServer.spawnX,
                'y': WorldServer_1.WorldServer.spawnY,
            },
            'blockDataArr': WorldServer_1.WorldServer.worldArr[WorldServer_1.WorldServer.spawnLayerNum][WorldServer_1.WorldServer.spawnY][WorldServer_1.WorldServer.spawnX],
        }); // Emit to all players.
        // Emit to specific player.
        UtilsServer_1.UtilsServer.io.to(socket.id).emit('playerMessage', {
            'status': 'success',
            'text': "Set spawn to " + spawnCoordStr,
        });
    };
    return ChatServer;
}());
exports.ChatServer = ChatServer;
//# sourceMappingURL=ChatServer.js.map