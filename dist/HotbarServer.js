"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Items = require('./public/generated/Items');
var Utils = require('./Utils');
var HotbarServer = /** @class */ (function () {
    function HotbarServer() {
    }
    HotbarServer.addItemsAtIndex = function (hotbarItemsArr, hotbarSlotArr, newItemIndex) {
        hotbarItemsArr = Utils.dereferenceObj(hotbarItemsArr);
        if (hotbarItemsArr[newItemIndex]['itemId'] == hotbarSlotArr['itemId'] && hotbarSlotArr['itemId'] in Items.itemsArr && Items.itemsArr[hotbarSlotArr['itemId']]['isStackable']) { // Add to existing stack.
            hotbarItemsArr[newItemIndex]['itemCount'] += hotbarSlotArr['itemCount'];
        }
        else {
            hotbarItemsArr[newItemIndex]['itemId'] = hotbarSlotArr['itemId']; // Start a new stack.
            hotbarItemsArr[newItemIndex]['itemCount'] = hotbarSlotArr['itemCount'];
            if (hotbarSlotArr['itemDurability']) {
                hotbarItemsArr[newItemIndex]['itemDurability'] = hotbarSlotArr['itemDurability'];
            }
            else if (hotbarSlotArr['itemId'] in Items.itemsArr && Items.itemsArr[hotbarSlotArr['itemId']]['itemDurability']) {
                hotbarItemsArr[newItemIndex]['itemDurability'] = Items.itemsArr[hotbarSlotArr['itemId']]['itemDurability'];
            }
        }
        return hotbarItemsArr;
    };
    HotbarServer.addItemsFromArr = function (hotbarItemsArr, itemsToAddArr) {
        for (var i in itemsToAddArr) {
            var itemArr = itemsToAddArr[i];
            var newItemIndex = Utils.getNewItemIndex(hotbarItemsArr, itemArr['itemId']);
            if (newItemIndex != null) {
                hotbarItemsArr = HotbarServer.addItemsAtIndex(hotbarItemsArr, itemArr, newItemIndex);
            }
            else {
                break;
            }
        }
        return hotbarItemsArr;
    };
    HotbarServer.removeItemsAtIndex = function (hotbarItemsArr, itemIndex, numItemsToRemove) {
        hotbarItemsArr[itemIndex]['itemCount'] -= numItemsToRemove;
        if (hotbarItemsArr[itemIndex]['itemCount'] <= 0) {
            hotbarItemsArr[itemIndex]['itemId'] = null;
            hotbarItemsArr[itemIndex]['itemCount'] = null;
            delete hotbarItemsArr[itemIndex]['itemDurability'];
        }
        return hotbarItemsArr;
    };
    HotbarServer.removeItemsFromArr = function (hotbarItemsArr, inputItemsToRemoveArr) {
        var itemsToRemoveArr = Utils.dereferenceObj(inputItemsToRemoveArr);
        for (var slotIndex in hotbarItemsArr) {
            if (hotbarItemsArr[slotIndex]['itemId'] in itemsToRemoveArr) {
                var itemsToDeductNum = Math.min(hotbarItemsArr[slotIndex]['itemCount'], itemsToRemoveArr[hotbarItemsArr[slotIndex]['itemId']]);
                hotbarItemsArr = HotbarServer.removeItemsAtIndex(hotbarItemsArr, parseInt(slotIndex), itemsToDeductNum);
                if (itemsToRemoveArr[hotbarItemsArr[slotIndex]['itemId']] <= 0) {
                    delete itemsToRemoveArr[hotbarItemsArr[slotIndex]['itemId']];
                }
                if (Object.keys(itemsToRemoveArr).length <= 0) {
                    break;
                }
            }
        }
        return hotbarItemsArr;
    };
    return HotbarServer;
}());
exports.HotbarServer = HotbarServer;
//# sourceMappingURL=HotbarServer.js.map