"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = require('./Utils');
var SimplexNoise = require("simplex-noise");
var NoiseMap = /** @class */ (function () {
    function NoiseMap() {
    }
    NoiseMap.generate = function (scale, octaves, isAddingFalloff) {
        if (isAddingFalloff === void 0) { isAddingFalloff = true; }
        var noiseMap = [];
        var simplex = new SimplexNoise(Math.random);
        var persistence = 0.6;
        var lacunarity = 1.9;
        var maxHeight = 0;
        var minHeight = 9999;
        for (var y = 0; y < Utils.worldHeight; y++) {
            var noiseRow = [];
            for (var x = 0; x < Utils.worldWidth; x++) {
                var amplitude = 1;
                var frequency = 1;
                var noiseHeight = 0;
                for (var i = 0; i < octaves; i++) {
                    var sampleX = x / scale * frequency;
                    var sampleY = y / scale * frequency;
                    var simplexValue = simplex.noise2D(sampleX, sampleY) * 2;
                    noiseHeight += simplexValue * amplitude;
                    amplitude *= persistence;
                    frequency *= lacunarity;
                }
                if (noiseHeight > maxHeight) {
                    maxHeight = noiseHeight;
                }
                if (noiseHeight < minHeight) {
                    minHeight = noiseHeight;
                }
                noiseRow.push(noiseHeight);
            }
            noiseMap.push(noiseRow);
        }
        var falloffMap;
        if (isAddingFalloff) {
            falloffMap = Utils.generateFalloffMap();
        }
        for (var y = 0; y < Utils.worldHeight; y++) {
            for (var x = 0; x < Utils.worldWidth; x++) {
                noiseMap[y][x] = (noiseMap[y][x] - minHeight) / (maxHeight - minHeight);
                if (isAddingFalloff) {
                    noiseMap[y][x] -= falloffMap[y][x];
                    noiseMap[y][x] = Math.max(noiseMap[y][x], 0);
                }
            }
        }
        return noiseMap;
    };
    return NoiseMap;
}());
exports.NoiseMap = NoiseMap;
//# sourceMappingURL=NoiseMap.js.map