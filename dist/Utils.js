/// <reference path="public/generated/Items.ts" />
if (typeof require !== 'undefined') {
    // @ts-ignore
    Items = require('./public/generated/Items');
}
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.init = function () {
        Utils.allFloorIdsArr = [];
        Utils.tileIndexToItemIdArr = {};
        for (var itemId in Items.itemsArr) {
            if (Items.itemsArr[itemId]['tileIndexesArr'] != null) {
                for (var _i = 0, _a = Items.itemsArr[itemId]['tileIndexesArr']; _i < _a.length; _i++) {
                    var tileIndex = _a[_i];
                    Utils.tileIndexToItemIdArr[tileIndex] = itemId;
                }
            }
            if (Items.itemsArr[itemId]['altTileIndexesArr'] != null) {
                for (var _b = 0, _c = Items.itemsArr[itemId]['altTileIndexesArr']; _b < _c.length; _b++) {
                    var tileIndex = _c[_b];
                    Utils.tileIndexToItemIdArr[tileIndex] = itemId;
                }
            }
            if (Items.itemsArr[itemId]['isFloor']) {
                Utils.allFloorIdsArr.push(itemId);
            }
        }
        Utils.allEntityTypesArr = {
            // @formatter:off
            'wolf': { 'label': "Wolf", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': Utils.worldBlocksNum * 0.0050, 'minInWorld': Utils.worldBlocksNum * 0.0050, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': true, 'feedingItemId': 'meat_rotten', 'spawnLayerNum': 0, 'spawnFloorIdsArr': Utils.playerSpawnFloorIdsArr, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 10, 'pointsGivenWhenKilled': 0, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 2, 'attackCooldownTicksNum': 4, 'alpha': 1.0, 'speed': Utils.playerSpeed * 0.9, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 80, 'height': 80, 'babyWidth': 80, 'babyHeight': 80 * (165 / 137), 'minSizeMult': 0.75, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': 'baby_wolf', 'hurtSoundEffectKey': 'wolf_hurt', 'deathSoundEffectKey': 'wolf_death', 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'meat_raw', 'itemCount': 1 }] },
            'polar_bear': { 'label': "Polar bear", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': Utils.worldBlocksNum * 0.0010, 'minInWorld': Utils.worldBlocksNum * 0.0025, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': true, 'feedingItemId': 'meat_rotten', 'spawnLayerNum': 0, 'spawnFloorIdsArr': ['snow', 'ice'], 'idleFloorIdsArr': ['snow', 'ice'], 'moveFloorIdsArr': ['snow', 'ice'], 'health': 20, 'pointsGivenWhenKilled': 0, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 3, 'attackCooldownTicksNum': 4, 'alpha': 1.0, 'speed': Utils.playerSpeed, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 220 * 0.4, 'height': 195 * 0.4, 'babyWidth': 220 * 0.4, 'babyHeight': 220 * 0.4 * (136 / 182), 'minSizeMult': 0.75, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': 'baby_polar_bear', 'hurtSoundEffectKey': 'polar_bear_hurt', 'deathSoundEffectKey': 'polar_bear_death', 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'meat_raw', 'itemCount': 2 }] },
            'cow': { 'label': "Cow", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': Utils.worldBlocksNum * 0.0040, 'minInWorld': Utils.worldBlocksNum * 0.0002, 'isMonster': false, 'hasPanicMode': true, 'isSavingInRedis': true, 'cyclesUntilAdultNum': 1, 'canBreed': true, 'feedingItemId': 'wheat', 'spawnLayerNum': 0, 'spawnFloorIdsArr': Utils.playerSpawnFloorIdsArr, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 10, 'pointsGivenWhenKilled': 10, 'isPassive': true, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 215 * 0.4, 'height': 197 * 0.4, 'babyWidth': 215 * 0.4, 'babyHeight': 215 * 0.4 * (136 / 153), 'minSizeMult': 0.75, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': 'baby_cow', 'hurtSoundEffectKey': 'cow_hurt', 'deathSoundEffectKey': 'cow_death', 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'meat_raw', 'itemCount': 2 }, { 'itemId': 'leather', 'itemCount': 2 }] },
            'pig': { 'label': "Pig", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': Utils.worldBlocksNum * 0.0020, 'minInWorld': Utils.worldBlocksNum * 0.0002, 'isMonster': false, 'hasPanicMode': true, 'isSavingInRedis': true, 'cyclesUntilAdultNum': 1, 'canBreed': true, 'feedingItemId': 'carrot', 'spawnLayerNum': 0, 'spawnFloorIdsArr': Utils.playerSpawnFloorIdsArr, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 15, 'pointsGivenWhenKilled': 15, 'isPassive': true, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 1.1, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 220 * 0.4, 'height': 194 * 0.4, 'babyWidth': 220 * 0.4, 'babyHeight': 220 * 0.4 * (136 / 152), 'minSizeMult': 0.75, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': 'baby_pig', 'hurtSoundEffectKey': 'pig_hurt', 'deathSoundEffectKey': 'pig_hurt', 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'meat_raw', 'itemCount': 4 }] },
            'sheep': { 'label': "Sheep", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': Utils.worldBlocksNum * 0.0020, 'minInWorld': Utils.worldBlocksNum * 0.0002, 'isMonster': false, 'hasPanicMode': true, 'isSavingInRedis': true, 'cyclesUntilAdultNum': 1, 'canBreed': true, 'feedingItemId': 'wheat', 'spawnLayerNum': 0, 'spawnFloorIdsArr': Utils.playerSpawnFloorIdsArr, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 10, 'pointsGivenWhenKilled': 10, 'isPassive': true, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 132 * 0.6, 'height': 127 * 0.6, 'babyWidth': 132 * 0.6, 'babyHeight': 132 * 0.6 * (132 / 130), 'minSizeMult': 0.75, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': 'baby_sheep', 'hurtSoundEffectKey': 'sheep_hurt', 'deathSoundEffectKey': 'sheep_death', 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'meat_raw', 'itemCount': 2 }, { 'itemId': 'wool', 'itemCount': 1 }] },
            'chicken': { 'label': "Chicken", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': Utils.worldBlocksNum * 0.0020, 'minInWorld': Utils.worldBlocksNum * 0.0002, 'isMonster': false, 'hasPanicMode': true, 'isSavingInRedis': true, 'cyclesUntilAdultNum': 1, 'canBreed': true, 'feedingItemId': 'seeds', 'spawnLayerNum': 0, 'spawnFloorIdsArr': Utils.playerSpawnFloorIdsArr, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 5, 'pointsGivenWhenKilled': 5, 'isPassive': true, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 1.3, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 128 * 0.4, 'height': 145 * 0.4, 'babyWidth': 129 * 0.4, 'babyHeight': 128 * 0.4, 'minSizeMult': 0.75, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': 'baby_chicken', 'hurtSoundEffectKey': 'chicken_hurt', 'deathSoundEffectKey': 'chicken_hurt', 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'meat_raw', 'itemCount': 1 }] },
            'zombie': { 'label': "Zombie", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': true, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 25, 'pointsGivenWhenKilled': 30, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 3, 'attackCooldownTicksNum': 8, 'alpha': 1.0, 'speed': Utils.playerSpeed * 0.5, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 69 * 0.7, 'height': 86 * 0.7, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 0.75, 'maxSizeMult': 1.25, 'angleOffset': 0, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': 'zombie_hurt', 'deathSoundEffectKey': 'zombie_death', 'projectileHitParticleEffectKey': null },
            'slime': { 'label': "Slime", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': true, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 15, 'pointsGivenWhenKilled': 60, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': true, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': 2, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': 7, 'fuseAutoDecrementLengthNum': 4, 'damage': null, 'attackCooldownTicksNum': null, 'alpha': 0.7, 'speed': Utils.playerSpeed * 1.1, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 51, 'height': 50, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 0.75, 'maxSizeMult': 1.25, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': 'slime_hurt', 'deathSoundEffectKey': 'slime_death', 'projectileHitParticleEffectKey': null },
            'slime_fire': { 'label': "Fire slime", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': true, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['water']), 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['water']), 'moveFloorIdsArr': null, 'health': 15, 'pointsGivenWhenKilled': 60, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': true, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': 2, 'explosionFloorPcnt': 0.05, 'explosionFloorId': 'lava', 'fuseLengthNum': 7, 'fuseAutoDecrementLengthNum': 4, 'damage': null, 'attackCooldownTicksNum': null, 'alpha': 0.7, 'speed': Utils.playerSpeed * 1.1, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 51, 'height': 50, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 0.75, 'maxSizeMult': 1.25, 'angleOffset': -90, 'lootKey': 'slime', 'babySpriteStr': null, 'hurtSoundEffectKey': 'slime_hurt', 'deathSoundEffectKey': 'slime_death', 'projectileHitParticleEffectKey': null },
            'slime_ice': { 'label': "Ice slime", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': true, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 15, 'pointsGivenWhenKilled': 60, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': true, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': 2, 'explosionFloorPcnt': 0.05, 'explosionFloorId': 'ice', 'fuseLengthNum': 7, 'fuseAutoDecrementLengthNum': 4, 'damage': null, 'attackCooldownTicksNum': null, 'alpha': 0.7, 'speed': Utils.playerSpeed * 1.1, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': 1.0, 'width': 51, 'height': 50, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 0.75, 'maxSizeMult': 1.25, 'angleOffset': -90, 'lootKey': 'slime', 'babySpriteStr': null, 'hurtSoundEffectKey': 'slime_hurt', 'deathSoundEffectKey': 'slime_death', 'projectileHitParticleEffectKey': null },
            'archer': { 'label': "Archer", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': true, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 25, 'pointsGivenWhenKilled': 45, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': null, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 0.9, 'hitboxMult': 1, 'knockbackMult': 0.25, 'damageRadiusMult': null, 'width': 74 * 0.7, 'height': 86 * 0.7, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 0.75, 'maxSizeMult': 1.25, 'angleOffset': 0, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': 'archer_hurt', 'deathSoundEffectKey': 'archer_death', 'projectileHitParticleEffectKey': null },
            'axehead': { 'label': "Axehead", 'hasNameTag': true, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': true, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': 200, 'pointsGivenWhenKilled': 1000, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 4, 'attackCooldownTicksNum': 1, 'alpha': 1.0, 'speed': Utils.playerSpeed * 1.5, 'hitboxMult': 1.5, 'knockbackMult': 1.5, 'damageRadiusMult': 2.5, 'width': 256, 'height': 256, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 1.00, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': 'axehead_hurt', 'deathSoundEffectKey': 'axehead_death', 'projectileHitParticleEffectKey': null },
            // 'ghost':            {'label': "Ghost",        'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null,                          'minInWorld': null,                          'isMonster': true,  'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null,          'spawnLayerNum': null, 'spawnFloorIdsArr': null,                                                         'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']),   'moveFloorIdsArr': null,            'health': 15,   'pointsGivenWhenKilled': 60,   'isPassive': false, 'isRideable': false, 'isColliding': false, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null,                'projectileDespawnMillis': null,          'lightCircleRadius': null, 'lightCircleTint': null,      'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 4,    'attackCooldownTicksNum': 8,    'alpha': 0.5, 'speed': Utils.playerSpeed * 1.5, 'hitboxMult': 1,   'damageRadiusMult': 1.0,  'width':  51,       'height':  73,       'babyWidth': null,         'babyHeight': null,                     'minSizeMult': 0.75, 'maxSizeMult': 1.25, 'angleOffset':   0, 'lootKey': null,    'babySpriteStr': null,                 'hurtSoundEffectKey': 'zombie_hurt',     'deathSoundEffectKey': 'zombie_death'},
            // Projectiles.
            'arrowEntity': { 'label': "Arrow", 'hasNameTag': false, 'tickIntervalMillis': 10, 'initNum': null, 'minInWorld': null, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': null, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': null, 'pointsGivenWhenKilled': null, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': true, 'isExplosive': false, 'projectileMaxDistance': 20 * Utils.tileSize, 'projectileDespawnMillis': 5 * 60 * 1000, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 5, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 10, 'hitboxMult': 1, 'knockbackMult': 0.5, 'damageRadiusMult': 1.0, 'width': 128 * 0.5, 'height': 28 * 0.5, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 1.00, 'maxSizeMult': 1.00, 'angleOffset': 0, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': null, 'deathSoundEffectKey': null, 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'arrow', 'itemCount': 1 }] },
            'fireballEntity': { 'label': "Fireball", 'hasNameTag': false, 'tickIntervalMillis': 10, 'initNum': null, 'minInWorld': null, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': null, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': null, 'pointsGivenWhenKilled': null, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': true, 'isExplosive': true, 'projectileMaxDistance': 10 * Utils.tileSize, 'projectileDespawnMillis': null, 'lightCircleRadius': 100, 'lightCircleTint': 'orange', 'explosionPower': 1, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': 100, 'fuseAutoDecrementLengthNum': 100, 'damage': 20, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 5, 'hitboxMult': 1, 'knockbackMult': 1.0, 'damageRadiusMult': 1.0, 'width': 44 * 0.9, 'height': 48 * 0.9, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 1.00, 'maxSizeMult': 1.00, 'angleOffset': 0, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': null, 'deathSoundEffectKey': null, 'projectileHitParticleEffectKey': null },
            'snowballEntity': { 'label': "Snowball", 'hasNameTag': false, 'tickIntervalMillis': 10, 'initNum': null, 'minInWorld': null, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': null, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': null, 'pointsGivenWhenKilled': null, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': true, 'isExplosive': false, 'projectileMaxDistance': 10 * Utils.tileSize, 'projectileDespawnMillis': 5 * 60 * 1000, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0.5, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 5, 'hitboxMult': 1, 'knockbackMult': 0.5, 'damageRadiusMult': 1.0, 'width': 35, 'height': 35, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 1.00, 'maxSizeMult': 1.00, 'angleOffset': 0, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': null, 'deathSoundEffectKey': null, 'projectileHitParticleEffectKey': 'snowballHit', 'yieldItemsArr': [{ 'itemId': 'snowball', 'itemCount': 1 }] },
            'eggEntity': { 'label': "Egg", 'hasNameTag': false, 'tickIntervalMillis': 10, 'initNum': null, 'minInWorld': null, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': false, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': null, 'idleFloorIdsArr': Utils.removeByValue(Utils.allFloorIdsArr, ['lava', 'water']), 'moveFloorIdsArr': null, 'health': null, 'pointsGivenWhenKilled': null, 'isPassive': false, 'isRideable': false, 'isColliding': true, 'isProjectile': true, 'isExplosive': false, 'projectileMaxDistance': 10 * Utils.tileSize, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0.5, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 5, 'hitboxMult': 1, 'knockbackMult': 0.5, 'damageRadiusMult': 1.0, 'width': 68 * 0.3, 'height': 78 * 0.3, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 1.00, 'maxSizeMult': 1.00, 'angleOffset': 0, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': null, 'deathSoundEffectKey': null, 'projectileHitParticleEffectKey': 'eggHit', 'yieldItemsArr': [{ 'itemId': 'egg', 'itemCount': 1 }] },
            // Other.
            'boatEntity': { 'label': "Boat", 'hasNameTag': false, 'tickIntervalMillis': Utils.entityTickIntervalMillis, 'initNum': null, 'minInWorld': null, 'isMonster': false, 'hasPanicMode': false, 'isSavingInRedis': true, 'cyclesUntilAdultNum': null, 'canBreed': false, 'feedingItemId': null, 'spawnLayerNum': null, 'spawnFloorIdsArr': null, 'idleFloorIdsArr': null, 'moveFloorIdsArr': null, 'health': 10, 'pointsGivenWhenKilled': null, 'isPassive': true, 'isRideable': true, 'isColliding': true, 'isProjectile': false, 'isExplosive': false, 'projectileMaxDistance': null, 'projectileDespawnMillis': null, 'lightCircleRadius': null, 'lightCircleTint': null, 'explosionPower': null, 'explosionFloorPcnt': null, 'explosionFloorId': null, 'fuseLengthNum': null, 'fuseAutoDecrementLengthNum': null, 'damage': 0, 'attackCooldownTicksNum': null, 'alpha': 1.0, 'speed': Utils.playerSpeed * 1.4, 'hitboxMult': 1, 'knockbackMult': null, 'damageRadiusMult': null, 'width': 67 * 0.8, 'height': 122 * 0.8, 'babyWidth': null, 'babyHeight': null, 'minSizeMult': 1.00, 'maxSizeMult': 1.00, 'angleOffset': -90, 'lootKey': null, 'babySpriteStr': null, 'hurtSoundEffectKey': null, 'deathSoundEffectKey': null, 'projectileHitParticleEffectKey': null, 'yieldItemsArr': [{ 'itemId': 'boat', 'itemCount': 1 }] },
        };
    };
    Utils.time = function () {
        return (new Date()).getTime();
    };
    Utils.randBool = function () {
        return Math.random() >= 0.5;
    };
    Utils.randBetween = function (min, max) {
        return Math.round(Math.random() * (max - min) + min);
    };
    Utils.randElFromArr = function (arr) {
        if (arr.length == 1 && arr[0]) {
            return arr[0];
        }
        return arr[Math.floor(Math.random() * arr.length)];
    };
    Utils.randId = function (length) {
        var id = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var possibleLength = possible.length;
        for (var i = 0; i < length; i++) {
            id += possible.charAt(Math.floor(Math.random() * possibleLength));
        }
        return id;
    };
    Utils.ucfirst = function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    Utils.lcfirst = function (string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    };
    Utils.distance = function (coord1, coord2) {
        var dx = coord1['x'] - coord2['x'];
        var dy = coord1['y'] - coord2['y'];
        return Math.sqrt(dx * dx + dy * dy);
    };
    Utils.rotation = function (coord1, coord2) {
        var dx = coord1['x'] - coord2['x'];
        var dy = coord1['y'] - coord2['y'];
        return Math.atan2(dy, dx);
    };
    Utils.dereferenceObj = function (obj) {
        return JSON.parse(JSON.stringify(obj));
    };
    Utils.toDegrees = function (angleRadians) {
        return angleRadians * (180 / Math.PI);
    };
    Utils.toRadians = function (angleDegrees) {
        return angleDegrees * (Math.PI / 180);
    };
    Utils.hasAmmoForProjectile = function (playerObj) {
        var projectileAmmoId = Items.itemsArr[playerObj.selectedItemId]['projectileAmmoId'];
        var projectileAmmoIndex = null;
        if (projectileAmmoId == playerObj.selectedItemId) {
            projectileAmmoIndex = playerObj.selectedItemIndex;
        }
        else {
            for (var slotIndex in playerObj.hotbarItemsArr) {
                if (playerObj.hotbarItemsArr[slotIndex]['itemId']
                    &&
                        playerObj.hotbarItemsArr[slotIndex]['itemId'] == projectileAmmoId
                    &&
                        Items.itemsArr[playerObj.hotbarItemsArr[slotIndex]['itemId']]['isProjectile']) {
                    projectileAmmoIndex = parseInt(slotIndex);
                    break;
                }
            }
        }
        return projectileAmmoIndex;
    };
    Utils.removeByValue = function (inArr, valuesArr) {
        inArr = Utils.dereferenceObj(inArr);
        for (var _i = 0, valuesArr_1 = valuesArr; _i < valuesArr_1.length; _i++) {
            var value = valuesArr_1[_i];
            var valueIndex = inArr.indexOf(value);
            if (valueIndex !== -1) {
                inArr.splice(valueIndex, 1);
            }
        }
        return inArr;
    };
    Utils.getNewItemIndex = function (hotbarItemsArr, itemId) {
        // Add to existing stacks.
        for (var slotIndex in hotbarItemsArr) {
            var hotbarItemSlot = hotbarItemsArr[slotIndex];
            if (hotbarItemSlot['itemId'] == itemId && itemId in Items.itemsArr && Items.itemsArr[itemId]['isStackable']) {
                return parseInt(slotIndex);
            }
        }
        // If no existing stacks, start a new stack.
        for (var slotIndex in hotbarItemsArr) {
            var hotbarItemSlot = hotbarItemsArr[slotIndex];
            if (hotbarItemSlot['itemId'] == null) {
                return parseInt(slotIndex);
            }
        }
        return null;
    };
    Utils.hasRequiredItemsForCrafting = function (hotbarItemsArr, requiredItemsForCraftingArr) {
        var materialsArr = {};
        requiredItemsForCraftingArr = Utils.dereferenceObj(requiredItemsForCraftingArr);
        for (var i = 0; i < hotbarItemsArr.length; i++) {
            if (hotbarItemsArr[i]['itemId'] in materialsArr) {
                materialsArr[hotbarItemsArr[i]['itemId']] += hotbarItemsArr[i]['itemCount'];
            }
            else {
                materialsArr[hotbarItemsArr[i]['itemId']] = hotbarItemsArr[i]['itemCount'];
            }
        }
        for (var itemId in requiredItemsForCraftingArr) {
            if (!(itemId in materialsArr) || materialsArr[itemId] < requiredItemsForCraftingArr[itemId]) {
                return false;
            }
        }
        return true;
    };
    Utils.itemCraftingTimeMillis = function (itemId) {
        var craftingTimeMillis = 0;
        if (itemId
            &&
                Items.itemsArr[itemId]['craftingSpeedMillis']) {
            craftingTimeMillis = Items.itemsArr[itemId]['craftingSpeedMillis'];
        }
        return craftingTimeMillis;
    };
    Utils.generateFalloffMap = function () {
        var map = [];
        for (var i = 0; i < Utils.worldHeight; i++) {
            var mapRow = [];
            for (var j = 0; j < Utils.worldWidth; j++) {
                var x = (i / Utils.worldWidth) * 2 - 1;
                var y = (j / Utils.worldHeight) * 2 - 1;
                var value = Math.max(Math.abs(x), Math.abs(y));
                mapRow.push(Math.pow(value, 20));
            }
            map.push(mapRow);
        }
        return map;
    };
    Utils.shuffleArray = function (array) {
        var _a;
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            _a = [array[j], array[i]], array[i] = _a[0], array[j] = _a[1];
        }
        return array;
    };
    Utils.invertDirectionLetter = function (directionLetter) {
        if (directionLetter == 'n') {
            return 's';
        }
        else if (directionLetter == 's') {
            return 'n';
        }
        else if (directionLetter == 'e') {
            return 'w';
        }
        else if (directionLetter == 'w') {
            return 'e';
        }
    };
    Utils.getNewCoordFromDirection = function (coordArr, directionLetter, numTiles, isDungeon) {
        if (isDungeon === void 0) { isDungeon = false; }
        if (directionLetter == 'n') { // North.
            return {
                'x': coordArr['x'],
                'y': (isDungeon) ? coordArr['y'] - numTiles : Math.max(coordArr['y'] - numTiles, 0),
            };
        }
        else if (directionLetter == 'e') { // East.
            return {
                'x': (isDungeon) ? coordArr['x'] + numTiles : Math.min(coordArr['x'] + numTiles, Utils.worldWidth - 1),
                'y': coordArr['y'],
            };
        }
        else if (directionLetter == 's') { // South.
            return {
                'x': coordArr['x'],
                'y': (isDungeon) ? coordArr['y'] + numTiles : Math.min(coordArr['y'] + numTiles, Utils.worldHeight - 1),
            };
        }
        else { // West.
            return {
                'x': (isDungeon) ? coordArr['x'] - numTiles : Math.max(coordArr['x'] - numTiles, 0),
                'y': coordArr['y'],
            };
        }
    };
    Utils.toRomanNumerals = function (inNum) {
        var romanNumeralsArr = {
            'M': 1000,
            'CM': 900,
            'D': 500,
            'CD': 400,
            'C': 100,
            'XC': 90,
            'L': 50,
            'XL': 40,
            'X': 10,
            'IX': 9,
            'V': 5,
            'IV': 4,
            'I': 1,
        };
        var outStr = '';
        for (var numeralStr in romanNumeralsArr) {
            var numeralNum = Math.floor(inNum / romanNumeralsArr[numeralStr]);
            inNum -= numeralNum * romanNumeralsArr[numeralStr];
            outStr += numeralStr.repeat(numeralNum);
        }
        return outStr;
    };
    Utils.isDev = true;
    Utils.isRestrictingJoining = false;
    Utils.isTakingScreenshots = false;
    Utils.worldWidth = 200;
    Utils.worldHeight = 200;
    Utils.worldBlocksNum = Utils.worldWidth * Utils.worldHeight;
    Utils.dayLengthMillis = 5 * 60 * 1000;
    Utils.nightLengthMillis = 3 * 60 * 1000;
    Utils.cycleLengthMillis = Utils.dayLengthMillis + Utils.nightLengthMillis;
    /*
     * Hot         0.66-1.00   desert      savanna     swamp
     * Temperate   0.33-0.66   meadow      forest      jungle
     * Cold        0.00-0.33   tundra      taiga       ice
     *                         0.00-0.33   0.33-0.66   0.66-1.00
     *                         Dry         Fertile     Wet
     */
    Utils.biomeDetailsArr = {
        '0': {
            // @formatter:off
            // Cold & dry.
            'tundra': { 'label': "Tundra", 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.01, 'flower_blue': 0.04, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            'mountains': { 'label': "Mountains", 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.50, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.20, 'ore_silver': 0.05, 'flower_blue': 0.04, 'tuft': 0.02 } },
            // Cold & fertile.
            'taiga': { 'label': "Taiga", 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.10, 'tree_birch': 0.005, 'tree_apple': 0.02, 'stone': 0.01, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            'christmas': { 'label': "Christmas", 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.50, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree': 0.10, 'tree_birch': 0.005, /*'tree_christmas': 0.10, */ 'tree_apple': 0.02, 'stone': 0.01, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            // Cold & wet.
            'ice': { 'label': "Ice", 'floorItemId': 'ice', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'stone': 0.03, 'snow': 0.02, 'tree': 0.50 } },
            'ice_mesa': { 'label': "Ice mesa", 'floorItemId': 'ice', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.50, 'altMax': 1.01, 'blocksToSpawnArr': { 'stone': 0.20, 'ore_silver': 0.05 } },
            // Temperate & dry.
            'meadow': { 'label': "Meadow", 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.01, 'tree_birch': 0.005, 'flower_red': 0.02, 'flower_purple': 0.02, 'flower_white': 0.02, 'flower_yellow': 0.02, 'wheat_stage_4': 0.05 } },
            'plains': { 'label': "Plains", 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.50, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree': 0.01, 'tree_birch': 0.005, 'flower_red': 0.02, 'water': 0.01, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            // Temperate & fertile.
            'forest': { 'label': "Forest", 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.00, 'altMax': 0.66, 'blocksToSpawnArr': { 'tree': 0.10, 'tree_birch': 0.005, 'tree_apple': 0.02, 'stone': 0.01, 'flower_red': 0.02, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            'forest_birch': { 'label': "Birch forest", 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.66, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree': 0.005, 'tree_birch': 0.10, 'tree_apple': 0.02, 'stone': 0.01, 'flower_white': 0.02, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            // Temperate & wet.
            'jungle': { 'label': "Jungle", 'floorItemId': 'grass', 'temperatureMin': 0.33, 'temperatureMax': 0.66, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree': 0.25, 'tree_apple': 0.02, 'water': 0.02, 'tuft_long': 0.05, 'carrot_stage_4': 0.05 } },
            // Hot & dry.
            'desert': { 'label': "Desert", 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 0.66, 'blocksToSpawnArr': { 'cactus': 0.20, 'stone': 0.01, 'tuft_dead': 0.02, 'sugar_cane_stage_4': 0.02 } },
            'mesa': { 'label': "Mesa", 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.66, 'altMax': 1.01, 'blocksToSpawnArr': { 'cactus': 0.05, 'stone': 0.20, 'ore_gold': 0.05, 'tuft_dead': 0.01, 'sugar_cane_stage_4': 0.02 } },
            // Hot & fertile.
            'savanna': { 'label': "Savanna", 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.33, 'humidityMax': 0.66, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree_acacia': 0.05, 'water': 0.02, 'tuft_long': 0.20, 'tuft': 0.10, 'tuft_dead': 0.02, 'sugar_cane_stage_4': 0.02 } },
            // Hot & wet.
            'swamp': { 'label': "Swamp", 'floorItemId': 'grass', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 0.50, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.01, 'water': 0.10, 'tuft_dead': 0.15, 'carrot_stage_4': 0.05 } },
            'bog': { 'label': "Bog", 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.50, 'altMax': 1.01, 'blocksToSpawnArr': { 'tree': 0.01, 'stone': 0.01, 'water': 0.10, 'tuft_long': 0.05, 'tuft': 0.10, 'carrot_stage_4': 0.05 } },
        },
        '-1': {
            // @formatter:off
            'glaciers': { 'label': "Glacial wastes", 'floorItemId': 'ice', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'ore_silver': 0.05, 'water': 0.10, 'tuft_dead': 0.02, 'stone': 0.02, } },
            'blizzard': { 'label': "Snowstorm", 'floorItemId': 'snow', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'ore_coal': 0.05, 'flower_blue': 0.10, 'tuft': 0.02, 'wheat_stage_4': 0.05 } },
            'mushroom': { 'label': "Mushroom wilderness", 'floorItemId': 'dirt_coarse', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'water': 0.05, 'mushroom_tan': 0.08, 'mushroom_red': 0.08, 'mushroom_brown': 0.08, 'wheat_stage_4': 0.05 } },
            'flooded_dunes': { 'label': "Flooded dunes", 'floorItemId': 'sand', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'ore_gold': 0.02, 'water': 0.10, 'tuft_dead': 0.02, 'cactus': 0.10, } },
        },
        '-2': {
            // @formatter:off
            'plurple': { 'label': "Plurple", 'floorItemId': 'plurple', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'mushroom_blue': 0.10, 'ore_diamond': 0.01 } },
            'giant_mushroom': { 'label': "Mushroom megafauna", 'floorItemId': 'dirt_coarse', 'temperatureMin': 0.00, 'temperatureMax': 0.33, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'water': 0.05, 'mushroom_tan': 0.08, 'mushroom_red': 0.08, 'mushroom_brown': 0.08, 'wheat_stage_4': 0.05 } },
            'flowers': { 'label': "Floral valley", 'floorItemId': 'grass_stone', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.00, 'humidityMax': 0.33, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'grass_flowers': 0.10, 'flower_red': 0.02, 'flower_purple': 0.02, 'flower_white': 0.02, 'flower_yellow': 0.02 } },
            'dripping_caves': { 'label': "Dripping caves", 'floorItemId': 'water', 'temperatureMin': 0.66, 'temperatureMax': 1.01, 'humidityMin': 0.66, 'humidityMax': 1.01, 'altMin': 0.00, 'altMax': 1.01, 'blocksToSpawnArr': { 'stone': 0.10, 'ore_diamond': 0.02 } },
        },
        '-3': {
        // @formatter:off
        // @formatter:on
        },
    };
    Utils.playerSwingTimeMillis = 250;
    Utils.playerDefaultSpeed = 200;
    Utils.playerSpeed = Utils.playerDefaultSpeed;
    Utils.playerDefaultMaxHealth = 20;
    Utils.playerDefaultMaxWarmth = 20;
    Utils.playerDefaultMaxHunger = 20;
    Utils.allPlayerImagesArr = {
        'hitman': { 'width': 69, 'height': 86, 'src': '/public/assets/player/hitman.png' },
        'manBlue': { 'width': 69, 'height': 86, 'src': '/public/assets/player/manBlue.png' },
        'manBrown': { 'width': 69, 'height': 86, 'src': '/public/assets/player/manBrown.png' },
        'manOld': { 'width': 69, 'height': 86, 'src': '/public/assets/player/manOld.png' },
        'robot': { 'width': 69, 'height': 86, 'src': '/public/assets/player/robot.png' },
        'soldier': { 'width': 74, 'height': 86, 'src': '/public/assets/player/soldier.png' },
        'survivor': { 'width': 73, 'height': 86, 'src': '/public/assets/player/survivor.png' },
        'womanGreen': { 'width': 76, 'height': 86, 'src': '/public/assets/player/womanGreen.png' },
    };
    Utils.playerSpawnFloorIdsArr = ['sand', 'dirt', 'grass'];
    Utils.allFloorIdsArr = [];
    Utils.playerDifficultiesArr = {
        // @formatter:off
        'hard': { 'label': "Hard", 'description': "You will die lots", 'difficultyMult': 1.25 },
        'normal': { 'label': "Normal", 'description': "A good challenge", 'difficultyMult': 1.00 },
        'easy': { 'label': "Easy", 'description': "For the less skilled", 'difficultyMult': 0.75 },
    };
    Utils.playerGameModesArr = {
        // @formatter:off
        'survival': { 'label': "Survival", 'description': "Has warmth bar", 'isSpawningMonsters': true, 'isSeenByMonsters': true, 'hasHealth': true, 'hasWarmth': true, 'hasHunger': true, 'hasInfiniteBlocks': false, 'isShowingTerrain': false },
        'adventure': { 'label': "Adventure", 'description': "No warmth bar", 'isSpawningMonsters': true, 'isSeenByMonsters': true, 'hasHealth': true, 'hasWarmth': false, 'hasHunger': true, 'hasInfiniteBlocks': false, 'isShowingTerrain': false },
        'peaceful': { 'label': "Peaceful", 'description': "No monsters", 'isSpawningMonsters': false, 'isSeenByMonsters': false, 'hasHealth': true, 'hasWarmth': false, 'hasHunger': true, 'hasInfiniteBlocks': false, 'isShowingTerrain': false },
        'creative': { 'label': "Creative", 'description': "Infinite blocks, immortal", 'isSpawningMonsters': true, 'isSeenByMonsters': true, 'hasHealth': false, 'hasWarmth': false, 'hasHunger': false, 'hasInfiniteBlocks': true, 'isShowingTerrain': true },
    };
    Utils.tileSize = 64;
    Utils.tileIndexToItemIdArr = {};
    Utils.entityTickIntervalMillis = 250;
    Utils.startLayerNum = 0;
    Utils.layersNum = 4;
    Utils.endLayerNum = Utils.startLayerNum - Utils.layersNum + 1;
    Utils.directionsArr = ['n', 'e', 's', 'w'];
    Utils.allEnchantmentsArr = {
        // @formatter:off
        'unbreaking': { 'label': "Unbreaking", 'itemTypesArr': null, 'tierMin': 1, 'tierMax': 5, 'isTreasure': false, 'description': null },
        'sharpness': { 'label': "Sharpness", 'itemTypesArr': ['sword', 'book'], 'tierMin': 1, 'tierMax': 5, 'isTreasure': false, 'description': null },
        'knockback': { 'label': "Knockback", 'itemTypesArr': ['sword', 'book'], 'tierMin': 1, 'tierMax': 3, 'isTreasure': false, 'description': null },
    };
    Utils.isWorldPage = false;
    return Utils;
}());
Utils.init();
if (typeof module !== 'undefined') {
    module.exports = Utils;
}
//# sourceMappingURL=Utils.js.map