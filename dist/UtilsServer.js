"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = require('./Utils');
var Items = require('./public/generated/Items');
var crypto = require("crypto");
var PlayerServer_1 = require("./PlayerServer");
var UtilsServer = /** @class */ (function () {
    function UtilsServer() {
    }
    UtilsServer.isNearBlock = function (playerCoordArr, blockCoordsArr) {
        for (var coordStr in blockCoordsArr) {
            var coordArr = blockCoordsArr[coordStr];
            if (playerCoordArr['x'] / Utils.tileSize <= coordArr['x'] + 2
                &&
                    playerCoordArr['x'] / Utils.tileSize >= coordArr['x'] - 1
                &&
                    playerCoordArr['y'] / Utils.tileSize <= coordArr['y'] + 2
                &&
                    playerCoordArr['y'] / Utils.tileSize >= coordArr['y'] - 1) {
                return true;
            }
        }
        return false;
    };
    UtilsServer.isAnyPlayerAtBlock = function (coordArr) {
        for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
            if (Math.floor(PlayerServer_1.PlayerServer.allPlayerObjs[playerId]['x'] / Utils.tileSize) == coordArr['x'] && Math.floor(PlayerServer_1.PlayerServer.allPlayerObjs[playerId]['y'] / Utils.tileSize) == coordArr['y']) {
                return true;
            }
        }
        return false;
    };
    // Mostly from: https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/
    // Read about good hashing and salting practices here: https://crackstation.net/hashing-security.htm#properhashing
    UtilsServer.generateSalt = function (lengthNum) {
        return crypto.randomBytes(Math.ceil(lengthNum / 2)).toString('hex').slice(0, lengthNum);
    };
    UtilsServer.getPasswordHash = function (passwordCode, salt) {
        var hash = crypto.createHmac('sha512', salt);
        hash.update(passwordCode);
        return hash.digest('hex');
    };
    UtilsServer.checkBlockNearWater = function (floorLayerArr, x, y, waterRadiusNum) {
        return UtilsServer.checkRadiusForBlock(x, y, waterRadiusNum, function (xLoop, yLoop) {
            var worldCell = floorLayerArr[yLoop][xLoop];
            return (worldCell && Items.itemsArr['water']['tileIndexesArr'].indexOf(worldCell[0]) != -1);
        });
    };
    UtilsServer.checkRadiusForBlock = function (centerX, centerY, radiusNum, checkBlockFunction) {
        for (var y = Math.max(centerY - radiusNum, 0); y <= Math.min(centerY + radiusNum, Utils.worldHeight - 1); y++) {
            for (var x = Math.max(centerX - radiusNum, 0); x <= Math.min(centerX + radiusNum, Utils.worldWidth - 1); x++) {
                if (checkBlockFunction(x, y)) {
                    return true;
                }
            }
        }
        return false;
    };
    UtilsServer.isCoord3x3Empty = function (worldArr, x, y) {
        return !UtilsServer.checkRadiusForBlock(x, y, 1, function (xLoop, yLoop) {
            if (x != xLoop || y != yLoop) { // Not center block.
                if (UtilsServer.isCoordCollidingWithWorld(worldArr, xLoop, yLoop)[0]) {
                    return true;
                }
            }
            return false;
        });
    };
    UtilsServer.isCoordCollidingWithWorld = function (worldArr, xRounded, yRounded) {
        if (yRounded in worldArr
            &&
                xRounded in worldArr[yRounded]
            &&
                worldArr[yRounded][xRounded] != null) {
            var worldArrBlock = worldArr[yRounded][xRounded];
            var itemId = null;
            if (worldArrBlock[0] in Utils.tileIndexToItemIdArr) {
                itemId = Utils.tileIndexToItemIdArr[worldArrBlock[0]];
            }
            var isDisablingCollision = (itemId
                &&
                    itemId in Items.itemsArr
                &&
                    Items.itemsArr[itemId]['isDisablingCollision']);
            if (!isDisablingCollision) {
                return [true, itemId];
            }
            else {
                return [false, itemId];
            }
        }
        return [false, null];
    };
    UtilsServer.isCorrectTool = function (selectedItemId, correctToolType) {
        return (selectedItemId.indexOf(correctToolType + "_") == 0
            ||
                (correctToolType == 'scythe'
                    &&
                        selectedItemId.indexOf('hoe_') == 0));
    };
    UtilsServer.playerSurviveNightPoints = 200;
    UtilsServer.playerDefaultRangeDistance = 1;
    UtilsServer.playerDefaultRangeRadius = 35;
    UtilsServer.tierMultArr = {
        'wood': 2,
        'stone': 3,
        'silver': 4,
        'gold': 5,
        'diamond': 6,
    };
    UtilsServer.tierBlockMultArr = {
        'wood': 2,
        'stone': 4,
        'silver': 6,
        'gold': 8,
        'diamond': 12,
    };
    UtilsServer.redisClient = null;
    UtilsServer.io = null;
    return UtilsServer;
}());
exports.UtilsServer = UtilsServer;
//# sourceMappingURL=UtilsServer.js.map