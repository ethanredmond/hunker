"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = require('./Utils');
var UtilsServer_1 = require("./UtilsServer");
var PlayerServer_1 = require("./PlayerServer");
var DayNightCycleServer = /** @class */ (function () {
    function DayNightCycleServer() {
    }
    DayNightCycleServer.startDay = function () {
        var updatePlayerScoresArr = {};
        for (var playerId in PlayerServer_1.PlayerServer.allPlayerObjs) {
            var playerObj = PlayerServer_1.PlayerServer.allPlayerObjs[playerId];
            if (!playerObj.isGhost) {
                playerObj.playerScore += UtilsServer_1.UtilsServer.playerSurviveNightPoints;
                updatePlayerScoresArr[playerId] = playerObj.playerScore;
                playerObj.nightsSurvivedNum++;
                UtilsServer_1.UtilsServer.io.to(playerId).emit('nightSurvived', {
                    'nightsSurvivedNum': playerObj.nightsSurvivedNum,
                });
                if (playerObj.userName) {
                    UtilsServer_1.UtilsServer.redisClient.hset('player:' + playerObj.userName, 'nightsSurvivedNum', playerObj.nightsSurvivedNum.toString());
                }
            }
        }
        UtilsServer_1.UtilsServer.io.emit('startDay', {
            'status': 'success',
            'updatePlayerScoresArr': updatePlayerScoresArr,
        }); // Emit message to all players.
        console.log('startDay');
        DayNightCycleServer.stage = 'day';
        DayNightCycleServer.nextTimeout = setTimeout(function () {
            DayNightCycleServer.startNight();
        }, Utils.dayLengthMillis);
    };
    DayNightCycleServer.startNight = function () {
        UtilsServer_1.UtilsServer.io.emit('startNight'); // Emit message to all players.
        console.log('startNight');
        DayNightCycleServer.stage = 'night';
        DayNightCycleServer.nextTimeout = setTimeout(function () {
            DayNightCycleServer.startDay();
        }, Utils.nightLengthMillis);
    };
    DayNightCycleServer.stage = 'day';
    DayNightCycleServer.gameStartMillis = null;
    DayNightCycleServer.nextTimeout = null;
    return DayNightCycleServer;
}());
exports.DayNightCycleServer = DayNightCycleServer;
//# sourceMappingURL=DayNightCycleServer.js.map